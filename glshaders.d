module glshaders is aliced;

import iv.glbinds;

__gshared bool glutilsShowShaderWarnings = true;


immutable string shaderScanlinesSrc =
"/* DooM2D: Midnight on the Firing Line\n"~
" * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>\n"~
" * Understanding is not required. Only obedience.\n"~
" *\n"~
" * This program is free software: you can redistribute it and/or modify\n"~
" * it under the terms of the GNU General Public License as published by\n"~
" * the Free Software Foundation, either version 3 of the License, or\n"~
" * (at your option) any later version.\n"~
" *\n"~
" * This program is distributed in the hope that it will be useful,\n"~
" * but WITHOUT ANY WARRANTY; without even the implied warranty of\n"~
" * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"~
" * GNU General Public License for more details.\n"~
" *\n"~
" * You should have received a copy of the GNU General Public License\n"~
" * along with this program. If not, see <http://www.gnu.org/licenses/>.\n"~
" */\n"~
"#version 120\n"~
"\n"~
"uniform sampler2D tex;\n"~
"uniform float cmult;\n"~
"uniform float cmult2;\n"~
"/*uniform bool scanlines;*/\n"~
"\n"~
"\n"~
"void main () {\n"~
"  vec4 color = texture2D(tex, gl_TexCoord[0].xy);\n"~
"  //gl_FragColor = vec4(intens, intens, intens, color.w);\n"~
"  //color = vec4(1.0, 0.0, 0.0, 1.0);\n"~
"  if (/*scanlines &&*/ mod(floor(gl_FragCoord.y), 3) != 0) {\n"~
"    float xm = cmult;\n"~
"    if (mod(floor(gl_FragCoord.y), 3) == 2) xm = cmult2;\n"~
"    color = clamp(color*vec4(xm, xm, xm, 1.0), 0.0, 1.0);\n"~
"  }\n"~
"  gl_FragColor = color;\n"~
"}\n";


// ////////////////////////////////////////////////////////////////////////// //
public struct SVec2I { int x, y; }
public struct SVec3I { int x, y, z; alias r = x; alias g = y; alias b = z; }
public struct SVec4I { int x, y, z, w; alias r = x; alias g = y; alias b = z; alias a = w; }

public struct SVec2F { float x, y; }
public struct SVec3F { float x, y, z; alias r = x; alias g = y; alias b = z; }
public struct SVec4F { float x, y, z, w; alias r = x; alias g = y; alias b = z; alias a = w; }


public final class Shader {
  string shaderName;
  GLuint prg = 0;
  GLint[string] vars;

  @property uint id () const pure nothrow @nogc => prg;

  this (string ashaderName, const(char)[] src) {
    shaderName = ashaderName;
    if (src.length > int.max) {
      import core.stdc.stdio : printf;
      printf("shader '%.*s' code too long!", cast(uint)ashaderName.length, ashaderName.ptr);
      assert(0);
    }
    auto shaderId = glCreateShader(GL_FRAGMENT_SHADER);
    auto sptr = src.ptr;
    GLint slen = cast(int)src.length;
    glShaderSource(shaderId, 1, &sptr, &slen);
    glCompileShader(shaderId);
    GLint success = 0;
    glGetShaderiv(shaderId, GL_COMPILE_STATUS, &success);
    if (!success || glutilsShowShaderWarnings) {
      import core.stdc.stdio : printf;
      import core.stdc.stdlib : malloc, free;
      GLint logSize = 0;
      glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &logSize);
      if (logSize > 0) {
        auto logStrZ = cast(GLchar*)malloc(logSize);
        glGetShaderInfoLog(shaderId, logSize, null, logStrZ);
        printf("shader '%.*s' compilation messages:\n%s\n", cast(uint)ashaderName.length, ashaderName.ptr, logStrZ);
        free(logStrZ);
      }
    }
    if (!success) assert(0);
    prg = glCreateProgram();
    glAttachShader(prg, shaderId);
    glLinkProgram(prg);
  }

  GLint varId(NT) (NT vname) if (is(NT == char[]) || is(NT == const(char)[]) || is(NT == immutable(char)[])) {
    GLint id = -1;
    if (vname.length > 0 && vname.length <= 128) {
      if (auto vi = vname in vars) {
        id = *vi;
      } else {
        char[129] buf = void;
        buf[0..vname.length] = vname[];
        buf[vname.length] = 0;
        id = glGetUniformLocation(prg, buf.ptr);
        //{ import core.stdc.stdio; printf("[%.*s.%s]=%i\n", cast(uint)shaderName.length, shaderName.ptr, buf.ptr, id); }
        static if (is(NT == immutable(char)[])) {
          vars[vname.idup] = id;
        } else {
          vars[vname.idup] = id;
        }
        if (id < 0) {
          import core.stdc.stdio : printf;
          printf("shader '%.*s': unknown variable '%.*s'\n", cast(uint)shaderName.length, shaderName.ptr, cast(uint)vname.length, vname.ptr);
        }
      }
    }
    return id;
  }

  // get unified var id
  GLint opIndex(NT) (NT vname) if (is(NT == char[]) || is(NT == const(char)[]) || is(NT == immutable(char)[])) {
    auto id = varId(vname);
    if (id < 0) {
      import core.stdc.stdio : printf;
      printf("shader '%.*s': unknown variable '%.*s'\n", cast(uint)shaderName.length, shaderName.ptr, cast(uint)vname.length, vname.ptr);
      assert(0);
    }
    return id;
  }

  private import std.traits;
  void opIndexAssign(T, NT) (in auto ref T v, NT vname)
  if (((isIntegral!T && T.sizeof <= 4) || (isFloatingPoint!T && T.sizeof == float.sizeof) || isBoolean!T ||
       is(T : SVec2I) || is(T : SVec3I) || is(T : SVec4I) ||
       is(T : SVec2F) || is(T : SVec3F) || is(T : SVec4F)) &&
      (is(NT == char[]) || is(NT == const(char)[]) || is(NT == immutable(char)[])))
  {
    auto id = varId(vname);
    if (id < 0) return;
    //{ import core.stdc.stdio; printf("setting '%.*s' (%d)\n", cast(uint)vname.length, vname.ptr, id); }
         static if (isIntegral!T || isBoolean!T) glUniform1i(id, cast(int)v);
    else static if (isFloatingPoint!T) glUniform1f(id, cast(float)v);
    else static if (is(SVec2I : T)) glUniform2i(id, cast(int)v.x, cast(int)v.y);
    else static if (is(SVec3I : T)) glUniform3i(id, cast(int)v.x, cast(int)v.y, cast(int)v.z);
    else static if (is(SVec4I : T)) glUniform4i(id, cast(int)v.x, cast(int)v.y, cast(int)v.z, cast(int)v.w);
    else static if (is(SVec2F : T)) glUniform2f(id, cast(float)v.x, cast(float)v.y);
    else static if (is(SVec3F : T)) glUniform3f(id, cast(float)v.x, cast(float)v.y, cast(float)v.z);
    else static if (is(SVec4F : T)) glUniform4f(id, cast(float)v.x, cast(float)v.y, cast(float)v.z, cast(float)v.w);
    else static assert(0, "wtf?!");
  }

  void activateObj () nothrow @nogc { /*if (prg)*/ glUseProgram(prg); }
  void deactivateObj () nothrow @nogc { glUseProgram(0); }
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared Shader shadScanlines;


public void initScanlineShader () {
  shadScanlines = new Shader("scanlines", shaderScanlinesSrc);
}
