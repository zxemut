/* Sound support
 * Copyright (c) 2017 Ketmar // Invisible Vector
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * modified by Ketmar // Invisible Vector
 */
module soundalsa;
private:

import iv.alsa;
import iv.cmdcon;
import iv.pxclock;

//version = alsa_simple;
//version = alsa_debug;
//version = alsa_time_debug;

__gshared snd_pcm_t* pcm;
__gshared uint pcmChans = 2;
__gshared uint pcmRate;
version(alsa_simple) {} else {
  __gshared bool sndJustStarted = true;
  __gshared snd_pcm_uframes_t realBufSize;
}


///
public bool soundrvInit (bool stereo, ref uint sampleRate) {
  soundrvDeinit();
  pcmChans = (stereo ? 2 : 1);
  version(alsa_simple) {
    int err;
    if ((err = snd_pcm_open(&pcm, "default", SND_PCM_STREAM_PLAYBACK, 0)) < 0) {
      conwriteln("ALSA error: ", snd_strerror(err));
      pcm = null; // just in case
      return false;
    }
    if ((err = snd_pcm_set_params(pcm, SND_PCM_FORMAT_S16_LE, SND_PCM_ACCESS_RW_INTERLEAVED, pcmChans, sampleRate, 0, /*500000*/1000000/50)) < 0) {
      conwriteln("ALSA error: ", snd_strerror(err));
      snd_pcm_close(pcm);
      pcm = null;
      return false;
    }
    pcmRate = sampleRate;
  } else {
    sndJustStarted = true;
    uint sr = sampleRate;
    try {
      pcm = alsaInit(null, sr, cast(ubyte)pcmChans);
    } catch (Exception e) {
      conwriteln("ALSA error: ", e.msg);
      pcm = null;
      return false;
    }
    sampleRate = sr;
  }
  return true;
}


///
public void soundrvDeinit () {
  if (pcm !is null) {
    snd_pcm_close(pcm);
    pcm = null;
  }
}


///
public void soundrvFrame (const(short)* smpbuf, int frmcount) nothrow @trusted {
  auto frms = frmcount; // now in frames
  version(alsa_simple) {
    version(alsa_time_debug) auto stc = clockMilli;
    while (frms > 0) {
      snd_pcm_sframes_t frames = snd_pcm_writei(pcm, smpbuf, frms);
      if (frames < 0) {
        frames = snd_pcm_recover(pcm, cast(int)frames, 0);
        if (frames < 0) {
          import core.stdc.stdio : printf;
          import core.stdc.stdlib : exit, EXIT_FAILURE;
          printf("snd_pcm_writei failed: %s\n", snd_strerror(cast(int)frames));
          //exit(EXIT_FAILURE);
          assert(0);
        }
      } else {
        frms -= frames;
        smpbuf += frames*pcmChans;
      }
    }
  } else {
    int averrcount = 0;
    if (sndJustStarted) {
      sndJustStarted = false;
      __gshared short[1024] sbuf;
      if (snd_pcm_prepare(pcm) != 0) assert(0, "oops"); // alas
      if (snd_pcm_start(pcm) != 0) assert(0, "oops"); // alas
      for (;;) {
        auto avail = snd_pcm_avail/*_update*/(pcm); // "_update" for mmaped i/o
        if (avail < 0) {
          import core.stdc.errno : EPIPE, EINTR, ESTRPIPE;
          import core.stdc.stdio;
          if (avail != -EPIPE && avail != -EINTR && avail != -ESTRPIPE) {
            //fprintf(stderr, "ALSA ERROR: %s\n", snd_strerror(cast(int)avail));
            assert(0);
          }
          snd_pcm_recover(pcm, cast(int)avail, 1);
          if (++averrcount > 16) assert(0);
          continue;
        }
        averrcount = 0;
        version(alsa_time_debug) { import core.stdc.stdio; stderr.fprintf("first fill: %u\n", cast(uint)avail); }
        //sbuf.length = avail*pcmChans;
        auto xleft = avail;
        while (xleft > 0) {
          auto wrx = cast(uint)(avail > sbuf.length/2 ? sbuf.length/2 : avail);
          auto xerr = snd_pcm_writei(pcm, sbuf.ptr, wrx);
          if (xerr < 0) {
            snd_pcm_recover(pcm, cast(int)xerr, 1);
          } else {
            version(alsa_time_debug) { import core.stdc.stdio; stderr.fprintf("first wrote: %u\n", cast(uint)xerr); }
          }
          xleft -= wrx;
        }
        break;
      }
    }
    version(alsa_time_debug) auto stc = clockMilli;
    auto fleft = frmcount;
    averrcount = 0;
    int awaitcount = 0;
    while (fleft > 0) {
      auto avail = snd_pcm_avail/*_update*/(pcm); // "_update" for mmaped i/o
      //{ import core.stdc.stdio : stderr, fprintf; stderr.fprintf("AVAIL: %d\n", cast(int)avail); }
      if (avail < 0) {
        /*{ import core.stdc.stdio : stderr, fprintf; stderr.fprintf("AVAILBAD: %d\n", cast(int)avail); }*/
        import core.stdc.errno : EPIPE, EINTR, ESTRPIPE;
        import core.stdc.stdio;
        if (avail != -EPIPE && avail != -EINTR && avail != -ESTRPIPE) {
          //fprintf(stderr, "ALSA ERROR: %s\n", snd_strerror(cast(int)avail));
          assert(0);
        }
        snd_pcm_recover(pcm, cast(int)avail, 1);
        if (++averrcount > 16) assert(0);
        continue;
      }
      averrcount = 0;
      // now wait or write
      auto used = realBufSize-avail;
      if (used <= fleft || awaitcount >= 2) {
        awaitcount = 0;
        version(alsa_debug) { import core.stdc.stdio; stderr.fprintf("avail: %u; used: %u\n", cast(uint)avail, cast(uint)used); }
        auto err = snd_pcm_writei(pcm, smpbuf, fleft);
        if (err < 0) {
          import core.stdc.stdio : fprintf, stderr;
          import core.stdc.errno : EPIPE, EINTR, ESTRPIPE;
          if (err == -EPIPE /*|| err == -EINTR || err == -ESTRPIPE*/) {
          } else if (err == -11) {
            // we can't write, that's wrong
            fprintf(stderr, "ALSA: write failed (%s)\n", snd_strerror(cast(int)err));
          } else {
            fprintf(stderr, "ALSA: write failed %d (%s)\n", cast(int)err, snd_strerror(cast(int)err));
          }
          snd_pcm_recover(pcm, cast(int)err, 1);
          //fleft = sndSamplesSize/2/*numchans*/;
          //bpos = sndsilence; // write silence instead
          //res = false;
          //continue waitnwrite;
          break;
        }
        //version(follin_write_debug) { import core.stdc.stdio; printf("Follin: written %u of %u frames\n", cast(uint)err, cast(uint)fleft); }
        smpbuf += cast(uint)(err*pcmChans);
        fleft -= err;
      } else {
        ++awaitcount;
        // no room, wait
        if (used > frmcount) {
          uint over = used-frmcount;
          uint mcswait = 1000_0*over/(pcmRate/100)+100; //441; // for 48000 it will just wait a little less
          version(alsa_debug) { import core.stdc.stdio; stderr.fprintf("avail: %u; need: %u; used: %u; over: %u; mcswait=%u; avc=%u\n", cast(uint)avail, cast(uint)fleft, cast(uint)used, over, mcswait, cast(uint)awaitcount); }
          clockSleepMicro(mcswait);
        } else {
          conwriteln("!!!ALSA: used=", used, "; frmcount=", frmcount);
        }
      }
    }
  }
  version(alsa_time_debug) {
    auto ste = clockMilli;
    { import core.stdc.stdio; stderr.fprintf("frame time: %u msecs for %u frames\n", cast(uint)(ste-stc), frmcount); }
  }
}


version(alsa_simple) {} else
snd_pcm_t* alsaInit (const(char)* alsaDev, ref uint srate, ubyte chans) {
  snd_pcm_t* apcm = null;
  snd_pcm_hw_params_t* hw_params = null;
  snd_pcm_sw_params_t* sw_params = null;
  snd_pcm_uframes_t rlbufsize;
  uint sr = srate;

  if (chans < 1 || chans > 2) throw new Exception("invalid number of channels");
  if (alsaDev is null || !alsaDev[0]) alsaDev = "default";

  static void alsaCall (int err, string msg) @trusted {
    if (err < 0) {
      import std.string : fromStringz;
      throw new Exception((msg~" ("~snd_strerror(err).fromStringz~")").idup);
    }
  }

  fuck_alsa_messages();

  alsaCall(snd_pcm_open(&apcm, alsaDev, SND_PCM_STREAM_PLAYBACK, 0), "cannot open audio device");
  scope(failure) snd_pcm_close(apcm);

  alsaCall(snd_pcm_hw_params_malloc(&hw_params), "cannot allocate hardware parameter structure");
  scope(exit) snd_pcm_hw_params_free(hw_params);

  alsaCall(snd_pcm_hw_params_any(apcm, hw_params), "cannot initialize hardware parameter structure");
  alsaCall(snd_pcm_hw_params_set_access(apcm, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED), "cannot set access type");
  alsaCall(snd_pcm_hw_params_set_format(apcm, hw_params, SND_PCM_FORMAT_S16_LE), "cannot set sample format");
  if (alsaDev[0] == 'p' && alsaDev[1] == 'l' && alsaDev[2] == 'u' && alsaDev[3] == 'g' && alsaDev[4] == ':') {
    alsaCall(snd_pcm_hw_params_set_rate_resample(apcm, hw_params, 1), "cannot turn on resampling");
    alsaCall(snd_pcm_hw_params_set_rate(apcm, hw_params, sr, 0), "cannot set sample rate");
  } else {
    alsaCall(snd_pcm_hw_params_set_rate_resample(apcm, hw_params, 0), "cannot turn off resampling");
    alsaCall(snd_pcm_hw_params_set_rate_near(apcm, hw_params, &sr, null), "cannot set sample rate");
  }
  alsaCall(snd_pcm_hw_params_set_channels(apcm, hw_params, chans), "cannot set channel count");

  //{ import core.stdc.stdio : fprintf, stderr; stderr.fprintf("sampling rate: %u (%u)\n", cast(uint)sr, cast(uint)srate); }

  //alsaCall(snd_pcm_hw_params_set_buffer_size_near(apcm, hw_params, &rlbufsize), "cannot set buffer size");
  rlbufsize = sr/50; // in frames
  //rlbufsize *= 100;
  if (rlbufsize < 1) assert(0, "wtf?!");
  auto wantbs = rlbufsize;
  alsaCall(snd_pcm_hw_params_set_buffer_size_near(apcm, hw_params, &rlbufsize), "cannot set buffer size");
  if (rlbufsize < wantbs) throw new Exception("ALSA: alas");
  if (rlbufsize > wantbs*3) throw new Exception("ALSA: alas");
  realBufSize = rlbufsize;
  //alsaCall(snd_pcm_hw_params_set_buffer_size(apcm, hw_params, rlbufsize), "cannot set buffer size");
  auto latency = 1000*rlbufsize/sr;
  /*
  {
    import core.stdc.stdio : fprintf, stderr;
    stderr.fprintf("ALSA: sample rate: %u (%u); frames in buffer: %u (%u); latency: %u\n", cast(uint)sr, cast(uint)srate, cast(uint)rlbufsize, cast(uint)wantbs, cast(uint)latency);
  }
  */
  alsaCall(snd_pcm_hw_params(apcm, hw_params), "cannot set parameters");

  alsaCall(snd_pcm_sw_params_malloc(&sw_params), "cannot allocate software parameters structure");
  scope(exit) snd_pcm_sw_params_free(sw_params);

  alsaCall(snd_pcm_sw_params_current(apcm, sw_params), "cannot initialize software parameters structure");
  alsaCall(snd_pcm_sw_params_set_avail_min(apcm, sw_params, rlbufsize), "cannot set minimum available count");
  alsaCall(snd_pcm_sw_params_set_start_threshold(apcm, sw_params, rlbufsize), "cannot set start mode");
  alsaCall(snd_pcm_sw_params(apcm, sw_params), "cannot set software parameters");
  alsaCall(snd_pcm_nonblock(apcm, 0), "cannot set blocking mode");
  //alsaCall(snd_pcm_nonblock(apcm, 1), "cannot set non-blocking mode");

  srate = sr;
  pcmRate = sr;

  return apcm;
}
