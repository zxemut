/* ZX Spectrum Emulator
 * Copyright (C) 2017 Ketmar // Invisible Vector
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
module pipev;

import arsd.simpledisplay;


public final class PipeEvent : Timer {
private:
  int[2] fd = -1; // [0]: read; [1]: write
  void delegate (ubyte eid) onPulseCB;

  int eventCount;

public:
  this (void delegate (ubyte eid) onPulse) {
    import core.sys.posix.unistd : pipe;
    if (pipe(fd) != 0) throw new Exception("can't create pipe event");
    onPulseCB = onPulse;
    super(int.max/8, delegate () {}, fd[0]);
  }

  ~this () { close(); }

  protected override void trigger () {
    //conwriteln("!!!");
    auto id = recv();
    if (onPulseCB !is null) onPulseCB(id);
  }

  @property bool isOpen () const pure nothrow @safe @nogc { return (fd[0] >= 0); }

  void close () /*nothrow @trusted @nogc*/ {
    destroy();
    import core.sys.posix.unistd : close;
    if (fd[0] >= 0) { close(fd[0]); fd[0] = -1; }
    if (fd[1] >= 0) { close(fd[1]); fd[1] = -1; }
  }

  void send (ubyte id/*, const(ubyte)[] data=null*/) {
    {
      import core.atomic;
      atomicOp!"+="(*cast(shared int*)&eventCount, 1);
    }
    /*synchronized(this)*/ {
      if (!isOpen) throw new Exception("can't send event to closed pipe");
      if (!rawWrite(fd[1], (&id)[0..1])) throw new Exception("pipe writing error");
    }
    /*
    size_t len = data.length;
    if (!rawWrite(fd[1], (&len)[0..1])) throw new Exception("pipe writing error");
    if (!rawWrite(fd[1], data)) throw new Exception("pipe writing error");
    */
  }

  ubyte recv () {
    /*synchronized(this)*/ {
      if (!isOpen) throw new Exception("can't receive event from closed pipe");
      ubyte[1] id;
      if (!rawRead(fd[0], id[])) throw new Exception("pipe reading error");
      {
        import core.atomic;
        atomicOp!"-="(*cast(shared int*)&eventCount, 1);
      }
      return id[0];
    }
  }

  bool hasSomething () {
    {
      import core.atomic;
      return (atomicLoad(*cast(shared int*)&eventCount) != 0);
    }
    /*
    synchronized(this) {
      if (!isOpen) return false; // obviously
      import core.sys.posix.sys.select;
      import core.sys.posix.sys.time;
      fd_set rd;
      timeval tv;
      for (;;) {
        FD_ZERO(&rd);
        FD_SET(fd[0], &rd);
        tv.tv_sec = 0;
        tv.tv_usec = 0;
        auto res = select(fd[0]+1, &rd, null, null, &tv);
        if (res < 0) {
          import core.stdc.errno : errno, EINTR;
          if (errno != EINTR) return false;
        } else {
          return (res > 0);
        }
      }
    }
    */
  }

static final:
  bool rawWrite (int fd, const(void)[] data) {
    import core.sys.posix.unistd : write;
    if (fd < 0) return false;
    auto bp = cast(const(ubyte)*)data.ptr;
    auto left = data.length;
    while (left > 0) {
      auto wr = write(fd, bp, left);
      if (wr < 0) {
        import core.stdc.errno : errno, EINTR;
        if (errno != EINTR) return false;
      } else if (wr == 0) {
        return false;
      } else {
        bp += wr;
        left -= wr;
      }
    }
    return true;
  }

  bool rawRead (int fd, void[] data) {
    import core.sys.posix.unistd : read;
    if (fd < 0) return false;
    auto bp = cast(ubyte*)data.ptr;
    auto left = data.length;
    while (left > 0) {
      auto rd = read(fd, bp, left);
      if (rd < 0) {
        import core.stdc.errno : errno, EINTR;
        if (errno != EINTR) return false;
      } else if (rd == 0) {
        return false;
      } else {
        bp += rd;
        left -= rd;
      }
    }
    return true;
  }
}
