import iv.vfs.io;

public immutable char[256] koi8from866Table = [
  '\x00','\x01','\x02','\x03','\x04','\x05','\x06','\x07','\x08','\x09','\x0a','\x0b','\x0c','\x0d','\x0e','\x0f',
  '\x10','\x11','\x12','\x13','\x14','\x15','\x16','\x17','\x18','\x19','\x1a','\x1b','\x1c','\x1d','\x1e','\x1f',
  '\x20','\x21','\x22','\x23','\x24','\x25','\x26','\x27','\x28','\x29','\x2a','\x2b','\x2c','\x2d','\x2e','\x2f',
  '\x30','\x31','\x32','\x33','\x34','\x35','\x36','\x37','\x38','\x39','\x3a','\x3b','\x3c','\x3d','\x3e','\x3f',
  '\x40','\x41','\x42','\x43','\x44','\x45','\x46','\x47','\x48','\x49','\x4a','\x4b','\x4c','\x4d','\x4e','\x4f',
  '\x50','\x51','\x52','\x53','\x54','\x55','\x56','\x57','\x58','\x59','\x5a','\x5b','\x5c','\x5d','\x5e','\x5f',
  '\x60','\x61','\x62','\x63','\x64','\x65','\x66','\x67','\x68','\x69','\x6a','\x6b','\x6c','\x6d','\x6e','\x6f',
  '\x70','\x71','\x72','\x73','\x74','\x75','\x76','\x77','\x78','\x79','\x7a','\x7b','\x7c','\x7d','\x7e','\x7f',
  '\xe1','\xe2','\xf7','\xe7','\xe4','\xe5','\xf6','\xfa','\xe9','\xea','\xeb','\xec','\xed','\xee','\xef','\xf0',
  '\xf2','\xf3','\xf4','\xf5','\xe6','\xe8','\xe3','\xfe','\xfb','\xfd','\xff','\xf9','\xf8','\xfc','\xe0','\xf1',
  '\xc1','\xc2','\xd7','\xc7','\xc4','\xc5','\xd6','\xda','\xc9','\xca','\xcb','\xcc','\xcd','\xce','\xcf','\xd0',
  '\x90','\x91','\x92','\x81','\x87','\xb2','\x3f','\x3f','\x3f','\xb5','\xa1','\xa8','\xae','\x3f','\xac','\x83',
  '\x84','\x89','\x88','\x86','\x80','\x8a','\xaf','\xb0','\xab','\xa5','\xbb','\xb8','\xb1','\xa0','\xbe','\xb9',
  '\xba','\x3f','\x3f','\xaa','\xa9','\xa2','\x3f','\x3f','\xbc','\x85','\x82','\x8d','\x8c','\x8e','\x8f','\x8b',
  '\xd2','\xd3','\xd4','\xd5','\xc6','\xc8','\xc3','\xde','\xdb','\xdd','\xdf','\xd9','\xd8','\xdc','\xc0','\xd1',
  '\xb3','\xa3','\xb4','\xa4','\xb7','\xa7','\x3f','\x3f','\x9c','\x95','\x9e','\x96','\x3f','\x3f','\x94','\x9a',
];

/* 240 � */
/* 241 � */

/*
176..223
242..255
*/


//font6x8
mixin(import("egfxfont.d"));
mixin(import("z866.d"));
//static assert(font6x8[32*8..32*8+8] == [0, 0, 0, 0, 0, 0, 0, 0]);
//public immutable ubyte[2048] font6x8 = [
//static immutable ubyte[] diskLedIcon = cast(immutable(ubyte)[])import("databin/diskled.rgb");

/*
176..223
254..255
*/

void fixFont () {
  void copyChar (uint cd, uint cs) {
    assert(cs <= 255);
    assert(cd <= 255);
    if (cs < 32) return;
    font6x8[cd*8..cd*8+8] = font866[(cs-32)*8..(cs-32)*8+8];
  }

  foreach (immutable ch; 33..128) copyChar(ch, ch);
  foreach (immutable sch; 128..256) {
    //uint ch = cast(ubyte)koi8from866Table[sch];
    uint ch = sch;
    if (ch >= 176 && ch <= 223) continue;
    if (ch >= 242 && ch <= 255) continue;
    if (ch == 240 || ch == 241) continue;
    copyChar(ch, sch);
  }
  copyChar(247, 247);
  copyChar(248, 248);
  copyChar(253, 253);
  copyChar(254, 254);
}


bool dontTouch (uint ch) {
  return
    (ch >= 176 && ch <= 223) ||
    (ch >= 254 && ch <= 255);
}


bool hasPixelAtX (uint ch, int x) {
  if (x < 0 || x > 7) return false;
  immutable ubyte mask = 0x80>>x;
  foreach (immutable ubyte b; font6x8[ch*8..ch*8+8]) if (b&mask) return true;
  return false;
}


// hi nibble: lshift; lo nibble: width
void processChar (VFile fo, uint ch) {
  if (dontTouch(ch)) {
    fo.writeNum!ubyte(0x06);
  } else if (ch == 32) {
    fo.writeNum!ubyte(0x04); // space is 4px wide
  } else {
    int stx = 0;
    while (stx < 8 && !hasPixelAtX(ch, stx)) ++stx;
    if (stx >= 8) {
      //writeln("WDT: ", ch);
      fo.writeNum!ubyte(0x06);
    } else {
      foreach (immutable _; 0..8) {
        if (hasPixelAtX(ch, 0)) break;
        foreach (ref ubyte b; font6x8[ch*8..ch*8+8]) b <<= 1;
      }
      int etx = 7;
      while (etx >= 0 && !hasPixelAtX(ch, etx)) --etx;
      ++etx;
      if (ch >= '0' && ch <= '9') {
        if (etx > 6) assert(0, "digits are fucked");
        etx = 6;
        if (ch == '1') {
          foreach (ref ubyte b; font6x8[ch*8..ch*8+8]) b >>= 1;
        }
      }
      fo.writeNum!ubyte(cast(ubyte)etx);
      if (ch != 127 && ch != 153 && ch != 158 && ch != 233 && ch != 238 && ch != 155 && ch != 235) {
        if (etx > 6) {
          writeln(ch, ": ", stx, "; ", etx);
          foreach (immutable dy; 0..8) {
            write("  ");
            ubyte b = font6x8[ch*8+dy];
            foreach (immutable dx; 0..8) {
              write(b&0x80 ? '#' : '.');
              b <<= 1;
            }
            writeln;
          }
        }
        assert(etx <= 6);
      }
      //writeln(ch, ": ", stx, "; ", etx);
    }
  }
  fo.rawWriteExact(font6x8[ch*8..ch*8+8]);
}


void main () {
  fixFont();
  auto fo = VFile("zxpfont.fnt", "w");
  foreach (immutable ch; 0..256) processChar(fo, ch);
}
