import iv.vfs.io;

//font6x8
mixin(import("egfxfont.d"));
static assert(font6x8[32*8..32*8+8] == [0, 0, 0, 0, 0, 0, 0, 0]);
//public immutable ubyte[2048] font6x8 = [
//static immutable ubyte[] diskLedIcon = cast(immutable(ubyte)[])import("databin/diskled.rgb");

/*
176..223
254..255
*/

bool dontTouch (uint ch) {
  return
    (ch >= 176 && ch <= 223) ||
    (ch >= 254 && ch <= 255);
}


bool hasPixelAtX (uint ch, int x) {
  if (x < 0 || x > 7) return false;
  immutable ubyte mask = 0x80>>x;
  foreach (immutable ubyte b; font6x8[ch*8..ch*8+8]) if (b&mask) return true;
  return false;
}


// hi nibble: lshift; lo nibble: width
void processChar (VFile fo, uint ch) {
  if (dontTouch(ch)) {
    fo.writeNum!ubyte(0x06);
  } else if (ch == 32) {
    fo.writeNum!ubyte(0x03); // space is 4px wide
  } else {
    int stx = 0;
    while (stx < 8 && !hasPixelAtX(ch, stx)) ++stx;
    if (stx >= 8) {
      //writeln("WDT: ", ch);
      fo.writeNum!ubyte(0x06);
    } else {
      int etx = stx+1;
      while (etx < 8 && hasPixelAtX(ch, etx)) ++etx;
      fo.writeNum!ubyte(cast(ubyte)((stx<<4)|(etx-stx)));
      assert(stx < 6);
      assert(etx <= 6);
      //writeln(ch, ": ", stx, "; ", etx);
    }
  }
  fo.rawWriteExact(font6x8[ch*8..ch*8+8]);
}


void main () {
  auto fo = VFile("zxpfont.fnt", "w");
  foreach (immutable ch; 0..256) processChar(fo, ch);
}
