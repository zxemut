/* ZX Spectrum Emulator
 * Copyright (C) 2012-2017 Ketmar // Invisible Vector
 * This file originating from Unreal Spectrum (DeathSoft mod)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
module wd93;
private:

import iv.bclamp;
import iv.cmdcon;
import iv.vfs;
import iv.zymosis;

import emuconfig;


// ////////////////////////////////////////////////////////////////////////// //
public __gshared ubyte[] wdsnapbuf; // large temporary buffer (for reading snapshots)
public __gshared uint wdsnapsize;

shared static this () { wdsnapbuf.length = 8*1048576; }

public __gshared ubyte[] defaultBootHob;

//@property int z80tstates () nothrow @trusted @nogc { return emuz80.tstates; }
@property long totalTS () nothrow @trusted @nogc { return emuFullFrameTS+cast(long)emuz80.tstates; }

@property ushort z80pc () nothrow @trusted @nogc { return emuz80.PC; }
@property void z80pc (uint v) nothrow @trusted @nogc { emuz80.PC = v&0xffff; }

@property ushort z80pop () nothrow @trusted { return emuz80.pop(); }

public __gshared ubyte delegate (uint pc) nothrow trdosROMread;


// ////////////////////////////////////////////////////////////////////////// //
__gshared bool trdTrapsDebug = false;

shared static this () {
  conRegVar!trdTrapsDebug("trd_debug_traps", "show TRD traps debug info");
}


// ////////////////////////////////////////////////////////////////////////// //
shared int led1793Load;
shared int led1793Save;
shared int led1793Format;
shared int led1793Seek;

enum LedFrames = 25*2; // ~0.5 seconds

void ledUpdate (ref shared int led) nothrow @trusted @nogc {
  import core.atomic;
  atomicStore(led, LedFrames); // ~0.5 seconds
}


/// call this in redraw loop, ONCE!
/// returns alpha (i.e. 255 means "don't show")
public ubyte led1793Check () nothrow @trusted @nogc {
  import core.atomic;
  int maxled = 0;
  int ledv;

  void decIt(alias sym) () {
    ledv = atomicOp!"-="(sym, 1);
    if (ledv < 0) {
      atomicStore(sym, 0);
    } else {
     if (maxled < ledv) maxled = ledv;
    }
  }

  decIt!led1793Load;
  decIt!led1793Save;
  decIt!led1793Format;
  decIt!led1793Seek;

  if (maxled < 1) return 255;
  if (maxled >= LedFrames/2) return 0; // fully opaque
  return 255-clampToByte(255*maxled/(LedFrames/2));
}


// ////////////////////////////////////////////////////////////////////////// //
//#define align_by(a,b) (((ULONG_PTR)(a) + ((b)-1)) & ~((b)-1))
//uint align_by (uint a, uint b) pure nothrow @safe @nogc { pragma(inline, true); return ((a+(b-1))&~(b-1)); }


// ////////////////////////////////////////////////////////////////////////// //
// for WD1793 engine
uint wd93_crc (const(ubyte)* ptr, uint size) nothrow @trusted @nogc {
  uint crc = 0xCDB4;
  while (size--) {
    crc ^= (*ptr++)<<8;
      for (int j = 8; j; j--) // todo: rewrite with pre-calc'ed table
        if ((crc *= 2)&0x10000) crc ^= 0x1021; // bit representation of x^12+x^5+1
  }
  //return _byteswap_ushort(crc); // return crc&0xFFFF;
  crc &= 0xffff;
  return cast(ushort)((crc>>8)|(crc<<8));
}


// for TD0
ushort crc16 (const(ubyte)* buf, uint size) nothrow @trusted @nogc {
  static immutable ushort[256] crcTab = [
    0x0000, 0x97A0, 0xB9E1, 0x2E41, 0xE563, 0x72C3, 0x5C82, 0xCB22,
    0xCAC7, 0x5D67, 0x7326, 0xE486, 0x2FA4, 0xB804, 0x9645, 0x01E5,
    0x032F, 0x948F, 0xBACE, 0x2D6E, 0xE64C, 0x71EC, 0x5FAD, 0xC80D,
    0xC9E8, 0x5E48, 0x7009, 0xE7A9, 0x2C8B, 0xBB2B, 0x956A, 0x02CA,
    0x065E, 0x91FE, 0xBFBF, 0x281F, 0xE33D, 0x749D, 0x5ADC, 0xCD7C,
    0xCC99, 0x5B39, 0x7578, 0xE2D8, 0x29FA, 0xBE5A, 0x901B, 0x07BB,
    0x0571, 0x92D1, 0xBC90, 0x2B30, 0xE012, 0x77B2, 0x59F3, 0xCE53,
    0xCFB6, 0x5816, 0x7657, 0xE1F7, 0x2AD5, 0xBD75, 0x9334, 0x0494,
    0x0CBC, 0x9B1C, 0xB55D, 0x22FD, 0xE9DF, 0x7E7F, 0x503E, 0xC79E,
    0xC67B, 0x51DB, 0x7F9A, 0xE83A, 0x2318, 0xB4B8, 0x9AF9, 0x0D59,
    0x0F93, 0x9833, 0xB672, 0x21D2, 0xEAF0, 0x7D50, 0x5311, 0xC4B1,
    0xC554, 0x52F4, 0x7CB5, 0xEB15, 0x2037, 0xB797, 0x99D6, 0x0E76,
    0x0AE2, 0x9D42, 0xB303, 0x24A3, 0xEF81, 0x7821, 0x5660, 0xC1C0,
    0xC025, 0x5785, 0x79C4, 0xEE64, 0x2546, 0xB2E6, 0x9CA7, 0x0B07,
    0x09CD, 0x9E6D, 0xB02C, 0x278C, 0xECAE, 0x7B0E, 0x554F, 0xC2EF,
    0xC30A, 0x54AA, 0x7AEB, 0xED4B, 0x2669, 0xB1C9, 0x9F88, 0x0828,
    0x8FD8, 0x1878, 0x3639, 0xA199, 0x6ABB, 0xFD1B, 0xD35A, 0x44FA,
    0x451F, 0xD2BF, 0xFCFE, 0x6B5E, 0xA07C, 0x37DC, 0x199D, 0x8E3D,
    0x8CF7, 0x1B57, 0x3516, 0xA2B6, 0x6994, 0xFE34, 0xD075, 0x47D5,
    0x4630, 0xD190, 0xFFD1, 0x6871, 0xA353, 0x34F3, 0x1AB2, 0x8D12,
    0x8986, 0x1E26, 0x3067, 0xA7C7, 0x6CE5, 0xFB45, 0xD504, 0x42A4,
    0x4341, 0xD4E1, 0xFAA0, 0x6D00, 0xA622, 0x3182, 0x1FC3, 0x8863,
    0x8AA9, 0x1D09, 0x3348, 0xA4E8, 0x6FCA, 0xF86A, 0xD62B, 0x418B,
    0x406E, 0xD7CE, 0xF98F, 0x6E2F, 0xA50D, 0x32AD, 0x1CEC, 0x8B4C,
    0x8364, 0x14C4, 0x3A85, 0xAD25, 0x6607, 0xF1A7, 0xDFE6, 0x4846,
    0x49A3, 0xDE03, 0xF042, 0x67E2, 0xACC0, 0x3B60, 0x1521, 0x8281,
    0x804B, 0x17EB, 0x39AA, 0xAE0A, 0x6528, 0xF288, 0xDCC9, 0x4B69,
    0x4A8C, 0xDD2C, 0xF36D, 0x64CD, 0xAFEF, 0x384F, 0x160E, 0x81AE,
    0x853A, 0x129A, 0x3CDB, 0xAB7B, 0x6059, 0xF7F9, 0xD9B8, 0x4E18,
    0x4FFD, 0xD85D, 0xF61C, 0x61BC, 0xAA9E, 0x3D3E, 0x137F, 0x84DF,
    0x8615, 0x11B5, 0x3FF4, 0xA854, 0x6376, 0xF4D6, 0xDA97, 0x4D37,
    0x4CD2, 0xDB72, 0xF533, 0x6293, 0xA9B1, 0x3E11, 0x1050, 0x87F0,
  ];
  uint crc = 0;
  while (size--) crc = (crc>>8)^crcTab.ptr[(crc&0xff)^*buf++];
  //return _byteswap_ushort(crc);
  crc &= 0xffff;
  return cast(ushort)((crc>>8)|(crc<<8));
}


// for UDI
void udi_buggy_crc (ref int crc, const(ubyte)* buf, uint len) nothrow @trusted @nogc {
  while (len--) {
    crc ^= -1^*buf++;
    for (int k = 8; k--; ) { int temp = -(crc&1); crc >>= 1; crc ^= 0xEDB88320&temp; }
    crc ^= -1;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
//bool done_fdd(bool Cancelable);

//enum Z80FQ = 3500000; // todo: #define as (wd93conf.frame*wd93conf.intfq)
@property uint Z80FQ () nothrow @trusted @nogc { pragma(inline, true); return Config.machine.cpuSpeed; }
enum FDD_RPS = 5; // rotation speed

enum MAX_TRACK_LEN = 6250;
enum MAX_CYLS = 86; // don't load images with so many tracks
enum MAX_PHYS_CYL = 86; // don't seek over it
enum MAX_SEC = 256;


// ////////////////////////////////////////////////////////////////////////// //
struct SectorHeader {
  ubyte c, s, n, l;
  ushort crc; // CRC заголовка сектора
  // флаги crc зоны адреса и данных:
  // При форматировании:
  //   0 - генерируется правильное значение crc
  //   1 - запись crc из crc(для адреса)/crcd(для данных)
  //   2 - ошибочный crc (генерируется инверсией правильного crc))
  // При чтении (функция seek устанавливает поля c1 и c2):
  //   0 - рассчитанное crc не совпадает с указанным в заголовке (возвращается crc error)
  //   1 - рассчитанное crc совпадает с указанным в заголовке
  ubyte c1, c2;
  ubyte* data; // Указатель на данные сектора внутри трэка
  ubyte* id; // Указатель на заголовок сектора внутри трэка
  ubyte* wp; // Указатель на битовую карту сбойных байтов сектора внутри трэка (используется только при загрузке)
  uint wp_start; // Номер первого бита в карте сбойных байтов (относительно начала трэка) для данного сектора
  uint datlen; // Размер сектора в байтах
  uint crcd; // used to load specific CRC from FDI-file
}


enum SeekMode { SeekOnly = 0, LoadSectors = 1 }


bool test_bit (const(ubyte)* data, uint bit) nothrow @trusted @nogc {
  return (data[bit>>3]&(1U<<(bit&7))) != 0;
}

void set_bit (ubyte* data, uint bit) nothrow @trusted @nogc {
  data[bit>>3] |= (1U<<(bit&7));
}

void clr_bit (ubyte* data, uint bit) nothrow @trusted @nogc {
  data[bit>>3] &= ~(1U<<(bit&7));
}


// ////////////////////////////////////////////////////////////////////////// //
struct TrackCache {
  // cached track position
  FDD drive;
  uint cyl, side;

  // generic track data
  uint trklen;
  // pointer to data inside UDI
  ubyte* trkd; // данные
  ubyte* trki; // битовая карта синхроимпульсов
  ubyte* trkwp; // битовая карта сбойных байтов
  uint ts_byte;// = Z80FQ/(MAX_TRACK_LEN*FDD_RPS); // wd93cpu.t per byte
  SeekMode sf; // flag: is sectors filled
  uint s; // no. of sectors

  // sectors on track
  SectorHeader[MAX_SEC] hdr;

  void set (uint pos) nothrow @trusted @nogc { set_bit(trki, pos); }
  void res (uint pos) nothrow @trusted @nogc { clr_bit(trki, pos); }
  bool bit (uint pos) const nothrow @trusted @nogc { return test_bit(trki, pos); }

  void setWriteProtected (uint pos) nothrow @trusted @nogc { set_bit(trkwp, pos); }
  bool getWriteProtected (uint pos) const nothrow @trusted @nogc { return test_bit(trkwp, pos); }

  void write (uint pos, ubyte b, byte index) nothrow @trusted @nogc {
    if (trkd is null) return;
    trkd[pos] = b;
    if (index) set(pos); else res(pos);
  }

  void clear () nothrow @trusted @nogc {
    drive = null;
    trkd = null;
    ts_byte = Z80FQ/(MAX_TRACK_LEN*FDD_RPS);
  }

  void seek (FDD d, uint cyl, uint side, SeekMode fs) nothrow @trusted {
    if (d is drive && sf == fs && cyl == this.cyl && side == this.side) return;

    drive = d;
    sf = fs;
    s = 0;
    this.cyl = cyl;
    this.side = side;
    if (cyl >= d.cyls || !d.rawdata) { trkd = null; return; }

    assert(cyl < MAX_CYLS);
    trkd = d.trkd[cyl][side];
    trki = d.trki[cyl][side];
    trkwp = d.trkwp[cyl][side];
    trklen = d.trklen[cyl][side];
    if (!trklen) { trkd = null; return; }

    ts_byte = Z80FQ/(trklen*FDD_RPS);
    if (fs == SeekMode.SeekOnly) return; // else find sectors

    for (uint i = 0; i < trklen-8; ++i) {
      import std.algorithm : min;
      if (trkd[i] != 0xA1 || trkd[i+1] != 0xFE || !bit(i)) continue; // Поиск idam
      if (s == MAX_SEC) assert(0, "too many sectors");
      SectorHeader* h = &hdr[s++]; // Заполнение заголовка
      h.id = trkd+i+2; // Указатель на заголовок сектора
      h.c = h.id[0];
      h.s = h.id[1];
      h.n = h.id[2];
      h.l = h.id[3];
      h.crc = *cast(ushort*)(trkd+i+6);
      h.c1 = (wd93_crc(trkd+i+1, 5) == h.crc);
      h.data = null;
      h.datlen = 0;
      h.wp_start = 0;
      //if (h.l > 5) continue; [vv]
      uint end = min(trklen-8, i+8+43); // 43-DD, 30-SD
      // Формирование указателя на зону данных сектора
      for (uint j = i+8; j < end; ++j) {
        if (trkd[j] != 0xA1 || !bit(j) || bit(j+1)) continue;
        if (trkd[j+1] == 0xF8 || trkd[j+1] == 0xFB) {
          // Найден data am
          h.datlen = 128<<(h.l&3); // [vv] FD1793 use only 2 lsb of sector size code
          h.data = trkd+j+2;
          h.c2 = (wd93_crc(h.data-1, h.datlen+1) == *cast(ushort*)(h.data+h.datlen));
          if (trkwp) {
            for (uint b = 0; b < h.datlen; ++b) {
              if (getWriteProtected(j+2+b)) {
                h.wp_start = j+2; // Есть хотябы один сбойный байт
                break;
              }
            }
          }
        }
        break;
      }
    }
  }

  void format () nothrow @trusted {
    import core.stdc.string : memcpy, memset;

    memset(trkd, 0, trklen);
    memset(trki, 0, cast(uint)(trklen+7U)>>3);
    memset(trkwp, 0, cast(uint)(trklen+7U)>>3);

    ubyte* dst = trkd;

    uint i;

    //6250-6144=106
    //gap4a(80)+sync0(12)+iam(3)+1+s*(gap1(50)+sync1(12)+idam(3)+1+4+2+gap2(22)+sync2(12)+data_am(3)+1+2)
    uint gap4a = 80;
    uint sync0 = 12;
    uint i_am = 3;
    uint gap1 = 40;
    uint sync1 = 12;
    uint id_am = 3;
    uint gap2 = 22;
    uint sync2 = 12;
    uint data_am = 3;

    uint data_sz = 0;
    for (uint isx = 0; isx < s; isx++) {
      SectorHeader* sechdr = hdr.ptr+isx;
      data_sz += (128<<(sechdr.l&3)); // n
    }

    if ((gap4a+sync0+i_am+1+data_sz+s*(gap1+sync1+id_am+1+4+2+gap2+sync2+data_am+1+2)) >= MAX_TRACK_LEN) {
      // Превышение стандартной длины дорожки, сокращаем параметры до минимальных
      gap4a = 1;
      sync0 = 1;
      i_am = 1;
      gap1 = 1;
      sync1 = 1;
      id_am = 1;
      gap2 = 1;
      sync2 = 1;
      data_am = 1;
    }

    memset(dst, 0x4E, gap4a); dst += gap4a; // gap4a
    memset(dst, 0, sync0); dst += sync0; //sync

    for (i = 0; i < i_am; i++) write(dst++-trkd, 0xC2, 1); // iam
    *dst++ = 0xFC;

    for (uint isx = 0; isx < s; isx++) {
      memset(dst, 0x4E, gap1); dst += gap1; // gap1 // 50 [vv] // fixme: recalculate gap1 only for non standard formats
      memset(dst, 0, sync1); dst += sync1; //sync
      for (i = 0; i < id_am; i++) write(dst++-trkd, 0xA1, 1); // idam
      *dst++ = 0xFE;

      SectorHeader* sechdr = hdr.ptr+isx;
      *dst++ = sechdr.c; // c
      *dst++ = sechdr.s; // h
      *dst++ = sechdr.n; // s
      *dst++ = sechdr.l; // n

      uint crc = wd93_crc(dst-5, 5); // crc
      if (sechdr.c1 == 1) crc = sechdr.crc;
      if (sechdr.c1 == 2) crc ^= 0xFFFF;
      *cast(uint*)dst = crc;
      dst += 2;

      if (sechdr.data) {
        memset(dst, 0x4E, gap2); dst += gap2; // gap2
        memset(dst, 0, sync2); dst += sync2; //sync
        for (i = 0; i < data_am; i++) write(dst++-trkd, 0xA1, 1); // data am
        *dst++ = 0xFB;

        //if (sechdr.l > 5) errexit("strange sector"); // [vv]
        uint len = 128<<(sechdr.l&3); // data
        if (sechdr.data != cast(ubyte*)1) {
          memcpy(dst, sechdr.data, len);
          if (sechdr.wp) {
            // Копирование битовой карты сбойных байтов
            uint wp_start = dst-trkd;
            sechdr.wp_start = wp_start;
            for (uint b = 0; b < len; b++) {
              if (test_bit(sechdr.wp, b)) setWriteProtected(wp_start+b);
            }
          }
        } else {
          memset(dst, 0, len);
        }

        crc = wd93_crc(dst-1, len+1); // crc
        if (sechdr.c2 == 1) crc = sechdr.crcd;
        if (sechdr.c2 == 2) crc ^= 0xFFFF;
        *cast(uint*)(dst+len) = crc;
        dst += len+2;
      }
    }
    if (dst > trklen+trkd) {
      //printf("cyl=%u, h=%u, additional len=%u\n", cyl, side, dst-(trklen+trkd));
      //errexit("track too long");
      assert(0);
    }
    while (dst < trkd+trklen) *dst++ = 0x4E;
  }

  int writeSector (uint sec, const(ubyte)* data) nothrow @trusted {
    import core.stdc.string : memcpy;
    SectorHeader* h = getSector(sec);
    if (h is null || !h.data) return 0;
    uint sz = h.datlen;
    if (h.data != data) memcpy(h.data, data, sz);
    *cast(ushort*)(h.data+sz) = cast(ushort)wd93_crc(h.data-1, sz+1);
    return sz;
  }

  SectorHeader* getSector (uint sec) nothrow @trusted {
    uint i;
    for (i = 0; i < s; i++) if (hdr[i].n == sec) break;
    if (i == s) return null;
    if (/*(hdr[i].l&3) != 1 ||*/ hdr[i].c != cyl) return null; // [vv]
    return &hdr[i];
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public class WD1793 {
  enum WDSTATE {
    S_IDLE = 0,
    S_WAIT,

    S_DELAY_BEFORE_CMD,
    S_CMD_RW,
    S_FOUND_NEXT_ID,
    S_RDSEC,
    S_READ,
    S_WRSEC,
    S_WRITE,
    S_WRTRACK,
    S_WR_TRACK_DATA,

    S_TYPE1_CMD,
    S_STEP,
    S_SEEKSTART,
    S_RESTORE,
    S_SEEK,
    S_VERIFY,
    S_VERIFY2,

    S_WAIT_HLT,
    S_WAIT_HLT_RW,

    S_RESET
  }

  long next, time;
  long idx_tmo;

  FDD seldrive;
  uint tshift;

  WDSTATE state, state2;

  ubyte cmd;
  ubyte data, track, sector;
  ubyte rqs, status;
  ubyte sign_status; // Внешние сигналы (пока только HLD)

  uint drive, side; // update this with changing 'system'

  byte stepdirection;
  ubyte system; // beta128 system register

  uint idx_cnt; // idx counter

  // read/write sector(s) data
  long end_waiting_am;
  uint foundid; // index in trkcache.hdr for next encountered ID and bytes before this ID
  uint rwptr, rwlen;

  // format track data
  uint start_crc;

  enum CMDBITS : ubyte {
    SEEK_RATE     = 0x03,
    SEEK_VERIFY   = 0x04,
    SEEK_HEADLOAD = 0x08,
    SEEK_TRKUPD   = 0x10,
    SEEK_DIR      = 0x20,

    WRITE_DEL     = 0x01,
    SIDE_CMP_FLAG = 0x02,
    DELAY         = 0x04,
    SIDE          = 0x08,
    SIDE_SHIFT    = 3,
    MULTIPLE      = 0x10,
  }

  enum BETA_STATUS : ubyte {
    DRQ   = 0x40,
    INTRQ = 0x80,
  }

  enum WD_STATUS : ubyte {
    WDS_BUSY      = 0x01,
    WDS_INDEX     = 0x02,
    WDS_DRQ       = 0x02,
    WDS_TRK00     = 0x04,
    WDS_LOST      = 0x04,
    WDS_CRCERR    = 0x08,
    WDS_NOTFOUND  = 0x10,
    WDS_SEEKERR   = 0x10,
    WDS_RECORDT   = 0x20,
    WDS_HEADL     = 0x20,
    WDS_WRFAULT   = 0x20,
    WDS_WRITEP    = 0x40,
    WDS_NOTRDY    = 0x80
  }

  enum WD_SYS : ubyte {
    SYS_HLT = 0x08
  }

  enum WD_SIG : ubyte {
    SIG_HLD = 0x01
  }

  FDD[4] fdd;

public:
  // ctor
  this () {
    foreach (ubyte i; 0..4) fdd[i] = new FDD(i);
    for (ubyte i = 0; i < 4; i++) fdd[i].Id = i; // [vv] Для удобства отладки
    seldrive = fdd[0];
    idx_cnt = 0;
    idx_tmo = long.max;
    sign_status = 0;
  }

  private void process () nothrow @trusted {
    import core.stdc.string : memcpy;

    time = totalTS;

    if (!(sign_status&WD_SIG.SIG_HLD) && !(system&0x20)) seldrive.motor = 0;

    if (seldrive.rawdata) status &= ~cast(uint)WD_STATUS.WDS_NOTRDY; else status |= WD_STATUS.WDS_NOTRDY;

    if (!(cmd&0x80) || (cmd&0xF0) == 0xD0) {
      // seek/step commands
      status &= ~cast(uint)WD_STATUS.WDS_INDEX;

      if (state != WDSTATE.S_IDLE) {
        status &= ~cast(uint)(WD_STATUS.WDS_TRK00|WD_STATUS.WDS_INDEX);
        if (!seldrive.track) status |= WD_STATUS.WDS_TRK00;
      }

      // todo: test spinning
      if (seldrive.rawdata && seldrive.motor && ((time+tshift)%(Z80FQ/FDD_RPS) < (Z80FQ*4/1000))) {
        // index every turn, len=4ms (if disk present)
        if (state == WDSTATE.S_IDLE) {
          if (time < idx_tmo) status |= WD_STATUS.WDS_INDEX;
        } else {
          status |= WD_STATUS.WDS_INDEX;
        }
      }
    }

    for (;;) {
      if (!seldrive.motor) status |= WD_STATUS.WDS_NOTRDY; else status &= ~cast(uint)WD_STATUS.WDS_NOTRDY;
      switch (state) {
        // ----------------------------------------------------
        case WDSTATE.S_IDLE:
          status &= ~cast(uint)WD_STATUS.WDS_BUSY;
          if (idx_cnt >= 15 || time > idx_tmo) {
            idx_cnt = 15;
            status &= WD_STATUS.WDS_NOTRDY;
            status |= WD_STATUS.WDS_NOTRDY;
            seldrive.motor = 0;
            sign_status &= ~cast(uint)WD_SIG.SIG_HLD;
          }
          rqs = BETA_STATUS.INTRQ;
          return;

        case WDSTATE.S_WAIT:
          if (time < next) return;
          state = state2;
          break;

        // ----------------------------------------------------
        // Подана команда типа 2 или 3 (read/write)/(read am/read trk/write trk)
        case WDSTATE.S_DELAY_BEFORE_CMD:
          if (!Config.WD93.noDelay && (cmd&CMDBITS.DELAY)) { // Проверка бита E=1
            next += (Z80FQ*15/1000); // 15ms delay
          }
          state2 = WDSTATE.S_WAIT_HLT_RW;
          state = WDSTATE.S_WAIT;
          break;

        case WDSTATE.S_WAIT_HLT_RW:
          if (!(system&0x08)) return; // HLT = 0 (бесконечное ожидание HLT)
          state = WDSTATE.S_CMD_RW;
          goto case;

        case WDSTATE.S_CMD_RW:
          if (((cmd&0xE0) == 0xA0 || (cmd&0xF0) == 0xF0) && Config.WD93.writeProt(drive)) {
            status |= WD_STATUS.WDS_WRITEP;
            state = WDSTATE.S_IDLE;
            break;
          }

          if ((cmd&0xC0) == 0x80 || (cmd&0xF8) == 0xC0) {
            // read/write sectors or read am-find next AM
            end_waiting_am = next+5*Z80FQ/FDD_RPS; // max wait disk 5 turns
            findMarker();
            break;
          }

          if ((cmd&0xF8) == 0xF0) {
            // write track
            rqs = BETA_STATUS.DRQ;
            status |= WD_STATUS.WDS_DRQ;
            next += 3*seldrive.t.ts_byte;
            state2 = WDSTATE.S_WRTRACK;
            state = WDSTATE.S_WAIT;
            break;
          }

          if ((cmd&0xF8) == 0xE0) {
            // read track
            load();
            rwptr = 0;
            rwlen = seldrive.t.trklen;
            state2 = WDSTATE.S_READ;
            getIndex();
            break;
          }

          // else unknown command
          state = WDSTATE.S_IDLE;
          break;

        case WDSTATE.S_FOUND_NEXT_ID:
          if (!seldrive.rawdata) {
            // no disk-wait again
            end_waiting_am = next+5*Z80FQ/FDD_RPS;
          nextmk:
            findMarker();
            break;
          }

          if (next >= end_waiting_am) {
           nf:
            status |= WD_STATUS.WDS_NOTFOUND;
            state = WDSTATE.S_IDLE;
            break;
          }

          if (foundid == -1) goto nf;

          status &= ~cast(uint)WD_STATUS.WDS_CRCERR;
          load();

          if (!(cmd&0x80)) {
            // verify after seek
            if (seldrive.t.hdr[foundid].c != track) goto nextmk;
            if (!seldrive.t.hdr[foundid].c1) {
              status |= WD_STATUS.WDS_CRCERR;
              goto nextmk;
            }
            state = WDSTATE.S_IDLE; break;
          }

          if ((cmd&0xF0) == 0xC0) {
            // read AM
            rwptr = seldrive.t.hdr[foundid].id-seldrive.t.trkd;
            rwlen = 6;

           read_first_byte:
            ubyte data_mask = 0;
            if (seldrive.t.trkwp) {
              // проверяем карту сбойных байтов
              if (seldrive.t.getWriteProtected(rwptr)) {
                data_mask = 0xFF;
                seldrive.t.hdr[foundid].c1 = 0; // bad address crc
              }
            }
            data = seldrive.t.trkd[rwptr++];
            data ^= data_mask;
            rwlen--;
            rqs = BETA_STATUS.DRQ;
            status |= WD_STATUS.WDS_DRQ;
            next += seldrive.t.ts_byte;
            state = WDSTATE.S_WAIT;
            state2 = WDSTATE.S_READ;
            break;
          }

          // else R/W sector(s)
          if (seldrive.t.hdr[foundid].c != track || seldrive.t.hdr[foundid].n != sector) goto nextmk;
          if ((cmd&CMDBITS.SIDE_CMP_FLAG) && (((cmd>>CMDBITS.SIDE_SHIFT)^seldrive.t.hdr[foundid].s)&1)) goto nextmk;
          if (!seldrive.t.hdr[foundid].c1) { status |= WD_STATUS.WDS_CRCERR; goto nextmk; }

          if (cmd&0x20) {
            // write sector(s)
            rqs = BETA_STATUS.DRQ;
            status |= WD_STATUS.WDS_DRQ;
            next += seldrive.t.ts_byte*9;
            state = WDSTATE.S_WAIT; state2 = WDSTATE.S_WRSEC;
            break;
          }

          // read sector(s)
          if (!seldrive.t.hdr[foundid].data) goto nextmk; // Сектор без зоны данных
          if (!Config.WD93.noDelay) next += seldrive.t.ts_byte*(seldrive.t.hdr[foundid].data-seldrive.t.hdr[foundid].id); // Задержка на пропуск заголовка сектора и пробела между заголовком и зоной данных
          state = WDSTATE.S_WAIT;
          state2 = WDSTATE.S_RDSEC;
          break;

        case WDSTATE.S_RDSEC:
          if (seldrive.t.hdr[foundid].data[-1] == 0xF8) status |= WD_STATUS.WDS_RECORDT; else status &= ~cast(uint)WD_STATUS.WDS_RECORDT;
          rwptr = seldrive.t.hdr[foundid].data-seldrive.t.trkd; // Смещение зоны данных сектора (в байтах) относительно начала трека
          rwlen = 128<<(seldrive.t.hdr[foundid].l&3); // [vv]
          goto read_first_byte;

        case WDSTATE.S_READ:
          if (notReady) break;
          load();

          if (!seldrive.t.trkd) {
            status |= WD_STATUS.WDS_NOTFOUND;
            state = WDSTATE.S_IDLE;
            break;
          }

          if (rwlen) {
            ledUpdate(led1793Load);
            if (rqs&BETA_STATUS.DRQ) status |= WD_STATUS.WDS_LOST;

            ubyte data_mask = 0;
            if (seldrive.t.trkwp) {
              // проверяем карту сбойных байтов
              if (seldrive.t.getWriteProtected(rwptr)) {
                data_mask = 0xFF;
                if ((cmd&0xE0) == 0x80) {
                  // read sector
                  seldrive.t.hdr[foundid].c2 = 0; // bad data crc
                }
                if ((cmd&0xF0) == 0xC0) {
                  // read address
                  seldrive.t.hdr[foundid].c1 = 0; // bad address crc
                }
              }
            }
            data = seldrive.t.trkd[rwptr++];
            data ^= data_mask;
            rwlen--;
            rqs = BETA_STATUS.DRQ;
            status |= WD_STATUS.WDS_DRQ;

            if (!Config.WD93.noDelay) next += seldrive.t.ts_byte; else next = time+1;
            state = WDSTATE.S_WAIT;
            state2 = WDSTATE.S_READ;
          } else {
            if ((cmd&0xE0) == 0x80) {
              // read sector
              if (!seldrive.t.hdr[foundid].c2) status |= WD_STATUS.WDS_CRCERR;
              if (cmd&CMDBITS.MULTIPLE) {
                sector++;
                state = WDSTATE.S_CMD_RW;
                break;
              }
            }

            // FIXME: Временный хак для zx-format 3
            if (((cmd&0xF8) == 0xE0) && (seldrive.t.trklen < MAX_TRACK_LEN)) {
              // read track
              status |= WD_STATUS.WDS_LOST;
            }

            if ((cmd&0xF0) == 0xC0) {
              // read address
              if (!seldrive.t.hdr[foundid].c1) status |= WD_STATUS.WDS_CRCERR;
            }
            state = WDSTATE.S_IDLE;
          }
          break;

        case WDSTATE.S_WRSEC:
          load();
          if (rqs&BETA_STATUS.DRQ) {
            status |= WD_STATUS.WDS_LOST;
            state = WDSTATE.S_IDLE;
            break;
          }
          seldrive.optype |= 1;
          rwptr = seldrive.t.hdr[foundid].id+6+11+11-seldrive.t.trkd;
          for (rwlen = 0; rwlen < 12; rwlen++) seldrive.t.write(rwptr++, 0, 0);
          for (rwlen = 0; rwlen < 3; rwlen++) seldrive.t.write(rwptr++, 0xA1, 1);
          seldrive.t.write(rwptr++, (cmd&CMDBITS.WRITE_DEL ? 0xF8 : 0xFB), 0);
          rwlen = 128<<(seldrive.t.hdr[foundid].l&3); // [vv]
          state = WDSTATE.S_WRITE;
          break;

        case WDSTATE.S_WRITE:
          if (notReady) break;
          if (rqs&BETA_STATUS.DRQ) status |= WD_STATUS.WDS_LOST, data = 0;
          ledUpdate(led1793Save);
          seldrive.t.write(rwptr++, data, 0); rwlen--;
          if (rwptr == seldrive.t.trklen) rwptr = 0;
          seldrive.t.sf = SeekMode.SeekOnly; // invalidate sectors
          if (rwlen) {
            if (!Config.WD93.noDelay) next += seldrive.t.ts_byte;
            state = WDSTATE.S_WAIT; state2 = WDSTATE.S_WRITE;
            rqs = BETA_STATUS.DRQ; status |= WD_STATUS.WDS_DRQ;
          } else {
            uint len = (128<<(seldrive.t.hdr[foundid].l&3))+1; //[vv]
            ubyte[2056] sc = void;
            if (rwptr < len) {
              memcpy(sc.ptr, seldrive.t.trkd+seldrive.t.trklen-rwptr, rwptr);
              memcpy(sc.ptr+rwptr, seldrive.t.trkd, len-rwptr);
            } else {
              memcpy(sc.ptr, seldrive.t.trkd+rwptr-len, len);
            }
            uint crc = wd93_crc(sc.ptr, len);
            seldrive.t.write(rwptr++, cast(ubyte)crc, 0);
            seldrive.t.write(rwptr++, cast(ubyte)(crc>>8), 0);
            seldrive.t.write(rwptr, 0xFF, 0);
            if (cmd&CMDBITS.MULTIPLE) {
              sector++;
              state = WDSTATE.S_CMD_RW;
              break;
            }
            state = WDSTATE.S_IDLE;
          }
          break;

        case WDSTATE.S_WRTRACK:
          if (rqs&BETA_STATUS.DRQ) {
            status |= WD_STATUS.WDS_LOST;
            state = WDSTATE.S_IDLE;
            break;
          }
          seldrive.optype |= 2;
          state2 = WDSTATE.S_WR_TRACK_DATA;
          start_crc = 0;
          getIndex();
          end_waiting_am = next+5*Z80FQ/FDD_RPS;
          break;

        case WDSTATE.S_WR_TRACK_DATA:
          if (notReady) break;
          ledUpdate(led1793Format);
          if (rqs&BETA_STATUS.DRQ) {
            status |= WD_STATUS.WDS_LOST;
            data = 0;
          }
          seldrive.t.seek(seldrive, seldrive.track, side, SeekMode.SeekOnly);
          seldrive.t.sf = SeekMode.SeekOnly; // invalidate sectors

          if (!seldrive.t.trkd) {
            state = WDSTATE.S_IDLE;
            break;
          }

          ubyte marker = 0, byt = data;
          uint crc;
          switch (data) {
            case 0xF5:
              byt = 0xA1;
              marker = 1;
              start_crc = rwptr+1;
              break;
            case 0xF6:
              byt = 0xC2;
              marker = 1;
              break;
            case 0xF7:
              crc = wd93_crc(seldrive.t.trkd+start_crc, rwptr-start_crc);
              byt = crc&0xFF;
              break;
            default: break;
          }

          seldrive.t.write(rwptr++, byt, marker);
          rwlen--;
          if (data == 0xF7) {
            seldrive.t.write(rwptr++, (crc>>8)&0xFF, 0);
            rwlen--; // second byte of CRC16
          }

          if (cast(int)rwlen > 0) {
            if (!Config.WD93.noDelay) next += seldrive.t.ts_byte;
            state2 = WDSTATE.S_WR_TRACK_DATA;
            state = WDSTATE.S_WAIT;
            rqs = BETA_STATUS.DRQ;
            status |= WD_STATUS.WDS_DRQ;
            break;
          }
          state = WDSTATE.S_IDLE;
          break;

        // ----------------------------------------------------
        case WDSTATE.S_TYPE1_CMD: // Подана команда типа 1 (restore/seek/step)
          status &= ~cast(uint)(WD_STATUS.WDS_CRCERR|WD_STATUS.WDS_SEEKERR|WD_STATUS.WDS_WRITEP);
          rqs = 0;

          if (Config.WD93.writeProt(drive)) status |= WD_STATUS.WDS_WRITEP;

          seldrive.motor = (cmd&CMDBITS.SEEK_HEADLOAD) || (system&0x20 ? next+2*Z80FQ : 0);

          state2 = WDSTATE.S_SEEKSTART; // default is seek/restore

          if (cmd&0xE0) {
            // single step
            if (cmd&0x40) stepdirection = (cmd&CMDBITS.SEEK_DIR ? -1 : 1); // step in/step out
            state2 = WDSTATE.S_STEP;
          }

          next += (Z80FQ*14)/1000000;
          state = WDSTATE.S_WAIT; // Задержка 14мкс перед появленеим статуса BUSY
          break;

        case WDSTATE.S_STEP:
          status |= WD_STATUS.WDS_BUSY;
          ledUpdate(led1793Seek);

          if (seldrive.track == 0 && stepdirection < 0) {
            // проверка TRK00
            track = 0;
            state = WDSTATE.S_VERIFY; // Обработка бита команды V
            break;
          }

          // Обработкабита T=1 (для seek всегда 1, для restore всегда 0, но обновление регистра трэка делается)
          if (!(cmd&0xF0) || (cmd&CMDBITS.SEEK_TRKUPD)) track += stepdirection;

          if (seldrive.motor) {
            seldrive.track += stepdirection;
            if (seldrive.track == cast(ubyte)-1) seldrive.track = 0;
            if (seldrive.track >= MAX_PHYS_CYL) seldrive.track = MAX_PHYS_CYL;
            seldrive.t.clear();
          }

          static immutable uint[4] steps = [ 6, 12, 20, 30 ];
          if (!Config.WD93.noDelay) next += steps[cmd&CMDBITS.SEEK_RATE]*Z80FQ/1000;

          version(MOD_9X) {
            if (!Config.WD93.noDelay && wd93conf.fdd_noise) Beep((stepdirection > 0? 600 : 800), 2);
          }

          state2 = (cmd&0xE0 ? WDSTATE.S_VERIFY /*Команда step*/: WDSTATE.S_SEEK /*Команда seek/restore*/);
          state = WDSTATE.S_WAIT;
          break;

        case WDSTATE.S_SEEKSTART:
          status |= WD_STATUS.WDS_BUSY;
          if (!(cmd&0x10)) {
            // Команда restore
            if (!Config.WD93.noDelay) {
              state2 = WDSTATE.S_RESTORE;
              next += (Z80FQ*21)/1000000; // задержка ~21мкс перед загрузкой регистра трэка
              state = WDSTATE.S_WAIT;
              break;
            }
            state = WDSTATE.S_RESTORE;
            break;
          }
          state = WDSTATE.S_SEEK;
          break;

        case WDSTATE.S_RESTORE:
          track = 0xFF;
          data = 0;
          state = WDSTATE.S_SEEK;
          break;

        case WDSTATE.S_SEEK:
          if (data == track) {
            state = WDSTATE.S_VERIFY;
            break;
          }
          stepdirection = (data < track ? -1 : 1);
          state = WDSTATE.S_STEP;
          break;

        case WDSTATE.S_VERIFY:
          if (!(cmd&CMDBITS.SEEK_VERIFY)) {
            // Проверка номера трэка не нужна V=0
            status |= WD_STATUS.WDS_BUSY;
            state2 = WDSTATE.S_IDLE;
            state = WDSTATE.S_WAIT;
            idx_tmo = next+15*Z80FQ/FDD_RPS; // 15 disk turns
            next += (105*Z80FQ)/1000000; // Задержка 105мкс со статусом BUSY
            break;
          }

          // Проверка номера трэка нужна V=1
          sign_status |= WD_SIG.SIG_HLD;

          // Ожидание 15мс
          //       |
          //       v
          // Ожидание HLT == 1
          //       |
          //       v
          // S_VERIFY2
          if (!Config.WD93.noDelay) {
            next += (15*Z80FQ)/1000; // Задержка 15мс
            state2 = WDSTATE.S_WAIT_HLT;
            state = WDSTATE.S_WAIT;
            break;
          }
          state = WDSTATE.S_WAIT_HLT;
          goto case;

        case WDSTATE.S_WAIT_HLT:
          if (!(system&0x08)) return; // HLT = 0 (бесконечное ожидание HLT)
          state = WDSTATE.S_VERIFY2;
          goto case;

        case WDSTATE.S_VERIFY2:
          end_waiting_am = next+6*Z80FQ/FDD_RPS; // max wait disk 6 turns
          load();
          findMarker();
          break;

        // ----------------------------------------------------
        case WDSTATE.S_RESET: // seek to trk0, but don't be busy
          if (!seldrive.track) {
            state = WDSTATE.S_IDLE;
          } else {
            seldrive.track--;
            seldrive.t.clear();
          }
          // if (!seldrive.track) track = 0;
          next += 6*Z80FQ/1000;
          break;

        default:
          assert(0, "WD1793 in wrong state");
      }
    }
  }

  private void findMarker () nothrow @trusted {
    if (Config.WD93.noDelay && seldrive.track != track) seldrive.track = track;
    load();

    foundid = -1;
    if (seldrive.motor && seldrive.rawdata) {
      uint div = seldrive.t.trklen*seldrive.t.ts_byte; // Длина дорожки в тактах wd93cpu
      uint i = cast(uint)((next+tshift)%div)/seldrive.t.ts_byte; // Позиция байта соответствующего текущему такту на дорожке
      uint wait = -1;

      // Поиск заголовка минимально отстоящего от текущего байта
      for (uint isx = 0; isx < seldrive.t.s; isx++) {
        uint pos = seldrive.t.hdr[isx].id-seldrive.t.trkd; // Смещение (в байтах) заголовка относительно начала дорожки
        uint dist = (pos > i ? pos-i : seldrive.t.trklen+pos-i); // Расстояние (в байтах) от заголовка до текущего байта
        if (dist < wait) {
          wait = dist;
          foundid = isx;
        }
      }

      if (foundid != -1)
        wait *= seldrive.t.ts_byte; // Задержка в тактах от текущего такта до такта чтения первого байта заголовка
      else
        wait = 10*Z80FQ/FDD_RPS;

      if (Config.WD93.noDelay && foundid != -1) {
        // adjust tshift, that id appares right under head
        uint pos = seldrive.t.hdr[foundid].id-seldrive.t.trkd+2;
        tshift = cast(uint)(((pos*seldrive.t.ts_byte)-(next%div)+div)%div);
        wait = 100; // delay=0 causes fdc to search infinitely, when no matched id on track
      }

      next += wait;
    } else {
      // else no index pulses - infinite wait
      next = totalTS+1;
    }

    if (seldrive.rawdata && next > end_waiting_am) {
      next = end_waiting_am;
      foundid = -1;
    }
    state = WDSTATE.S_WAIT;
    state2 = WDSTATE.S_FOUND_NEXT_ID;
  }

  private bool notReady () nothrow @trusted {
    // fdc is too fast in no-delay mode, wait until cpu handles DRQ, but not more 'end_waiting_am'
    if (Config.WD93.noDelay && (rqs&BETA_STATUS.DRQ) && (next < (end_waiting_am-5*Z80FQ/FDD_RPS)+(600*Z80FQ)/1000)) {
      state2 = state;
      state = WDSTATE.S_WAIT;
      next += seldrive.t.ts_byte;
      return true;
    }
    return false;
  }

  private void getIndex () nothrow @trusted {
    uint trlen = seldrive.t.trklen*seldrive.t.ts_byte;
    uint ticks = cast(uint)((next+tshift)%trlen);
    if (!Config.WD93.noDelay) next += (trlen-ticks);
    rwptr = 0;
    rwlen = seldrive.t.trklen;
    state = WDSTATE.S_WAIT;
  }

  void load () nothrow @trusted {
    seldrive.t.seek(seldrive, seldrive.track, side, SeekMode.LoadSectors);
  }

  ubyte inp (ubyte port) nothrow @trusted {
    process();
    if (port&0x80) return rqs|(system&0x3F);
    if (port == 0x1F) {
      rqs &= ~cast(uint)BETA_STATUS.INTRQ;
      return rdStatus();
    }
    if (port == 0x3F) return track;
    if (port == 0x5F) return sector;
    if (port == 0x7F) {
      status &= ~cast(uint)WD_STATUS.WDS_DRQ;
      rqs &= ~cast(uint)BETA_STATUS.DRQ;
      return data;
    }
    return 0xFF;
  }

  ubyte rdStatus () nothrow @trusted {
    if (!(cmd&0x80)) {
      // hld & hlt
      return status|((sign_status&WD_SIG.SIG_HLD) && (system&8) ? WD_STATUS.WDS_HEADL : 0);
    }
    return status;
  }

  void outp (ubyte port, ubyte val) nothrow @trusted {
    process();

    if (port == 0x1F) {
      // cmd

      // force interrupt (type 4)
      if ((val&0xF0) == 0xD0) {
        ubyte Cond = (val&0xF);
        next = totalTS;
        idx_cnt = 0;
        idx_tmo = next+15*Z80FQ/FDD_RPS; // 15 disk turns
        cmd = val;

        if (Cond == 0) {
          state = WDSTATE.S_IDLE; rqs = 0;
          status &= ~cast(uint)WD_STATUS.WDS_BUSY;
          return;
        }

        if (Cond&8) {
          // unconditional int
          state = WDSTATE.S_IDLE; rqs = BETA_STATUS.INTRQ;
          status &= ~cast(uint)WD_STATUS.WDS_BUSY;
          return;
        }

        if (Cond&4) {
          // int by idam (unimplemented yet)
          state = WDSTATE.S_IDLE; rqs = BETA_STATUS.INTRQ;
          status &= ~cast(uint)WD_STATUS.WDS_BUSY;
          return;
        }

        if (Cond&2) {
          // int 1.0 rdy (unimplemented yet)
          state = WDSTATE.S_IDLE; rqs = BETA_STATUS.INTRQ;
          status &= ~cast(uint)WD_STATUS.WDS_BUSY;
          return;
        }

        if (Cond&1) {
          // int 0.1 rdy (unimplemented yet)
          state = WDSTATE.S_IDLE; rqs = BETA_STATUS.INTRQ;
          status &= ~cast(uint)WD_STATUS.WDS_BUSY;
          return;
        }

        return;
      }

      if (status&WD_STATUS.WDS_BUSY) return;
      cmd = val;
      next = totalTS;
      status |= WD_STATUS.WDS_BUSY;
      rqs = 0;
      idx_cnt = 0;
      idx_tmo = long.max;

      //-----------------------------------------------------------------------

      if (cmd&0x80) {
        // read/write command (type 2, 3)
        status = (status|WD_STATUS.WDS_BUSY)&~cast(uint)(WD_STATUS.WDS_DRQ|WD_STATUS.WDS_LOST|WD_STATUS.WDS_NOTFOUND|WD_STATUS.WDS_RECORDT|WD_STATUS.WDS_WRITEP);

        // continue disk spinning
        seldrive.motor = next+2*Z80FQ;

        // abort if no disk
        if (status&WD_STATUS.WDS_NOTRDY) {
          state2 = WDSTATE.S_IDLE;
          state = WDSTATE.S_WAIT;
          next = totalTS+Z80FQ/FDD_RPS;
          rqs = BETA_STATUS.INTRQ;
          return;
        }

        sign_status |= WD_SIG.SIG_HLD;

        state = WDSTATE.S_DELAY_BEFORE_CMD;
        return;
      }

      // (type 1)
      if (cmd&CMDBITS.SEEK_HEADLOAD) // h = 1
        sign_status |= WD_SIG.SIG_HLD;
      else
        sign_status &= ~cast(uint)WD_SIG.SIG_HLD;

      // else seek/step command
      status &= ~cast(uint)WD_STATUS.WDS_BUSY;
      state = WDSTATE.S_TYPE1_CMD;
      return;
    }

    //=======================================================================

    if (port == 0x3F) {
      track = val;
      return;
    }

    if (port == 0x5F) {
      sector = val;
      return;
    }

    if (port == 0x7F) {
      data = val;
      rqs &= ~cast(uint)BETA_STATUS.DRQ;
      status &= ~cast(uint)WD_STATUS.WDS_DRQ;
      return;
    }

    if (port&0x80) {
      // FF
      // system
      drive = val&3;
      side = ~(val>>4)&1;
      seldrive = fdd[drive];
      seldrive.t.clear();

      if (!(val&0x04)) {
        // reset
        status = WD_STATUS.WDS_NOTRDY;
        rqs = BETA_STATUS.INTRQ;
        seldrive.motor = 0;
        state = WDSTATE.S_IDLE;
        idx_cnt = 0;
        version(all) { // move head to trk00
          //steptime = 6*(Z80FQ/1000); // 6ms
          next += 1*Z80FQ/1000; // 1ms before command
          state = WDSTATE.S_RESET;
          //seldrive.track = 0;
        }
      } else {
        if ((system^val)&WD_SYS.SYS_HLT) {
          // hlt 0.1
          if (!(status&WD_STATUS.WDS_BUSY)) idx_cnt++;
        }
      }

      if (val&0x20) {
        // В quorum бит D5 управляет мотором дисковода
        sign_status |= WD_SIG.SIG_HLD;
        seldrive.motor = next+2*Z80FQ;
      }
      system = val;
    }
  }

  void trdosTraps () nothrow @trusted {
    uint pc = z80pc;
    if (pc < 0x3DFD) return;

    // Позиционирование на соседнюю дорожку (пауза)
    if (pc == 0x3DFD && trdosROMread(0x3DFD) == 0x3E && trdosROMread(0x3DFF) == 0x0E) {
      if (trdTrapsDebug) conwriteln("TRDTRAP: next track");
      z80pc = z80pop();
      emuz80.AF.a = 0;
      emuz80.BC.c = 0;
    }

    // Позиционирование на произвольную дорожку (пауза)
    if (pc == 0x3EA0 && trdosROMread(0x3EA0) == 0x06 && trdosROMread(0x3EA2) == 0x3E) {
      if (trdTrapsDebug) conwriteln("TRDTRAP: seek track");
      z80pc = z80pop();
      emuz80.AF.a = 0;
      emuz80.BC.b = 0;
    }

    if (pc == 0x3E01 && trdosROMread(0x3E01) == 0x0D) {
      if (trdTrapsDebug) conwriteln("TRDTRAP: delay skip");
      emuz80.AF.a = emuz80.BC.c = 1;
      return;
    } // no delays

    if (pc == 0x3FEC && trdosROMread(0x3FED) == 0xA2 && (state == WDSTATE.S_READ || (state2 == WDSTATE.S_READ && state == WDSTATE.S_WAIT))) {
      if (trdTrapsDebug) conwriteln("TRDTRAP: read byte");
      ledUpdate(led1793Load);
      if (rqs&BETA_STATUS.DRQ) {
        emuz80.memPokeB(emuz80.HL, data);
        //wd93cpu.DbgMemIf.wm(wd93cpu.hl, data); // move byte from controller
        emuz80.HL++;
        emuz80.BC.b--;
        rqs &= ~cast(uint)BETA_STATUS.DRQ;
        status &= ~cast(uint)WD_STATUS.WDS_DRQ;
      }
      if (seldrive.t.trkd) {
        while (rwlen) {
          // move others
          emuz80.memPokeB(emuz80.HL, seldrive.t.trkd[rwptr++]);
          //wd93cpu.DbgMemIf.wm(wd93cpu.hl, seldrive.t.trkd[rwptr++]);
          rwlen--;
          emuz80.HL++;
          emuz80.BC.b--;
        }
      }
      z80pc = z80pc+2; // skip INI
      return;
    }

    if (pc == 0x3FD1 && trdosROMread(0x3FD2) == 0xA3 && (rqs&BETA_STATUS.DRQ) && (rwlen>1) && (state == WDSTATE.S_WRITE || (state2 == WDSTATE.S_WRITE && state == WDSTATE.S_WAIT))) {
      if (trdTrapsDebug) conwriteln("TRDTRAP: write byte");
      ledUpdate(led1793Save);
      while (rwlen > 1) {
        //seldrive.t.write(rwptr++, wd93cpu.DbgMemIf.rm(wd93cpu.hl), 0);
        seldrive.t.write(rwptr++, emuz80.memPeekB(emuz80.HL), 0);
        rwlen--;
        emuz80.HL++;
        emuz80.BC.b--;
      }
      z80pc = z80pc+2; // skip OUTI
      return;
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public class FDD {
  ubyte Id;
  // drive data

  long motor; // 0 - not spinning, >0 - time when it'll stop
  ubyte track; // head position

  // disk data
  ubyte* rawdata; // used in VirtualAlloc/VirtualFree
  uint rawsize;
  uint cyls, sides;
  uint[2][MAX_CYLS] trklen;
  ubyte*[2][MAX_CYLS] trkd; // данные
  ubyte*[2][MAX_CYLS] trki; // битовые карты синхроимпульсов
  ubyte*[2][MAX_CYLS] trkwp; // битовые карты сбойных байтов
  ubyte optype; // bits: 0-not modified, 1-write sector, 2-format track
  ubyte snaptype;

  TrackCache t; // used in read/write image
  char[0x200] name;
  char[0x200] dsc;

public:
  @property const(char)[] namestr () const pure nothrow @trusted @nogc {
    foreach (immutable idx, char ch; name[]) if (ch == 0) return name[0..idx];
    return name[];
  }

  @property const(char)[] dscstr () const pure nothrow @trusted @nogc {
    foreach (immutable idx, char ch; dsc[]) if (ch == 0) return dsc[0..idx];
    return dsc[];
  }

public:
  ubyte index; // INIT THIS!

public:
  this (ubyte aindex) nothrow @trusted { index = aindex; clear(); }

  ~this () nothrow @trusted { clear(); }

  void clear () nothrow @trusted {
    import core.stdc.string : memset;

    if (rawdata !is null) {
      import core.stdc.stdlib : free;
      free(rawdata);
      rawdata = null;
    }

    motor = 0;
    track = 0;

    rawsize = 0;
    cyls = 0;
    sides = 0;
    name[] = 0;
    dsc[] = 0;
    memset(trklen.ptr, 0,  trklen.sizeof);
    memset(trkd.ptr, 0,  trkd.sizeof);
    memset(trki.ptr, 0,  trki.sizeof);
    memset(trkwp.ptr, 0,  trkwp.sizeof);
    optype = 0;
    snaptype = 0;
    Config.WD93.writeProt(index, false);
    /*wd93comp.wd.trkcache.clear(); [vv]*/
    t.clear();
  }

  protected void allocRawData (uint size) nothrow @trusted {
    import core.stdc.stdlib : realloc;
    if (size >= uint.max/8) assert(0, "invalid allocRawData call");
    if (rawsize != size) {
      rawdata = cast(ubyte*)realloc(rawdata, size+1024);
      if (rawdata is null) assert(0, "out of memory");
      rawsize = size;
    }
    rawdata[0..rawsize] = 0;
  }

  void newDisk (uint cyls, uint sides) nothrow @trusted {
    import core.stdc.stdlib : malloc;
    clear();
    this.cyls = cyls;
    this.sides = sides;
    uint len = MAX_TRACK_LEN;
    uint bitmap_len = uint(len+7U)>>3;
    uint len2 = len+bitmap_len*2;
    allocRawData(cyls*sides*len2);
    for (uint c = 0; c < cyls; c++) {
      for (uint h = 0; h < sides; h++) {
        trklen[c][h] = len;
        trkd[c][h] = rawdata+len2*(c*sides+h);
        trki[c][h] = trkd[c][h]+len;
        trkwp[c][h] = trkd[c][h]+len+bitmap_len;
      }
    }
    // wd93comp.wd.trkcache.clear(); // already done in free()
  }

  // //////////////////////////////////////////////////////////////////// //
  // TRD/SCL/HoBeta READER
  enum TRDDiskType : ubyte {
    DS_80 = 0x16,
    DS_40 = 0x17,
    SS_80 = 0x18,
    SS_40 = 0x19,
  }

  enum TRD_SIG = 0x10;

  align(1) static struct TRDDirEntryBase {
  align(1):
    char[8] name;
    ubyte type;
    ushort start;
    ushort length;
    ubyte seccnt; // Длина файла в секторах
  }

  align(1) static struct TRDDirEntry {
  align(1):
    char[8] name;
    ubyte type;
    ushort start;
    ushort length;
    ubyte seccnt; // Длина файла в секторах
    //
    ubyte sec; // Начальный сектор
    ubyte trk; // Начальная дорожка
  }

  align(1) static struct TRDSec9 {
  align(1):
    ubyte Zero;         // 00
    ubyte[224] Reserved;
    ubyte FirstFreeSec; // E1
    ubyte FirstFreeTrk; // E2
    ubyte DiskType;     // E3
    ubyte FileCnt;      // E4
    ushort FreeSecCnt;  // E5, E6
    ubyte TrDosSig;     // E7
    ubyte[2] Res1;      // | 0
    ubyte[9] Res2;      // | 32
    ubyte Res3;         // | 0
    ubyte DelFileCnt;   // F4
    char[8] Label;   // F5-FC
    ubyte[3] Res4;      // | 0
  }

  align(1) static struct SCLHeader {
  align(1):
    char[8] Sig; // SINCLAIR
    ubyte FileCnt;
    TRDDirEntryBase[0] Files;
  }

  // Проверка на то что диск имеет tr-dos формат
  bool isTRD () nothrow @trusted {
    static bool checkFiller (const(void)[] buf, char fill) nothrow @trusted @nogc {
      const(char)* p = cast(const(char)*)buf.ptr;
      foreach (immutable _; 0..buf.length) if (*p++ != fill) return false;
      return true;
    }

    t.seek(this, 0, 0, SeekMode.LoadSectors);

    SectorHeader* hdr = t.getSector(9);
    if (hdr is null) return false;

    //conwriteln("000");
    if ((hdr.l&3) != 1) return false; // Проверка кода размера сектора (1 - 256 байт)

    TRDSec9* sec9 = cast(TRDSec9*)hdr.data;
    if (sec9 is null) return false;

    //conwriteln("001");
    if (sec9.Zero != 0) return false;

    //conwriteln("002");
    if (sec9.TrDosSig != TRD_SIG) return false;

    //conwriteln("003");
    if (!(checkFiller(sec9.Res2[], ' ') || checkFiller(sec9.Res2[], 0))) return false;

    //conwriteln("004");
    if (!(sec9.DiskType == TRDDiskType.DS_80 || sec9.DiskType == TRDDiskType.DS_40 ||
          sec9.DiskType == TRDDiskType.SS_80 || sec9.DiskType == TRDDiskType.SS_40)) return false;

    //conwriteln("005");
    return true;
  }

  void formatTRD (uint CylCnt) nothrow @trusted {
    static immutable ubyte[16][3] lv =
      [ [ 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16 ],
        [ 1,9,2,10,3,11,4,12,5,13,6,14,7,15,8,16 ],
        [ 1,12,7,2,13,8,3,14,9,4,15,10,5,16,11,6 ] ];
    const(ubyte)* ilv;
    if (/*Config.WD93.trdInterleave < 0 ||*/ Config.WD93.trdInterleave >= lv.length) {
      ilv = lv[0].ptr;
    } else {
      ilv = lv[Config.WD93.trdInterleave].ptr;
    }
    newDisk(CylCnt, 2);
    for (uint c = 0; c < cyls; c++) {
      for (uint side = 0; side < 2; side++) {
        t.seek(this, c, side, SeekMode.SeekOnly);
        t.s = 16;
        for (uint sn = 0; sn < 16; sn++) {
          uint s = ilv[sn]; //lv[Config.WD93.trdInterleave][sn];
          t.hdr[sn].n = cast(ubyte)s;
          t.hdr[sn].l = 1;
          t.hdr[sn].c = cast(ubyte)c;
          t.hdr[sn].s = 0;
          t.hdr[sn].c1 = t.hdr[sn].c2 = 0;
          t.hdr[sn].data = cast(ubyte*)1;
        }
        t.format();
      }
    }
  }

  void emptyDiskEmpty (uint FreeSecCnt=2544) nothrow @trusted {
    uint SecCnt = FreeSecCnt+16;
    uint CylCnt = SecCnt/(16*2)+(SecCnt%(16*2) ? 1 : 0);
    newDisk(CylCnt, 2);
  }

  void emptyDiskTRD (uint FreeSecCnt=2544) nothrow @trusted {
    uint SecCnt = FreeSecCnt+16;
    uint CylCnt = SecCnt/(16*2)+(SecCnt%(16*2) ? 1 : 0);
    formatTRD(CylCnt);
    t.seek(this, 0, 0, SeekMode.LoadSectors);
    SectorHeader* sec9hdr = t.getSector(9);
    if (sec9hdr is null) return;
    TRDSec9* sec9 = cast(TRDSec9*)sec9hdr.data;
    sec9.FirstFreeTrk = 1; // first free track
    sec9.DiskType = TRDDiskType.DS_80; // 80T,DS
    sec9.FreeSecCnt = cast(ushort)FreeSecCnt; // free sec
    sec9.TrDosSig = TRD_SIG; // trdos flag
    sec9.Label[] = ' '; // label
    sec9.Res2[] = ' '; // reserved
    t.writeSector(9, sec9hdr.data); // update sector CRC
  }

  bool addfile (const(ubyte)* hdr, const(ubyte)* data, bool tosys=false) nothrow @trusted {
    import core.stdc.string : memcpy;

    if (!isTRD) { conwriteln("not a TR-DOS disk"); return false; }

    t.seek(this, 0, 0, SeekMode.LoadSectors);
    SectorHeader* sec9hdr = t.getSector(9);
    assert(sec9hdr !is null);

    TRDSec9* sec9 = cast(TRDSec9*)sec9hdr.data;
    assert(sec9 !is null);

    if (sec9.FileCnt >= 128) { conwriteln("TRD: too many files"); return false; } // Каталог заполнен полностью

    uint len = (cast(TRDDirEntry*)hdr).seccnt;
    uint pos = sec9.FileCnt*TRDDirEntry.sizeof;
    SectorHeader* dir = t.getSector(1+pos/0x100);

    if (dir is null) { conwriteln("TRD: can't find directory"); return false; }

    if (tosys && len > 7*256) { conwriteln("TRD: file is too big for system area; using normal placement"); tosys = false; }
    if (!tosys && sec9.FreeSecCnt < len) { conwriteln("TRD: no room: len=", len, "; freesectors=", sec9.FreeSecCnt); return false; } // На диске нет места
    //conwriteln("TRD: room: len=", len, "; freesectors=", sec9.FreeSecCnt);

    TRDDirEntry* TrdDirEntry = cast(TRDDirEntry*)(dir.data+(pos&0xFF));
    memcpy(TrdDirEntry, hdr, 14);
    if (tosys) {
      TrdDirEntry.sec = 9;
      TrdDirEntry.trk = 0;
    } else {
      TrdDirEntry.sec = sec9.FirstFreeSec;
      TrdDirEntry.trk = sec9.FirstFreeTrk;
    }

    t.writeSector(1+pos/0x100, dir.data);

    //pos = sec9.FirstFreeSec+16*sec9.FirstFreeTrk;
    pos = TrdDirEntry.sec+16*TrdDirEntry.trk;
    sec9.FirstFreeSec = (pos+len)&0x0F;
    sec9.FirstFreeTrk = ((pos+len)>>4)&0xff;
    sec9.FileCnt++;
    sec9.FreeSecCnt -= len;
    t.writeSector(9, sec9hdr.data);

    // goto next track. s8 become invalid
    for (uint i = 0; i < len; i++, pos++) {
      t.seek(this, pos/32, (pos/16)&1, SeekMode.LoadSectors);
      if (!t.trkd) { conwriteln("TRD: no dir track"); return false; }
      if (!t.writeSector((pos&0x0F)+1, data+i*0x100)) { conwriteln("TRD: can't write sector"); return false; }
    }

    return true;
  }

  // destroys wdsnapbuf - use after loading all files
  void addboot () nothrow @trusted {
    static immutable ubyte[273] bootsmall = [
      0x62,0x6f,0x6f,0x74,0x20,0x20,0x20,0x20,0x42,0xfc,0x00,0xfc,0x00,0x00,0x01,0xd8,
      0x73,0x00,0x01,0xf8,0x00,0xe7,0xbc,0xa7,0x3a,0xf9,0xc0,0xb0,0x22,0x32,0x33,0x39,
      0x31,0x30,0x22,0x3a,0xea,0x3a,0xf7,0x22,0xe8,0x0f,0xe8,0xbf,0xc0,0xc3,0xbb,0xca,
      0xbd,0xc8,0xca,0xb6,0xbc,0xbb,0xe8,0xde,0xd6,0xd6,0xd9,0x22,0xfd,0x36,0x53,0x0e,
      0xcd,0x6b,0x0d,0x09,0x01,0x05,0x09,0xe5,0xcd,0x13,0x3d,0x06,0x13,0x21,0x52,0x5d,
      0x96,0xd7,0x23,0x10,0xfb,0x3e,0x02,0xcd,0x01,0x16,0xe1,0x54,0x5d,0x01,0x08,0x00,
      0x35,0x28,0x28,0x34,0x28,0x2a,0xe5,0x09,0x7e,0xe1,0xfe,0x42,0x20,0x1d,0xe5,0x3e,
      0x20,0xd7,0x7e,0xd7,0xed,0xa0,0xea,0x9c,0x5d,0x21,0xd1,0x70,0x34,0x7e,0xd6,0x03,
      0x28,0x05,0x30,0xfa,0x3e,0x20,0xd7,0x3e,0x20,0xd7,0xe1,0x0e,0x10,0x09,0x18,0xcd,
      0x26,0x57,0x7e,0x0f,0xb6,0x77,0x2b,0x7c,0xb7,0x20,0xf7,0x48,0x21,0xf5,0x57,0x97,
      0x11,0x0b,0x00,0x06,0x03,0x19,0xb9,0x28,0x06,0x3c,0x10,0xf9,0x2b,0x18,0xf4,0x06,
      0x0a,0xed,0x5b,0xd3,0x70,0x22,0xd3,0x70,0x3e,0x0e,0x12,0x13,0x36,0x22,0x23,0x10,
      0xf9,0x21,0x08,0x5c,0x70,0x7e,0xfe,0x0d,0x28,0x30,0xfe,0x20,0xca,0x66,0x5d,0xcb,
      0xef,0xfe,0x70,0x28,0x1b,0xfe,0x6f,0x28,0x0a,0xfe,0x61,0x28,0x11,0xfe,0x71,0x20,
      0xe4,0x0d,0x0d,0x0d,0xf2,0xc6,0x5d,0xed,0x4b,0xd1,0x70,0x0d,0x18,0xae,0x0c,0x0c,
      0x0c,0x3a,0xd1,0x70,0x3d,0x91,0x30,0xa4,0x18,0xa1,0x60,0x69,0x29,0x29,0x29,0x11,
      0x01,0x68,0x19,0x11,0x52,0x5d,0x0e,0x08,0xed,0xb0,0xc3,0x03,0x3d,0x80,0xaa,0x01,
      0x00,
      /* kempstonized
      0x62,0x6F,0x6F,0x74,0x20,0x20,0x20,0x20,0x42,0xFC,0x00,0xFC,0x00,0x00,0x01,0xD8,
      0x73,0x00,0x01,0xF8,0x00,0xE7,0xBC,0xA7,0x3A,0xF9,0xC0,0xB0,0x22,0x32,0x33,0x39,
      0x30,0x38,0x22,0x3A,0xEA,0x3A,0xF7,0x22,0x17,0x09,0x20,0x50,0x4F,0x4C,0x54,0x45,
      0x52,0x47,0x45,0x59,0x53,0x54,0x2B,0x4B,0x38,0x22,0xFD,0x36,0x53,0x0E,0xCD,0x6B,
      0x0D,0x09,0x01,0x05,0x09,0xE5,0xCD,0x13,0x3D,0x11,0x52,0x5D,0x0E,0x11,0xCD,0x3C,
      0x20,0x3E,0x02,0xCD,0x01,0x16,0xE1,0x54,0x5D,0x01,0x08,0x00,0x35,0x28,0x28,0x34,
      0x28,0x2A,0xE5,0x09,0x7E,0xE1,0xFE,0x42,0x20,0x1D,0xE5,0x3E,0x20,0xD7,0x7E,0xD7,
      0xED,0xA0,0xEA,0x98,0x5D,0x21,0xD1,0x70,0x34,0x7E,0xD6,0x03,0x28,0x05,0x30,0xFA,
      0x3E,0x20,0xD7,0x3E,0x20,0xD7,0xE1,0x0E,0x10,0x09,0x18,0xCD,0x26,0x57,0x7E,0x0F,
      0xB6,0x77,0x2B,0xCB,0x74,0x20,0xF7,0x48,0x21,0xF5,0x57,0xAF,0x11,0x0B,0x00,0x06,
      0x03,0x19,0xB9,0x28,0x06,0x3C,0x10,0xF9,0x2B,0x18,0xF4,0x06,0x0A,0xED,0x5B,0xD3,
      0x70,0x22,0xD3,0x70,0x3E,0x0E,0x12,0x13,0x36,0x22,0x23,0x10,0xF9,0x21,0x08,0x5C,
      0x70,0x06,0x05,0x76,0x10,0xFD,0x7E,0xFE,0x20,0xCA,0x64,0x5D,0xAF,0xDB,0x1F,0x1F,
      0x38,0x2E,0x1F,0x38,0x1E,0x1F,0x38,0x26,0x1F,0x38,0x16,0x1F,0x30,0xE3,0x60,0x69,
      0x29,0x29,0x29,0x11,0x01,0x68,0x19,0x11,0x52,0x5D,0x0E,0x08,0xED,0xB0,0xC3,0x03,
      0x3D,0x0D,0x0D,0x0D,0xF2,0xC2,0x5D,0xED,0x4B,0xD1,0x70,0x0D,0x18,0x9A,0x0C,0x0C,
      0x0C,0x3A,0xD1,0x70,0x3D,0x91,0x30,0x90,0x18,0x8D,0x00,0x00,0x00,0x80,0xAA,0x01,
      0x00,
      */
    ];

    conwriteln("addboot: checking disk...");
    if (!isTRD) { conwriteln("not a TR-DOS disk"); return; }

    t.seek(this, 0, 0, SeekMode.LoadSectors);

    SectorHeader* hdr = t.getSector(9);
    assert(hdr !is null);

    TRDSec9* sec9 = cast(TRDSec9*)hdr.data;
    assert(sec9 !is null);

    for (uint s = 0; s < 8; s++) {
      SectorHeader* sc = t.getSector(1+s);
      if (sc is null) return;
      TRDDirEntry* TrdDirEntry = cast(TRDDirEntry*)sc.data;
      for (uint i = 0; i < 16; i++) {
        import core.stdc.string : memcmp;
        if (memcmp(TrdDirEntry[i].name.ptr, "boot    B".ptr, 9) == 0) return;
      }
    }

    if (defaultBootHob.length) {
      conwriteln("addboot: adding boot...");
      wdsnapbuf[0..defaultBootHob.length] = defaultBootHob[];
      wdsnapsize = cast(uint)defaultBootHob.length;
    } else {
      conwriteln("addboot: adding built-in boot...");
      wdsnapbuf[0..bootsmall.length] = bootsmall[];
      wdsnapsize = cast(uint)bootsmall.length;
    }

    readHOB(Config.WD93.trdAddBootToSys);
  }

  bool readSCL () nothrow @trusted {
    uint size = 0, i;
    SCLHeader* SclHdr = cast(SCLHeader*)wdsnapbuf;
    for (i = 0; i < SclHdr.FileCnt; i++) size += SclHdr.Files.ptr[i].seccnt;
    conwriteln("SCL size: ", size, " sectors");
    if (size < 2544) size = 2544; else if (size > 2635) size = 2635;
    //if (size > 2544) size = 2544;
    //size = 2544; // so we'll have room for boot, for example
    emptyDiskTRD(size);
    ubyte* data = wdsnapbuf.ptr+SCLHeader.sizeof+SclHdr.FileCnt*TRDDirEntryBase.sizeof;
    for (i = 0; i < SclHdr.FileCnt; i++) {
      if (!addfile(cast(ubyte*)&SclHdr.Files.ptr[i], data)) {
        conwriteln("can't add file #", i, "...");
        return false;
      }
      data += SclHdr.Files.ptr[i].seccnt*0x100;
    }
    if (Config.WD93.trdAddBoot) addboot();
    return true;
  }

  bool readHOB (bool tosys=false) nothrow @trusted {
    if (rawdata is null) emptyDiskTRD(2544);
    wdsnapbuf[13] = wdsnapbuf[14]; // copy length
    return addfile(wdsnapbuf.ptr, wdsnapbuf.ptr+0x11, tosys);
  }

  bool readTRD () nothrow @trusted {
    uint CylCnt = wdsnapsize/(256*16*2)+(wdsnapsize%(256*16*2) ? 1 : 0);
    if (CylCnt > MAX_CYLS) {
      conwritefln!"cylinders (%d) > MAX_CYLS(%d)"(CylCnt, MAX_CYLS);
      return false;
    }
    formatTRD(CylCnt);
    for (uint i = 0; i < wdsnapsize; i += 0x100) {
      t.seek(this, i>>13, (i>>12)&1, SeekMode.LoadSectors);
      t.writeSector(((i>>8)&0x0F)+1, wdsnapbuf.ptr+i);
    }
    if (Config.WD93.trdAddBoot) addboot();
    return true;
  }

  void writeTRD (VFile fo) {
    static immutable ubyte[256] zerosec = 0;
    for (uint i = 0; i < cyls*sides*16; i++) {
      t.seek(this, i>>5, (i>>4)&1, SeekMode.LoadSectors);
      SectorHeader* hdr = t.getSector((i&0x0F)+1);
      const(ubyte)* ptr = zerosec.ptr;
      if (hdr && hdr.data) ptr = hdr.data;
      fo.rawWriteExact(ptr[0..256]);
    }
  }

  // will destroy wdsnapbuf
  void writeSCL (VFile fo) {
    if (!isTRD) { conwriteln("the disk doesn't look like TR-DOS one. T_T"); throw new Exception("SCL error"); }

    t.seek(this, 0, 0, SeekMode.LoadSectors);
    SectorHeader* sec9hdr = t.getSector(9);
    assert(sec9hdr !is null);

    TRDSec9* sec9 = cast(TRDSec9*)sec9hdr.data;
    assert(sec9 !is null);

    if (sec9.FileCnt > 128) { conwriteln("TRD: too many files"); throw new Exception("SCL error"); } // Каталог заполнен полностью

    uint wdsbpos = 0;

    void put (const(void)[] data) {
      import core.stdc.string : memcpy;
      if (wdsbpos+data.length > wdsnapbuf.length) { conwriteln("wtf?!"); throw new Exception("SCL error"); }
      memcpy(wdsnapbuf.ptr+wdsbpos, data.ptr, data.length);
      wdsbpos += cast(uint)data.length;
    }

    void putB (ubyte b) { put((&b)[0..1]); }
    //void putW (ushort w) { put(w&0xff); put(w>>8); }

    put("SINCLAIR");
    putB(0); // file count, will be fixed later

    // write headers
    foreach (immutable uint fidx; 0..sec9.FileCnt) {
      SectorHeader* dir = t.getSector(1+fidx/16);
      if (dir is null) throw new Exception("SCL error");
      auto th = (cast(TRDDirEntry*)dir.data)+fidx%16;
      if (th.name[0] == 0) continue;
      // increment file count
      ++wdsnapbuf[8];
      //conwriteln("files: ", wdsnapbuf[8], " (", fidx, ")");
      put((cast(ubyte*)th)[0..14]); // header
    }

    // write data
    foreach (immutable uint fidx; 0..sec9.FileCnt) {
      t.seek(this, 0, 0, SeekMode.LoadSectors);
      SectorHeader* dir = t.getSector(1+fidx/16);
      if (dir is null) throw new Exception("SCL error");
      auto th = (cast(TRDDirEntry*)dir.data)+fidx%16;
      if (th.name[0] == 0) continue;
      auto size = th.seccnt;
      ubyte sec = th.sec;
      ubyte trk = th.trk;
      //conwriteln("file idx: ", fidx, "; sector: ", th.sec, "; track: ", th.trk, "; size: ", size);
      while (size-- > 0) {
        t.seek(this, trk/2, trk&1, SeekMode.LoadSectors);
        dir = t.getSector(1+sec);
        if (dir is null) throw new Exception("SCL error");
        put((cast(ubyte*)dir.data)[0..256]);
        if (++sec == 16) { sec = 0; ++trk; }
      }
    }
    //conwriteln("done");

    // calculate checksum
    uint csum = 0;
    foreach (immutable ubyte b; wdsnapbuf[0..wdsbpos]) csum += b;
    putB(csum&0xff);
    putB((csum>>8)&0xff);
    putB((csum>>16)&0xff);
    putB((csum>>24)&0xff);

    fo.rawWriteExact(wdsnapbuf[0..wdsbpos]);
  }

  // //////////////////////////////////////////////////////////////////// //
  // UDI READER
  static align(1) struct UDI {
  align(1):
    char[3] label;
    ubyte cyls;
    ubyte sides;
  }

  // http://speccy.info/UDI
  bool readUDI () nothrow @trusted {
    if (wdsnapsize < 16) return false;
    if ((cast(const(char)[])wdsnapbuf)[0..4] != "UDI!") return false;
    if (wdsnapbuf.ptr[8] != 0) return false; // invalid version
    if (wdsnapbuf.ptr[9] == 0 || wdsnapbuf.ptr[9] > MAX_CYLS) return false;
    if (wdsnapbuf.ptr[10] > 2) return false; // sides
    //if (wdsnapbuf.ptr[11] != 0) return false; // reserved
    // extra data
    if (wdsnapbuf.ptr[12] != 0 || wdsnapbuf.ptr[13] != 0) return false;
    if (wdsnapbuf.ptr[14] != 0 || wdsnapbuf.ptr[15] != 0) return false;

    clear();
    uint c, s;
    uint mem = 0;
    const(ubyte)* ptr = wdsnapbuf.ptr+0x10;

    for (c = 0; c <= wdsnapbuf[9]; c++) {
      for (s = 0; s <= wdsnapbuf[10]; s++) {
        if (*ptr) return false;
        uint sz = *cast(ushort*)(ptr+1);
        sz += sz/8+(sz&7 ? 1 : 0);
        mem += sz;
        ptr += 3+sz;
        if (ptr > wdsnapbuf.ptr+wdsnapsize) return false;
      }
    }

    cyls = wdsnapbuf[9]+1;
    sides = wdsnapbuf[10]+1;

    if (cyls > MAX_CYLS) {
       conwritefln!"cylinders (%d) > MAX_CYLS(%d)"(cyls, MAX_CYLS);
       return false;
    }

    if (sides > 2) {
      conwritefln!"sides (%d) > 2"(sides);
      return false;
    }

    /*
    rawsize = align_by(mem, 4096);
    rawdata = (unsigned char*)VirtualAlloc(0, rawsize, MEM_COMMIT, PAGE_READWRITE);
    */
    allocRawData(mem);
    ptr = wdsnapbuf.ptr+0x10;
    ubyte* dst = rawdata;

    for (c = 0; c < cyls; c++) {
      for (s = 0; s < sides; s++) {
        import core.stdc.string : memcpy;
        uint sz = *cast(ushort*)(ptr+1);
        trklen[c][s] = sz;
        trkd[c][s] = dst;
        trki[c][s] = dst+sz;
        sz += sz/8+(sz&7 ? 1 : 0);
        memcpy(dst, ptr+3, sz);
        ptr += 3+sz;
        dst += sz;
      }
    }
    uint crc1 = *(cast(uint*)ptr);
    int crc2 = -1;
    udi_buggy_crc(crc2, wdsnapbuf.ptr, cast(uint)(ptr-wdsnapbuf.ptr));
    if (crc1 != crc2) {
      conwritefln!"udi crc mismatch: image=0x%08X, calc=0x%08X"(crc1, crc2);
    }

    if (wdsnapbuf[11]&1) {
      dsc[] = 0;
      foreach (ref char dch; dsc[]) {
        if (*ptr == 0) break;
        dch = cast(char)*ptr;
        ++ptr;
      }
    }

    if (Config.WD93.trdAddBoot) addboot();
    return true;
  }

  void writeUDI (VFile fo) {
    import core.stdc.string : memcpy, memset;

    wdsnapbuf[] = 0;
    wdsnapbuf[0] = 'U';
    wdsnapbuf[1] = 'D';
    wdsnapbuf[2] = 'I';
    wdsnapbuf[3] = '!';
    wdsnapbuf[8] = wdsnapbuf[11] = 0;
    wdsnapbuf[9] = cast(ubyte)(cyls-1);
    wdsnapbuf[10] = cast(ubyte)(sides-1);
    //*(unsigned*)(wdsnapbuf+12) = 0;

    ubyte* dst = wdsnapbuf.ptr+0x10;
    for (uint c = 0; c < cyls; c++) {
      for (uint s = 0; s < sides; s++) {
        *dst++ = 0;
        uint len = trklen[c][s];
        *cast(ushort*)dst = cast(ushort)len;
        dst += 2;
        memcpy(dst, trkd[c][s], len);
        dst += len;
        len = (len+7)/8;
        memcpy(dst, trki[c][s], len);
        dst += len;
      }
    }
    if (dscstr.length) {
      //strcpy((char*)dst, dsc), dst += strlen(dsc)+1, wdsnapbuf[11] = 1;
      memcpy(dst, dscstr.ptr, dscstr.length);
      dst += dscstr.length;
      *dst++ = 0;
      wdsnapbuf[11] = 1;
    }
    *cast(uint*)(wdsnapbuf.ptr+4) = cast(uint)(dst-wdsnapbuf.ptr);
    int crc = -1;
    udi_buggy_crc(crc, wdsnapbuf.ptr, cast(uint)(dst-wdsnapbuf.ptr));
    *cast(uint*)dst = crc;
    dst += 4;
    fo.rawWriteExact(wdsnapbuf[0..cast(uint)(dst-wdsnapbuf.ptr)]);
  }

  // //////////////////////////////////////////////////////////////////// //
  // ISD READER
  void formatISD () nothrow @trusted {
    static immutable ubyte[5] sn = [ 1, 2, 3, 4, 9 ];
    newDisk(80, 2);
    for (uint c = 0; c < cyls; c++) {
      for (uint h = 0; h < 2; h++) {
        t.seek(this, c, h, SeekMode.SeekOnly);
        t.s = 5;
        for (uint s = 0; s < 5; s++) {
          uint n = sn[s];
          t.hdr[s].n = cast(ubyte)n;
          t.hdr[s].l = 3;
          t.hdr[s].c = cast(ubyte)c;
          t.hdr[s].s = 0;
          t.hdr[s].c1 = t.hdr[s].c2 = 0;
          t.hdr[s].data = cast(ubyte*)1;
        }
        t.format();
      }
    }
  }

  bool readISD () nothrow @trusted {
    static immutable ubyte[5] sn = [ 1, 2, 3, 4, 9 ];
    formatISD();
    for (uint c = 0; c < cyls; c++) {
      for (uint h = 0; h < 2; h++) {
        for (uint s = 0; s < 5; s++) {
          t.seek(this, c, h, SeekMode.LoadSectors);
          t.writeSector(sn[s], wdsnapbuf.ptr+(c*10+h*5+s)*1024);
        }
      }
    }
    return true;
  }

  void writeISD (VFile fo) {
    for (uint c = 0; c < 80; c++) {
      for (uint h = 0; h < 2; h++) {
        t.seek(this, c, h, SeekMode.LoadSectors);
        for (uint s = 0; s < 5; s++) {
          fo.rawWriteExact(t.hdr[s].data[0..1024]);
        }
      }
    }
  }

  // //////////////////////////////////////////////////////////////////// //
  // FDI
  align(1) static struct TFdiSecHdr {
  align(1):
    enum {
      FL_DELETED_DATA = 0x80,
      FL_NO_DATA = 0x40,
      FL_GOOD_CRC_4096 = 0x20,
      FL_GOOD_CRC_2048 = 0x10,
      FL_GOOD_CRC_1024 = 0x8,
      FL_GOOD_CRC_512 = 0x4,
      FL_GOOD_CRC_256 = 0x2,
      FL_GOOD_CRC_128 = 0x1,
    }

    ubyte c;
    ubyte h;
    ubyte r;
    ubyte n;
    // flags:
    // bit 7 - 1 = deleted data (F8) / 0 = normal data (FB)
    // bit 6 - 1 - sector with no data
    // bits 0..5 - 1 = good crc for sector size (128, 256, 512, 1024, 2048, 4096)
    ubyte fl;
    ushort DataOffset;
  }

  align(1) static struct TFdiTrkHdr {
  align(1):
    uint TrkOffset;
    ushort Res1;
    ubyte  Spt;
    TFdiSecHdr[0] Sec;
  }

  align(1) static struct TFdiHdr {
  align(1):
    char[3] Sig;
    ubyte Rw;
    ushort c;
    ushort h;
    ushort TextOffset;
    ushort DataOffset;
    ushort AddLen;
    ubyte[0] AddData; // AddLen -> TFdiAddInfo
    //TFdiTrkHdr Trk[c*h];
  }

  align(1) static struct TFdiAddInfo {
  align(1):
    enum BAD_BYTES = 1;
    enum FDI_2 = 2;
    ushort Ver; // 2 - FDI 2
    ushort AddInfoType; // 1 - bad bytes info
    uint TrkAddInfoOffset; // -> TFdiTrkAddInfo
    uint DataOffset;
  }

  align(1) static struct TFdiSecAddInfo {
  align(1):
    ubyte Flags; // 1 - Массив сбойных байтов присутствует
    // Смещение битового массива сбойных байтов внутри трэка
    // Число битов определяется размером сектора
    // Один бит соответствует одному сбойному байту
    ushort DataOffset;
  }

  align(1) static struct TFdiTrkAddInfo {
  align(1):
    uint TrkOffset; // Смещение массива сбойных байтов для трэка относительно TFdiAddInfo->DataOffset,
                    // 0xFFFFFFFF - Массив описателей параметров секторов отсутствует
    TFdiSecAddInfo[0] Sec; // Spt
  }

  // http://speccy.info/FDI
  bool readFDI () nothrow @trusted {
    if (wdsnapsize < 16) return false;
    if ((cast(const(char)[])wdsnapbuf)[0..3] != "FDI") return false;
    if (wdsnapbuf.ptr[5] != 0 || wdsnapbuf.ptr[4] == 0 || wdsnapbuf.ptr[4] > MAX_CYLS) return false;
    if (wdsnapbuf.ptr[7] != 0 || wdsnapbuf.ptr[6] < 1 || wdsnapbuf.ptr[6] > 2) return false; // heads

    const(TFdiHdr)* FdiHdr = cast(const(TFdiHdr)*)wdsnapbuf.ptr;
    uint cyls = FdiHdr.c;
    uint sides = FdiHdr.h;
    uint AddLen = FdiHdr.AddLen;

    if (cyls > MAX_CYLS) {
      conwritefln!"cylinders (%d) > MAX_CYLS(%d)"(cyls, MAX_CYLS);
      return false;
    }

    if (sides > 2) {
      conwritefln!"sides (%d) > 2"(sides);
      return false;
    }

    newDisk(cyls, sides);

    const(TFdiAddInfo)* FdiAddInfo = null;
    const(TFdiTrkAddInfo)* FdiTrkAddInfo = null;
    if (AddLen >= TFdiAddInfo.sizeof) {
      // Проверить параметры FdiAddInfo (версию, тип и т.д.)
      FdiAddInfo = cast(const(TFdiAddInfo)*)FdiHdr.AddData.ptr;
      if (FdiAddInfo.Ver >= TFdiAddInfo.FDI_2 && FdiAddInfo.AddInfoType == TFdiAddInfo.BAD_BYTES) {
        FdiTrkAddInfo = cast(const(TFdiTrkAddInfo)*)(wdsnapbuf.ptr+FdiAddInfo.TrkAddInfoOffset);
      }
    }

    //strncpy(dsc, (const char *)&wdsnapbuf[FdiHdr.TextOffset], sizeof(dsc));
    //dsc[sizeof(dsc) - 1] = 0;
    dsc[] = 0;
    foreach (immutable idx, ref char dch; dsc[]) {
      if (wdsnapbuf[FdiHdr.TextOffset+idx] == 0) break;
      dch = cast(char)(wdsnapbuf[FdiHdr.TextOffset+idx]);
    }

    const(TFdiTrkHdr)* FdiTrkHdr = cast(const(TFdiTrkHdr)*)&wdsnapbuf[TFdiHdr.sizeof+FdiHdr.AddLen];
    ubyte* dat = wdsnapbuf.ptr+FdiHdr.DataOffset;

    for (uint c = 0; c < cyls; c++) {
      for (uint s = 0; s < sides; s++) {
        t.seek(this, c,s, SeekMode.SeekOnly);

        ubyte *t0 = dat+FdiTrkHdr.TrkOffset;
        uint ns = FdiTrkHdr.Spt;
        ubyte *wp0 = null;

        if (FdiTrkAddInfo && FdiTrkAddInfo.TrkOffset != uint.max) {
          wp0 = wdsnapbuf.ptr+FdiAddInfo.DataOffset+FdiTrkAddInfo.TrkOffset;
          if (wp0 > wdsnapbuf.ptr+wdsnapsize) {
            conwriteln("bad bytes data is beyond disk image end");
            return false;
          }
        }

        for (uint sec = 0; sec < ns; sec++) {
          t.hdr[sec].c = FdiTrkHdr.Sec.ptr[sec].c;
          t.hdr[sec].s = FdiTrkHdr.Sec.ptr[sec].h;
          t.hdr[sec].n = FdiTrkHdr.Sec.ptr[sec].r;
          t.hdr[sec].l = FdiTrkHdr.Sec.ptr[sec].n;
          t.hdr[sec].c1 = 0;
          t.hdr[sec].wp = null;

          if (FdiTrkHdr.Sec.ptr[sec].fl&TFdiSecHdr.FL_NO_DATA) {
            t.hdr[sec].data = null;
          } else {
             if (t0+FdiTrkHdr.Sec.ptr[sec].DataOffset > wdsnapbuf.ptr+wdsnapsize) {
               conwriteln("sector data is beyond disk image end");
               return false;
             }
             t.hdr[sec].data = t0+FdiTrkHdr.Sec.ptr[sec].DataOffset;

             if (FdiTrkAddInfo && FdiTrkAddInfo.TrkOffset != uint.max) {
                 t.hdr[sec].wp = (FdiTrkAddInfo.Sec.ptr[sec].Flags&1 ? (wp0+FdiTrkAddInfo.Sec.ptr[sec].DataOffset) : null);
             }
             /+
             #if 0
             if(FdiTrkHdr.Sec.ptr[sec].n > 3)
             {
                 ubyte buf[1+1024];
                 buf[0] = (FdiTrkHdr.Sec.ptr[sec].fl&TFdiSecHdr::FL_DELETED_DATA ? 0xF8 : 0xFB);
                 memcpy(buf+1, t.hdr[sec].data, 128U<<(FdiTrkHdr.Sec.ptr[sec].n&3));

                 ushort crc_calc = wd93_crc(buf, (128U<<(FdiTrkHdr.Sec.ptr[sec].n&3))+1);
                 ushort crc_from_hdr = *(ushort*)(t.hdr[sec].data+(128U<<(FdiTrkHdr.Sec.ptr[sec].n&3)));
                 printf("phys: c=%-2u, h=%u, s=%u|hdr: c=0x%02X, h=0x%02X, r=0x%02X, n=%02X(%u)|crc1=0x%04X, crc2=0x%04X\n",
                     c, s, sec,
                     FdiTrkHdr.Sec.ptr[sec].c, FdiTrkHdr.Sec.ptr[sec].h, FdiTrkHdr.Sec.ptr[sec].r, FdiTrkHdr.Sec.ptr[sec].n, (FdiTrkHdr.Sec.ptr[sec].n&3),
                     crc_calc, crc_from_hdr);

                 if(crc_calc == crc_from_hdr)
                 {
                     TFdiTrkHdr *FdiTrkHdrRW = const_cast<TFdiTrkHdr *>(FdiTrkHdr);
                     FdiTrkHdrRW.Sec.ptr[sec].fl |= (1<<(FdiTrkHdr.Sec.ptr[sec].n&3));

                 }
             }
             #endif
             +/
             t.hdr[sec].c2 = (FdiTrkHdr.Sec.ptr[sec].fl&(1<<(FdiTrkHdr.Sec.ptr[sec].n&3)) ? 0 : 2); // [vv]
          }
        } // sec
        t.s = ns;
        t.format();

        if (FdiTrkAddInfo) {
          FdiTrkAddInfo = cast(const(TFdiTrkAddInfo)*)((cast(const(ubyte)*)FdiTrkAddInfo)+TFdiTrkAddInfo.sizeof+
              (FdiTrkAddInfo.TrkOffset != uint.max ? FdiTrkHdr.Spt*TFdiSecAddInfo.sizeof : 0));
         }

         FdiTrkHdr = cast(const(TFdiTrkHdr)*)((cast(const(ubyte)*)FdiTrkHdr)+TFdiTrkHdr.sizeof+FdiTrkHdr.Spt*TFdiSecHdr.sizeof);
      } // s
    }
    if (Config.WD93.trdAddBoot) addboot();
    return true;
  }

  void writeFDI (VFile fo) {
    auto fspos = fo.tell;

    uint b, c, s, se, total_s = 0;
    uint sectors_wp = 0; // Общее число секторов для которых пишутся заголовки с дополнительной информацией
    uint total_size = 0; // Общий размер данных занимаемый секторами

    // Подсчет общего числа секторов на диске
    for (c = 0; c < cyls; c++) {
      for (s = 0; s < sides; s++) {
        t.seek(this, c, s, SeekMode.LoadSectors);
        for (se = 0; se < t.s; se++) {
          total_size += (t.hdr[se].data ? t.hdr[se].datlen : 0);
        }
        for (se = 0; se < t.s; se++) {
          if (t.hdr[se].wp_start) {
            sectors_wp += t.s;
            break;
          }
        }
        total_s += t.s;
      }
    }

    uint AddLen = (sectors_wp ? cast(uint)TFdiAddInfo.sizeof : 0);
    uint tlen = cast(uint)dscstr.length+1;
    uint hsize = cast(uint)(TFdiHdr.sizeof+AddLen+cyls*sides*TFdiTrkHdr.sizeof+total_s*TFdiSecHdr.sizeof);
    uint AddHdrsSize = cast(uint)(cyls*sides*TFdiTrkAddInfo.sizeof+sectors_wp*TFdiSecAddInfo.sizeof);

    // Формирование FDI заголовка
    TFdiHdr* FdiHdr = cast(TFdiHdr*)wdsnapbuf;
    FdiHdr.Sig = "FDI";
    FdiHdr.Rw = 0;
    FdiHdr.c = cast(ushort)cyls;
    FdiHdr.h = cast(ushort)sides;
    FdiHdr.TextOffset = cast(ushort)hsize;
    FdiHdr.DataOffset = cast(ushort)(FdiHdr.TextOffset+tlen);
    FdiHdr.AddLen = cast(ushort)AddLen;

    TFdiAddInfo* FdiAddInfo = cast(TFdiAddInfo*)FdiHdr.AddData.ptr;
    if (AddLen) {
      FdiAddInfo.Ver = TFdiAddInfo.FDI_2; // FDI ver 2
      FdiAddInfo.AddInfoType = TFdiAddInfo.BAD_BYTES; // Информация о сбойных байтах
      FdiAddInfo.TrkAddInfoOffset = FdiHdr.DataOffset+total_size;
      FdiAddInfo.DataOffset = FdiAddInfo.TrkAddInfoOffset+AddHdrsSize;
    }

    // Запись FDI заголовка с дополнительными данными
    fo.rawWriteExact((cast(ubyte*)FdiHdr)[0..TFdiHdr.sizeof+AddLen]);

    uint trkoffs = 0;
    for (c = 0; c < cyls; c++) {
      for (s = 0; s < sides; s++) {
        t.seek(this, c, s, SeekMode.LoadSectors);

        // Формирование заголовка трэка
        TFdiTrkHdr FdiTrkHdr;
        FdiTrkHdr.TrkOffset = trkoffs;
        FdiTrkHdr.Res1 = 0;
        FdiTrkHdr.Spt = cast(ubyte)t.s;

        // Запись заголовка трэка
        fo.rawWriteExact((cast(ubyte*)&FdiTrkHdr)[0..FdiTrkHdr.sizeof]);

        uint secoffs = 0;
        for (se = 0; se < t.s; se++) {
          // Формирование заголовка сектора
          TFdiSecHdr FdiSecHdr;
          FdiSecHdr.c = t.hdr[se].c;
          FdiSecHdr.h = t.hdr[se].s;
          FdiSecHdr.r = t.hdr[se].n;
          FdiSecHdr.n = t.hdr[se].l;
          FdiSecHdr.fl = 0;

          if (t.hdr[se].data) {
            if (t.hdr[se].data[-1] == 0xF8)
              FdiSecHdr.fl |= TFdiSecHdr.FL_DELETED_DATA;
            else
              FdiSecHdr.fl |= (t.hdr[se].c2 ? (1<<(t.hdr[se].l&3)) : 0); // [vv]
          } else {
            FdiSecHdr.fl |= TFdiSecHdr.FL_NO_DATA;
          }

          FdiSecHdr.DataOffset = cast(ushort)secoffs;

          // Запись заголовка сектора
          fo.rawWriteExact((cast(ubyte*)&FdiSecHdr)[0..FdiSecHdr.sizeof]);
          secoffs += t.hdr[se].datlen;
        }
        trkoffs += secoffs;
      }
    }

    // Запись комментария
    auto cfpos = fo.tell;
    if (cfpos > fspos+FdiHdr.TextOffset) assert(0, "FDI writer internal error");
    while (cfpos < fspos+FdiHdr.TextOffset) {
      fo.writeNum!ubyte(0);
      ++cfpos;
    }
    //fo.seek(fspos+FdiHdr.TextOffset);
    if (tlen > 1) fo.rawWriteExact(dsc[0..tlen]);
    fo.writeNum!ubyte(0);

    // Запись зон данных трэков
    for (c = 0; c < cyls; c++) {
      for (s = 0; s < sides; s++) {
        t.seek(this, c, s, SeekMode.LoadSectors);
        for (uint sex = 0; sex < t.s; sex++) {
          if (t.hdr[sex].data) {
            fo.rawWriteExact((cast(ubyte*)t.hdr[sex].data)[0..t.hdr[sex].datlen]);
          }
        }
      }
    }

    // Запись дополниетльной информации (информации о сбойных байтах)
    if (AddLen) {
      trkoffs = 0;
      for (c = 0; c < cyls; c++) {
        for (s = 0; s < sides; s++) {
          t.seek(this, c, s, SeekMode.LoadSectors);

          // Формирование заголовка трэка
          TFdiTrkAddInfo FdiTrkAddInfo;
          FdiTrkAddInfo.TrkOffset = uint.max;
          for (b = 0; b < t.trklen; b++) {
            if (t.getWriteProtected(b)) {
              FdiTrkAddInfo.TrkOffset = trkoffs;
              break;
            }
          }

          // Запись заголовка трэка
          fo.rawWriteExact((cast(ubyte*)&FdiTrkAddInfo)[0..FdiTrkAddInfo.sizeof]);

          uint secoffs = 0;
          if (FdiTrkAddInfo.TrkOffset != uint.max) {
            for (se = 0; se < t.s; se++) {
              // Формирование заголовка сектора
              TFdiSecAddInfo FdiSecAddInfo;
              FdiSecAddInfo.Flags = 0;
              FdiSecAddInfo.DataOffset = 0;

              if (t.hdr[se].wp_start) {
                FdiSecAddInfo.Flags |= 1;
                FdiSecAddInfo.DataOffset = cast(ushort)secoffs;
              }

              // Запись заголовка сектора
              fo.rawWriteExact((cast(ubyte*)&FdiSecAddInfo)[0..FdiSecAddInfo.sizeof]);
              secoffs += (t.hdr[se].wp_start ? ((t.hdr[se].datlen+7)>>3) : 0);
            }
          }
          trkoffs += secoffs;
        }
      }

      // Запись зон сбойных байтов
      for (c = 0; c < cyls; c++) {
        for (s = 0; s < sides; s++) {
          t.seek(this, c, s, SeekMode.LoadSectors);
          for (uint sex = 0; sex < t.s; sex++) {
            if (t.hdr[sex].wp_start) {
              uint nbits = t.hdr[sex].datlen;
              ubyte[1024U>>3U] wp_bits = 0;
              for (b = 0; b < nbits; b++) {
                if (t.getWriteProtected(t.hdr[sex].wp_start+b)) set_bit(wp_bits.ptr, b);
              }
              fo.rawWriteExact(wp_bits[0..(nbits+7)>>3]);
            }
          }
        }
      }
    }
  }

  // //////////////////////////////////////////////////////////////////// //
  // `wdsnapbuf` and `wdsnapsize` should be set
  bool readDiskImage () nothrow @trusted {
    if (wdsnapsize < 16) return false; // alas
    // .scl?
    if ((cast(const(char)[])wdsnapbuf)[0..8] == "SINCLAIR") return readSCL();
    if ((cast(const(char)[])wdsnapbuf)[0..4] == "UDI!") return readUDI();
    if ((cast(const(char)[])wdsnapbuf)[0..3] == "FDI") return readFDI();
    if (wdsnapsize < 256*16) return false; // simple sanity check
    // assume .trd
    return readTRD();
  }
}
