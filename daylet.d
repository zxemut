/* daylet -- .AY music file player
 * Copyright (C) 2001 Russell Marks and Ian Collier
 * Copyright (C) 2012-2017 Ketmar // Invisible Vector
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
module daylet;

import core.time : MonoTime;

import iv.alsa;
import iv.cmdcon;
import iv.cmdcontty;
import iv.rawtty;
import iv.strex;
import iv.vfs;

import zxinfo;
import emuconfig;
import sound;

import modplay;


// ////////////////////////////////////////////////////////////////////////// //
struct TimeTag {
  int min;
  int sec;
  int subsecframes;
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared ZXModule[] aymodules;
__gshared TimeTag tunetime;
__gshared int defaultStopAfter = 3*60*50; /* in intrs */
__gshared int stopafter = 3*60*50; /* in intrs */
__gshared int fadetime = 10*50;  /* fadeout time *after* that in intr, 0=none */
//__gshared bool doneFade = false;
__gshared bool paused = false;
__gshared bool playOneTrackOnly = false;
__gshared int curTrackIdx = 0;

//enum FRAME_STATES_48 = 69888; /*(3500000/50)*/
//enum FRAME_STATES_128 = 70908; /*(3546900/50)*/
//enum FRAME_STATES_CPC = 4000000/50;


// ////////////////////////////////////////////////////////////////////////// //
void tunetimeReset () {
  tunetime.min = tunetime.sec = tunetime.subsecframes = 0;
  //doneFade = false;
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared uint silentFrames = 0;
__gshared uint silentMax = cast(uint)(3*50); // max frames of silence before skipping
__gshared int skipIntrs = 0;


// returns:
//  <0: go to next file
//  >0: moved to next track
//   0: continue with this track
int sendSoundFrame () {
  if (paused && skipIntrs < 1) {
    if (sndvSkipSynth) {
      sndvSkipSynth = false;
      soundForceReinit(false); // don't reset AY
    }
    soundBlankFrame();
  } else {
    // check for fade needed
    /+
    if (!done_fade && stopafter && (tunetime.min*60+tunetime.sec)*50+tunetime.subsecframes >= stopafter) {
      done_fade = 1;
      if (fadetime) sound_start_fade(fadetime/50);
    }
    +/
    if (skipIntrs > 0) {
      --skipIntrs;
      sndvSkipSynth = (skipIntrs > 0);
      if (!sndvSkipSynth) soundForceReinit(false); // don't reset AY
    }
    // incr time
    if (++tunetime.subsecframes >= 50) {
      tunetime.subsecframes = 0;
      if (++tunetime.sec >= 60) {
        tunetime.sec = 0;
        ++tunetime.min;
      }
    }
    // play frame, and stop if it's been silent for a while
    if (soundFrame!true()) {
      ++silentFrames;
      //conwriteln("silent frames: ", silent_for);
    } else {
      silentFrames = 0;
    }
    if ((silentMax > 0 && silentFrames >= silentMax) || (tunetime.min*60+tunetime.sec)*50+tunetime.subsecframes >= stopafter+fadetime) {
      silentFrames = 0;
      // do next track, or file, or just stop
      ++curTrackIdx;
      if (playOneTrackOnly) return -1;
      if (curTrackIdx >= aymodules.length) return -1;
      return 1;
    }
  }
  return 0;
}


const(char)[] safeChars (const(char)[] s) {
  foreach (char ch; s) {
    if (ch < ' ' || ch == 127) {
      auto res = new char[](s.length);
      foreach (immutable idx, char cc; s) {
        if (cc < ' ' || cc == 127) cc = '.';
        res[idx] = cc;
      }
      return res;
    }
  }
  return s;
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared string[] ayplaylist;


enum Action { None, Quit, Prev, Next }

Action ayPlayFile (VFile fl, bool lastTrack, bool ttyGoUp) {
  Action ares = Action.None;

  aymodules = null;

  ubyte[] moddata;
  scope(exit) delete moddata;

  auto fsz = fl.size;
  if (fsz < 1 || fsz > 4*1024*1024) throw new Exception("invalid file size");
  moddata.length = cast(uint)fsz;
  fl.seek(0);
  fl.rawReadExact(moddata);
  aymodules = zxModuleDetect(moddata);
  if (aymodules.length == 0) throw new Exception("not a module");

  if (ttyGoUp) {
    conwrite("\r");
    foreach (immutable _; 0..6) conwrite("\x1b[A\x1b[K");
    ttyGoUp = false;
  }

  conwriteln("*** ", fl.name, " ***");
  conwriteln("tracks: ", aymodules.length);

  curTrackIdx = (lastTrack ? cast(int)aymodules.length-1 : 0);

  int oldtime = -1;
  bool oldpause = !paused;
  MonoTime pauseTime;
  bool pauseStars = false;

  void drawTime(bool force=false) () {
    import core.stdc.stdio : snprintf;
    int newtime = tunetime.min*60+tunetime.sec;
    static if (!force) {
      if (skipIntrs > 0 && !paused) return;
      if (newtime == oldtime && oldpause == paused) {
        if (!paused) return;
        if (!oldpause) {
          // just paused
          pauseTime = MonoTime.currTime;
          pauseStars = false;
          oldpause = paused;
        } else {
          auto curtm = MonoTime.currTime;
          if ((curtm-pauseTime).total!"msecs" < 1000) return;
          pauseTime = curtm;
          pauseStars = !pauseStars;
        }
      }
    }
    if (oldpause != paused) pauseStars = false;
    char[128] xbuf;
    oldtime = newtime;
    oldpause = paused;
    auto len = snprintf(xbuf.ptr, xbuf.length, "\r%u:%02u / ", cast(uint)(newtime/60), cast(uint)(newtime%60));
    ttyRawWrite(xbuf[0..len]);
    auto sta = stopafter/50;
    if (!stopafter) {
      ttyRawWrite("--:--");
    } else {
      len = snprintf(xbuf.ptr, xbuf.length, "%d:%02d (%2d)", cast(uint)(sta/60), cast(uint)(sta%60), cast(uint)(fadetime/50/60));
      ttyRawWrite(xbuf[0..len]);
    }
    if (paused) ttyRawWrite("!"); else ttyRawWrite(" ");
    int tw = ttyWidth;
    if (!sta || tw-58-2 < 2) return;
    if (newtime > sta) newtime = sta;
    xbuf[] = (pauseStars ? '*' : '=');
    //xbuf[] = '=';
    xbuf[0] = '[';
    xbuf[57] = ']';
    xbuf[1+(56*newtime/sta)] = '|';
    ttyRawWrite(xbuf[0..58]);
    {
      len = snprintf(xbuf.ptr, xbuf.length, "%4d", cast(uint)Config.volumeAY);
      ttyRawWrite(xbuf[0..len]);
    }
  }

  bool processKeys () {
    while (ttyIsKeyHit) {
      auto key = ttyReadKey(0, 20);
      if (!ttyconEvent(key)) {
        switch (key.key) {
          case TtyEvent.Key.Char:
            if (key.ch == '<') { if (curTrackIdx > 0) --curTrackIdx; else ares = Action.Prev; return true; }
            if (key.ch == '>') { ++curTrackIdx; return true; }
            if (key.ch == 'q') { ares = Action.Quit; return true; }
            if (key.ch == '-') {
              if (Config.volumeAY < 10) Config.volumeAY = 0; else Config.volumeAY -= 10;
              drawTime!true();
              soundReinit();
              break;
            }
            if (key.ch == '+') {
              if (Config.volumeAY > 390) Config.volumeAY = 400; else Config.volumeAY += 10;
              drawTime!true();
              soundReinit();
              break;
            }
            if (key.ch == ' ') paused = !paused;
            if (key.ch == '?') {
              ttyRawWrite(
                "\r\x1b[4A=== HELP ===\x1b[K\n"~
                "  <: previous   -: AY volume down\x1b[K\n"~
                "  >: next       +: AY volume up\x1b[K\n"~
                "  q: quit\x1b[K\n"~
                "  space: pause\x1b[K\n"
              );
              drawTime!true();
              break;
            }
            break;
          case TtyEvent.Key.Right:
            skipIntrs += 50*5;
            break;
          case TtyEvent.Key.Left:
            int curtime = (tunetime.min*60+tunetime.sec)*50;
            soundReinit();
            aymodules[curTrackIdx].reset();
            tunetimeReset();
            if ((curtime -= 50*5) < 0) curtime = 0;
            skipIntrs = curtime;
            break;
          default: break;
        }
      }
    }
    return false;
  }

  ttyGoUp = false;
  trackloop: while (ares == Action.None) {
    if (curTrackIdx >= aymodules.length) break;
    auto mod = aymodules[curTrackIdx];

    sndvSkipSynth = false;
    skipIntrs = 0;
    Config.machine = zxFindMachine(mod.modelName);
    soundReinit();

    if (ttyGoUp) {
      conwrite("\r\x1b[K");
      foreach (immutable _; 0..3) conwrite("\x1b[A\x1b[K");
      ttyGoUp = false;
    }
    ttyGoUp = true;

    conwriteln("track : [", curTrackIdx+1, "/", aymodules.length, "] ", mod.name.safeChars, " (", mod.typeStr, ")");
    conwriteln("author: ", mod.author.safeChars);
    conwriteln("misc  : ", mod.misc.safeChars);

    //if (oldtime >= 0) { ttyRawWrite("\r\n"); oldtime = -1; }

    silentFrames = 0;
    soundResetAY();
    tunetimeReset();
    mod.reset();
    stopafter = mod.intrCount;
    fadetime = mod.fadeCount;
    if (stopafter == 0) {
      stopafter = defaultStopAfter;
      fadetime = 50; //10*50;
    }
    //if (fadetime < 50) fadetime = 50;

    drawTime!true();

    // play track
    for (;;) {
      if (!paused || skipIntrs > 0) {
        if (!mod.doIntr()) continue trackloop; // moved to next track
        auto ff = sendSoundFrame();
        if (ff < 0) { ares = Action.Next; break trackloop; }  // go to next file
        if (ff > 0) continue trackloop; // moved to next track
      } else {
        // paused
        soundBlankFrame();
      }
      {
        auto conoldcdump = conDump;
        scope(exit) conDump = conoldcdump;
        conDump = ConDump.none;
        conProcessQueue();
      }
      drawTime();
      if (processKeys()) break;
      ttyconDraw();
      if (isQuitRequested) ares = Action.Quit;
      if (ares != Action.None) break trackloop;
    }
  }

  if (oldtime >= 0) ttyRawWrite("\r\n");
  if (ares == Action.None) ares = Action.Next;
  return ares;
}


// ////////////////////////////////////////////////////////////////////////// //
void main (string[] args) {
  if (ttyIsRedirected) assert(0, "no redirects, please!");

  fuck_alsa_messages();

  conRegUserVar!bool("calc_total", "calculate total time");
  conRegUserVar!bool("dump_bad", "dump bad modules to stderr when calculating totals");

  conRegUserVar!bool("shuffle", "shuffle playlist");
  conRegUserVar!string("dbg_default_ay", "debug: default AY file");

  //conRegVar!zxmodel("zx_model", "zx spectrum model");
  conRegVar!silentMax("silent_max_frames", "max frames of silence before skipping track");

  conRegVar!(Config.volumeBeeper)(0, 400, "volume_beeper", "beeper volume");
  conRegVar!(Config.volumeAY)(0, 400, "volume_ay", "ay volume");
  conRegVar!(Config.stereoType)("stereo_mode", "AY stereo mode: ABC, ACB, NONE");
  conRegVar!(Config.speakerType)("speaker_type", "speaker type: TV, Beeper, Default, Flat, Crisp");

  concmd("exec daylet.rc tan");
  conProcessQueue(256*1024); // load config
  conProcessArgs!true(args);

  // process all console commands
  foreach (immutable _; 0..42) if (!conProcessQueue()) break;

  //Config.stereoType = Config.Stereo.BCA;

  if (args.length == 1) {
    string day = conGetVar!string("dbg_default_ay");
    if (day.length) args ~= day;
  }

  foreach (string fname; args[1..$]) {
    if (fname.length == 0) continue;
    if (fname.length > 1 && fname[0..2] == "!/") fname = exeDir~fname[1..$];
    if (fname.endsWithCI(".zip")) {
      auto drv = vfsAddPak(fname);
      auto aname = fname;
      fname = null;
      foreach (const ref de; vfsFileList()) {
        if (de.name.zxModuleGoodExtension) {
          ayplaylist ~= aname~":"~de.name;
        }
      }
      vfsRemovePak(drv);
    } else if (fname.zxModuleGoodExtension) {
      ayplaylist ~= fname;
    }
  }

  if (ayplaylist.length < 1) assert(0, "no files!");
  if (ayplaylist.length > 1024*1024*32) assert(0, "too many files");

  if (ayplaylist.length > 1) conwriteln(ayplaylist.length, " modules found");

  if (conGetVar!bool("calc_total")) {
    ubyte[] moddata;
    scope(exit) delete moddata;
    long total = 0;

    static struct ModInfo {
      string name;
      uint intrs;
    }
    ModInfo[string] modcache;

    try {
      auto fl = VFile(".zxmodules.cache");
      for (;;) {
        auto nlen = fl.readNum!uint;
        if (nlen == 0) break;
        auto name = new char[](nlen);
        fl.rawReadExact(name);
        ModInfo mi;
        mi.name = cast(string)name; // it is safe to cast here
        mi.intrs = fl.readNum!uint;
        modcache[mi.name] = mi;
      }
    } catch (Exception e) {
    }

    for (int idx = 0; idx < ayplaylist.length; ++idx) {
      try {
        if (auto mip = ayplaylist[idx] in modcache) {
          total += cast(long)mip.intrs;
        } else {
          auto fl = VFile(ayplaylist[idx]);
          auto fsz = fl.size;
          if (fsz < 1 || fsz > 4*1024*1024) throw new Exception("invalid file size");
          moddata.assumeSafeAppend;
          moddata.length = cast(uint)fsz;
          fl.seek(0);
          fl.rawReadExact(moddata);
          auto aym = zxModuleDetect!true(moddata);
          if (aym.length == 0) throw new Exception("not a module");
          ModInfo mi;
          mi.name = ayplaylist[idx];
          foreach (ZXModule mod; aym) {
            mi.intrs += mod.intrCount+mod.fadeCount;
            total += cast(long)mod.intrCount+mod.fadeCount;
          }
          auto fo = VFile(".zxmodules.cache", "a");
          fo.writeNum!uint(cast(uint)mi.name.length);
          fo.rawWriteExact(mi.name[]);
          fo.writeNum!uint(mi.intrs);
          modcache[mi.name] = mi;
        }
      } catch (Exception e) {
        conwriteln("BAD MODULE: ", ayplaylist[idx], " (", e.msg, ")");
        if (conGetVar!bool("dump_bad")) {
          import core.stdc.stdio;
          stderr.fprintf("%.*s\n", cast(uint)ayplaylist[idx].length, ayplaylist[idx].ptr);
        }
        { import std.algorithm : remove; ayplaylist = ayplaylist.remove(idx); }
        --idx;
      }
    }

    conwrite(ayplaylist.length, " module", (ayplaylist.length != 1 ? "s" : ""), ", ");
    total /= 50; // seconds
    auto days = total/(60*60*24);
    total %= 60*60*24;
    auto hours = total/(60*60);
    total %= 60*60;
    auto mins = total/60;
    total %= 60;
         if (days > 0) conwritefln!"%s day%s %02s:%02s:%02s"(days, (days != 1 ? "s" : ""), hours, mins, total);
    else if (hours > 0) conwritefln!"%s:%02s:%02s"(hours, mins, total);
    else conwritefln!"%2s:%02s"(mins, total);
  }

  if (conGetVar!bool("shuffle")) {
    import std.random : randomShuffle;
    ayplaylist.randomShuffle;
  }

  ttySetRaw();
  scope(exit) ttySetNormal();
  ttyconInit();

  int aycurfile = 0;
  bool fromLast = false;
  bool ttyGoUp = false;

  mainloop: while (aycurfile < ayplaylist.length) {
    try {
      string fname = ayplaylist[aycurfile];
      VFile fl = VFile(fname);
      Action act;
      {
        soundInit();
        if (!sndvEnabled) assert(0, "cannot init sound");
        scope(exit) soundDeinit();
        act = ayPlayFile(fl, fromLast, ttyGoUp);
        fromLast = false;
        ttyGoUp = true;
      }
      final switch (act) {
        case Action.None:
        case Action.Next:
          ++aycurfile;
          break;
        case Action.Prev:
          if (aycurfile > 0) { --aycurfile; fromLast = true; }
          break;
        case Action.Quit:
          break mainloop;
      }
    } catch (Exception e) {
      ttyGoUp = false;
      conwriteln("ERROR(", ayplaylist[aycurfile], "): ", e.msg);
      { import std.algorithm : remove; ayplaylist = ayplaylist.remove(aycurfile); }
    }
  }
}
