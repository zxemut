/* Sound support
 * Copyright (c) 2000-2004 Russell Marks, Matan Ziv-Av, Philip Kendall
 * Copyright (c) 2016 Fredrick Meunier
 * Copyright (c) 2017 Ketmar // Invisible Vector
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
module sound;
/* The AY white noise RNG algorithm is based on info from MAME's ay8910.c -
 * MAME's licence explicitly permits free use of info (even encourages it).
 */
import iv.cmdcon;

import zxinfo;
import emuconfig;
version(linux) {
  import soundalsa;
} version(Windows) {
  import soundshitdows;
}


// ////////////////////////////////////////////////////////////////////////// //
public __gshared bool sndvEnabled = false; // are we currently using the sound card?
__gshared bool sndvSkipSynthV = false; // set to `true` to skip synthesis part; useful for player rewinding

public @property bool sndvSkipSynth () { return sndvSkipSynthV; }
public @property sndvSkipSynth (bool v) {
  if (sndvSkipSynthV != v) {
    sndvSkipSynthV = v;
    soundReinit();
  }
}


// ////////////////////////////////////////////////////////////////////////// //
private:
import iv.blipbuf;

//alias BlipSynth = BlipSynthBase!(BlipBuffer.Good, 65535);
alias BlipSynth = BlipSynthBase!(BlipBuffer.Good, 32767);


__gshared bool sndvRealStereo;
__gshared uint sndvLastSampleRate;

/* assume all three tone channels together match the beeper volume (ish).
 * Must be <=127 for all channels; 50+2+(24*3) = 124.
 * (Now scaled up for 16-bit.)
 */
enum AmplBeeper = 50*256;
enum AmplTape = 2*256;
enum AmplAYTone = 24*256; // three of these

/* max. number of sub-frame AY port writes allowed;
 * given the number of port writes theoretically possible in a
 * 50th I think this should be plenty.
 */
__gshared int sndvLastSilentLevelL = int.min;
__gshared int sndvLastSilentLevelR = int.min;

__gshared uint sndvFrameSize; // number samples in one sound frame


final class AYEmu {
private:
  static immutable uint[16] toneLevels = (){
    /* AY output doesn't match the claimed levels; these levels are based
     * on the measurements posted to comp.sys.sinclair in Dec 2001 by
     * Matthew Westcott, adjusted as I described in a followup to his post,
     * then scaled to 0..0xffff.
     */
    static immutable int[16] levels = [
      0x0000, 0x0385, 0x053D, 0x0770,
      0x0AD7, 0x0FD5, 0x15B0, 0x230C,
      0x2B4C, 0x43C1, 0x5A4B, 0x732F,
      0x9204, 0xAFF1, 0xD921, 0xFFFF
    ];

    uint[16] res;
    // scale the values down to fit
    foreach (immutable f; 0..16) res[f] = (levels[f]*AmplAYTone+0x8000)/0xffff;
    return res;
  }();

private:
  Config.Stereo stereoType = Config.Stereo.ABC;

  uint[3] toneTick, toneHigh;
  uint noiseTick;
  uint toneCycles, envCycles;
  uint envInternalTick, envTick;
  uint[3] tonePeriod;
  uint noisePeriod, envPeriod;

  // local copy of the AY registers
  ubyte[16] registers;

  BlipSynth synthA, synthB, synthC;
  BlipSynth rsynthA, rsynthB, rsynthC;
  BlipSynth mrsynth;

  static struct ChangeRec {
    uint tstates;
    ubyte reg, val;
  }

  ChangeRec[] regChangelog;
  uint rclCount;

private:
  int rng = 1;
  int noiseToggle = 0;
  int envFirst = 1, envRev = 0, envCounter = 15;

public:
  this () {
    regChangelog.length = 16384;
    reset();
  }

  // call this after initializing blip buffer
  void setupBlips () {
    immutable double treble = speakerEqConfig[Config.speakerType].treble;

    if (synthA is null) synthA = new BlipSynth();
    synthA.setVolumeTreble(soundGetVolume(Config.volumeAY), treble);

    if (synthB is null) synthB = new BlipSynth();
    synthB.setVolumeTreble(soundGetVolume(Config.volumeAY), treble);

    if (synthC is null) synthC = new BlipSynth();
    synthC.setVolumeTreble(soundGetVolume(Config.volumeAY), treble);

    /* important to override these settings if not using stereo
     * (it would probably be confusing to mess with the stereo
     * settings in settings_current though, which is why we make copies
     * rather than using the real ones).
     */

    rsynthA = null;
    rsynthB = null;
    rsynthC = null;

    stereoType = Config.stereoType;
    if (stereoType != Config.Stereo.None) {
      enum MidMultiplier = 0.7;

      // another mid synth for right channel, slightly muted
      if (mrsynth is null) mrsynth = new BlipSynth();
      mrsynth.setOutputVolumeTreble(rightBlipBuf, soundGetVolume(Config.volumeAY)*MidMultiplier, treble);

      BlipSynth l, r, m;
      // attach the BlipSynth's we've already created as appropriate, and create one more BlipSynth for the middle channel's right buffer
      enum SetupMixin(string mode) =
        "case Config.Stereo."~mode~":
          l = synth"~mode[0]~";
          m = synth"~mode[1]~";
          r = synth"~mode[2]~";
          rsynth"~mode[1]~" = mrsynth;
          break;";
      final switch (stereoType) {
        mixin(SetupMixin!"ACB");
        mixin(SetupMixin!"ABC");
        mixin(SetupMixin!"BAC");
        mixin(SetupMixin!"BCA");
        mixin(SetupMixin!"CAB");
        mixin(SetupMixin!"CBA");
        case Config.Stereo.None: assert(0);
      }

      l.output = leftBlipBuf;
      r.output = rightBlipBuf;
      // middle synth should sound in left and right channels, slightly muted
      // mrsynth will take care of right channel
      m.setOutputVolumeTreble(leftBlipBuf, soundGetVolume(Config.volumeAY)*MidMultiplier, treble);
    } else {
      synthA.output = leftBlipBuf;
      synthB.output = leftBlipBuf;
      synthC.output = leftBlipBuf;
    }
  }

  /// call this on machine reset
  void reset () nothrow @trusted @nogc {
    noiseTick = noisePeriod = 0;
    envInternalTick = envTick = envPeriod = 0;
    toneCycles = envCycles = 0;
    toneTick[] = 0;
    toneHigh[] = 0;
    tonePeriod[] = 1;
    rclCount = 0;
    rng = 1;
    noiseToggle = 0;
    envFirst = 1;
    envRev = 0;
    envCounter = 15;
    foreach (immutable f; 0..16) writeReg(f, 0, 0);
  }

  /// don't make the change immediately; record it for later, to be made by sound_frame() (via soundOverlay())
  void writeReg (ubyte reg, ubyte val, uint nowts) nothrow @trusted @nogc {
    if (rclCount < regChangelog.length) {
      static immutable ubyte[16] ayvaluemask = [
        0xff, 0x0f, 0xff, 0x0f, 0xff, 0x0f, 0x1f, 0xff,
        0x1f, 0x1f, 0x1f, 0xff, 0xff, 0x0f, 0xff, 0xff,
      ];
      regChangelog.ptr[rclCount].tstates = nowts;
      regChangelog.ptr[rclCount].reg = reg&0x0f;
      regChangelog.ptr[rclCount].val = val&ayvaluemask.ptr[reg&0x0f];
      ++rclCount;
    }
  }

  private void doTone(ubyte chan) (int level, uint toneCount, ref int var) nothrow @trusted @nogc {
    toneTick.ptr[chan] += toneCount;
    if (toneTick.ptr[chan] >= tonePeriod.ptr[chan]) {
      toneTick.ptr[chan] -= tonePeriod.ptr[chan];
      toneHigh.ptr[chan] = !toneHigh.ptr[chan];
    }
    var = (level ? (toneHigh.ptr[chan] ? level : 0) : 0);
  }

  void soundOverlay () nothrow @trusted @nogc {
    // bitmasks for envelope
    enum AY_ENV_CONT = 8;
    enum AY_ENV_ATTACK = 4;
    enum AY_ENV_ALT = 2;
    enum AY_ENV_HOLD = 1;

    // the AY steps down the external clock by 16 for tone and noise generators
    enum AY_CLOCK_DIVISOR = 16;
    // all Spectrum models and clones with an AY seem to count down the master clock by 2 to drive the AY
    enum AY_CLOCK_RATIO = 2;

    int[3] toneLevel;
    int mixer, envshape;
    int level;
    uint rclPos;
    uint rclLeft = rclCount;
    int reg, r;
    int chan1, chan2, chan3;
    int lastChan1 = 0, lastChan2 = 0, lastChan3 = 0;
    uint toneCount, noiseCount;

    rclCount = 0;

    // If no AY chip, don't produce any AY sound (!)
    /+
    if( !( periph_is_active( PERIPH_TYPE_FULLER) ||
           periph_is_active( PERIPH_TYPE_MELODIK ) ||
           machine_current.capabilities & LIBSPECTRUM_MACHINE_CAPABILITY_AY ) )
      return;
    +/

    for (uint curts = 0; curts < Config.machine.timings.tstatesPerFrame; curts += AY_CLOCK_DIVISOR*AY_CLOCK_RATIO) {
      // update ay registers
      while (rclPos < rclLeft && curts >= regChangelog.ptr[rclPos].tstates) {
        reg = regChangelog.ptr[rclPos].reg;
        registers.ptr[reg] = regChangelog.ptr[rclPos].val;
        ++rclPos;
        // fix things as needed for some register changes
        switch (reg) {
          case 0: case 1: case 2: case 3: case 4: case 5:
            r = reg>>1;
            // a zero-len period is the same as 1
            tonePeriod.ptr[r] = (registers.ptr[reg&~1]|(registers.ptr[reg|1]&15)<<8);
            if (!tonePeriod.ptr[r]) ++tonePeriod.ptr[r];
            // important to get this right, otherwise e.g. Ghouls 'n' Ghosts has really scratchy, horrible-sounding vibrato
            if (toneTick.ptr[r] >= tonePeriod.ptr[r]*2) toneTick.ptr[r] %= tonePeriod.ptr[r]*2;
            break;
          case 6:
            noiseTick = 0;
            noisePeriod = registers.ptr[reg]&31;
            break;
          case 11: case 12:
            envPeriod = registers.ptr[11]|(registers.ptr[12]<<8);
            break;
          case 13:
            envInternalTick = envTick = envCycles = 0;
            envFirst = 1;
            envRev = 0;
            envCounter = (registers.ptr[13]&AY_ENV_ATTACK ? 0 : 15);
            break;
          default: break;
        }
      }

      // the tone level if no enveloping is being used
      foreach (immutable g; 0..3) toneLevel.ptr[g] = toneLevels.ptr[registers.ptr[8+g]&15];

      // envelope
      envshape = registers.ptr[13];
      level = toneLevels.ptr[envCounter];

      foreach (immutable g; 0..3) if (registers.ptr[8+g]&16) toneLevel.ptr[g] = level;

      // envelope output counter gets incr'd every 16 AY cycles
      envCycles += AY_CLOCK_DIVISOR;
      noiseCount = 0;
      while (envCycles >= 16) {
        envCycles -= 16;
        ++noiseCount;
        ++envTick;
        while (envTick >= envPeriod) {
          envTick -= envPeriod;

          // do a 1/16th-of-period incr/decr if needed
          if (envFirst || ((envshape&AY_ENV_CONT) && !(envshape&AY_ENV_HOLD))) {
            if (envRev) {
              envCounter -= (envshape&AY_ENV_ATTACK ? 1 : -1);
            } else {
              envCounter += (envshape&AY_ENV_ATTACK ? 1 : -1);
            }
            if (envCounter < 0 ) envCounter = 0;
            if (envCounter > 15) envCounter = 15;
          }

          ++envInternalTick;
          while (envInternalTick >= 16) {
            envInternalTick -= 16;

            // end of cycle
            if (!(envshape&AY_ENV_CONT)) {
              envCounter = 0;
            } else {
              if (envshape&AY_ENV_HOLD) {
                if (envFirst && (envshape&AY_ENV_ALT)) envCounter = (envCounter ? 0 : 15);
              } else {
                // non-hold
                if (envshape&AY_ENV_ALT) {
                  envRev = !envRev;
                } else {
                  envCounter = (envshape&AY_ENV_ATTACK ? 0 : 15);
                }
              }
            }

            envFirst = 0;
          }

          // don't keep trying if period is zero
          if (!envPeriod) break;
        }
      }

      /* generate tone+noise... or neither.
       * (if no tone/noise is selected, the chip just shoves the
       * level out unmodified. This is used by some sample-playing
       * stuff.)
       */
      chan1 = toneLevel.ptr[0];
      chan2 = toneLevel.ptr[1];
      chan3 = toneLevel.ptr[2];
      mixer = registers.ptr[7];

      toneCycles += AY_CLOCK_DIVISOR;
      toneCount = toneCycles>>3;
      toneCycles &= 7;

      if ((mixer&1) == 0) {
        level = chan1;
        doTone!0(level, toneCount, chan1);
      }
      if ((mixer&0x08) == 0 && noiseToggle) chan1 = 0;

      if ((mixer&2) == 0) {
        level = chan2;
        doTone!1(level, toneCount, chan2);
      }
      if ((mixer&0x10) == 0 && noiseToggle) chan2 = 0;

      if ((mixer&4) == 0) {
        level = chan3;
        doTone!2(level, toneCount, chan3);
      }
      if ((mixer&0x20) == 0 && noiseToggle) chan3 = 0;

      if (lastChan1 != chan1) {
        if (Config.sndAYEnabled && !sndvSkipSynthV) {
          synthA.update(curts, chan1);
          if (rsynthA !is null) rsynthA.update(curts, chan1);
        }
        lastChan1 = chan1;
      }

      if (lastChan2 != chan2) {
        if (Config.sndAYEnabled && !sndvSkipSynthV) {
          synthB.update(curts, chan2);
          if (rsynthB !is null) rsynthB.update(curts, chan2);
        }
        lastChan2 = chan2;
      }

      if (lastChan3 != chan3) {
        if (Config.sndAYEnabled && !sndvSkipSynthV) {
          synthC.update(curts, chan3);
          if (rsynthC !is null) rsynthC.update(curts, chan3);
        }
        lastChan3 = chan3;
      }

      // update noise RNG/filter
      noiseTick += noiseCount;
      while (noiseTick >= noisePeriod) {
        noiseTick -= noisePeriod;
        //if ((rng&1)^(rng&2 ? 1 : 0)) noiseToggle = !noiseToggle;
        if ((rng&1)^((rng>>1)&1)) noiseToggle = !noiseToggle;
        // rng is 17-bit shift reg, bit 0 is output. input is bit 0 xor bit 3.
        if (rng&1) rng ^= 0x24000;
        rng >>= 1;
        // don't keep trying if period is zero
        if (!noisePeriod) break;
      }
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared BlipBuffer leftBlipBuf;
__gshared BlipBuffer rightBlipBuf;
__gshared BlipBuffer.Sample[] samples;
__gshared BlipSynth lsynthBeeper, rsynthBeeper;
__gshared BlipSynth lsynthTape, rsynthTape;
__gshared AYEmu sndvAY;


// ////////////////////////////////////////////////////////////////////////// //
double soundGetVolume (int volume) {
  if (volume < 0) volume = 0; else if (volume > 400) volume = 400;
  return volume/100.0;
}


// returns the emulation speed adjusted processor speed
uint effectiveProcessorSpeed () nothrow @trusted @nogc { return cast(uint)(cast(ulong)Config.machine.cpuSpeed*Config.emuSpeed/100); }


// ////////////////////////////////////////////////////////////////////////// //
bool initBlipBuffer (ref BlipBuffer buf) {
  if (buf is null) buf = new BlipBuffer();
  buf.clockRate = effectiveProcessorSpeed;
  /* Allow up to 1s of playback buffer - this allows us to cope with slowing
     down to 2% of speed where a single Speccy frame generates just under 1s
     of sound */
  if (buf.setSampleRate(Config.sampleRate, 1000) != buf.Result.OK) return false;
  buf.bassFreq = speakerEqConfig[Config.speakerType].bass;
  return true;
}


void initBlip (BlipBuffer buf, ref BlipSynth synth, ushort volume) {
  if (synth is null) synth = new BlipSynth();
  synth.setOutputVolumeTreble(buf, soundGetVolume(volume), speakerEqConfig[Config.speakerType].treble);
}


// ////////////////////////////////////////////////////////////////////////// //
int isGoodEmuSpeed () {
  enum MIN_SPEED_PERCENTAGE = 2;
  enum MAX_SPEED_PERCENTAGE = 500;
  return (Config.emuSpeed >= MIN_SPEED_PERCENTAGE && Config.emuSpeed <= MAX_SPEED_PERCENTAGE);
}


/// initialize sound system
public void soundInit (bool resetAY=true) {
  if (sndvEnabled) soundrvDeinit();
  sndvEnabled = false;

  /* Allow sound as long as emulation speed is greater than 2%
     (less than that and a single Speccy frame generates more
     than a seconds worth of sound which is bigger than the
     maximum BlipBuffer of 1 second) */
  if (!isGoodEmuSpeed()) return;

  // only try for stereo if we need it
  if (!soundrvInit(true, Config.sampleRate)) return; // always stereo

  sndvLastSampleRate = Config.sampleRate;
  sndvRealStereo = (Config.stereoType != Config.Stereo.None);

  if (sndvAY is null) sndvAY = new AYEmu();

  sndvEnabled = true;
  soundSetupSamplers();
  if (resetAY) sndvAY.reset();
}


/// shutdown sound system
public void soundDeinit () {
  if (sndvEnabled) {
    soundrvDeinit();
    sndvEnabled = false;
  }
}


/// doesn't reset AY state; should be called when ANY sound config changed
public void soundReinit (bool resetAY=false) {
  if (!isGoodEmuSpeed()) {
    if (sndvEnabled) soundrvDeinit();
    sndvEnabled = false;
    return;
  }

  if (!sndvEnabled || sndvLastSampleRate != Config.sampleRate) { soundInit(); return; }

  sndvRealStereo = (Config.stereoType != Config.Stereo.None);
  soundSetupSamplers();
  if (resetAY) sndvAY.reset();
}


/// doesn't reset AY state
public void soundForceReinit (bool resetAY=false) {
  if (!sndvEnabled) return;
  soundInit(resetAY);
}


// ////////////////////////////////////////////////////////////////////////// //
void soundSetupSamplers () {
  if (!initBlipBuffer(leftBlipBuf)) assert(0, "cannot initialize blip buffers");
  if (!initBlipBuffer(rightBlipBuf)) assert(0, "cannot initialize blip buffers");

  initBlip(leftBlipBuf, lsynthBeeper, Config.volumeBeeper);
  initBlip(rightBlipBuf, rsynthBeeper, Config.volumeBeeper); // just in case

  initBlip(leftBlipBuf, lsynthTape, Config.volumeTape);
  initBlip(rightBlipBuf, rsynthTape, Config.volumeTape); // just in case

  /* Adjust relative processor speed to deal with adjusting sound generation
     frequency against emulation speed (more flexible than adjusting generated
     sample rate) */
  float hz = cast(float)effectiveProcessorSpeed/Config.machine.timings.tstatesPerFrame;

  //{ import iv.cmdcon; conwriteln("hz=", cast(float)Config.machine.cpuSpeed/Config.machine.timings.tstatesPerFrame); }
  //{ import iv.cmdcon; conwriteln("hz=", cast(float)Config.machine.aySpeed/Config.machine.timings.tstatesPerFrame); }

  // size of audio data we will get from running a single Spectrum frame
  sndvFrameSize = cast(int)(cast(float)Config.sampleRate/hz);
  ++sndvFrameSize;

  //{ import iv.cmdcon; conwriteln("hz=", hz, "; frsz=", sndvFrameSize); }

  samples.assumeSafeAppend;
  samples.length = sndvFrameSize*2/*sndvChans*/;

  sndvLastSilentLevelL = int.min;
  sndvLastSilentLevelR = int.min;

  sndvAY.setupBlips();
}


// ////////////////////////////////////////////////////////////////////////// //
public void soundBlankFrame () nothrow @trusted {
  if (!sndvEnabled || sndvSkipSynthV) return;
  leftBlipBuf.clear!true();
  if (sndvRealStereo) rightBlipBuf.clear!true();
  samples[0..sndvFrameSize*2/*sndvChans*/] = 0;
  soundrvFrame(samples.ptr, sndvFrameSize);
}


/// send next sound frame to sound card
/// return `true` if frame was silent
public bool soundFrame(bool checkSilent=false) () nothrow @trusted {
  if (!sndvEnabled) return true;

  if (sndvSkipSynthV) return false;

  // overlay AY sound
  sndvAY.soundOverlay();

  leftBlipBuf.endFrame(Config.machine.timings.tstatesPerFrame);
  //conwriteln("avail: ", leftBlipBuf.samplesAvail);
  //samples[0..sound_framesiz*sound_channels] = 0;

  int count;
  if (sndvRealStereo) {
    rightBlipBuf.endFrame(Config.machine.timings.tstatesPerFrame);
    // read left channel into even samples, right channel into odd samples: LRLRLRLRLR...
    count = leftBlipBuf.readSamples!true(samples.ptr, sndvFrameSize);
    auto c1 = rightBlipBuf.readSamples!true(samples.ptr+1, count);
    if (c1 != count) assert(0, "blip synth fucked");
    // there can be one or two extra samples
    // drop them on the floor
    rightBlipBuf.removeSamples(rightBlipBuf.samplesAvail);
  } else {
    count = leftBlipBuf.readSamples!(true, true)(samples.ptr, sndvFrameSize); // fake stereo
  }
  // there can be one or two extra samples
  // drop them on the floor
  leftBlipBuf.removeSamples(leftBlipBuf.samplesAvail);

  /* check for a silent frame.
   * bit nasty, but it's the only way to be sure. :-)
   * We check pre-fade, and make a separate check for having faded-out
   * later. This is to avoid problems with beeper `silence' which is
   * really a constant high/low level (something similar is also
   * possible with the AY).
   *
   * To cope with beeper and arguably-buggy .ay files, we have to treat
   * *any* non-varying level as silence. Fair enough in a way, as it
   * will indeed be silent, but a bit of a pain.
   */
  static if (checkSilent) {
    bool silent = true;
    short* ptr = samples.ptr;
    // stereo anyway
    auto chkl = *ptr++;
    auto chkr = *ptr++;
    if (chkl != sndvLastSilentLevelL || chkr != sndvLastSilentLevelR) {
      silent = false;
    } else {
      foreach (immutable _; 1..count) {
        if (*ptr++ != chkl) { silent = false; break; }
        if (*ptr++ != chkr) { silent = false; break; }
      }
    }
    // even if they're all the same, it doesn't count if the level's changed since last time...
    // save last sample for comparison next time
    sndvLastSilentLevelL = samples[count-2];
    sndvLastSilentLevelR = samples[count-1];
  } else {
    enum silent = false;
  }

  soundrvFrame(samples.ptr, count);

  return silent;
}


// ////////////////////////////////////////////////////////////////////////// //
/// call this after machine reset
public void soundResetAY () nothrow @trusted @nogc {
  if (sndvAY !is null) sndvAY.reset();
}


/// don't make the change immediately; record it for later, to be made by sound_frame() (via soundOverlay())
public void soundWriteAY (ubyte reg, ubyte val, uint nowts) nothrow @trusted @nogc {
  if (sndvEnabled && sndvAY !is null) sndvAY.writeReg(reg, val, nowts);
}

/// when something was written to ULA port, this should be called
/// on is: bit0 is tape microphone bit, bit1 is speaker bit, i.e.: (!!(b&0x10)<<1)+((!(b&0x8))|tape_microphone)
public void soundWriteBeeper (uint at_tstates, int on) nothrow @trusted @nogc {
  static immutable int[4] beeperAmpl = [ 0, AmplTape, AmplBeeper, AmplBeeper+AmplTape ];

  if (!sndvEnabled || (!Config.sndBeeperEnabled && !Config.sndTapeEnabled)) return;

  if (sndvSkipSynthV) return;

  /+
  if (tape_is_playing) {
    // timex machines have no loading noise
    if (!/*settings_current.*/sound_load || machine_current.timex) on = on&0x02;
  } else {
    // ULA book says that MIC only isn't enough to drive the speaker as output voltage is below the 1.4v threshold
    if (on == 1) on = 0;
  }
  +/

  if (Config.sndTapeEnabled) {
    int val = (on&0x01 ? AmplBeeper : 0);
    lsynthTape.update(at_tstates, val);
    if (sndvRealStereo) rsynthTape.update(at_tstates, val);
  }

  if (Config.sndBeeperEnabled) {
    // ULA book says that MIC only isn't enough to drive the speaker as output voltage is below the 1.4v threshold
    if (on == 1) on = 0;
    int val = beeperAmpl[on];
    lsynthBeeper.update(at_tstates, val);
    if (sndvRealStereo) rsynthBeeper.update(at_tstates, val);
  }
}
