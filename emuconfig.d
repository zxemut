/*
 * Copyright (c) 2017 Ketmar // Invisible Vector
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
module emuconfig;
private:

import std.net.curl : download;

import iv.cmdcon;
import iv.strex;
import iv.vfs;
import iv.zymosis;

import zxinfo;


// ////////////////////////////////////////////////////////////////////////// //
public struct Config {
  @disable this ();
  @disable this (this);

static:
  __gshared ZXTimings machine = baseTimings[0];

  /* Stereo separation types:
   *  * ACB is used in the Melodik interface.
   *  * ABC stereo is used in the Pentagon/Scorpion.
   *  * BAC stereo does seem to exist but is quite rare:
   *      Z80Stealth emulates BAC stereo but that's about all.
   *  * CAB, BCA and CBA don't get many search results.
   */
  enum Stereo { None, ACB, ABC, BAC, BCA, CAB, CBA }
  enum Speaker { TV, Beeper, Default, Flat, Crisp }
  __gshared ushort emuSpeed = 100;
  __gshared uint sampleRate = 48000;
  __gshared ushort volumeBeeper = 100; // [0..400]
  __gshared ushort volumeTape = 100; // [0..400]
  __gshared ushort volumeAY = 100; // [0..400]; it sounds slightly better with 200 as default
  __gshared bool sndBeeperEnabled = true;
  __gshared bool sndTapeEnabled = false;
  __gshared bool sndAYEnabled = true;
  __gshared Speaker speakerType = Speaker.Default;
  __gshared Stereo stereoType = Stereo.ABC;
  __gshared bool floatingBus = true;
  __gshared bool maxSpeed = false;
  __gshared bool kbdMatrix = true; // emulate keyboard matrix effect

  __gshared bool kempstonJ = true;
  __gshared bool kempstonM = true;
  __gshared bool kempstonWheel = true;
  __gshared bool kempstonMSwapB = false; // swap buttons

  __gshared bool rockULAIn = true; // always start with 0xff in ULA in

  __gshared bool ayPortRead = true;

  __gshared bool noFlic = false;
  __gshared bool noFlicDetector = false;


  // WD1793 config
  static struct WD93 {
  static:
    __gshared string trdos128model = "pentagon"; // default 128K model for CLI TR-DOS reset

    __gshared bool noDelay; // skip WD93 delays (instant disk)?
    __gshared bool writeProtA; // write-protection for the given FDD (will be reset on disk clear/load)
    __gshared bool writeProtB; // write-protection for the given FDD (will be reset on disk clear/load)
    __gshared bool writeProtC; // write-protection for the given FDD (will be reset on disk clear/load)
    __gshared bool writeProtD; // write-protection for the given FDD (will be reset on disk clear/load)
    __gshared ubyte trdInterleave; // interleaving for new TR-DOS disks: [0..2]
    __gshared bool trdAddBoot = true;
    __gshared bool trdAddBootToSys = false;
    __gshared bool trdTraps = false;

    bool writeProt (uint idx) nothrow @trusted @nogc {
      switch (idx) {
        case 0: return writeProtA;
        case 2: return writeProtB;
        case 3: return writeProtC;
        case 4: return writeProtD;
        default:
      }
      return true;
    }

    void writeProt (uint idx, bool v) nothrow @trusted @nogc {
      switch (idx) {
        case 0: writeProtA = v; return;
        case 2: writeProtB = v; return;
        case 3: writeProtC = v; return;
        case 4: writeProtD = v; return;
        default:
      }
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public __gshared ZymCPU emuz80; // set to valid ZymCPU instance, or emu will segfault
public __gshared long emuFullFrameTS; // inc with ts-per-frame by each frame
public __gshared ubyte zxBorderColor = 0;


// ////////////////////////////////////////////////////////////////////////// //
public struct SpeakerEqConfig {
  int bass;
  double treble;
}

public static immutable SpeakerEqConfig[5] speakerEqConfig = [
  SpeakerEqConfig(200, -37.0), // TV
  SpeakerEqConfig(1000, -67.0), // Beeper
  SpeakerEqConfig(16, -8.0), // Default
  SpeakerEqConfig(1, 0.0), // Flat
  SpeakerEqConfig(1, 5.0), // Crisp
];


// ////////////////////////////////////////////////////////////////////////// //
public string exeDir () nothrow @trusted @nogc {
  version(Posix) {
    __gshared size_t dir = 0;
    __gshared size_t dirlen = 0;
    if (dirlen == 0) {
      import core.stdc.stdio : snprintf;
      import core.stdc.stdlib : malloc;
      import core.sys.posix.unistd : readlink;
      auto dirp = cast(char*)malloc(8194);
      if (dirp is null) assert(0, "out of memory");
      version(rdmd) {
        mixin(import("zxemut_home_dir.d"));
        dirp[0..MyHomeDir.length] = MyHomeDir[];
        dirlen = MyHomeDir.length;
      } else {
        auto dl = readlink("/proc/self/exe", dirp, 8192);
        if (dl <= 0) {
          dirp[0] = '.';
          dirlen = 1;
        } else {
          while (dl > 0 && dirp[dl-1] == '/') --dl;
          dirlen = dl;
        }
      }
      dir = cast(size_t)dirp;
    }
    return (cast(immutable(char)*)dir)[0..dirlen];
  } else {
    return ".";
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public string configDir () nothrow @trusted @nogc {
  version(Posix) {
    __gshared size_t dir = 0;
    __gshared size_t dirlen = 0;
    if (dirlen == 0) {
      import core.stdc.stdio : snprintf;
      import core.stdc.stdlib : malloc;
      import core.sys.posix.unistd : readlink;
      auto dirp = cast(char*)malloc(8194);
      if (dirp is null) assert(0, "out of memory");
      version(rdmd) {
        mixin(import("zxemut_home_dir.d"));
        dirp[0..MyHomeDir.length] = MyHomeDir[];
        dirlen = MyHomeDir.length;
      } else {
        auto dl = readlink("/proc/self/exe", dirp, 8192);
        if (dl <= 0) {
          dirp[0] = '.';
          dirlen = 1;
        } else {
          while (dl > 0 && dirp[dl-1] == '/') --dl;
          dirlen = dl;
        }
      }
      dir = cast(size_t)dirp;
    }
    return (cast(immutable(char)*)dir)[0..dirlen];
  } else {
    return ".";
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public string dataDir () nothrow @trusted @nogc {
  version(Posix) {
    __gshared size_t dir = 0;
    __gshared size_t dirlen = 0;
    if (dirlen == 0) {
      import core.stdc.stdio : snprintf;
      import core.stdc.stdlib : malloc;
      import core.sys.posix.unistd : readlink;
      auto dirp = cast(char*)malloc(8194);
      if (dirp is null) assert(0, "out of memory");
      version(rdmd) {
        mixin(import("zxemut_home_dir.d"));
        dirp[0..MyHomeDir.length] = MyHomeDir[];
        dirlen = MyHomeDir.length;
      } else {
        auto dl = readlink("/proc/self/exe", dirp, 8192);
        if (dl <= 0) {
          dirp[0] = '.';
          dirlen = 1;
        } else {
          while (dl > 0 && dirp[dl-1] == '/') --dl;
          dirlen = dl;
        }
      }
      dir = cast(size_t)dirp;
    }
    return (cast(immutable(char)*)dir)[0..dirlen];
  } else {
    return ".";
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public bool xfIsDiskExt (const(char)[] fn) nothrow @trusted @nogc {
  return (fn.endsWithCI(".trd") || fn.endsWithCI(".scl") || fn.endsWithCI(".fdi") || fn.endsWithCI(".udi"));
}

public bool xfIsHoBetaExt (const(char)[] fn) nothrow @trusted @nogc {
  return (fn.length > 3 && fn[$-2] == '$' && fn[$-3] == '.');
}

public bool xfIsSnapExt (const(char)[] fn) nothrow @trusted @nogc {
  return (fn.endsWithCI(".sna") || fn.endsWithCI(".z80") || fn.endsWithCI(".szx"));
}

public bool xfIsTapeExt (const(char)[] fn) nothrow @trusted @nogc {
  return (fn.endsWithCI(".tap") || fn.endsWithCI(".tzx") || fn.endsWithCI(".pzx"));
}

public bool xfIsROMExt (const(char)[] fn) nothrow @trusted @nogc {
  return fn.endsWithCI(".rom");
}

public bool xfIsRCExt (const(char)[] fn) nothrow @trusted @nogc {
  return fn.endsWithCI(".rc");
}


// ////////////////////////////////////////////////////////////////////////// //
struct OpenEx {
  VFile snap; // snapshot
  VFile rc; // console *.rc file
  VFile[4] disk; // 4 possible disks
  VFile rom; // rom file
  VFile tape; // tape file

  @property bool hasSnap () { return snap.isOpen; }
  @property bool hasRC () { return rc.isOpen; }
  @property bool hasDisk (int idx=0) { return (idx >= 0 && idx < disk.length ? disk[idx].isOpen : false); }
  @property bool hasROM () { return rom.isOpen; }
  @property bool hasTape () { return tape.isOpen; }
  // just in case you will need it
  void close () { snap.close(); rc.close(); foreach (immutable idx; 0..disk.length) disk[idx].close(); rom.close(); tape.close(); }
}


//TODO: what to do with HoBetas?
public OpenEx xfopenEx (const(char)[] fname, scope bool delegate (const(char)[] fname) isGoodExt) {
  // internets
  if (fname.startsWithCI("http://") || fname.startsWithCI("https://") || fname.startsWithCI("ftp://")) {
    import std.file : exists, mkdirRecurse;
    enum DestDir = "/tmp/zxemut/rundowns/";
    mkdirRecurse(DestDir);
    char[] dfn = new char[](fname.length+DestDir.length);
    dfn[0..DestDir.length] = DestDir[];
    dfn[DestDir.length..$] = fname[];
    foreach (ref char ch; dfn[DestDir.length..$]) {
      if (ch == '/' || ch == '\\' || ch == ':' || ch <= ' ' || ch == 127) ch = '_';
    }
    if (!dfn.exists) {
      conwriteln("downloading: ", fname);
      download(fname, cast(string)dfn); // it is safe to cast here
    } else {
      conwriteln("cached: ", fname);
    }
    return xfopenEx(dfn, isGoodExt);
  }
  OpenEx res;
  // check specials
       if (fname.startsWithCI("$EXE/")) fname = exeDir~fname[4..$].idup;
  else if (fname.startsWithCI("$DATA/")) fname = dataDir~fname[5..$].idup;
  else if (fname.startsWithCI("$CFG/")) fname = configDir~fname[4..$].idup;
  // archives
  if (fname.endsWithCI(".zip")) {
    auto did = vfsAddPak(fname, "arc:");
    scope(exit) vfsRemovePak(did);
    foreach_reverse (const ref de; vfsFileList()) {
      if (!de.name.startsWith("arc:")) continue;
      if (isGoodExt(de.name)) {
        if (de.name.xfIsDiskExt || de.name.xfIsHoBetaExt) {
          foreach (immutable idx; 0..res.disk.length) {
            if (!res.disk[idx].isOpen) { res.disk[idx] = VFile(de.name); break; }
          }
        } else if (de.name.xfIsSnapExt) {
          res.snap = VFile(de.name);
        } else if (de.name.xfIsROMExt) {
          res.rom = VFile(de.name);
        } else if (de.name.xfIsRCExt) {
          res.rc = VFile(de.name);
        } else if (de.name.xfIsTapeExt) {
          res.tape = VFile(de.name);
        }
      }
    }
    if (res.hasSnap || res.hasDisk || res.hasROM || res.hasTape) return res;
    throw new VFSException("no suitable file found in archive '"~fname.idup~"'");
  }
  // other files
  if (isGoodExt(fname)) {
    //TODO: load ${fname}.rc?
         if (fname.xfIsDiskExt || fname.xfIsHoBetaExt) res.disk[0] = VFile(fname);
    else if (fname.xfIsSnapExt) res.snap = VFile(fname);
    else if (fname.xfIsROMExt) res.rom = VFile(fname);
    else if (fname.xfIsTapeExt) res.tape = VFile(fname);
    //else if (de.name.xfIsRCExt) res.rc = VFile(fname);
    else throw new VFSException("unknown file format: '"~fname.idup~"'");
  }
  return res;
}


public auto xfopenEx (const(char)[] fname, scope bool function (const(char)[] fname) isGoodExt) {
  import std.functional : toDelegate;
  return xfopenEx(fname, isGoodExt.toDelegate);
}
