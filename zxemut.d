/* ZX Spectrum Emulator
 * Copyright (C) 2012-2017 Ketmar // Invisible Vector
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
module zxemut is aliced;

import core.atomic;
import std.concurrency;

import arsd.simpledisplay;

import iv.bclamp;
import iv.cmdcon;
import iv.cmdcongl;
import iv.pxclock;
import iv.strex;
import iv.vfs;
import iv.zymosis;

import glshaders;

import egfx;

//import pipev;

import zxsnap;
import zxinfo;
import emuconfig;
import sound;

import wd93;

import autotape;
import lstape;
import tapeaccel;


// ////////////////////////////////////////////////////////////////////////// //
enum KMouseInMax = 8; // grab mouse after this
enum KMouseInIdle = 75; // release mouse after this
__gshared int kmouseInCount = 0;
__gshared bool kmouseGrabbed = false;
__gshared bool kmouseForceGrab = false;


// ////////////////////////////////////////////////////////////////////////// //
static immutable ubyte[6912] keyHelpScr = cast(immutable(ubyte)[6912])import("databin/keyhelp.scr");
__gshared bool showKeyHelp = false;


// ////////////////////////////////////////////////////////////////////////// //
//enum GLTexType = GL_RGBA;
enum GLTexType = GL_BGRA;
__gshared bool vbufVSync = false;
__gshared bool vbufEffVSync = false;


// ////////////////////////////////////////////////////////////////////////// //
enum EventId {
  Quit = 13,
  ScreenReady = 42,
  ScreenRepaint, // but don't rebuild and reupload texture
}

class ScreenReadyEvent {}
class ScreenRepaintEvent {}
class QuitEvent {}
__gshared ScreenReadyEvent evScrReady;


// ////////////////////////////////////////////////////////////////////////// //
__gshared WD1793 wd1793;


// ////////////////////////////////////////////////////////////////////////// //
__gshared bool emuSoundEnabled = true;
__gshared bool dbgExecDump = false;
__gshared bool dbgFBusHitDump = false;
__gshared bool noFlicDetected = false;
__gshared bool memContended;
__gshared bool optAYLeds = true;
__gshared bool optNoFlicDetectMsg = true;
__gshared bool optPentagonFloatingBus = true;
__gshared bool optTapeAutorun = true;

__gshared ubyte kmsDX, kmsDY, kmsBut, kmsWheel;


// ////////////////////////////////////////////////////////////////////////// //
// y to zx bitmap offset
static immutable uint[192] zxscry2a = (){
  uint[192] res;
  uint pos = 0;
  foreach (immutable h; 0..3) {
    foreach (immutable y; 0..8) {
      foreach (immutable z; 0..8) {
        res[pos++] = (z<<8)|(y<<5)|(h<<11);
      }
    }
  }
  assert(pos == res.length);
  return res;
}();

// zx bitmap offset to y
static immutable uint[32*192] zxscra2y = () {
  uint[32*192] res;
  uint pos = 0;
  foreach (immutable h; 0..3) {
    foreach (immutable y; 0..8) {
      foreach (immutable z; 0..8) {
        uint sta = (z<<8)|(y<<5)|(h<<11);
        res[sta..sta+32] = h*64+y*8+z;
      }
    }
  }
  return res;
}();


// ////////////////////////////////////////////////////////////////////////// //
__gshared string zxmodel = "48";
__gshared ubyte[1024*1024] zxram; // max 1MB of memory
__gshared ubyte[1024*1024] zxrom; // max 1MB of memory
__gshared ubyte[32768] zxtrd; // max 1MB of memory
__gshared ubyte[] zxulacont, zxulacontport;
__gshared bool trdosAvail = false;
__gshared bool trdosPaged = false;


struct TSXY {
  uint vbofs=uint.max; // pixel offset in screen buffer
  bool border;
  int addr=-1; // scr$ bmp offset for fetching; <0: do nothing
  int attr=-1; // scr$ attr offset for fetching
  // for scr$
  //ubyte tswrt; // [0..4]
    // for output:
    //    0: start writing already read bmp/attr pixels, [0..3]
  ubyte tsrds; // [0..4]
    // for reading:
    //    0: bmp fetch from [addr]
    //    1: attr fetch from [attr]; draw screen with (0) and (1)
    //    2: bmp fetch from [addr]
    //    3: attr fetch from [attr]
    //    4: nothing
    //    5: nothing
    //    6: draw screen with (2) and (3)
    //    7: ...
  // so tswrt is actually just shifted forward by 4. to render the things,
  // i can just flag "do render at next tstate" when tsrds == 3
  // or i can simply render 8 bytes right now, at tsrds==3, why not?
@property const pure nothrow @safe @nogc:
  bool valid () { pragma(inline, true); return (vbofs != uint.max); }
  bool scr () { pragma(inline, true); return (addr >= 0); }
}
__gshared TSXY[] tsraypos;


void zxLoadRom () {
  char[4096] rfname = 0;
  uint rfpos = 0;
  void put (const(char)[] s...) {
    foreach (char ch; s) {
      if (rfpos < rfname.length) rfname[rfpos++] = ch;
    }
  }
  put(dataDir);
  put("/roms/");
  foreach (char ch; Config.machine.name[]) {
    if (ch >= 'A' && ch <= 'Z') ch += 32;
    put(ch);
  }
  put(".rom");
  zxrom[] = 0;
  try {
    auto flx = xfopenEx(rfname[0..rfpos], &xfIsROMExt);
    if (!flx.hasROM) throw new Exception("fuuuuuu");
    auto fl = flx.rom;
    auto fsz = fl.size;
    if (fsz > zxrom.length) fsz = cast(uint)zxrom.length;
    fl.rawReadExact(zxrom[0..cast(uint)fsz]);
    conwriteln("rom: '", rfname[0..rfpos], "'");
  } catch (Exception e) {
    conwriteln("FUCK! can't load rom: '", rfname[0..rfpos], "'");
  }

  trdosAvail = false;
  zxtrd[] = 0;
  try {
    rfpos = 0;
    put(dataDir);
    put("/roms/trdos.rom");
    auto flx = xfopenEx(rfname[0..rfpos], &xfIsROMExt);
    if (!flx.hasROM) throw new Exception("fuuuuuu");
    auto fl = flx.rom;
    auto fsz = fl.size;
    if (fsz > zxtrd.length) fsz = cast(uint)zxtrd.length;
    fl.rawReadExact(zxtrd[0..cast(uint)fsz]);
    //conwriteln("rom: '", rfname[0..rfpos], "'");
    trdosAvail = true;
  } catch (Exception e) {
    conwriteln("FUCK! can't load trdos rom");
  }
}


void zxInitModel () {
  Config.machine = zxFindMachine(zxmodel);
  zxmodel = Config.machine.name;
  conwriteln("initializing model '", zxmodel, "'");
  tapeCurrentIs48K = (zxmodel == "48");

  zxLoadRom();

  zxulacont.assumeSafeAppend;
  zxulacont.length = Config.machine.timings.tstatesPerFrame;
  zxulacont[] = 0;
  // memory contention
  foreach (immutable y; 0..192) {
    assert(Config.machine.timings.horizontalScreen == 128); // always
    auto ts = Config.machine.timings.topLeftPixel+y*Config.machine.timings.tstatesPerLine-1; // it starts here
    //if (y == 0) conwriteln("***: ", ts);
    //if (Config.machine.mem == 48 && y == 0) assert(ts == 14336);
    foreach (immutable cc; 0..Config.machine.timings.horizontalScreen/8) {
      zxulacont[ts++] = 6; // 14336
      zxulacont[ts++] = 5; // 14337
      zxulacont[ts++] = 4; // 14338 First display byte fetch
      zxulacont[ts++] = 3; // 14339 Border finish, First attribute fetch, 1st and 2nd pixels output
      zxulacont[ts++] = 2; // 14340 3rd and 4th pixels output
      zxulacont[ts++] = 1; // 14341 Second display byte fetch, 5th and 6th pixels output
      zxulacont[ts++] = 0; // 14342 Second attribute byte fetch, 7th and 8th pixels output
      zxulacont[ts++] = 0; // 14343 9th and 10th pixels output
    }
  }
  //if (Config.machine.mem == 48) assert(zxulacont[14342] == 0);
  zxulacontport = zxulacont;
  assert(zxulacontport.length);
  /+
  //??? port contention is shifted 2 tstates up?
  zxulacontport.assumeSafeAppend;
  zxulacontport.length = zxulacont.length;
  zxulacontport[] = zxulacont[];
  //zxulacontport[] = 0;
  //zxulacontport[0..$-2] = zxulacont[2..$];
  //assert(zxulacontport[14336] == zxulacont[14338]);
  //conwriteln(zxulacontport[14336]);
  //???
  //zxulacont[] = zxulacontport[];
  +/

  tsraypos.assumeSafeAppend;
  tsraypos.length = Config.machine.timings.tstatesPerFrame+16; // for pentagons
  tsraypos[] = TSXY.init;

  // 14338
  uint tsrstart = Config.machine.timings.topLeftPixel-Config.machine.timings.leftBorder-Config.machine.timings.tstatesPerLine*Config.machine.timings.topBorder+2;
  int cury = VBufScrYOfs-Config.machine.timings.topBorder;
  // top border
  foreach (immutable y; 0..Config.machine.timings.topBorder) {
    auto ts = tsrstart;
    int curx = VBufScrXOfs-Config.machine.timings.leftBorder*2;
    foreach (immutable idx; 0..Config.machine.timings.tstatesPerLine-Config.machine.timings.horizontalRetrace) {
      if (cury >= 0 && cury < VBufHeight && curx >= 0 && curx+2 <= VBufWidth) {
        tsraypos[ts].vbofs = cury*VBufWidth+curx;
      }
      tsraypos[ts].border = true; // draw border
      ++ts;
      curx += 2;
    }
    tsrstart += Config.machine.timings.tstatesPerLine;
    ++cury;
  }
  // left border, screen, right border
  foreach (immutable scry; 0..192) {
    auto ts = tsrstart;
    int curx = VBufScrXOfs-Config.machine.timings.leftBorder*2;
    // left border
    foreach (immutable idx; 0..Config.machine.timings.leftBorder) {
      if (cury >= 0 && cury < VBufHeight && curx >= 0 && curx+2 <= VBufWidth) {
        tsraypos[ts].vbofs = cury*VBufWidth+curx;
      }
      tsraypos[ts].border = true; // draw border
      ++ts;
      curx += 2;
    }
    // 14338
    //if (scry == 0) conwriteln(":TS=", ts);
    // screen
    foreach (immutable idx; 0..Config.machine.timings.horizontalScreen/8) {
      assert(idx < 16);
      int scrx = idx*2;
      int attr = 0x1800+(scry/8)*32+scrx;
      int addr = zxscry2a.ptr[scry]+scrx;
      //    0: bmp fetch from [addr]
      //    1: attr fetch from [attr]
      //    2: draw screen with (0) and (1)
      //    3: bmp fetch from [addr]
      //    4: attr fetch from [attr]
      //    5: draw screen with (2) and (3)
      //    6: nothing
      //    7: nothing
      foreach (immutable ubyte cc; 0..8) {
        if (cury >= 0 && cury < VBufHeight && curx >= 0 && curx+2 <= VBufWidth) {
          tsraypos[ts].vbofs = cury*VBufWidth+curx;
        }
        tsraypos[ts].addr = addr;
        tsraypos[ts].attr = attr;
        tsraypos[ts].tsrds = cc;
        // postfixes
        switch (cc) {
           case 0: if (idx == 0) tsraypos[ts].border = true; break; // still border
           case 2: ++addr; ++attr; break;
           default: break;
        }
        ++ts;
        curx += 2;
      }
      /*
      if (Config.machine.mem == 48) {
        assert(zxulacont[ts-8] == 4);
        assert(zxulacont[ts-4] == 0);
      }
      */
    }
    // right border
    foreach (immutable idx; 0..Config.machine.timings.rightBorder) {
      if (cury >= 0 && cury < VBufHeight && curx >= 0 && curx+2 <= VBufWidth) {
        tsraypos[ts].vbofs = cury*VBufWidth+curx;
      }
      tsraypos[ts].border = true; // draw border
      ++ts;
      curx += 2;
    }
    tsrstart += Config.machine.timings.tstatesPerLine;
    ++cury;
  }
  // bottom border
  foreach (immutable y; 0..Config.machine.timings.bottomBorder) {
    auto ts = tsrstart;
    int curx = VBufScrXOfs-Config.machine.timings.leftBorder*2;
    foreach (immutable idx; 0..Config.machine.timings.tstatesPerLine-Config.machine.timings.horizontalRetrace) {
      if (cury >= 0 && cury < VBufHeight && curx >= 0 && curx+2 <= VBufWidth) {
        tsraypos[ts].vbofs = cury*VBufWidth+curx;
      }
      tsraypos[ts].border = true; // draw border
      ++ts;
      curx += 2;
    }
    tsrstart += Config.machine.timings.tstatesPerLine;
    ++cury;
  }
  //conwriteln(Config.machine.timings.topLeftPixel+Config.machine.timings.tstatesPerLine*192);
  //foreach (immutable idx, const ref ri; tsraypos) conwriteln("ts:", idx, "; ri=", ri);
}


// ////////////////////////////////////////////////////////////////////////// //
class Z80CPU : ZymCPU {
  static struct PortInMask { ushort mask, value; string dsc; }
  static struct PortOutMask { ushort mask, value; string dsc; }

  bool resetToTRDOS;

  ubyte ayReg;
  ubyte[16] ayState;
  ubyte[] zxscr;
  ubyte zxUlaOut;
  ubyte p7ffd;

  ubyte[9] keybState; // 8: kempston

  uint zxNextScreenTS = 0;

  bool kmouseWasIn;

  this () {
    super();
    setupMemory();
  }

  final nextFrame () nothrow @trusted {
    if (tstates >= tsmax) {
      tstates %= tsmax;
      if (kmouseWasIn) {
        if (kmouseInCount < 0) kmouseInCount = 0;
        if (kmouseInCount < KMouseInMax) {
          ++kmouseInCount;
        } else {
          if (!kmouseGrabbed) {
            conwriteln("mouse: autograb");
            kmouseGrabbed = true;
          }
        }
      } else {
        if (kmouseInCount > 0) kmouseInCount = 0;
        if (kmouseInCount > -KMouseInIdle) {
          --kmouseInCount;
        } else {
          if (kmouseGrabbed) {
            conwriteln("mouse: autorelease");
            kmouseGrabbed = false;
          }
        }
      }
      kmouseWasIn = false;
    }
  }

  // active screen bank
  final @property ubyte curScreenBank () const nothrow @trusted @nogc {
    pragma(inline, true);
    return 5+((p7ffd>>2)&0x02);
  }

  // active ROM bank
  final @property ubyte curROMBank () const nothrow @trusted @nogc {
    pragma(inline, true);
    return ((p7ffd>>4)&0x01);
  }

  // active RAM bank
  final @property ubyte curRAMBank () const nothrow @trusted @nogc {
    pragma(inline, true);
    return
      Config.machine.mem < 512 ? (p7ffd&0x07) :
      cast(ubyte)((p7ffd&0x07)+((p7ffd&0xc0)>>3));
  }

  // is bank switching locked?
  final @property bool isLock128 () const nothrow @trusted @nogc {
    pragma(inline, true);
    return (p7ffd&0x20 ? true : false);
  }

  final void zxScrUpdateReset () nothrow @trusted @nogc {
    //conwriteln("zxScrUpdateReset()");
    zxNextScreenTS = 0;
    vbofslast = uint.max;
  }

  ubyte zxscrbmp0, zxscrattr0; // we have to remember it for non-contended models
  ubyte zxscrbmp1, zxscrattr1; // we have to remember it for non-contended models
  uint vbofslast = uint.max;

  final void zxScrUpdateAt (uint ts) nothrow @trusted @nogc {
    /*
    void draw8pix (uint vbofs, ubyte scr, ubyte iattr) nothrow @trusted @nogc {
      auto dvp = zxscr.ptr+vbofs;
      ubyte pattr = (iattr&0xc0)|((iattr&0x07)<<3)|((iattr>>3)&0x07);
      foreach (immutable dx; 0..8) {
        *dvp++ = (scr&0x80 ? iattr : pattr);
        scr <<= 1;
      }
    }
    */

    void draw2pix(ubyte firstbit) (uint vbofs, ubyte scr, ubyte iattr) nothrow @trusted @nogc {
      auto dvp = zxscr.ptr+vbofs;
      ubyte pattr = (iattr&0xc0)|((iattr&0x07)<<3)|((iattr>>3)&0x07);
      *dvp++ = (scr&(0x01<<firstbit) ? iattr : pattr);
      *dvp   = (scr&(0x01<<(firstbit-1)) ? iattr : pattr);
    }

    //conwriteln("zxScrUpdateAt: ", zxNextScreenTS, " to ", ts);
    auto scrmemp = zxram.ptr+0x4000*curScreenBank;
    auto tpl = tsraypos.length;
    while (zxNextScreenTS <= ts) {
      if (zxNextScreenTS >= tpl) { zxNextScreenTS = ts; break; }
      auto ri = tsraypos.ptr+(zxNextScreenTS++);
      if (ri.valid) {
        //conwriteln("updating at ", zxNextScreenTS, "; ri.x=", ri.x, "; ri.y=", ri.y, "; ri.addr=", ri.addr, "; ri.attr=", ri.attr);
        if (ri.border) {
          //assert(ri.x >= 0 && ri.y >= 0 && ri.x+2 <= VBufWidth && ri.y < VBufHeight);
          if (vbofslast != uint.max) {
            assert(vbofslast == ri.vbofs);
            draw2pix!1(vbofslast, zxscrbmp1, zxscrattr1);
            vbofslast = uint.max;
          } else {
            // border, 2 pixels
            auto dvp = zxscr.ptr+ri.vbofs;
            *dvp++ = zxBorderColor;
            *dvp   = zxBorderColor;
          }
        }
        if (ri.scr) {
          switch (ri.tsrds) {
            case 0: zxscrbmp0 = scrmemp[ri.addr]; if (!ri.border) { assert(vbofslast != uint.max); draw2pix!1(vbofslast, zxscrbmp1, zxscrattr1); vbofslast = uint.max; } break;
            case 1: zxscrattr0 = scrmemp[ri.attr]; draw2pix!7(ri.vbofs, zxscrbmp0, zxscrattr0); break;
            case 2: draw2pix!5(ri.vbofs, zxscrbmp0, zxscrattr0); break;
            case 3: zxscrbmp1 = scrmemp[ri.addr]; draw2pix!3(ri.vbofs, zxscrbmp0, zxscrattr0); break;
            case 4: zxscrattr1 = scrmemp[ri.attr]; draw2pix!1(ri.vbofs, zxscrbmp0, zxscrattr0); break;
            case 5: draw2pix!7(ri.vbofs, zxscrbmp1, zxscrattr1); break;
            case 6: draw2pix!5(ri.vbofs, zxscrbmp1, zxscrattr1); break;
            case 7: draw2pix!3(ri.vbofs, zxscrbmp1, zxscrattr1); vbofslast = ri.vbofs+2; break;
            default: break;
          }
        }
      }
    }
    //conwriteln("zxScrUpdateAt: DONE at ", zxNextScreenTS);
  }

  final void zxScrUpdateFlush () nothrow @trusted @nogc {
    //conwriteln("zxScrUpdateFlush()");
    zxScrUpdateAt(Config.machine.timings.tstatesPerFrame-1);
    zxScrUpdateReset();
  }

  final void rebuildZXScreen () nothrow @trusted @nogc {
    zxScrUpdateReset();
    zxScrUpdateFlush();
  }

  final @property uint tsmax () const nothrow @trusted @nogc {
    pragma(inline, true);
    return Config.machine.timings.tstatesPerFrame;
  }

  final @property uint intrLen () const nothrow @trusted @nogc {
    pragma(inline, true);
    return Config.machine.timings.interruptLength;
  }

  final void fillSnap (ref ZXSnap zxs) nothrow @trusted {
    zxs.model = ZXSnap.Model.zx48;
    zxs.lateTimings = false;
    zxs.halted = halted;
    zxs.lastWasEI = (prevWasEIDDR == EIDDR.BlockInt);
    zxs.AF = AF;
    zxs.BC = BC;
    zxs.DE = DE;
    zxs.HL = HL;
    zxs.AFx = AFx;
    zxs.BCx = BCx;
    zxs.DEx = DEx;
    zxs.HLx = HLx;
    zxs.IX = IX;
    zxs.IY = IY;
    zxs.SP = SP;
    zxs.PC = PC;
    zxs.MEMPTR = MEMPTR;
    zxs.I = I;
    zxs.R = R;
    zxs.IFF1 = IFF1;
    zxs.IFF2 = IFF2;
    zxs.IM = IM;
    zxs.tstates = tstates;
    zxs.intReqLen = Config.machine.timings.interruptLength;

    zxs.border = zxBorderColor;
    zxs.p7FFD = p7ffd;
    zxs.p1FFD = 0x00;
    zxs.pEFF7 = 0x00;
    zxs.pFE = zxUlaOut;

    zxs.trdosPaged = false;

    zxs.ayType = ZXSnap.AYType.AY128;
    zxs.ayCyrReg = ayReg;
    zxs.ayRegs = ayState[];

    void saveRAMPage (ubyte page) {
      zxs.ram[page].length = ZXSnap.PageSize;
      zxs.ram[page][] = zxram[0x4000*page..0x4000*(page+1)];
    }

    if (Config.machine.mem == 48) {
      saveRAMPage(5);
      saveRAMPage(2);
      saveRAMPage(0);
    } else {
      foreach (immutable ubyte pg; 0..8) saveRAMPage(pg);
    }
  }

  final void realizeSnap (in ref ZXSnap zxs) {
    soundResetAY();
    keybState[] = 0xff;
    zxKeyboardStateDown[] = 0xff;
    zxKeyboardStateUp[] = 0xff;

    prevWasEIDDR = (zxs.lastWasEI ? EIDDR.BlockInt : EIDDR.Normal);
    halted = zxs.halted;
    tstates = zxs.tstates%tsmax;
    MEMPTR = zxs.MEMPTR;
    AF = zxs.AF;
    BC = zxs.BC;
    DE = zxs.DE;
    HL = zxs.HL;
    AFx = zxs.AFx;
    BCx = zxs.BCx;
    DEx = zxs.DEx;
    HLx = zxs.HLx;
    IX = zxs.IX;
    IY = zxs.IY;
    SP = zxs.SP;
    PC = prevPC = lastPC = zxs.PC;
    I = zxs.I;
    R = zxs.R;
    IFF1 = zxs.IFF1;
    IFF2 = zxs.IFF2;
    IM = zxs.IM;
    conprintf("SNA: PC=#%04X SP=#%04X tstates=%s I=#%02X R=#%02X IFF1=%s IFF2=%s IM=%s prevWasEIDDR=%s halted=%s\n", PC, SP, tstates, I, R, IFF1, IFF2, IM, prevWasEIDDR, halted);
    conprintf("AF :#%04X BC :#%04X DE :#%04X HL :#%04X IX:#%04X\n", AF.w, BC.w, DE.w, HL.w, IX.w);
    conprintf("AF':#%04X BC':#%04X DE':#%04X HL':#%04X IY:#%04X\n", AFx.w, BCx.w, DEx.w, HLx.w, IY.w);

    zxBorderColor = zxs.border&0x07;
    //zxBorderColor = 7;
    zxUlaOut = zxs.pFE;

    tapeCurrentIs48K = false;
    final switch (zxs.model) with (ZXSnap.Model) {
      case zx48: zxmodel = "48"; tapeCurrentIs48K = true; break;
      case zx128: zxmodel = "128"; break;
      case plus2: zxmodel = "plus2"; break;
      case plus2a: zxmodel = "plus2a"; break;
      case plus3: zxmodel = "plus3"; break;
      case plus3e: zxmodel = "plus3e"; break;
      case pentagon128: zxmodel = "pentagon"; break;
      case pentagon512: zxmodel = "pentagon512"; break;
      case pentagon1024: zxmodel = "pentagon1024"; break;
    }
    conprintf("ULA:#%02X  7FFD:#%04X  [%s]\n", zxUlaOut, zxs.p7FFD, zxmodel);
    zxInitModel();
    emuFullFrameTS = 0;

    if (zxs.ayType == ZXSnap.AYType.AY128) {
      ayReg = zxs.ayCyrReg&0x0f;
      ayState[] = zxs.ayRegs[];
      foreach (ubyte idx; 0..16) soundWriteAY(idx, zxs.ayRegs[idx], 0);
    }

    void loadRAMPage (ubyte page) {
      if (zxs.ram[page].length != ZXSnap.PageSize) return;
      zxram[0x4000*page..0x4000*(page+1)] = zxs.ram[page][];
    }

    foreach (immutable ubyte pg; 0..256) loadRAMPage(pg);

    p7ffd = zxs.p7FFD;
    setupMemory();
    rebuildZXScreen();
  }

  override void reset (bool poweron=false) {
    ayReg = 0;
    ayState[] = 0;
    tstates = 0;
    zxNextScreenTS = 0;
    zxBorderColor = 0;
    zxUlaOut = 0xff;
    p7ffd = (Config.machine.mem == 48 ? 0x20 : 0x00); // locked for 48K
    emuFullFrameTS = 0;
    soundResetAY();
    soundReinit();
    trdosPaged = false;
    super.reset(poweron);
    if (resetToTRDOS) {
      resetToTRDOS = false;
      if (trdosAvail) {
        conwriteln("reset to TR-DOS...");
        if (Config.machine.mem > 48) p7ffd |= 0x10; // turn on BASIC ROM
        trdosPaged = true;
        setupMapping();
      }
    }
  }

  // use p7ffd, fix mapping
  final void setupMapping () nothrow @trusted @nogc {
    //conwritefln!"setupMapping: 0x%04X"(p7ffd);

    // 16KB
    void setMap (ushort addr, bool rom, bool wrhook, ubyte banknum, bool trdos=false) {
      static assert(0x4000%MemPage.Size == 0);
      if ((addr&0x3FFF) != 0) assert(0, "WTF?!");
      ubyte* mempx = (rom ? (trdos ? zxtrd.ptr : zxrom.ptr) : zxram.ptr)+0x4000*banknum;
      uint idx = addr/MemPage.Size;
      foreach (immutable iofs; 0..0x4000/MemPage.Size) {
        mem[idx] = MemPage.init;
        mem[idx].rom = rom;
        mem[idx].writeHook = wrhook;
        mem[idx].mem = mempx;
        mempx += MemPage.Size;
        if (!rom && Config.machine.contention != ZXContention.None) {
          switch (banknum) {
            case 1:
            case 3:
            case 5:
            case 7:
              mem[idx].contended = true;
              break;
            default: break;
          }
        }
        ++idx;
      }
    }

    //foreach (immutable idx; 0..65536/MemPage.Size) mem[idx].mem = null;
    // setup ROM
    if (trdosAvail && trdosPaged) {
      setMap(0x0000, rom:true, wrhook:false, banknum:0, trdos:true);
    } else {
      setMap(0x0000, rom:true, wrhook:false, banknum:curROMBank());
    }
    // setup first RAM page
    setMap(0x4000, rom:false, wrhook:(curScreenBank == 5), banknum:5);
    // setup second RAM page
    setMap(0x8000, rom:false, wrhook:false, banknum:2);
    // setup third RAM page
    setMap(0xC000, rom:false, wrhook:(curRAMBank == curScreenBank), banknum:curRAMBank());
    // just in case
    //foreach (immutable idx; 0..65536/MemPage.Size) if (mem[idx].mem is null) assert(0, "WTF?!");
  }

  override void setupMemory () {
    setupMapping();
    if (Config.machine.contention == ZXContention.None) {
      ulacont = null;
      ulacontport = null;
      memContended = false;
    } else {
      ulacont = zxulacont;
      ulacontport = zxulacontport;
      memContended = true;
    }
  }

  override void execHook () nothrow @trusted {
    if (!trdosAvail) {
      if (trdosPaged) { trdosPaged = false; setupMapping(); } // just in case
      return;
    }
    uint addr = PC;
    bool otp = trdosPaged;
    if (trdosPaged) {
      // page out TR-DOS?
      if (addr >= 0x4000) {
        //conwritefln!"leavehook: PC=#%04X (prevPC=#%04X; lastPC=#%04X)"(PC, prevPC, lastPC);
        trdosPaged = false;
      }
    } else {
      if (Config.machine.mem > 48) {
        // !48k
        //conwritefln!"enterhook: PC=#%04X (#%02X)"(PC, p7ffd&0x10);
        if ((addr&0xff00) == 0x3D00 && curROMBank == 1) {
          //conwritefln!"enterhook: PC=#%04X (prevPC=#%04X; lastPC=#%04X)"(PC, prevPC, lastPC);
          trdosPaged = true;
        }
      } else {
        // 48k
        if ((addr&0xfe00) == 0x3C00) {
          //conwritefln!"enterhook: PC=#%04X (prevPC=#%04X; lastPC=#%04X)"(PC, prevPC, lastPC);
          trdosPaged = true;
        }
      }
    }
    if (otp != trdosPaged) setupMapping();
    if (Config.WD93.trdTraps && trdosPaged && PC < 0x4000) {
      wd1793.trdosTraps();
    }
  }

  override void memWriteHook (ushort addr, ubyte b) nothrow @trusted {
    if (!Config.maxSpeed) {
      if (addr >= 0x4000 && addr < 0x4000+6912) {
        if (curScreenBank == 5) zxScrUpdateAt(tstates);
      } else if (addr >= 0xC000 && addr < 0xC000+6912) {
        if (curScreenBank == curRAMBank) zxScrUpdateAt(tstates);
      }
    }
  }

  final ubyte ulaIn (ushort port) nothrow @trusted {
    ubyte r = (Config.rockULAIn ? 0xff : zxUlaOut), p1 = (port>>8);
    ubyte[8] ks = keybState[0..8];
    //bool af = false; //isAutofireKeyboard();
    //for (int f = 0; f < 8; ++f) ks[f] = zxKeyboardState[f];
    //if (af) ks[af>>8] &= ~(af&0xff);

    // emulate keyboard matrix effect
    if (Config.kbdMatrix) {
      bool done;
      do {
        done = true;
        foreach (immutable k; 0..7) {
          foreach (immutable j; k+1..8) {
            if ((ks[k]|ks[j]) != 0xff && ks[k] != ks[j]) {
              ks[k] = ks[j] = (ks[k]&ks[j]);
              done = false;
            }
          }
        }
      } while (!done);
    }

    foreach (immutable f; 0..8) if ((p1&(1<<f)) == 0) r &= ks[f];

    // check for known tape loaders
    if (optFlashLoad &&
        !trdosPaged &&
        curtape !is null && !tapeNeedRewind && !tapeAutostarted &&
        (PC >= 0x4000 || (Config.machine.mem > 48 ? curROMBank == 1 : true)))
    {
      if (emuTapeFlashLoad()) return r;
    }

    emuTapeLoaderDetector();
    if (isTapePlaying) {
      if (tapeCurEdge()) r |= 0x40; else r &= ~0x40;
      // on is: bit0 is tape microphone bit, bit1 is speaker bit, i.e.: (!!(b&0x10)<<1)+((!(b&0x8))|tape_microphone)
      soundWriteBeeper(tstates, (r&0x40)>>6);
    }

    return r;
  }

  final void ulaOut (ushort port, ubyte value) nothrow @trusted {
    ubyte bc = (value&0x07);
    //if (optBrightBorder > 0) bc |= ((value>>optBrightBorder)&0x01)<<3;
    /*if (zxBorderColor != bc)*/ {
      zxScrUpdateAt(tstates-2);
      zxBorderColor = bc;
      //zxScrUpdateAt(tstates);
    }

    // /*if (!optTapePlaying || !optTapeSound)*/ soundBeeper(value&0x10, z80->tstates);
    // on is: bit0 is tape microphone bit, bit1 is speaker bit, i.e.: (!!(b&0x10)<<1)+((!(b&0x8))|tape_microphone)
    soundWriteBeeper(tstates, (!!(value&0x10)<<1)+(!(value&0x8)));

    if (Config.machine.mem == 48) {
      enum optZXIssue = 0;
      zxUlaOut =
        (optZXIssue == 0 ?
          (value&0x18 ? 0xff : 0xbf) : //issue2
          (value&0x10 ? 0xff : 0xbf)); //issue3, 128k
    } else {
      //128k
      zxUlaOut = (value&0x10 ? 0xff : 0xbf);
      //zxUlaOut = 0xbf; //+3
    }
  }

  override ubyte portRead (ushort port) nothrow @trusted {
    // WD93?
    if (trdosPaged) {
      ubyte p1 = port&0xff;
      // 1F = 0001|1111b
      // 3F = 0011|1111b
      // 5F = 0101|1111b
      // 7F = 0111|1111b
      // DF = 1101|1111b mouse port
      // FF = 1111|1111b
      if ((p1&0x9F) == 0x1F || p1 == 0xFF) {
        // 1F, 3F, 5F, 7F, FF
        return wd1793.inp(p1);
      }
    }
    // kempston mouse?
    if ((port&0x0020) == 0x0000) {
      if ((port|0xFA00) == 0xFADF) {
        ubyte res = 0xff;
        if (Config.kempstonM) {
          res = (~(kmsBut))&0x0f;
          if (Config.kempstonWheel) res |= kmsWheel<<4;
        }
        kmouseWasIn = true;
        return res;
      }
      if ((port|0xFA00) == 0xFBDF) { kmouseWasIn = true; return (Config.kempstonM ? kmsDX : 0xff); }
      if ((port|0xFA00) == 0xFFDF) { kmouseWasIn = true; return (Config.kempstonM ? kmsDY : 0xff); }
      // kempston joystick?
      return (Config.kempstonJ ? ((~(keybState[8]))&0x1f) : 0xff);
    }
    // AY?
    if (Config.ayPortRead && (port&0xC002) == 0xC000) {
      if (ayReg == 14) return (ayState[7]&0x40 ? 0xbf&ayState[ayReg] : 0xbf);
      if (ayReg == 15 && (ayState[7]&0x80) == 0) return 0xff;
      return ayState[ayReg];
    }
    // ULA?
    if ((port&0x0001) == 0x0000) return ulaIn(port);
    // floating bus
    if (Config.floatingBus && (!Config.machine.pentagon || optPentagonFloatingBus) && tstates >= 0 && tstates < tsraypos.length) {
      auto ri = tsraypos.ptr+tstates;
      if (ri.valid && ri.scr) {
        //    0: bmp fetch from [addr]
        //    1: attr fetch from [attr]
        //    2: draw screen with (0) and (1)
        //    3: bmp fetch from [addr]
        //    4: attr fetch from [attr]
        //    5: draw screen with (2) and (3)
        //    6: nothing
        //    7: nothing
        switch (ri.tsrds) {
          case 0: case 3:
            if (dbgFBusHitDump) conwritefln!"FLOATINGBUS: ts=%d; tsrds=%d"(tstates, ri.tsrds);
            return (zxram.ptr+0x4000*curScreenBank)[ri.addr]; // bmp
          case 1: case 2: case 4: case 5:
            if (dbgFBusHitDump) conwritefln!"FLOATINGBUS: ts=%d; tsrds=%d"(tstates, ri.tsrds);
            return (zxram.ptr+0x4000*curScreenBank)[ri.attr]; // attr
          default: break;
        }
      }
    }
    return 0xff;
  }

  override void portWrite (ushort port, ubyte value) nothrow @trusted {
    // WD93?
    if (trdosPaged) {
      ubyte p1 = port&0xff;
      if ((p1&0x1F) == 0x1F) {
        // 1F, 3F, 5F, 7F, FF
        wd1793.outp(p1, value);
        return;
      }
    }
    // AY register?
    if ((port&0xC002) == 0xC000) {
      ayReg = value&0x0f;
      //return;
    }
    // AY value?
    if ((port&0xC002) == 0x8000) {
      soundWriteAY(ayReg, value, tstates);
      switch (ayReg) {
        case 1: case 3: case 5: case 13: value &= 0x0f; break;
        case 6: case 8: case 9: case 10: value &= 0x1f; break;
        default: break;
      }
      ayState[ayReg] = value;
      //return;
    }
    // 128 paging?
    if (Config.machine.mem > 48) {
      if ((port&0x8002) == 0x0000) {
        if (p7ffd != value && !isLock128) {
          p7ffd = value;
          version(none) {
            __gshared ubyte prevSB = 5;
            ubyte newSB = 5+((p7ffd>>2)&0x02);
            if (newSB != prevSB) {
              conwriteln("screen bank change; ffts=", emuFullFrameTS, "; tstates=", tstates, "; prevSB=", prevSB, "; newSB=", newSB);
              prevSB = newSB;
            }
          }
          setupMapping();
        }
        //return;
      }
    }
    // ULA?
    if ((port&0x0001) == 0) ulaOut(port, value);
  }

  // returns next PC
  final uint disasmAt(bool showTS=false) (uint pc) nothrow @trusted {
    import core.stdc.stdio : sprintf;
    char[128] buf = void;
    ZDisop dop;

    pc &= 0xffff;
    disasm(dop, cast(ushort)pc);

    char* bp = buf.ptr;
    bp += sprintf(bp, "%04X: ", cast(uint)pc);
    foreach (immutable ofs; 0..6) {
      if (ofs < dop.len) {
        bp += sprintf(bp, "%02X ", cast(uint)(memPeekB(cast(ushort)(pc+ofs))));
      } else {
        bp += sprintf(bp, "   ");
      }
    }

    bp += sprintf(bp, "%.*s", dop.mnemo.length, dop.mnemo.ptr);

    foreach (immutable idx, const(char)[] arg; dop.args[]) {
      if (arg.length == 0) break;
      if (idx != 0) {
        bp += sprintf(bp, ",");
      } else {
        foreach (immutable _; dop.mnemo.length..6) bp += sprintf(bp, " ");
      }
      bp += sprintf(bp, "%.*s", arg.length, arg.ptr);
    }

    static if (showTS) {
      while (cast(usize)(bp-buf.ptr) < 44) *bp++ = ' ';
      bp += sprintf(bp, "; TS=%u", cast(uint)tstates);
    }

    conwriteln(buf[0..cast(usize)(bp-buf.ptr)]);

    return (pc+dop.len)&0xffff;
  }

  final void disPC () nothrow @trusted { disasmAt!true(PC); }
}

__gshared Z80CPU z80cpu;
__gshared bool paused = false;


// ////////////////////////////////////////////////////////////////////////// //
__gshared ubyte[] zxscr;
__gshared ubyte[] zxscro; // for "noflic"
__gshared bool zxFlashState;

// keyboard state works as follows:
//   for "key down" it goes to both `zxKeyboardStateDown` *and* in `zxKeyboardStateUp`
//   for "key up" it is goes to `zxKeyboardStateUp`
// when frame starts, `zxKeyboardStateDown` copied to the current state
// when frame ends, `zxKeyboardStateUp` is copied to `zxKeyboardStateDown`
__gshared ubyte[9] zxKeyboardStateDown; // 8: kempston
__gshared ubyte[9] zxKeyboardStateUp; // 8: kempston
__gshared uint[uint] zxKeyBinds; // key: Key; value: portmask, portmask
__gshared uint[char] zxCharBinds; // key: char; value: portmask, portmask

__gshared ubyte lastScrBank = 5;
__gshared uint scrBankSwitchCount = 0;


void zxDoKeyUpDown (ushort portmask, bool isdown) {
  if (portmask != 0) {
    synchronized(z80cpu) {
      if (isdown) {
        zxKeyboardStateDown[(portmask>>8)&0xff] &= ~(portmask&0xff);
        zxKeyboardStateUp[(portmask>>8)&0xff] &= ~(portmask&0xff);
      } else {
        zxKeyboardStateUp[(portmask>>8)&0xff] |= (portmask&0xff);
      }
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared uint[64] zxCharRelease; // !0: release this zx key (portmask) after hiword frames
__gshared uint zxCharReleaseCount;

void zxPushRelease (ushort mask) {
  if (mask == 0 || zxCharReleaseCount == zxCharRelease.length) return;
  zxCharRelease[zxCharReleaseCount++] = mask|0x01_0000;
}


void zxDoReleases () {
  uint pos = 0;
  while (pos < zxCharReleaseCount) {
    if ((zxCharRelease[pos]>>16) <= 1) {
      zxDoKeyUpDown(zxCharRelease[pos]&0xffff, false);
      foreach (immutable int cc; pos+1..zxCharReleaseCount) zxCharRelease[cc-1] = zxCharRelease[cc];
      --zxCharReleaseCount;
    } else {
      zxCharRelease[pos] -= 0x01_0000;
      ++pos;
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
void emuPauseSound () {
  if (!sndvEnabled) return;
  soundDeinit();
}


void emuResumeSound () {
  if (sndvEnabled) {
    if (!emuSoundEnabled) emuPauseSound();
  } else if (emuSoundEnabled) {
    soundInit();
    emuSoundEnabled = sndvEnabled;
    if (sndvEnabled) {
      soundResetAY();
      foreach (ubyte idx; 0..16) soundWriteAY(idx, z80cpu.ayState[idx], /*z80cpu.tstates*/0);
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
void emuSignalNewScreen () {
  __gshared ubyte flashTimer;
  __gshared bool flashState;

  if (Config.noFlicDetector) {
    //conwriteln("pb:", lastScrBank, "; cb:", z80cpu.curScreenBank, "; cnt:", scrBankSwitchCount);
    if (z80cpu.curScreenBank != lastScrBank) {
      lastScrBank = z80cpu.curScreenBank;
      if (!noFlicDetected) {
        if (++scrBankSwitchCount >= 4) {
          noFlicDetected = true;
          if (optNoFlicDetectMsg) conwriteln("NOFLIC activated");
        }
      }
    } else {
      scrBankSwitchCount = 0;
      if (noFlicDetected && optNoFlicDetectMsg) conwriteln("NOFLIC deactivated");
      noFlicDetected = false;
    }
  }

  synchronized(Z80CPU.classinfo) {
    if (Config.noFlic || noFlicDetected) zxscro[] = zxscr[];
    zxscr[] = z80cpu.zxscr[];
    zxFlashState = flashState;
  }
  //if (!evScrReady.hasSomething) evScrReady.send(EventId.ScreenReady);
  if (vbwin !is null && !vbwin.eventQueued!ScreenReadyEvent()) vbwin.postEvent(evScrReady);
  if (++flashTimer >= 16) { flashTimer = 0; flashState = !flashState; }
}


void emuIntr () {
  while (z80cpu.tstates < z80cpu.intrLen) {
    auto ots = z80cpu.tstates;
    auto ii = z80cpu.intr();
    //conwriteln("ots=", ots, "; ts=", z80cpu.tstates, "; ilen=", z80cpu.intrLen, "; ii=", ii);
    if (z80cpu.tstates >= z80cpu.intrLen) break;
    if (dbgExecDump) z80cpu.disPC();
    z80cpu.execStep();
  }
}


void emuFrame () {
  synchronized(z80cpu) {
    z80cpu.keybState[] = zxKeyboardStateDown[];
    zxKeyboardStateDown[] = zxKeyboardStateUp[];
  }
  if (z80cpu.tstates >= z80cpu.tsmax) {
    tapeNextFrame();
    emuFullFrameTS += z80cpu.tstates;
    z80cpu.zxScrUpdateFlush();
    emuSignalNewScreen();
    soundFrame();
    z80cpu.nextFrame();
  }
  emuIntr();
  if (dbgExecDump) {
    while (z80cpu.tstates < z80cpu.tsmax) {
      z80cpu.disPC();
      z80cpu.execStep();
    }
    //conwriteln("DONE");
  } else {
    z80cpu.nextEventTS = z80cpu.tsmax;
    z80cpu.exec();
  }
}


void emuThread (Tid ownerTid) {
  bool processConsoleCommands () {
    consoleLock();
    scope(exit) consoleUnlock();
    auto ccwasempty = conQueueEmpty();
    conProcessQueue();
    return (!ccwasempty && conQueueEmpty());
  }

  auto oldVolBeeper = Config.volumeBeeper;
  auto oldVolAY = Config.volumeAY;
  auto oldStereo = Config.stereoType;
  auto oldSpeaker = Config.speakerType;
  auto lastSoundEnabled = emuSoundEnabled;
  auto oldMaxSpeed = Config.maxSpeed;
  int maxspcount = 0;
  bool oldPaused = paused;

  try {
    for (;;) {
      processConsoleCommands();
      if (isQuitRequested()) break;
      if (oldMaxSpeed != Config.maxSpeed) {
        oldMaxSpeed = Config.maxSpeed;
        if (!Config.maxSpeed) z80cpu.rebuildZXScreen(); else maxspcount = 0;
      }
      if (!Config.noFlicDetector) noFlicDetected = false;
      if (oldVolBeeper != Config.volumeBeeper || oldVolAY != Config.volumeAY ||
          oldStereo != Config.stereoType || oldSpeaker != Config.speakerType ||
          lastSoundEnabled != emuSoundEnabled)
      {
        oldVolBeeper = Config.volumeBeeper;
        oldVolAY = Config.volumeAY;
        oldStereo = Config.stereoType;
        oldSpeaker = Config.speakerType;
        if (lastSoundEnabled != emuSoundEnabled) {
          if (lastSoundEnabled) emuPauseSound(); else emuResumeSound();
        } else {
          soundReinit();
        }
        lastSoundEnabled = emuSoundEnabled = sndvEnabled;
      }
      if (!Config.maxSpeed) maxspcount = 0;
      auto fst = clockMicro;
      if (!paused && !Config.maxSpeed) {
        if (emuSoundEnabled && !sndvEnabled) {
          emuResumeSound();
          lastSoundEnabled = emuSoundEnabled = sndvEnabled;
        }
        emuFrame();
        oldPaused = paused;
      } else {
        emuPauseSound();
        if (!paused) {
          emuFrame();
        } else {
          if (oldPaused != paused) emuSignalNewScreen();
        }
        oldPaused = paused;
        if (Config.maxSpeed) {
          if (++maxspcount >= 1000) {
            maxspcount = 0;
            z80cpu.rebuildZXScreen();
            emuSignalNewScreen();
          }
        }
      }
      fst = (clockMicro-fst+500)/1000;
      //conwriteln("frame time: ", fst);
      if (!sndvEnabled && !Config.maxSpeed) {
        if (fst < 20) clockSleepMilli(cast(uint)(20-fst));
      }
    }
    //evScrReady.send(EventId.Quit);
    if (vbwin !is null) vbwin.postEvent(new QuitEvent()); // it is ok to not check it here
  } catch (Throwable e) {
    // here, we are dead and fucked (the exact order doesn't matter)
    import core.stdc.stdlib : abort;
    import core.stdc.stdio : fprintf, stderr;
    import core.memory : GC;
    import core.thread : thread_suspendAll;
    GC.disable(); // yeah
    thread_suspendAll(); // stop right here, you criminal scum!
    auto s = e.toString();
    fprintf(stderr, "\n=== FATAL ===\n%.*s\n", cast(uint)s.length, s.ptr);
    abort(); // die, you bitch!
  }
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared bool cliWantReset = false;
__gshared bool cliDiskLoaded = false;
__gshared int loadAnyCLIDrive = 0;


// ////////////////////////////////////////////////////////////////////////// //
enum ImageType {
  Unknown,
  Disk,
  HoBeta,
  Snapshot,
  Tape,
}


ImageType loadAnyImg (ConString drv, ConString fname) {
  bool incCli = false;
  if (fname.length == 0 && drv.length == 0) { conwriteln("load: wutafuck?!"); return ImageType.Unknown; }
  if (fname.length == 0) {
    int dnum = parseDiskName(drv);
    if (dnum >= 0) { conwriteln("load: filename?"); return ImageType.Unknown; }
    fname = drv;
    switch (loadAnyCLIDrive) {
      case -1: drv = "A:"; break;
      case 0: incCli = true; drv = "A:"; break;
      case 1: incCli = true; drv = "B:"; break;
      case 2: incCli = true; drv = "C:"; break;
      case 3: incCli = true; drv = "D:"; break;
      case 4: incCli = true; drv = "E:"; break; // hack for snapshots
      default: conwriteln("too many disk images"); return ImageType.Unknown;
    }
  }
  try {
    auto flx = xfopenEx(fname, (fn) => xfIsDiskExt(fn) || xfIsHoBetaExt(fn) || xfIsSnapExt(fn) || xfIsTapeExt(fn));
    // snapshot?
    if (flx.hasSnap) {
      auto fl = flx.snap;
      ZXSnap snap;
      z80cpu.fillSnap(snap);
      snap.load(fl, fl.name);
      z80cpu.realizeSnap(snap);
      cliDiskLoaded = false;
      conwriteln("loaded snapshot '", fl.name, "'");
      return ImageType.Snapshot;
    }
    if (drv.length == 0) drv = "a:";
    if (drv.length > 2 || (drv.length == 2 && drv[1] != ':')) {
      conwriteln("invalid drive name: '", drv, "'");
      return ImageType.Unknown;
    }
    ubyte dnum = 0;
    switch (drv[0]) {
      case 'A': case 'a': case '0': dnum = 0; break;
      case 'B': case 'b': case '1': dnum = 1; break;
      case 'C': case 'c': case '2': dnum = 2; break;
      case 'D': case 'd': case '3': dnum = 3; break;
      default: conwriteln("invalid drive name: '", drv, "'"); return ImageType.Unknown;
    }
    // disk image?
    if (flx.hasDisk) {
      bool hasAnyDisk = false;
      bool onlyHoBeta = true;
      int didx = 0; // in OpenEx
      while (dnum < 4 && didx < flx.disk.length) {
        if (!flx.hasDisk(didx)) { ++didx; continue; }
        auto fl = flx.disk[didx++];
        auto fsz = fl.size;
        if (fsz < 14) { conwriteln("invalid disk image: '", fl.name, "'"); return ImageType.Unknown; }
        if (fsz > wdsnapbuf.length) fsz = wdsnapbuf.length;
        wdsnapbuf[] = 0;
        wdsnapsize = cast(uint)fsz;
        fl.rawReadExact(wdsnapbuf[0..wdsnapsize]);
        if (fl.name.xfIsHoBetaExt) {
          // HoBeta
          if (!wd1793.fdd[dnum].readHOB()) { conwriteln("invalid HoBeta file: '", fl.name, "'"); return ImageType.Unknown; }
          if (dnum == 0) cliDiskLoaded = true;
          conwriteln("loaded HoBeta file '", fl.name, "' to drive '", cast(char)('A'+dnum), ":'");
          hasAnyDisk = true;
        } else {
          // disk image
          if (!wd1793.fdd[dnum].readDiskImage()) { conwriteln("unknown disk image format: '", fl.name, "'"); return ImageType.Unknown; }
          if (dnum == 0) cliDiskLoaded = true;
          conwriteln("loaded disk '", fl.name, "' to drive '", cast(char)('A'+dnum), ":'");
          hasAnyDisk = true;
          onlyHoBeta = false;
          if (incCli) ++loadAnyCLIDrive;
          ++dnum; // next TR-DOS drive
        }
      }
      if (hasAnyDisk) {
        if (incCli && onlyHoBeta) ++loadAnyCLIDrive;
        return (onlyHoBeta ? ImageType.HoBeta : ImageType.Disk);
      }
    }
    static if (LibSpectrumHere) {
      if (flx.hasTape) {
        emuEjectTape();
        curtape = libspectrum_tape_alloc();
        if (curtape is null) { conwriteln("cannot allocate tape structure"); return ImageType.Unknown; }
        if (flx.tape.size > 1024*1024*32) { conwriteln("tape file too big"); return ImageType.Unknown; }
        if (flx.tape.size < 16) { conwriteln("tape file too small"); return ImageType.Unknown; }
        auto buf = new ubyte[](cast(uint)flx.tape.size);
        scope(exit) delete buf;
        flx.tape.seek(0);
        flx.tape.rawReadExact(buf);
        auto tres = libspectrum_tape_read(curtape, buf.ptr, buf.length, LIBSPECTRUM_ID_UNKNOWN);
        if (tres != LIBSPECTRUM_ERROR_NONE && tres != LIBSPECTRUM_ERROR_WARNING) { conwriteln("unknown tape format"); return ImageType.Unknown; }
        emuRewindTape(); // reset necessary flags
        conwriteln("loaded tape image from '", flx.tape.name, "'");
        // autorun tape
        if (optTapeAutorun) {
          VFile szx;
               if (zxmodel == "48") { szx = wrapMemoryRO(autotape48[]); }
          else if (zxmodel == "128" || zxmodel == "plus2" || zxmodel == "plus2a" || zxmodel == "plus3" || zxmodel == "plus3e") szx = wrapMemoryRO(autotape128[]);
          else if (zxmodel == "pentagon" || zxmodel == "pentagon512" || zxmodel == "pentagon1024") szx = wrapMemoryRO(autotapep128[]);
          if (szx.isOpen) {
            tapeCurrentIs48K = (zxmodel == "48");
            ZXSnap snap;
            z80cpu.fillSnap(snap);
            snap.load(szx, "autotape.szx");
            z80cpu.realizeSnap(snap);
          }
        }
        return ImageType.Tape;
      }
    }
    conwriteln("cannot guess format of file: ", fname);
  } catch (Exception e) {
    conwriteln("ERROR loading file: '", fname, "'");
    conwriteln("EXC: ", e.toString);
  }
  return ImageType.Unknown;
}


// ////////////////////////////////////////////////////////////////////////// //
bool isPrefix (const(char)[] s, const(char)[] pfx) pure nothrow @trusted @nogc {
  if (pfx.length == 0 || s.length > pfx.length || s.length == 0) return false;
  foreach (immutable idx, char c0; s) {
    if (idx >= pfx.length) break;
    if (c0 >= 'A' && c0 <= 'Z') c0 += 32;
    char c1 = pfx.ptr[idx];
    if (c1 >= 'A' && c1 <= 'Z') c1 += 32;
    if (c0 != c1) return false;
  }
  return true;
}


int parseDiskName (const(char)[] drv, const(char)[] def="a:") {
  if (drv.length == 0) drv = def;
  if (drv.length > 2 || (drv.length == 2 && drv[1] != ':')) return -1;
  switch (drv[0]) {
    case 'A': case 'a': case '0': return 0;
    case 'B': case 'b': case '1': return 1;
    case 'C': case 'c': case '2': return 2;
    case 'D': case 'd': case '3': return 3;
    default: break;
  }
  return -1;
}


const(char)[] fixKeyName (const(char)[] key) {
  switch (key) {
    case "0": key = "N0"; break;
    case "1": key = "N1"; break;
    case "2": key = "N2"; break;
    case "3": key = "N3"; break;
    case "4": key = "N4"; break;
    case "5": key = "N5"; break;
    case "6": key = "N6"; break;
    case "7": key = "N7"; break;
    case "8": key = "N8"; break;
    case "9": key = "N9"; break;
    default:
  }
       if (key.strEquCI("return")) key = "enter";
  else if (key.strEquCI("esc")) key = "escape";
  else if (key.strEquCI("win")) key = "windows";

  return key;
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared SimpleWindow vbwin;
__gshared bool vbfocused = false;
__gshared uint zxtexid; // OpenGL texture id
__gshared ubyte[] zxscrlast; // emulation thread will copy the built screen there (byte per pixel, spectrum attrs, use ink to draw)
__gshared ubyte[] zxscrlasto; // previous emu screen for "noflic" mode
__gshared float v_scanmult1 = 0.80f;
__gshared float v_scanmult2 = 0.75f;
__gshared bool v_scanlines = true;
__gshared bool v_shader_tried = false;


// ////////////////////////////////////////////////////////////////////////// //
void initConsole () {
  conRegVar!v_scanlines("v_scanlines", "enable scanlines shader?");
  conRegVar!v_scanmult1("v_scanmult1", "color multiplier for scanlines shader (line 1)");
  conRegVar!v_scanmult2("v_scanmult2", "color multiplier for scanlines shader (line 2)");

  conRegVar!VBufScale(1, 4, "v_scale", "window scale: [1..3]");

  conRegVar!bool("v_vsync", "sync to video refresh rate? (can skip frames and look jerky)",
    (ConVarBase self) => vbufVSync,
    (ConVarBase self, bool nv) {
      if (vbufVSync != nv) {
        vbufVSync = nv;
        //if (evScrReady !is null) evScrReady.send(EventId.ScreenRepaint);
        if (vbwin !is null && !vbwin.eventQueued!ScreenRepaintEvent) vbwin.postEvent(new ScreenRepaintEvent());
      }
    },
  );

  conRegVar!uint("v_borderpix", "visible border pixels: [0..48]",
    (ConVarBase self) => visibleBorderPixels,
    (ConVarBase self, uint nv) {
      if (nv > 48) nv = 48;
      if (visibleBorderPixels != nv) {
        visibleBorderPixels = nv;
        //if (evScrReady !is null) evScrReady.send(EventId.ScreenRepaint);
        if (vbwin !is null && !vbwin.eventQueued!ScreenRepaintEvent) vbwin.postEvent(new ScreenRepaintEvent());
      }
    },
  );

  conRegVar!kmouseForceGrab("mouse_grab_force", "grab mouse?");

  conRegVar!bool("keyhelp", "show Speccy keyboard help?",
    (ConVarBase self) => showKeyHelp,
    (ConVarBase self, bool nv) {
      if (showKeyHelp != nv) {
        showKeyHelp = nv;
        //if (evScrReady !is null) evScrReady.send(EventId.ScreenReady); // HACK!
        if (vbwin !is null && evScrReady !is null && !vbwin.eventQueued!ScreenReadyEvent) vbwin.postEvent(evScrReady); // HACK!
      }
    },
  );

  conRegVar!emuSoundEnabled("sound_enabled", "is sound enabled in emu?");

  conRegVar!zxmodel("zx_model", "zx spectrum model");

  conRegVar!memContended("contended", "is current model contended?", ConVarAttr.ReadOnly);

  conRegVar!(Config.volumeBeeper)(0, 400, "volume_beeper", "beeper volume");
  conRegVar!(Config.volumeTape)(0, 400, "volume_tape", "tape volume");
  conRegVar!(Config.volumeAY)(0, 400, "volume_ay", "ay volume");
  conRegVar!(Config.stereoType)("stereo_mode", "AY stereo mode: ABC, ACB, NONE");
  conRegVar!(Config.speakerType)("speaker_type", "speaker type: TV, Beeper, Default, Flat, Crisp");

  conRegVar!(Config.sndBeeperEnabled)("s_beeper", "is beeper audible?");
  conRegVar!(Config.sndTapeEnabled)("s_tape", "is beeper audible?");
  conRegVar!(Config.sndAYEnabled)("s_ay", "is AY audible?");

  conRegVar!(optAYLeds)("leds_ay", "show AY leds?");
  conRegVar!(optPentagonFloatingBus)("pentagon_floating_bus", "emulate floating bus on pentagon?");

  conRegVar!(Config.kempstonJ)("kempston_joystick", "emulate kempston joystick?");
  conRegVar!(Config.kempstonM)("kempston_mouse", "emulate kempston joystick?");
  conRegVar!(Config.kempstonWheel)("kempston_wheel", "emulate kempston mouse wheel?");
  conRegVar!(Config.kempstonMSwapB)("kmouse_swap_buttons", "swap kempston mouse buttons?");

  conRegVar!(Config.rockULAIn)("ula_rock_in", "always start with 0xFF on ULA in?");

  conRegVar!(Config.ayPortRead)("ay_port_read", "allow reading AY ports?");

  conRegVar!(Config.WD93.trdos128model)("trd_model128", "default 128K model for CLI TR-DOS reset");

  conRegVar!(Config.WD93.noDelay)("wd_nodelay", "skip WD93 delays (instant disk)?");
  conRegVar!(Config.WD93.trdInterleave)(0, 2, "trd_interleave", "interleaving for new TR-DOS disks: [0..2]");

  conRegVar!(Config.WD93.trdAddBoot)("trd_addboot", "add boot to boot-less TR-DOS disks?");
  conRegVar!(Config.WD93.trdAddBootToSys)("trd_sysboot", "put boot in unused sectors of track 0?");

  conRegFunc!((ConString fname) {
    defaultBootHob = null;
    if (fname.length == 0) return;
    try {
      auto flx = xfopenEx(fname, &xfIsHoBetaExt);
      if (!flx.hasDisk || !flx.disk[0].name.xfIsHoBetaExt) throw new Exception("fuuuuuu");
      auto fl = flx.disk[0];
      auto fsz = fl.size;
      if (fsz > 1024*40) { conwriteln("boot file too big"); return; }
      if (fsz < 16) { conwriteln("boot file too small"); return; }
      defaultBootHob.length = cast(uint)fsz;
      fl.rawReadExact(defaultBootHob);
      conwriteln("using boot '", fname, "'");
    } catch (Exception e) {
      conwriteln("error loading boot file '", fname, "'");
    }
  })("trd_bootfile", "use default boot from the given file");

  conRegVar!(Config.WD93.trdTraps)("trd_traps", "use TR-DOS traps to speedup disk i/o?");

  conRegVar!(Config.WD93.writeProtA)("wd_writeprot_a", "write protection for A:");
  conRegVar!(Config.WD93.writeProtB)("wd_writeprot_b", "write protection for B:");
  conRegVar!(Config.WD93.writeProtC)("wd_writeprot_c", "write protection for C:");
  conRegVar!(Config.WD93.writeProtD)("wd_writeprot_d", "write protection for D:");

  conRegVar!paused("paused", "emulation paused?");

  conRegVar!(Config.noFlic)("noflic", "\"noflic\" mode (blend two last screens)");
  conRegVar!(Config.noFlicDetector)("detect_noflic", "autodetect \"noflic\" mode?");
  conRegVar!optNoFlicDetectMsg("notify_noflic", "autodetected \"noflic\" mode message?");

  conRegVar!(Config.kbdMatrix)("kbd_matrix", "keyboard matrix effect");

  conRegVar!(Config.floatingBus)("floating_bus", "emulate floating bus?");
  conRegVar!(Config.maxSpeed)("max_speed", "execute at maximum speed?");

  conRegVar!dbgExecDump("dbg_exec_dump", "put execution dump to console (disasm, actually)");
  conRegVar!dbgFBusHitDump("dbg_fbus_dump", "dump some info when floating bus is read");


  conRegFunc!(() { conprintf("screen bank: %u\n", z80cpu.curScreenBank); })("screen_bank", "print current screen bank");

  conRegFunc!((ushort addr) {
    if (z80cpu is null) return;
    ubyte b = z80cpu.memPeekB(addr);
    conprintf("byte: %5u: %-3u  0x%04x: 0x%02x\n", addr, b, addr, b);
  })("peek", "peek byte");

  conRegFunc!((ushort addr) {
    if (z80cpu is null) return;
    uint w = z80cpu.memPeekB(addr)+256*z80cpu.memPeekB(cast(ushort)(addr+1));
    conprintf("word: %5u: %-5u  0x%04x: 0x%04x\n", addr, w, addr, w);
  })("wpeek", "peek word");

  conRegFunc!((ushort addr, ubyte value) {
    if (z80cpu is null) return;
    ubyte b = z80cpu.memPeekB(addr);
    conprintf("old byte: %5u: %-3u  0x%04x: 0x%02x\n", addr, b, addr, b);
    z80cpu.memPokeB(addr, value);
  })("poke", "poke byte");

  conRegFunc!((ushort addr, ushort value) {
    if (z80cpu is null) return;
    uint w = z80cpu.memPeekB(addr)+256*z80cpu.memPeekB(cast(ushort)(addr+1));
    conprintf("old word: %5u: %-5u  0x%04x: 0x%04x\n", addr, w, addr, w);
    z80cpu.memPokeB(addr, value&0xff);
    z80cpu.memPokeB(cast(ushort)(addr+1), (value>>8)&0xff);
  })("wpoke", "poke word");


  conRegFunc!((ushort addr, uint lines=16) {
    if (z80cpu is null) return;
    while (lines-- > 0) {
      addr = cast(ushort)z80cpu.disasmAt(addr);
    }
  })("disasm", "disasm addr [lines:32]");

  conRegFunc!(() {
    if (z80cpu is null) return;
    char[8] flgbuf = "sz.h.pnc";
    if (z80cpu.AF.f&z80cpu.Z80Flag.S) flgbuf[0] -= 32;
    if (z80cpu.AF.f&z80cpu.Z80Flag.Z) flgbuf[1] -= 32;
    if (z80cpu.AF.f&z80cpu.Z80Flag.F5) flgbuf[2] = '5';
    if (z80cpu.AF.f&z80cpu.Z80Flag.H) flgbuf[3] -= 32;
    if (z80cpu.AF.f&z80cpu.Z80Flag.F3) flgbuf[4] = '3';
    if (z80cpu.AF.f&z80cpu.Z80Flag.PV) flgbuf[5] -= 32;
    if (z80cpu.AF.f&z80cpu.Z80Flag.N) flgbuf[6] -= 32;
    if (z80cpu.AF.f&z80cpu.Z80Flag.C) flgbuf[7] -= 32;
    conprintf("AF:%04X  AF':%04X  SP:%04X  IR: %04X\n", z80cpu.AF.w, z80cpu.AFx.w, z80cpu.SP, z80cpu.I*256+z80cpu.R);
    conprintf("BC:%04X  BC':%04X  PC:%04X  T:%6u\n", z80cpu.BC.w, z80cpu.BCx.w, z80cpu.PC, cast(uint)z80cpu.tstates);
    conprintf("DE:%04X  DE':%04X  IX:%04X  IM%u,I:%u%u\n", z80cpu.DE.w, z80cpu.DEx.w, z80cpu.IX.w, z80cpu.IM, cast(uint)z80cpu.IFF1, cast(uint)z80cpu.IFF2);
    conprintf("HL:%04X  HL':%04X  IY:%04X  %s\n", z80cpu.HL.w, z80cpu.HLx.w, z80cpu.IY.w, flgbuf[]);
  })("regs", "dump registers");

  // kempston mouse
  conRegFunc!((int dx, int dy) {
    kmsDX += dx;
    kmsDY += dy;
  })("kmouse_move", "move kempston mouse by dx and dy");

  // bit 0: left; bit 1: right; bit 2: middle; pressed if bit reset
  conRegFunc!((ubyte but) {
    if (but <= 2) kmsBut |= (1<<but);
  })("kmouse_down", "press kempston mouse button");

  conRegFunc!((ubyte but) {
    if (but <= 2) kmsBut &= ~(1<<but);
  })("kmouse_up", "release kempston mouse button");

  conRegFunc!((int delta) {
    kmsWheel = (kmsWheel+delta)&0x0f;
  })("kmouse_wheel", "kempston mouse wheel rotation");


  conRegFunc!((ConString name="", ConString mode="") {
    if (mode.length == 0 && name.isPrefix("trdos")) { mode = "trdos"; name = ""; }
    if (name.length) {
      bool found;
      string n = zxFindMachine(name, &found).name;
      if (found) { zxmodel = n; tapeCurrentIs48K = (zxmodel == "48"); zxInitModel(); } else conwriteln("model '", name, "' not found");
    }
    z80cpu.resetToTRDOS = mode.isPrefix("trdos");
    z80cpu.reset();
    cliWantReset = true;
  })("reset", "reset machine (optional: model name)");

  conRegFunc!(() {
    zxKeyBinds.clear();
    zxCharBinds.clear();
  })("zxbind_clear", "clear ZX keyboard bindings");

  conRegFunc!((ConString key, ConString zxkey) {
    ushort pm0 = 0, pm1 = 0;

    bool zxpfx (string zxname, string pfx) {
      if (zxkey.length < pfx.length) return false;
      if (zxkey.startsWithCI(pfx)) {
        uint pos = cast(uint)pfx.length;
        while (pos < zxkey.length && zxkey[pos] <= ' ') ++pos;
        if (pos >= zxkey.length) return false;
        if (zxkey[pos] != '+') return false;
        ++pos;
        while (pos < zxkey.length && zxkey[pos] <= ' ') ++pos;
        if (pos >= zxkey.length) return false;
        zxkey = zxkey[pos..$];
        foreach (const ref ki; zxKeyInfo[]) if (strEquCI(ki.name, zxname)) { pm0 = ki.portmask; return true; }
      }
      return false;
    }

    key = key.fixKeyName;

    auto ozxk = zxkey;
    if (!zxpfx("cap", "cap") && !zxpfx("cap", "cs") && !zxpfx("sym", "sym") && !zxpfx("sym", "ss")) {}
    if (zxkey.length == 0) { conwriteln("invalid ZX key: \"", ozxk, "\""); return; }
    bool found = false;
    foreach (const ref ki; zxKeyInfo[]) {
      if (strEquCI(ki.name, zxkey)) {
        found = true;
        if (pm0 == 0) pm0 = ki.portmask; else pm1 = ki.portmask;
        break;
      }
    }
    if (!found) { conwriteln("invalid ZX key: \"", ozxk, "\""); return; }

    foreach (string kn; __traits(allMembers, Key)) {
      if (strEquCI(kn, key)) {
        zxKeyBinds[__traits(getMember, Key, kn)] = pm0|(pm1<<16);
        return;
      }
    }
    /*
    if (key.length == 1 && key[0] > ' ' && key[0] < 127) {
      zxCharBinds[key[0]] = pm0|(pm1<<16);
      return;
    }
    */
    conwriteln("unknown key: \"", key, "\"");
  })("zxbind", "set ZX keyboard binding");

  conRegFunc!((ConString key) {
    key = key.fixKeyName;
    foreach (string kn; __traits(allMembers, Key)) {
      if (strEquCI(kn, key)) {
        zxKeyBinds.remove(__traits(getMember, Key, kn));
        return;
      }
    }
    /*
    if (key.length == 1 && key[0] > ' ' && key[0] < 127) {
      zxCharBinds.remove(key[0]);
      return;
    }
    */
    conwriteln("unknown key: \"", key, "\"");
  })("zxunbind", "remove ZX keyboard binding");


  conRegFunc!((ConString fname) {
    try {
      ZXSnap snap;
      z80cpu.fillSnap(snap);
      auto flx = xfopenEx(fname, &xfIsSnapExt);
      if (!flx.hasSnap) throw new Exception("fuuuuu");
      snap.load(flx.snap, flx.snap.name);
      z80cpu.realizeSnap(snap);
      cliDiskLoaded = false;
      conwriteln("loaded snapshot '", fname, "'");
    } catch (Exception e) {
      conwriteln("ERROR loading snapshot: '", fname, "'");
    }
  })("loadsnap", "load SNA snapshot").cmdconSetFileCompleter(
    null, // "get current file"
    delegate (ConString fname) nothrow {
      import iv.vfs.util;
      return
        fname.getExtension.strEquCI(".sna") ||
        fname.getExtension.strEquCI(".z80") ||
        fname.getExtension.strEquCI(".szx");
    },
  );


  conRegFunc!((ConString drv, ConString fname="") {
    /+
    if (fname.length == 0 && drv.length == 0) { conwriteln("loaddisk: wutafuck?!"); return; }
    if (fname.length == 0) {
      int dnum = parseDiskName(drv);
      if (dnum >= 0) { conwriteln("loaddisk: filename?"); return; }
      fname = drv;
      drv = "A:";
    }
    int dnum = parseDiskName(drv);
    if (dnum < 0) { conwriteln("loaddisk: invalid drive name: '", drv, "'"); return; }
    try {
      wdsnapbuf[] = 0;
      auto fl = xfopenEx(fname, (fn) => xfIsDiskExt(fn) || xfIsHoBetaExt(fn));
      auto fsz = fl.size;
      if (fsz > wdsnapbuf.length) fsz = wdsnapbuf.length;
      wdsnapsize = cast(uint)fsz;
      fl.rawReadExact(wdsnapbuf[0..wdsnapsize]);
      if (!wd1793.fdd[dnum].readDiskImage()) {
        conwriteln("unknown disk image format: '", fname, "'");
        return;
      }
      if (dnum == 0) cliDiskLoaded = true;
      conwriteln("loaded disk '", fname, "' to drive '", cast(char)('A'+dnum), ":'");
    } catch (Exception e) {
      conwriteln("ERROR loading disk: '", fname, "'");
    }
    +/
    final switch (loadAnyImg(drv, fname)) {
      case ImageType.Unknown: break;
      case ImageType.Disk: break;
      case ImageType.HoBeta: break;
      case ImageType.Snapshot: break;
      case ImageType.Tape: break;
    }
  })("loaddisk", "load disk image (name [drive])").cmdconSetFileCompleter(
    null, // "get current file"
    delegate (ConString fname) nothrow {
      import iv.vfs.util;
      return
        fname.getExtension.strEquCI(".trd") ||
        fname.getExtension.strEquCI(".udi") ||
        fname.getExtension.strEquCI(".fdi") ||
        fname.getExtension.strEquCI(".scl");
    },
  );


  conRegFunc!((ConString drv, ConString fname="") {
    if (fname.length == 0 && drv.length == 0) { conwriteln("loadhob: wutafuck?!"); return; }
    if (fname.length == 0) {
      int dnum = parseDiskName(drv);
      if (dnum >= 0) { conwriteln("loadhob: filename?"); return; }
      fname = drv;
      drv = "A:";
    }
    int dnum = parseDiskName(drv);
    if (dnum < 0) { conwriteln("loadhob: invalid drive name: '", drv, "'"); return; }
    try {
      wdsnapbuf[] = 0;
      auto flx = xfopenEx(fname, &xfIsHoBetaExt);
      if (!flx.hasDisk) throw new Exception("fuuuuuuu");
      auto fl = flx.disk[0];
      auto fsz = fl.size;
      if (fsz < 14 || fsz > 65536+1024) {
        conwriteln("invalid HoBeta file: '", fname, "'");
        return;
      }
      wdsnapsize = cast(uint)fsz;
      fl.rawReadExact(wdsnapbuf[0..wdsnapsize]);
      if (!wd1793.fdd[dnum].readHOB()) {
        conwriteln("invalid HoBeta file: '", fname, "'");
        return;
      }
      if (dnum == 0) cliDiskLoaded = true;
      conwriteln("loaded HoBeta file '", fname, "' to drive '", cast(char)('A'+dnum), ":'");
    } catch (Exception e) {
      conwriteln("ERROR loading disk: '", fname, "'");
    }
  })("loadhob", "add HoBeta file to disk image (name [drive])").cmdconSetFileCompleter(
    null, // "get current file"
    delegate (ConString fname) nothrow {
      import iv.vfs.util;
      auto ext = fname.getExtension;
      return (ext.length > 2 && ext[1] == '$');
    },
  );


  conRegFunc!((ConString drv, ConString fmt="trdos") {
    int dnum = parseDiskName(drv);
    if (dnum < 0) { conwriteln("newdisk: invalid drive name: '", drv, "'"); return; }
    if (fmt.isPrefix("trdos")) {
      wd1793.fdd[dnum].emptyDiskTRD();
      conwriteln("empty TR-DOS disk inserted in drive '", cast(char)('A'+dnum), ":'");
    } else if (fmt.isPrefix("empty")) {
      wd1793.fdd[dnum].emptyDiskEmpty();
      conwriteln("empty disk inserted in drive '", cast(char)('A'+dnum), ":'");
    } else {
      conwriteln("newdisk: unknown format '", fmt, "'");
    }
  })("newdisk", "insert new empty disk image ([drive] [trdos|empty])");

  conRegFunc!((ConString drv, ConString fname="", ConString fmt="") {
    if (fname.length == 0 && fmt.length != 0) { conwriteln("savedisk: wutafuck?!"); return; }
    if (fname.length == 0) {
      int dnum = parseDiskName(drv);
      if (dnum >= 0) { conwriteln("savedisk: filename?"); return; }
      fname = drv;
      drv = "A:";
    }
    int dnum = parseDiskName(drv);
    if (dnum < 0) { conwriteln("savedisk: invalid drive name: '", drv, "'"); return; }
    // try to guess the format
    if (fmt.length == 0) {
           if (fname.endsWithCI(".trd")) fmt = "trdos";
      else if (fname.endsWithCI(".udi")) fmt = "udi";
      else if (fname.endsWithCI(".fdi")) fmt = "fdi";
      else if (fname.endsWithCI(".scl")) fmt = "scl";
      else { conwriteln("savedisk: can't guess disk format!"); return; }
    }
    try {
      if (fmt.isPrefix("trdos")) {
        wd1793.fdd[dnum].writeTRD(VFile(fname, "w"));
        conwriteln("TR-DOS disk from drive '", cast(char)('A'+dnum), ":' saved.");
      } else if (fmt.isPrefix("udi")) {
        wd1793.fdd[dnum].writeUDI(VFile(fname, "w"));
        conwriteln("UDI disk from drive '", cast(char)('A'+dnum), ":' saved.");
      } else if (fmt.isPrefix("fdi")) {
        wd1793.fdd[dnum].writeFDI(VFile(fname, "w"));
        conwriteln("FDI disk from drive '", cast(char)('A'+dnum), ":' saved.");
      } else if (fmt.isPrefix("scl")) {
        wd1793.fdd[dnum].writeSCL(VFile(fname, "w"));
        conwriteln("SCL disk from drive '", cast(char)('A'+dnum), ":' saved.");
      } else {
        conwriteln("newdisk: unknown format '", fmt, "'");
      }
    } catch (Exception e) {
      conwriteln("error writing disk to '", fname, "'");
    }
  })("savedisk", "save disk from drive: drive fname [trdos|udi|fdi]").cmdconSetFileCompleter(
    null, // "get current file"
    delegate (ConString fname) nothrow {
      import iv.vfs.util;
      return
        fname.getExtension.strEquCI(".trd") ||
        fname.getExtension.strEquCI(".udi") ||
        fname.getExtension.strEquCI(".fdi") ||
        fname.getExtension.strEquCI(".scl");
    },
  );


  conRegFunc!((ConString drv, ConString fname="") {
    final switch (loadAnyImg(drv, fname)) {
      case ImageType.Unknown: break;
      case ImageType.Disk: break;
      case ImageType.HoBeta: break;
      case ImageType.Snapshot: break;
      case ImageType.Tape: break;
    }
  })("load", "load snapshot/disk/hobeta/tape/etc.").cmdconSetFileCompleter(
    null, // "get current file"
    delegate (ConString fname) nothrow {
      import iv.vfs.util;
      auto ext = fname.getExtension;
      if (ext.length > 2 && ext[1] == '$') return true;
      return
        ext.strEquCI(".sna") ||
        ext.strEquCI(".z80") ||
        ext.strEquCI(".szx") ||
        ext.strEquCI(".trd") ||
        ext.strEquCI(".udi") ||
        ext.strEquCI(".fdi") ||
        ext.strEquCI(".scl") ||
        ext.strEquCI(".tap") ||
        ext.strEquCI(".tzx");
    },
  );

  static if (LibSpectrumHere) {
    conRegFunc!(() { emuEjectTape(); conwriteln("tape ejected"); })("tape_eject", "eject tape");

    conRegFunc!((ConString fname) {
      try {
        auto flx = xfopenEx(fname, (fn) => xfIsDiskExt(fn) || xfIsHoBetaExt(fn) || xfIsSnapExt(fn) || xfIsTapeExt(fn));
        if (!flx.hasTape) { conwriteln("cannot find tape to insert in '", fname, "'"); return; }
        emuEjectTape();
        curtape = libspectrum_tape_alloc();
        if (curtape is null) { conwriteln("cannot allocate tape structure"); return; }
        if (flx.tape.size > 1024*1024*32) { conwriteln("tape file too big"); return; }
        if (flx.tape.size < 16) { conwriteln("tape file too small"); return; }
        auto buf = new ubyte[](cast(uint)flx.tape.size);
        scope(exit) delete buf;
        flx.tape.seek(0);
        flx.tape.rawReadExact(buf);
        auto tres = libspectrum_tape_read(curtape, buf.ptr, buf.length, LIBSPECTRUM_ID_UNKNOWN);
        if (tres != LIBSPECTRUM_ERROR_NONE && tres != LIBSPECTRUM_ERROR_WARNING) { conwriteln("unknown tape format"); return; }
        emuRewindTape(); // reset necessary flags
        conwriteln("loaded tape image from '", flx.tape.name, "'");
      } catch (Exception e) {
        conwriteln("ERROR: ", e.msg);
      }
    })("tape_insert", "insert new tape").cmdconSetFileCompleter(
      null, // "get current file"
      delegate (ConString fname) nothrow {
        import iv.vfs.util;
        auto ext = fname.getExtension;
        return
          ext.strEquCI(".tap") ||
          ext.strEquCI(".tzx");
      },
    );
  }

  conRegVar!optTapeAutorun("tape_autorun", "autorun (and autoload!) tape on \"load\" command");
}


// ////////////////////////////////////////////////////////////////////////// //
void zxRebuildScreen (bool fls) nothrow @trusted @nogc {
  if (Config.noFlic || noFlicDetected) {
    const(ubyte)* spo = zxscrlasto.ptr;
    const(ubyte)* sp = zxscrlast.ptr;
    ubyte* dp = cast(ubyte*)zxtexbuf.ptr;
    foreach (immutable _; 0..VBufWidth*VBufHeight) {
      ubyte attro = *spo++;
      ubyte inko = (fls && (attro&0x80) != 0 ? attro>>3 : attro)&0x07;
      if (attro&0x40) inko += 8;
      ubyte attr = *sp++;
      ubyte ink = (fls && (attr&0x80) != 0 ? attr>>3 : attr)&0x07;
      if (attr&0x40) ink += 8;
      uint p0 = zxpal.ptr[inko];
      uint p1 = zxpal.ptr[ink];
      foreach (immutable cc; 0..3) {
        *dp++ = clampToByte(((p0&0xff)+(p1&0xff))/2);
        p0 >>= 8;
        p1 >>= 8;
      }
      ++dp;
    }
  } else {
    const(ubyte)* sp = zxscrlast.ptr;
    uint* dp = zxtexbuf.ptr;
    foreach (immutable _; 0..VBufWidth*VBufHeight) {
      ubyte attr = *sp++;
      //if (attr&0x80) conwriteln("FLASH: (", _%VBufWidth, ",", _/VBufWidth, ")");
      ubyte ink = (fls && (attr&0x80) != 0 ? attr>>3 : attr)&0x07;
      if (attr&0x40) ink += 8;
      *dp++ = zxpal.ptr[ink];
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared int ayLedsKeep = 0;
__gshared int[3] ayLedsPrev = 0;


void zxUpdateTexture () nothrow @trusted @nogc {
  import iv.glbinds;

  bool fls;
  synchronized(Z80CPU.classinfo) {
    if (Config.noFlic || noFlicDetected) zxscrlasto[] = zxscro[];
    zxscrlast[] = zxscr[];
    fls = zxFlashState;
  }
  if (showKeyHelp) {
    zxscr[] = 0;
    foreach (immutable uint y; 0..192) {
      auto dp = zxscr.ptr+(y+VBufScrYOfs)*VBufWidth+VBufScrXOfs;
      auto sb = keyHelpScr.ptr+zxscry2a.ptr[y];
      auto sa = keyHelpScr.ptr+0x1800+(y/8)*32;
      foreach (immutable x; 0..32) {
        ubyte b = *sb++;
        ubyte iattr = *sa++;
        ubyte pattr = (iattr&0xc0)|((iattr&0x07)<<3)|((iattr>>3)&0x07);
        foreach (immutable dx; 0..8) {
          *dp++ = (b&0x80 ? iattr : pattr);
          b <<= 1;
        }
      }
    }
    zxscrlast[] = zxscr[];
  }
  zxRebuildScreen(fls);

  ztDrawDiskLed(-1, -1, led1793Check());
  if (Config.maxSpeed) ztDrawTextOutP(-4, 4, "MAX", ztRGB!(0x00, 0xff, 0x00), ztRGB!(0, 0, 0));
  if (paused) ztDrawTextOutP(4, 4, "PAUSED", ztRGB!(0x00, 0xff, 0x00), ztRGB!(0, 0, 0));

  // draw tape state
  if (optTapeShowProgress) {
    auto tps = tapeGetState();
    if (!tps.complete && tps.blockCount > 0 && tps.total > 0) {
      int end = tps.curr;
      version(none) {
        if (end > tps.total) {
          import core.stdc.stdio;
          printf("FU?! end=%d; total=%d\n", end, tps.total);
        }
      }
      if (end > tps.total) end = tps.total; else if (end < 0) end = 0;
      int filled = cast(int)(cast(long)(winWidth-4)*tps.curr/tps.total);
      if (tps.playing || filled < winWidth-6) {
        enum TpXOfs = 2;
        enum TpYOfs = 2;
        enum TpHeight = 4;
        foreach (immutable int x; 0..winWidth-4) {
          uint clr = (x < filled ? ztRGB!(255, 255, 0) : ztRGB!(255, 128, 0));
          clr |= (tps.playing ? 0x5f_00_00_00U : 0xcf_00_00_00U);
          if (x == 0 || x == winWidth-4-1) {
            foreach (immutable int y; 1..TpHeight-1) ztPutPixel(x+TpXOfs, winHeight-TpYOfs-2+y, clr);
          } else {
            foreach (immutable int y; 0..TpHeight) ztPutPixel(x+TpXOfs, winHeight-TpYOfs-2+y, clr);
          }
        }
      }
    }
  }

  //ztDrawTextOutP(10, 10, "testimonial", ztRGB!(0xff, 0xff, 0x00), ztRGB!(0, 0, 0));
  //ztDrawTextOutP(10, 18, "This is the test of Various Width Letters, lol! hi!", ztRGB!(0xff, 0xff, 0x00), ztRGB!(0, 0, 0));

  if (optAYLeds) {
    bool ayplaying = ((z80cpu.ayState[8]&0x0f) || (z80cpu.ayState[9]&0x0f) || (z80cpu.ayState[10]&0x0f));
    if (ayplaying) ayLedsKeep = 20; else --ayLedsKeep;
    if (ayLedsKeep > 0) {
      int yofs = winHeight-2-7;
      int xofs = 2;
      foreach (immutable y; 0..7) {
        if (y&0x01) {
          ztPutPixel(xofs, yofs+y, ztRGB!(0x00, 0x00, 0xff));
          auto lednum = (y-1)/2;
          int vol = (z80cpu.ayState[8+lednum]&0x0f)+1;
          if (vol >= ayLedsPrev[lednum]) {
            ayLedsPrev[lednum] = vol;
          } else {
            vol = --ayLedsPrev[lednum];
            if (vol < 0) vol = ayLedsPrev[lednum] = 0;
          }
          foreach (immutable x; 1..16) ztPutPixel(xofs+x, yofs+y, 0);
          foreach (immutable x; 1..vol) ztPutPixel(xofs+x, yofs+y, ztRGB!(0x00, 0xff, 0xff));
          ztPutPixel(xofs+16, yofs+y, ztRGB!(0x00, 0x00, 0xff));
        } else {
          foreach (immutable x; 0..17) ztPutPixel(xofs+x, yofs+y, ztRGB!(0x00, 0x00, 0xff));
        }
      }
    } else {
      ayLedsKeep = 0;
    }
  } else {
    ayLedsKeep = 0;
  }

  glBindTexture(GL_TEXTURE_2D, zxtexid);
  glTexSubImage2D(GL_TEXTURE_2D, 0, 0/*x*/, 0/*y*/, VBufWidth, VBufHeight, GLTexType, GL_UNSIGNED_BYTE, zxtexbuf.ptr);
  //glBindTexture(GL_TEXTURE_2D, 0);
}


// ////////////////////////////////////////////////////////////////////////// //
void main (string[] args) {
  //{ import etc.linux.memoryerror; registerMemoryErrorHandler(); }
  sdpyWindowClass = "ZXEmuT";
  glconShowKey = "F12";
  //glconShowKey = "M-Grave";

  trdosROMread = delegate (uint pc) {
    pc &= 0xffff;
    return cast(ubyte)(pc < zxtrd.length ? zxtrd.ptr[pc] : 0);
  };

  zxscr.length = VBufWidth*VBufHeight;
  zxscro.length = zxscr.length;

  zxscrlast.length = zxscr.length;
  zxscrlasto.length = zxscrlast.length;

  zxInitModel();

  z80cpu = new Z80CPU();
  z80cpu.zxscr.length = zxscr.length;
  emuz80 = z80cpu;

  wd1793 = new WD1793();

  zxKeyboardStateDown[] = 0xff;
  zxKeyboardStateUp[] = 0xff;
  //z80cpu.reset();

  initConsole();

  concmdf!"exec \"%q/zxemut.rc\" tan"(configDir);
  concmd("exec zxemut.rc tan");
  conProcessQueue(256*1024); // load config
  conProcessArgs!true(args);

  // process all console commands
  foreach (immutable _; 0..42) if (!conProcessQueue()) break;

  // load normal files
  loadAnyCLIDrive = 0;
  foreach (string fname; args[1..$]) loadAnyImg(fname, "");
  loadAnyCLIDrive = -1;

  vbufEffScale = VBufScale;
  vbufEffVSync = vbufVSync;

  effVisibleBorderPixels = visibleBorderPixels;

  if (cliDiskLoaded && !cliWantReset) {
    zxmodel = Config.WD93.trdos128model;
    tapeCurrentIs48K = false;
    zxInitModel();
    z80cpu.resetToTRDOS = true;
    z80cpu.reset();
  }


  if (emuSoundEnabled) soundInit();
  scope(exit) soundDeinit();

  vbwin = new SimpleWindow(winWidthScaled, winHeightScaled, "ZXEmuT", OpenGlOptions.yes, Resizability.fixedSize);
  vbwin.hideCursor();


  bool rebuildTexture = false;

  void checkResize () {
    bool resizeWin = false;
    rebuildTexture = false;

    {
      consoleLock();
      scope(exit) consoleUnlock();
      if (VBufScale != vbufEffScale) {
        // window scale changed
        vbufEffScale = VBufScale;
        resizeWin = true;
      }
      if (visibleBorderPixels != effVisibleBorderPixels) {
        effVisibleBorderPixels = visibleBorderPixels;
        resizeWin = true;
        rebuildTexture = true;
      }
    }

    if (resizeWin) {
      vbwin.resize(winWidthScaled, winHeightScaled);
      glconResize(winWidthScaled, winHeightScaled);
      flushGui();
    }
  }

  vbwin.onFocusChange = delegate (bool focused) {
    if (!focused) {
      zxKeyboardStateDown[] = 0xff;
      zxKeyboardStateUp[] = 0xff;
      kmsBut = 0;
    }
    vbfocused = focused;
  };

  /*
  evScrReady = new PipeEvent(delegate (ubyte eid) {
    if (vbwin.closed) return;
    if (isQuitRequested) { vbwin.close(); return; }
    if (eid == EventId.ScreenReady) { zxUpdateTexture(); zxDoReleases(); }
    if (eid == EventId.ScreenReady || eid == EventId.ScreenRepaint) vbwin.redrawOpenGlSceneNow();
  });
  */
  evScrReady = new ScreenReadyEvent();

  vbwin.addEventListener((ScreenReadyEvent evt) {
    //conwriteln("screen ready! ", *cast(void**)&evt);
    if (isQuitRequested) { vbwin.close(); return; }
    if (vbwin.closed) return;
    zxUpdateTexture();
    zxDoReleases();
    checkResize();
    vbwin.redrawOpenGlSceneNow();
  });

  vbwin.addEventListener((ScreenRepaintEvent evt) {
    //conwriteln("screen repaint! ", *cast(void**)&evt);
    if (isQuitRequested) { vbwin.close(); return; }
    if (vbwin.closed) return;
    checkResize();
    vbwin.redrawOpenGlSceneNow();
  });

  vbwin.addEventListener((QuitEvent evt) {
    //conwriteln("quit! ", *cast(void**)&evt);
    if (isQuitRequested) { vbwin.close(); return; }
    if (vbwin.closed) return;
    vbwin.close();
  });

  vbwin.redrawOpenGlScene = delegate () {
    if (rebuildTexture) zxUpdateTexture();

    if (vbufEffVSync != vbufVSync) {
      vbufEffVSync = vbufVSync;
      vbwin.vsync = vbufEffVSync;
    }

    {
      enum w = VBufWidth;
      enum h = VBufHeight;

      glMatrixMode(GL_PROJECTION); // for ortho camera
      glLoadIdentity();
      // left, right, bottom, top, near, far
      //glViewport(0, 0, w*vbufEffScale, h*vbufEffScale);
      //glOrtho(0, w, h, 0, -1, 1); // top-to-bottom
      glViewport(0, 0, VBufWidth*vbufEffScale, VBufHeight*vbufEffScale);
      glOrtho(0, VBufWidth, VBufHeight, 0, -1, 1); // top-to-bottom
      glMatrixMode(GL_MODELVIEW);
      glLoadIdentity();

      if (v_scanlines) {
        if (!v_shader_tried) {
          v_shader_tried = true;
          initScanlineShader();
        }
        if (shadScanlines) {
          shadScanlines.activateObj();
          shadScanlines["tex"] = 0;
          shadScanlines["cmult"] = v_scanmult1;
          shadScanlines["cmult2"] = v_scanmult2;
        }
      }

      glEnable(GL_TEXTURE_2D);
      glDisable(GL_LIGHTING);
      glDisable(GL_DITHER);
      //glDisable(GL_BLEND);
      glDisable(GL_DEPTH_TEST);
      //glEnable(GL_BLEND);
      //glBlendFunc(GL_SRC_ALPHA, GL_ONE);
      //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
      glDisable(GL_BLEND);
      //glDisable(GL_STENCIL_TEST);

      enum x = 0;
      enum y = 0;

      glColor4f(1, 1, 1, 1);
      glBindTexture(GL_TEXTURE_2D, zxtexid);
      //scope(exit) glBindTexture(GL_TEXTURE_2D, 0);
      glBegin(GL_QUADS);
        glTexCoord2f(0.0f, 0.0f); glVertex2i(x-winX0, y+winY0); // top-left
        glTexCoord2f(1.0f, 0.0f); glVertex2i(w-winX0, y+winY0); // top-right
        glTexCoord2f(1.0f, 1.0f); glVertex2i(w-winX0, h+winY0); // bottom-right
        glTexCoord2f(0.0f, 1.0f); glVertex2i(x-winX0, h+winY0); // bottom-left
      glEnd();

      if (v_scanlines && shadScanlines) shadScanlines.deactivateObj();
    }
    glconDraw();
  };

  static if (is(typeof(&vbwin.closeQuery))) {
    vbwin.closeQuery = delegate () { concmd("quit"); };
  }

  vbwin.visibleForTheFirstTime = delegate () {
    import iv.glbinds;
    vbwin.setAsCurrentOpenGlContext();
    vbufEffVSync = vbufVSync;
    vbwin.vsync = vbufEffVSync;

    // initialize OpenGL texture
    {
      enum wrapOpt = GL_REPEAT;
      enum filterOpt = GL_NEAREST; //GL_LINEAR;
      enum ttype = GL_UNSIGNED_BYTE;

      glGenTextures(1, &zxtexid);
      if (zxtexid == 0) assert(0, "can't create kgi texture");

      //GLint gltextbinding;
      //glGetIntegerv(GL_TEXTURE_BINDING_2D, &gltextbinding);
      //scope(exit) glBindTexture(GL_TEXTURE_2D, gltextbinding);

      glBindTexture(GL_TEXTURE_2D, zxtexid);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapOpt);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapOpt);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filterOpt);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filterOpt);
      //glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
      //glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

      GLfloat[4] bclr = 0.0;
      glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, bclr.ptr);

      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, VBufWidth, VBufHeight, 0, GLTexType, GL_UNSIGNED_BYTE, zxtexbuf.ptr);
    }

    glconInit(winWidthScaled, winHeightScaled);
    vbwin.redrawOpenGlSceneNow();

    /*auto emuTid =*/ spawn(&emuThread, thisTid);
  };

  vbwin.eventLoop(/*1000/50*/0,
    delegate () {
      if (vbwin.closed) return;
      if (isQuitRequested) { vbwin.close(); return; }
      checkResize();
      vbwin.redrawOpenGlSceneNow();
    },
    delegate (KeyEvent event) {
      void zxReleaseKey (Key key) {
        if (auto zkp = key in zxKeyBinds) {
          zxDoKeyUpDown((*zkp)&0xffff, false);
          zxDoKeyUpDown(((*zkp)>>16)&0xffff, false);
        }
      }

      void zxReleaseIt (string mods="*") {
        zxReleaseKey(event.key);
        if (mods.indexOf('*') >= 0) mods = "MCSH";
        foreach (immutable char ch; mods) {
          if (ch == 'M' || ch == 'm') { zxReleaseKey(Key.Alt); zxReleaseKey(Key.Alt_r); }
          if (ch == 'C' || ch == 'c') { zxReleaseKey(Key.Ctrl); zxReleaseKey(Key.Ctrl_r); }
          if (ch == 'S' || ch == 's') { zxReleaseKey(Key.Shift); zxReleaseKey(Key.Shift_r); }
          if (ch == 'H' || ch == 'h') { zxReleaseKey(Key.Windows); zxReleaseKey(Key.Windows_r); }
        }
      }

      if (vbwin.closed) return;
      if (isQuitRequested) { vbwin.close(); return; }
      if (glconKeyEvent(event)) {
        //evScrReady.send(EventId.ScreenRepaint);
        zxReleaseIt();
        if (!vbwin.eventQueued!ScreenRepaintEvent) vbwin.postEvent(new ScreenRepaintEvent());
        return;
      }
      // emulator hotkeys first
      if (event.pressed) {
        if (event == "escape" && conGetVar!bool("keyhelp")) { zxReleaseIt(); concmd("keyhelp false"); return; }
        if (event == "F1") { zxReleaseIt(); concmd("paused toggle"); return; }
        if (event == "S-F1") { zxReleaseIt(); concmd("keyhelp toggle"); return; }
        if (event == "F9") { zxReleaseIt(); concmd("max_speed toggle"); return; }
        if (event == "M-F9") { zxReleaseIt(); concmd("mouse_grab_force toggle"); return; }
        if (event == "C-F12") { zxReleaseIt(); concmd("reset"); return; }
        if (event == "M-F12") { zxReleaseIt(); concmd("reset trdos"); return; }
        if (event == "H-Y") { zxReleaseIt(); concmd("s_ay toggle"); return; }
        if (event == "M-H-Y") { zxReleaseIt(); concmd("leds_ay toggle"); return; }
        if (event == "H-B") { zxReleaseIt(); concmd("s_beeper toggle"); return; }
        //conwriteln("key: ", event.toStr, ": ", event.modifierState&ModifierState.windows);
      }
      // zx bindings last
      if (auto zkp = event.key in zxKeyBinds) {
        zxDoKeyUpDown((*zkp)&0xffff, event.pressed);
        zxDoKeyUpDown(((*zkp)>>16)&0xffff, event.pressed);
        return;
      }
    },
    delegate (MouseEvent event) {
      if (vbwin.closed) return;
      if (event.type == MouseEventType.motion) {
        import std.math : abs;
        int dx = event.dx;
        int dy = -event.dy;
        if (vbufEffScale > 1 && abs(dx) > 1) dx /= 2;//vbufEffScale;
        if (vbufEffScale > 1 && abs(dy) > 1) dy /= 2;//vbufEffScale;
        concmdf!"kmouse_move %s %s"(dx, dy);
        if ((kmouseGrabbed || kmouseForceGrab) && vbfocused) vbwin.warpMouse(winWidthScaled/2, winHeightScaled/2);
        return;
      }
      if (event.type == MouseEventType.buttonPressed) {
        if (event.button == MouseButton.left) concmd(!Config.kempstonMSwapB ? "kmouse_down 0" : "kmouse_down 1");
        if (event.button == MouseButton.right) concmd(!Config.kempstonMSwapB ? "kmouse_down 1" : "kmouse_down 0");
        if (event.button == MouseButton.middle) concmd("kmouse_down 2");
        if (event.button == MouseButton.wheelUp) concmd("kmouse_wheel -1");
        if (event.button == MouseButton.wheelDown) concmd("kmouse_wheel 1");
        return;
      }
      if (event.type == MouseEventType.buttonReleased) {
        if (event.button == MouseButton.left) concmd(!Config.kempstonMSwapB ? "kmouse_up 0" : "kmouse_up 1");
        if (event.button == MouseButton.right) concmd(!Config.kempstonMSwapB ? "kmouse_up 1" : "kmouse_up 0");
        if (event.button == MouseButton.middle) concmd("kmouse_up 2");
        return;
      }
    },
    delegate (dchar ch) {
      if (vbwin.closed) return;
      if (glconCharEvent(ch)) {
        //evScrReady.send(EventId.ScreenRepaint);
        if (!vbwin.eventQueued!ScreenRepaintEvent) vbwin.postEvent(new ScreenRepaintEvent());
        return;
      }
      //if (ch > ' ' && ch < 127) conwriteln("key: '", cast(char)ch, "'");
      if (ch > ' ' && ch < 127) {
        char c = cast(char)ch;
        if (auto zkp = c in zxCharBinds) {
          zxDoKeyUpDown((*zkp)&0xffff, true);
          zxDoKeyUpDown(((*zkp)>>16)&0xffff, true);
          zxPushRelease((*zkp)&0xffff);
          zxPushRelease(((*zkp)>>16)&0xffff);
          return;
        }
      }
    },
  );
}
