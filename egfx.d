/* ZX Spectrum Emulator
 * Copyright (C) 2012-2017 Ketmar // Invisible Vector
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
module egfx is aliced;
private:

import core.atomic;
import std.concurrency;

import arsd.simpledisplay;

import iv.bclamp;
import iv.cmdcon;
import iv.vfs;

import emuconfig;


// ////////////////////////////////////////////////////////////////////////// //
//mixin(import("egfxfont.d"));
// font: (hi nibble: lshift; lo nibble: width); 8 data bytes
static immutable ubyte[] font6x8p = cast(immutable(ubyte)[])import("databin/zxpfont.fnt");
static immutable ubyte[] diskLedIcon = cast(immutable(ubyte)[])import("databin/diskled.rgb");


// ////////////////////////////////////////////////////////////////////////// //
public enum VBufWidth = /*256+40*2*/352; //full: 352
public enum VBufHeight = /*192+40*2*/288; //full: 288
public enum VBufScrXOfs = (VBufWidth-256)/2;
public enum VBufScrYOfs = (VBufHeight-192)/2;
public __gshared ubyte VBufScale = 2; // new window scale
public __gshared ubyte vbufEffScale = 2; // effective (current) window scale
public __gshared uint visibleBorderPixels = 48; // [0..48]
public __gshared uint effVisibleBorderPixels = 48; // [0..48]

public __gshared uint[] zxtexbuf; // OpenGL texture buffer

shared static this () {
  zxtexbuf.length = VBufWidth*VBufHeight+4;
}


public @property int winX0 () nothrow @trusted @nogc { pragma(inline, true); return 48-effVisibleBorderPixels; }
public @property int winY0 () nothrow @trusted @nogc { pragma(inline, true); return 48-effVisibleBorderPixels; }

public @property int winWidth () nothrow @trusted @nogc { pragma(inline, true); return VBufWidth-(48*2-effVisibleBorderPixels*2); }
public @property int winHeight () nothrow @trusted @nogc { pragma(inline, true); return VBufHeight-(48*2-effVisibleBorderPixels*2); }

public @property int winWidthScaled () nothrow @trusted @nogc { pragma(inline, true); return (VBufWidth-(48*2-effVisibleBorderPixels*2))*vbufEffScale; }
public @property int winHeightScaled () nothrow @trusted @nogc { pragma(inline, true); return (VBufHeight-(48*2-effVisibleBorderPixels*2))*vbufEffScale; }


// ////////////////////////////////////////////////////////////////////////// //
public __gshared uint[16] zxpal;

shared static this () nothrow @trusted @nogc {
  static immutable ubyte[16*3] zxPalette = [
      0,  0,  0,
      0,  0,192/*+32*/,
    192,  0,  0,
    192,  0,192,
      0,192,  0,
      0,192,192,
    192,192,  0,
    192,192,192,
      0,  0,  0,
      0,  0,255,
    255,  0,  0,
    255,  0,255,
      0,255,  0,
      0,255,255,
    255,255,  0,
    255,255,255,
  ];
  foreach (immutable c; 0..16) {
    zxpal[c] = zxPalette[c*3+0]|(zxPalette[c*3+1]<<8)|(zxPalette[c*3+2]<<16);
    zxpal[c] = ztrgb(zxPalette[c*3+0], zxPalette[c*3+1], zxPalette[c*3+2]);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// mix dc with ARGB (or ABGR) col; dc A is ignored (removed)
public uint ztColMix (uint dc, uint col) pure nothrow @trusted @nogc {
  pragma(inline, true);
  immutable uint a = 256-(col>>24); // to not loose bits
  //immutable uint dc = (da)&0xffffff;
  dc &= 0xffffff;
  immutable uint srb = (col&0xff00ff);
  immutable uint sg = (col&0x00ff00);
  immutable uint drb = (dc&0xff00ff);
  immutable uint dg = (dc&0x00ff00);
  immutable uint orb = (drb+(((srb-drb)*a+0x800080)>>8))&0xff00ff;
  immutable uint og = (dg+(((sg-dg)*a+0x008000)>>8))&0x00ff00;
  return orb|og;
}


// ////////////////////////////////////////////////////////////////////////// //
private template isGoodRGBInt(T) {
  import std.traits : Unqual;
  alias TT = Unqual!T;
  enum isGoodRGBInt =
    is(TT == ubyte) ||
    is(TT == short) || is(TT == ushort) ||
    is(TT == int) || is(TT == uint) ||
    is(TT == long) || is(TT == ulong);
}


// ////////////////////////////////////////////////////////////////////////// //
public uint ztrgb(T0, T1, T2) (T0 r, T1 g, T2 b) pure nothrow @trusted @nogc if (isGoodRGBInt!T0 && isGoodRGBInt!T1 && isGoodRGBInt!T2) {
  pragma(inline, true);
  return (clampToByte(r)<<16)|(clampToByte(g)<<8)|clampToByte(b);
}


public template ztRGB(int r, int g, int b) {
  enum ztRGB = (clampToByte(r)<<16)|(clampToByte(g)<<8)|clampToByte(b);
}


// ////////////////////////////////////////////////////////////////////////// //
public void ztPutPixel (int x, int y, uint c) nothrow @trusted @nogc {
  pragma(inline, true);
  //x += winX0;
  //y += winY0;
  x += 48-effVisibleBorderPixels;
  y += 48-effVisibleBorderPixels;
  if (x >= 0 && y >= 0 && x < VBufWidth && y < VBufHeight && (c&0xff000000) != 0xff000000) {
    /*
    ubyte* dp = cast(ubyte*)zxtexbuf.ptr;
    dp += y*(VBufWidth*4)+x*4;
    *dp++ = (c>>16)&0xff; //R
    *dp++ = (c>>8)&0xff; //G
    *dp++ = c&0xff; //B
    *dp = 0;
    */
    uint* dp = cast(uint*)(cast(ubyte*)zxtexbuf.ptr)+y*VBufWidth+x;
    *dp = ztColMix(*dp, c);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public void ztDrawChar(bool rawxy=false) (int x, int y, char ch, uint fg, uint bg=0xff000000) nothrow @trusted @nogc {
  if (!rawxy) {
    if (x < 0) x += winWidth;
    if (y < 0) y += winHeight;
  }
  foreach (immutable dy; 1..9) {
    ubyte b = font6x8p.ptr[cast(uint)ch*9+dy];
    if (b) {
      foreach (immutable dx; 0..6) {
        ztPutPixel(x+dx, y, (b&0x80 ? fg : bg));
        b <<= 1;
      }
    }
    ++y;
  }
}


// return char width
public int ztDrawCharP(bool rawxy=false) (int x, int y, char ch, uint fg, uint bg=0xff000000) nothrow @trusted @nogc {
  if (!rawxy) {
    if (x < 0) x += winWidth;
    if (y < 0) y += winHeight;
  }
  ubyte wdt = font6x8p.ptr[cast(uint)ch*9];
  ubyte shift = wdt>>4;
  wdt &= 0x0f;
  foreach (immutable dy; 1..9) {
    ubyte b = cast(ubyte)(font6x8p.ptr[cast(uint)ch*9+dy]<<shift);
    if (b) {
      foreach (immutable dx; 0..wdt) {
        ztPutPixel(x+dx, y, (b&0x80 ? fg : bg));
        b <<= 1;
      }
    }
    ++y;
  }
  return wdt;
}


// ////////////////////////////////////////////////////////////////////////// //
public int ztTextWidth (const(char)[] s) nothrow @trusted @nogc {
  return cast(int)s.length*6;
}


public int ztTextWidthP (const(char)[] s) nothrow @trusted @nogc {
  int res = 0;
  foreach (char ch; s) {
    if (res) ++res;
    res += font6x8p.ptr[cast(uint)ch*9];
  }
  return res;
}


// ////////////////////////////////////////////////////////////////////////// //
public void ztDrawText(bool rawxy=false) (int x, int y, const(char)[] s, uint fg, uint bg=0xff000000) nothrow @trusted @nogc {
  if (!rawxy) {
    if (x < 0) x += winWidth-ztTextWidth(s)+1;
    if (y < 0) y += winHeight-7;
  }
  foreach (char ch; s) {
    ztDrawChar!true(x, y, ch, fg, bg);
    x += 6;
  }
}


// return text width
public int ztDrawTextP(bool rawxy=false) (int x, int y, const(char)[] s, uint fg, uint bg=0xff000000) nothrow @trusted @nogc {
  if (!rawxy) {
    if (x < 0) x += winWidth-ztTextWidthP(s)+1;
    if (y < 0) y += winHeight-7;
  }
  int res = 0;
  foreach (char ch; s) {
    auto w = ztDrawCharP!true(x, y, ch, fg, bg);
    if (res) ++res;
    res += w;
    x += w+1;
  }
  return res;
}


// ////////////////////////////////////////////////////////////////////////// //
public void ztDrawTextOut(bool rawxy=false) (int x, int y, const(char)[] s, uint fg, uint ot) nothrow @trusted @nogc {
  if (!rawxy) {
    if (x < 0) x += winWidth-ztTextWidth(s)-1;
    if (y < 0) y += winHeight-7;
  }
  foreach (immutable dy; -1..2) {
    foreach (immutable dx; -1..2) {
      if (dx == 0 && dy == 0) continue;
      ztDrawText!true(x+dx, y+dy, s, ot);
    }
  }
  ztDrawText!true(x, y, s, fg);
}


// return text width
public int ztDrawTextOutP(bool rawxy=false) (int x, int y, const(char)[] s, uint fg, uint ot) nothrow @trusted @nogc {
  if (!rawxy) {
    if (x < 0) x += winWidth-ztTextWidthP(s)-1;
    if (y < 0) y += winHeight-7;
  }
  foreach (immutable dy; -1..2) {
    foreach (immutable dx; -1..2) {
      if (dx == 0 && dy == 0) continue;
      ztDrawTextP!true(x+dx, y+dy, s, ot);
    }
  }
  return ztDrawTextP!true(x, y, s, fg);
}


// ////////////////////////////////////////////////////////////////////////// //
// negative coords: shift from right/bottom, -1 means "right at the corner"
void ztDrawIcon(bool rawxy=false) (int x, int y, ubyte alpha, const(void)[] data) nothrow @trusted @nogc {
  if (alpha == 255) return;
  if (data.length < 4*2) return;
  auto dp = (cast(const(uint)*)data.ptr);
  if (dp[0] < 1 || dp[1] < 1 || dp[0] > 4096 || dp[1] > 4096) return; // just in case
  int w = *dp++;
  int h = *dp++;
  if (data.length < w*h+2) return; // alas
  if (!rawxy) {
    if (x < 0) x += winWidth-w+1;
    if (y < 0) y += winHeight-h+1;
  }
  uint aa = alpha<<24;
  foreach (immutable dy; 0..h) {
    foreach (immutable dx; 0..w) {
      ztPutPixel(x+dx, y, ((*dp++)&0xffffff)|aa);
    }
    ++y;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// negative coords: shift from right/bottom, -1 means "right at the corner"
public void ztDrawDiskLed(bool rawxy=false) (int x, int y, ubyte alpha=0) nothrow @trusted @nogc {
  if (alpha == 255) return;
  ztDrawIcon!rawxy(x, y, alpha, diskLedIcon);
}
