/* ZX Spectrum Snapshot I/O
 * Copyright (C) 2017 Ketmar // Invisible Vector
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
module zxsnap;

import iv.cmdcon;
import iv.strex;
import iv.vfs;


// ////////////////////////////////////////////////////////////////////////// //
class ZXSnapException : Exception {
  private import std.exception : basicExceptionCtors;
  mixin basicExceptionCtors;
}


// ////////////////////////////////////////////////////////////////////////// //
/** general ZX Spectrum snapshot info. if you want to preserve as much current
 * emulation state as you can, you should fill the snapshot with the current
 * state, and then call some loading routine.
 */
struct ZXSnap {
  enum PageSize = 0x4000;

  /// register pair
  union RegPair(string hi, string lo) {
  align(1):
    ushort w;
   version(LittleEndian) {
    mixin("struct { align(1): ubyte "~lo~", "~hi~"; }");
   }
   version(BigEndian) {
    mixin("struct { align(1): ubyte "~hi~", "~lo~"; }");
   }
   alias w this; // allow to use RegPair as ushort
  }

  enum Model {
    zx48,
    zx128,
    plus2,
    plus2a,
    plus3,
    plus3e,
    pentagon128,
    pentagon512,
    pentagon1024,
  }

  enum AYType {
    None,
    Fuller,
    AY128,
  }

  @property bool isPentagon () const pure nothrow @safe @nogc { return (model >= Model.pentagon128); }
  @property bool is128K () const pure nothrow @safe @nogc { return (model >= Model.zx128); }
  @property ushort memsize () const pure nothrow @safe @nogc {
    return
      model == Model.zx48 ? 48 :
      model >= Model.zx128 && model < Model.pentagon512 ? 128 :
      model == Model.pentagon512 ? 512 :
      model == Model.pentagon1024 ? 1024 :
      128;
  }

  Model model = Model.zx48; /// machine model
  Model default128 = Model.zx128; /// default 128K machine model; DO NOT SET TO 48K!
  bool lateTimings; /// only valid for zx16/zx48/zx128
  bool halted; /// is machine halted?
  bool lastWasEI; /// set if last executed command was /INT-blocking one
  /// registers
  RegPair!("b", "c") BC; ///
  RegPair!("d", "e") DE; ///
  RegPair!("h", "l") HL; ///
  RegPair!("a", "f") AF; ///
  RegPair!("xh", "xl") IX; ///
  RegPair!("yh", "yl") IY; ///
  /// alternate registers
  RegPair!("b", "c") BCx; ///
  RegPair!("d", "e") DEx; ///
  RegPair!("h", "l") HLx; ///
  RegPair!("a", "f") AFx; ///
  ushort SP; /// stack pointer
  ushort PC; /// program counter
  ushort MEMPTR; /// hidden MEMPTR register
  ubyte I, R; /// registers
  bool IFF1, IFF2; /// interrupt flip-flops
  ubyte IM; /// interrupt mode: [0..2]
  uint tstates; /// current tstate
  ubyte intReqLen; /// /INT length

  ubyte border; /// border color [0..15] (>7 for pentagon bright borders)
  ubyte p7FFD; /// last out to port #7FFD
  ubyte p1FFD; /// last out to port #1FFD
  ubyte pEFF7; /// last out to port #EFF7 (???)
  ubyte pFE; /// last out to port #FE (this can be useful to emulate different ZX issues, usually 0xff)

  bool trdosPaged; /// is TR-DOS ROM paged in?

  AYType ayType;
  ubyte ayCyrReg; /// last selected AY register
  ubyte[16] ayRegs; /// values of AY registers

  ubyte[][256] rom; /// ROM pages (either empty, or exactly PageSize bytes)
  ubyte[][256] ram; /// RAM pages (either empty, or exactly PageSize bytes)

public:
  // try to detect and load snapshot
  void load (VFile fl, const(char)[] fname=null) {
    //conwriteln("fname: [", fname, "]");
    char[4] sign;
    auto fsize = fl.size;
    if (fl.size < 24) throw new ZXSnapException("unknown snapshot format"); // arbitrary limit
    fl.seek(0);
    fl.rawReadExact(sign[]);
    if (sign == "ZXST") { loadSZX(fl); return; }
    if (sign == "RIFF") throw new ZXSnapException("ZX32 snapshots aren't supported yet");
    // alas, there is no way to detect Z80 or SNA reliably ('cmon, people, write signatures!)
    if (fname.length >= 4 && fname.endsWithCI(".z80")) { loadZ80(fl); return; }
    if (fname.length >= 4 && fname.endsWithCI(".sna")) { loadSNA(fl); return; }
    // try file size
    if (fsize == 49179 || fsize == 131103 || fsize == 147487) { loadSNA(fl); return; }
    throw new ZXSnapException("unknown snapshot format");
  }

private:
  void loadSNA (VFile fl) {
    fl.seek(0);
    auto fsize = fl.size;
    if (fsize != 49179 && fsize != 131103 && fsize != 147487) throw new ZXSnapException("invalid SNA");
    if (fsize == 49179) model = Model.zx48; else if (!is128K) model = default128;
    // ports
    p1FFD = 0x00;
    pEFF7 = 0x00;
    pFE = 0xff;
    trdosPaged = false;
    // z80 state
    I = fl.readNum!ubyte;
    HLx = fl.readNum!ushort;
    DEx = fl.readNum!ushort;
    BCx = fl.readNum!ushort;
    AFx = fl.readNum!ushort;
    HL = fl.readNum!ushort;
    DE = fl.readNum!ushort;
    BC = fl.readNum!ushort;
    IY = fl.readNum!ushort;
    IX = fl.readNum!ushort;
    IFF1 = IFF2 = ((fl.readNum!ubyte&0x04) != 0);
    R = fl.readNum!ubyte;
    AF = fl.readNum!ushort;
    SP = fl.readNum!ushort;
    if (SP < 0x4000) throw new ZXSnapException("invalid SNA"); // no, really!
    IM = fl.readNum!ubyte;
    if (IM > 2) throw new ZXSnapException("invalid SNA"); // 'cmon!
    border = fl.readNum!ubyte&0x0f;
    // memory
    ram[5].length = PageSize;
    fl.rawReadExact(ram[5]);
    ram[2].length = PageSize;
    fl.rawReadExact(ram[2]);
    ram[0].length = PageSize;
    fl.rawReadExact(ram[0]);

    ubyte peekB (ushort addr) {
      if (addr < 0x4000) throw new ZXSnapException("invalid SNA"); // oops
      if (addr >= 0x4000 && addr < 0x8000) return ram[5][addr-0x4000];
      if (addr >= 0x8000 && addr < 0xC000) return ram[2][addr-0x8000];
      assert(addr >= 0xC000);
      return ram[0][addr-0xC000];
    }

    // 48K?
    if (fsize == 49179) {
      p7FFD = 0x20;
      // we are done, get PC address
      PC = peekB(SP++);
      PC |= peekB(SP++)<<8;
      MEMPTR = PC; // RETN does this
      if (tstates == 0) tstates = 4+3+3; // ~RETN without contention
      return;
    }

    // oops, 128K
    PC = fl.readNum!ushort;
    p7FFD = fl.readNum!ubyte;
    trdosPaged = (fl.readNum!ubyte != 0);
    // fix currently paged bank
    ubyte cpb = p7FFD&0x07;
    if (cpb != 0) { ram[cpb].length = PageSize; ram[cpb][] = ram[0][]; }
    // load other banks
    foreach (ubyte pg; 0..8) {
      if (pg == 2 || pg == 5 || pg == cpb) continue;
      ram[pg].length = PageSize;
      fl.rawReadExact(ram[pg]);
    }
  }

  void loadZ80 (VFile fl) {
    fl.seek(0);

    halted = false;
    lastWasEI = false;

    AF.a = fl.readNum!ubyte;
    AF.f = fl.readNum!ubyte;
    BC = fl.readNum!ushort;
    HL = fl.readNum!ushort;
    PC = fl.readNum!ushort;
    SP = fl.readNum!ushort;
    I = fl.readNum!ubyte;
    R = fl.readNum!ubyte&0x7f;
    ubyte flags0 = fl.readNum!ubyte;
    if (flags0 == 0xff) flags0 = 1; // for compatibility
    R |= (flags0<<7);
    border = (flags0>>1)&0x07;
    // bit4: samrom
    // bit5: compressed
    DE = fl.readNum!ushort;
    BCx = fl.readNum!ushort;
    DEx = fl.readNum!ushort;
    HLx = fl.readNum!ushort;
    AFx.a = fl.readNum!ubyte;
    AFx.f = fl.readNum!ubyte;
    IY = fl.readNum!ushort;
    IX = fl.readNum!ushort;
    IFF1 = (fl.readNum!ubyte != 0);
    IFF2 = (fl.readNum!ubyte != 0);
    ubyte flags1 = fl.readNum!ubyte;
    IM = flags1&0x03;
    if (IM > 2) throw new ZXSnapException("invalid Z80 (invalid IM)"); // 'cmon!
    // bit2: issue2
    //assert(fl.tell == 30);

    // ports
    p7FFD = 0x20;
    p1FFD = 0x00;
    pEFF7 = 0x00;
    pFE = 0xff;
    trdosPaged = false;
    tstates = 0;

    ushort addr;
    ubyte pg;

    void pokeb1 (ubyte b) {
      if (addr < 0x4000) throw new ZXSnapException("invalid Z80 (bad POKE1 addr)");
      if (addr >= 0x4000 && addr < 0x8000) ram[5][addr&0x3FFF] = b;
      if (addr >= 0x8000 && addr < 0xC000) ram[2][addr&0x3FFF] = b;
      if (addr >= 0xC000) ram[0][addr&0x3FFF] = b;
      ++addr;
    }

    void pokeb2 (ubyte b) {
      if (addr >= 0x4000) throw new ZXSnapException("invalid Z80 (bad POKE2 addr)");
      ram[pg][addr++] = b;
    }

    if (PC == 0) {
      // version 2 or 3
      uint hdrlen = fl.readNum!ushort;
      if (hdrlen != 23 && hdrlen != 54 && hdrlen != 55) throw new ZXSnapException("invalid Z80"); // 'cmon!
      conwriteln("Z80 v", (hdrlen == 23 ? 2 : 3));
      PC = fl.readNum!ushort;
      ubyte hwmode = fl.readNum!ubyte;
      switch (hwmode) {
        case 0: model = Model.zx48; break; // 48, 48
        case 1: model = Model.zx48; break; // 48+IF1, 48+IF1
        case 2: throw new ZXSnapException("invalid Z80 (SamRam)"); // 'cmon! SamRam
        case 3: if (hdrlen == 23) model = Model.zx128; else model = Model.zx48; break; // 128K, 48K+MGT
        case 4: model = Model.zx128; break; // 128K+IF1, 128K
        case 5: if (hdrlen == 23) throw new ZXSnapException("invalid Z80 (128K+IF1)"); model = Model.zx128; break; // ..., 128K+IF1
        case 6: if (hdrlen == 23) throw new ZXSnapException("invalid Z80 (128K+MGT)"); model = Model.zx128; break; // ..., 128K+MGT
        case 7: throw new ZXSnapException("invalid Z80 (plus3:0)"); // +3
        case 8: throw new ZXSnapException("invalid Z80 (plus3:1)"); // +3
        case 9: model = Model.pentagon128; break;
        case 10: throw new ZXSnapException("invalid Z80 (scorpion256)"); // scorpion256
        case 11: throw new ZXSnapException("invalid Z80 (Didaktik-Kompakt)"); // Didaktik-Kompakt
        case 12: throw new ZXSnapException("invalid Z80 (plus2)"); // +2
        case 13: throw new ZXSnapException("invalid Z80 (plus2a)"); // +2A
        // other crap like timex and so on
        default: throw new ZXSnapException("invalid Z80 (unknown model)");
      }
      p7FFD = fl.readNum!ubyte;
      if (fl.readNum!ubyte == 0xff) throw new ZXSnapException("invalid Z80 (IF1 ROM paged)"); // IF1 ROM paged
      ubyte flags2 = fl.readNum!ubyte; // If bit 7 of byte 37 is set, the hardware types are modified slightly: any 48K machine becomes a 16K machine, any 128K machines becomes a +2 and any +3 machine becomes a +2A.
      if (flags2&0x80) throw new ZXSnapException("invalid Z80 (extended Z80 flag)"); // not yet
      //if (flags2&0x80) conwriteln("Z80: extended flag found");
      if (model == Model.zx48 && (flags2&0x04) == 0) {
        // no AY
        ayType = AYType.None;
        ayCyrReg = 0;
        ayRegs[] = 0;
        foreach (immutable cc; 0..17) fl.readNum!ubyte; // skip AY info
      } else {
        ayType = AYType.AY128;
        ayCyrReg = fl.readNum!ubyte;
        fl.rawReadExact(ayRegs[]);
      }
      if (hdrlen > 23) {
        //assert(fl.tell == 55);
        tstates = fl.readNum!ushort;
        tstates |= fl.readNum!ubyte<<16;
        fl.readNum!ubyte; // unused flag
        if (fl.readNum!ubyte == 0xff) throw new ZXSnapException("invalid Z80 (MGT ROM paged)"); // MGT ROM paged
        if (fl.readNum!ubyte == 0xff) throw new ZXSnapException("invalid Z80 (Multiface ROM paged)"); // Multiface ROM paged
        //if (fl.readNum!ubyte != 0xff) throw new ZXSnapException("invalid Z80"); // first 8KB is RAM
        //if (fl.readNum!ubyte != 0xff) throw new ZXSnapException("invalid Z80"); // second 8KB is RAM
        fl.readNum!ubyte;
        fl.readNum!ubyte;
        foreach (immutable cc; 0..10) fl.readNum!ubyte; // keyboard mappings for joy
        foreach (immutable cc; 0..10) fl.readNum!ubyte; // keyboard mappings for joy
        fl.readNum!ubyte; // MGT type
        fl.readNum!ubyte; // Disciple crap
        fl.readNum!ubyte; // Disciple crap
        if (hdrlen == 55) p1FFD = fl.readNum!ubyte;
      }
      assert(fl.tell == 30+2+hdrlen);
      // memory
      for (;;) {
        uint len = 0;
        ubyte tmpb;
        if (fl.rawRead((&tmpb)[0..1]).length != 1) break;
        len = tmpb;
        len |= fl.readNum!ubyte<<8;
        pg = fl.readNum!ubyte;
        // convert page number
        if (model == Model.zx48) {
          switch (pg) {
            case 0: throw new ZXSnapException("invalid Z80 (48:48K ROM)"); // 48K ROM
            case 1: throw new ZXSnapException("invalid Z80 (48:craprom)"); // some crap ROM
            case 2: throw new ZXSnapException("invalid Z80 (invalid 48K page 2)");
            case 3: throw new ZXSnapException("invalid Z80 (invalid 48K page 3)");
            case 4: pg = 2; break;
            case 5: pg = 0; break;
            case 6: throw new ZXSnapException("invalid Z80 (invalid 48K page 6)");
            case 7: throw new ZXSnapException("invalid Z80 (invalid 48K page 7)");
            case 8: pg = 5; break;
            default: throw new ZXSnapException("invalid Z80 (invalid 48K page)");
          }
        } else {
          switch (pg) {
            case 0: throw new ZXSnapException("invalid Z80 (128:48K ROM)"); // 48K ROM
            case 1: throw new ZXSnapException("invalid Z80 (128:craprom)"); // some crap ROM
            case 2: throw new ZXSnapException("invalid Z80 (SamRam ROM)"); // samram ROM
            case 3: pg = 0; break;
            case 4: pg = 1; break;
            case 5: pg = 2; break;
            case 6: pg = 3; break;
            case 7: pg = 4; break;
            case 8: pg = 5; break;
            case 9: pg = 6; break;
            case 10: pg = 7; break;
            default: throw new ZXSnapException("invalid Z80 (invalid 128K page)");
          }
        }
        //conwriteln("page #", pg, "; len=", len, "; pos=", fl.tell);
        conwritefln!"page #%s; len=%s; pos=0x%08x"(pg, len, fl.tell-3);
        ram[pg].length = PageSize;
        if (len == 0xffff) {
          // not compressed
          fl.rawReadExact(ram[pg]);
        } else {
          // compressed
          addr = 0;
          while (len-- > 0) {
            ubyte b = fl.readNum!ubyte;
            if (b != 0xED) {
              pokeb2(b);
            } else {
              if (len < 1) throw new ZXSnapException("invalid Z80 (missing second RLE byte)");
              b = fl.readNum!ubyte;
              --len;
              if (b == 0xED) {
                if (len < 2) throw new ZXSnapException("invalid Z80 (bad second RLE byte)");
                ubyte count = fl.readNum!ubyte;
                if (count == 0) throw new ZXSnapException("invalid Z80 (bad RLE count)");
                b = fl.readNum!ubyte;
                len -= 2;
                while (count--) pokeb2(b);
              } else {
                pokeb2(0xED);
                pokeb2(b);
              }
            }
          }
          //if (addr != 0x4000) conwriteln("BAD page #", pg, "; size=", addr);
          if (addr != 0x4000) throw new ZXSnapException("invalid Z80 (bad page size)");
        }
      }
    } else {
      conwriteln("Z80 v1");
      conwritefln!"  #%02X"(flags0);
      // version 1
      // memory
      ram[5].length = PageSize;
      ram[2].length = PageSize;
      ram[0].length = PageSize;
      if (flags0&0x20) {
        // compressed
        addr = 0x4000;
        while (addr >= 0x4000) {
          ubyte b = fl.readNum!ubyte;
          if (b != 0xED) {
            pokeb1(b);
          } else {
            b = fl.readNum!ubyte;
            if (b == 0xED) {
              ubyte count = fl.readNum!ubyte;
              if (count == 0) throw new ZXSnapException("invalid Z80 (RLE count)");
              b = fl.readNum!ubyte;
              while (count--) pokeb1(b);
            } else {
              pokeb1(0xED);
              pokeb1(b);
            }
          }
        }
        if (fl.readNum!ubyte != 0x00) throw new ZXSnapException("invalid Z80 (RLE postbyte 0)");
        if (fl.readNum!ubyte != 0xED) throw new ZXSnapException("invalid Z80 (RLE postbyte 1)");
        if (fl.readNum!ubyte != 0xED) throw new ZXSnapException("invalid Z80 (RLE postbyte 2)");
        if (fl.readNum!ubyte != 0x00) throw new ZXSnapException("invalid Z80 (RLE postbyte 3)");
        if (addr != 0) throw new ZXSnapException("invalid Z80 (RLE address)");
      } else {
        // not compressed
        fl.rawReadExact(ram[5]);
        fl.rawReadExact(ram[2]);
        fl.rawReadExact(ram[0]);
      }
    }
  }

  // magic already read
  void loadSZX (VFile fl) {
    enum MachineId {
      zx16,
      zx48,
      zx128,
      plus2,
      plus2a,
      plus3,
      plus3e,
      pentagon128,
      tc2048,
      tc2068,
      scorpion,
      se,
      ts2068,
      pentagon512,
      pentagon1024,
      ntsc48k,
      zx128ke,
    }

    /*
    template FOURCC(string s) if (s.length == 4) {
      enum FOURCC = cast(uint)((cast(uint)s[0])|((cast(uint)s[1])<<8)|((cast(uint)s[2])<<16)|((cast(uint)s[3])<<24));
    }
    */

    enum ZXSTMF_ALTERNATETIMINGS = 1;

    ubyte verMajor = fl.readNum!byte;
    ubyte verMinor = fl.readNum!byte;
    ubyte machineId = fl.readNum!byte;
    ubyte hdrFlags = fl.readNum!byte;
    if (verMajor > 2) throw new ZXSnapException("invalid SZX version");
    switch (machineId) {
      case MachineId.zx16: case MachineId.zx48: case MachineId.ntsc48k: model = Model.zx48; break;
      case MachineId.zx128: model = Model.zx128; break;
      case MachineId.plus2: model = Model.plus2; break;
      case MachineId.plus2a: model = Model.plus2a; break;
      case MachineId.plus3: model = Model.plus3; break;
      case MachineId.plus3e: model = Model.plus3e; break;
      case MachineId.pentagon128: model = Model.pentagon128; break;
      case MachineId.tc2048: case MachineId.tc2068: throw new ZXSnapException("SZX for TC is not supported");
      case MachineId.scorpion: throw new ZXSnapException("SZX for Scorpion is not supported");
      case MachineId.se: throw new ZXSnapException("SZX for SE is not supported");
      case MachineId.ts2068: throw new ZXSnapException("SZX for TS2068 is not supported");
      case MachineId.pentagon512: model = Model.pentagon512; break;
      case MachineId.pentagon1024: model = Model.pentagon1024; break;
      //case MachineId.ntsc48k: throw new ZXSnapException("SZX for NTSC48K is not supported");
      case MachineId.zx128ke: throw new ZXSnapException("SZX for 128KE is not supported");
      default: { import std.conv : to; throw new ZXSnapException("unknown SZX model: "~machineId.to!string); }
    }

    lateTimings = ((hdrFlags&ZXSTMF_ALTERNATETIMINGS) != 0);

    // load blocks
    enum MaxSize = 1024*1024*64;
    mainloop: for (;;) {
      char[4] id;
      // this way, 'cause rawRead can read *up* *to* requested number of bytes
      foreach (ref char b; id[]) if (fl.rawRead((&b)[0..1]).length == 0) break mainloop;
      uint size = fl.readNum!uint;
      //if (size > MaxSize) { fl.seek(size, Seek.Cur); continue; } // skip huge block (we aren't interested anyway)
      if (id == "AY\x00\x00") {
        // AY
        if (size < 18) throw new ZXSnapException("AY block too small");
        size -= 18;
        ubyte flags = fl.readNum!ubyte;
        ayCyrReg = fl.readNum!ubyte;
        fl.rawReadExact(ayRegs[]);
        switch (flags) {
          case 0: ayType = AYType.AY128; break;
          case 1: ayType = AYType.Fuller; break;
          case 2: ayType = AYType.AY128; break; // melodik; the same as 128K AY
          default: conwriteln("SZX: unknown AY type: ", flags); ayType = AYType.AY128; break;
        }
      } else if (id == "B128") {
        // betadisk 128
        //FIXME: we're interested only in flags for now
        if (size < 2) throw new ZXSnapException("SZX: Beta128 block too small");
        size -= 2;
        ushort flags = fl.readNum!ushort;
        if (flags&2) conwriteln("SZX: custom Beta128 ROMs are not supported");
        trdosPaged = ((flags&(1|4)) == (1|4)); // connected and paged in
      } else if (id == "RAMP") {
        // RAM page
        if (size < 3) throw new ZXSnapException("SZX: RAM block too small");
        size -= 3;
        ushort flags = fl.readNum!ushort;
        ubyte pno = fl.readNum!ubyte;
        // compressed?
        if (flags&1) {
          //conwriteln("SZX: compressed RAM page ", pno);
          if (size < 4) throw new ZXSnapException("SZX: RAM block too small");
          auto npos = fl.tell+size;
          {
            auto dc = wrapZLibStreamRO(wrapStreamRO(fl, fl.tell, size), PageSize);
            ram[pno].length = PageSize;
            dc.rawReadExact(ram[pno][]);
          }
          size = 0;
          fl.seek(npos);
        } else {
          if (size < PageSize) throw new ZXSnapException("SZX: RAM block too small");
          size -= PageSize;
          ram[pno].length = PageSize;
          fl.rawReadExact(ram[pno][]);
        }
      } else if (id == "SPCR") {
        // Spectrum Control Registers
        if (size < 4) throw new ZXSnapException("SZX: SPCR block too small");
        size -= 4;
        border = fl.readNum!ubyte&0x0f;
        p7FFD = fl.readNum!ubyte;
        if (memsize < 128) p7FFD = 0;
        if (model == Model.pentagon1024) {
          pEFF7 = fl.readNum!ubyte;
        } else {
          p1FFD = fl.readNum!ubyte;
          if (model < Model.plus2a) p1FFD = 0;
        }
        pFE = fl.readNum!ubyte;
      } else if (id == "Z80R") {
        // Z80 Registers
        if (size < 37) throw new ZXSnapException("SZX: Z80 Registers block too small");
        size -= 37;
        AF.w = fl.readNum!ushort;
        BC.w = fl.readNum!ushort;
        DE.w = fl.readNum!ushort;
        HL.w = fl.readNum!ushort;
        AFx.w = fl.readNum!ushort;
        BCx.w = fl.readNum!ushort;
        DEx.w = fl.readNum!ushort;
        HLx.w = fl.readNum!ushort;
        IX.w = fl.readNum!ushort;
        IY.w = fl.readNum!ushort;
        SP = fl.readNum!ushort;
        PC = fl.readNum!ushort;
        I = fl.readNum!ubyte;
        R = fl.readNum!ubyte;
        IFF1 = (fl.readNum!ubyte != 0);
        IFF2 = (fl.readNum!ubyte != 0);
        IM = fl.readNum!ubyte;
        if (IM > 2) IM = 0;
        tstates = fl.readNum!uint;
        fl.readNum!ubyte; // chHoldIntReqCycles (???)
        ubyte flags = fl.readNum!ubyte;
        lastWasEI = ((flags&1) != 0);
        halted = ((flags&2) != 0);
        if (halted) lastWasEI = false; // just in case
        MEMPTR = fl.readNum!ushort;
        if (verMinor < 3) MEMPTR = 0;
      }
      // skip the rest of the block
      if (size > 0) fl.seek(size, Seek.Cur);
    }
  }
}
