/* This file is paritally taken from libspectrum,
 * Copyright (c) 2003 Philip Kendall ( philip-fuse@shadowmagic.org.uk )
 * Copyright (c) 2016 Stuart Brady
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
module zxinfo;


// ////////////////////////////////////////////////////////////////////////// //
//public __gshared ZXTimings curmachine = baseTimings[0];


// ////////////////////////////////////////////////////////////////////////// //
public struct FrameTimigs {
  // line timings in tstates
  ushort leftBorder, horizontalScreen, rightBorder, horizontalRetrace;
  // frame timings in lines
  ushort topBorder, verticalScreen, bottomBorder, verticalRetrace;
  // how long does an interrupt last in tstates
  ubyte interruptLength;
  // how long after interrupt is the top-level pixel of the main screen displayed
  uint topLeftPixel;

  uint tstatesPerLine;
  uint linesPerFrame;
  uint tstatesPerFrame;

  this (ushort alb, ushort ahs, ushort arb, ushort ahr, ushort atb, ushort avs, ushort abb, ushort avr, ubyte ailen, uint atlp) pure nothrow @safe @nogc {
    leftBorder = alb;
    horizontalScreen = ahs;
    rightBorder = arb;
    horizontalRetrace = ahr;
    topBorder = atb;
    verticalScreen = avs;
    bottomBorder = abb;
    verticalRetrace = avr;
    interruptLength = ailen;
    topLeftPixel = atlp;
    tstatesPerLine = leftBorder+horizontalScreen+rightBorder+horizontalRetrace;
    linesPerFrame = topBorder+verticalScreen+bottomBorder+verticalRetrace;
    tstatesPerFrame = tstatesPerLine*linesPerFrame;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
private:
static immutable FrameTimigs frameTimingFerranti5C6C = FrameTimigs(
  24, 128, 24, 48, /* Horizontal, 224 clocks per line */
  48, 192, 48, 24, /* Vertical, 312 lines per frame */
  32, 14336
);

static immutable FrameTimigs frameTimingFerranti7C = FrameTimigs(
  24, 128, 24, 52, /* Horizontal, 228 clocks per line */
  48, 192, 48, 23, /* Vertical, 311 lines per frame */
  36, 14362
);

static immutable FrameTimigs frameTimingAmstradAsic = FrameTimigs(
  24, 128, 24, 52, /* Horizontal, 228 clocks per line */
  48, 192, 48, 23, /* Vertical, 311 lines per frame */
  32, 14365
);

static immutable FrameTimigs frameTimingPentagon = FrameTimigs(
  36, 128, 28, 32, /* Horizontal, 224 clocks per line */
  64, 192, 48, 16, /* Vertical 320 lines per frame */
  36, 17988
);


// ////////////////////////////////////////////////////////////////////////// //
public enum ZXContention {
  None,
  C48,
  C128,
}

/// the frame timings of a machine
public struct ZXTimings {
  string name;
  uint cpuSpeed; // processor speed in Hz
  uint aySpeed; // AY clock speed in Hz
  ushort mem; // memory, kb
  bool pentagon;
  ZXContention contention;
  immutable(FrameTimigs)* timings;
}


// ////////////////////////////////////////////////////////////////////////// //
// the actual data from which the full timings are constructed
public static immutable ZXTimings[18] baseTimings = [
  ZXTimings("48",           3500000,       0,   48, false, ZXContention.C48,  &frameTimingFerranti5C6C),
  ZXTimings("128",          3546900, 1773400,  128, false, ZXContention.C128, &frameTimingFerranti7C),
  ZXTimings("Plus2",        3546900, 1773400,  128, false, ZXContention.C128, &frameTimingFerranti7C),
  ZXTimings("Plus2A",       3546900, 1773400,  128, false, ZXContention.C128, &frameTimingAmstradAsic),
  ZXTimings("Plus3",        3546900, 1773400,  128, false, ZXContention.C128, &frameTimingAmstradAsic),
  ZXTimings("Plus3e",       3546900, 1773400,  128, false, ZXContention.C128, &frameTimingAmstradAsic),
  ZXTimings("Pentagon",     3584000, 1792000,  128,  true, ZXContention.None, &frameTimingPentagon),
  ZXTimings("Pentagon512",  3584000, 1792000,  512,  true, ZXContention.None, &frameTimingPentagon),
  //ZXTimings("Pentagon1024", 3584000, 1792000, 1024,  true, ZXContention.None, &frameTimingPentagon),
];


// ////////////////////////////////////////////////////////////////////////// //
import iv.strex;

public immutable(ZXTimings) zxFindMachine (const(char)[] name, bool* found=null) nothrow @trusted @nogc {
  int firstAbbr = -1;
  foreach (immutable idx, const ref bt; baseTimings[]) {
    if (strEquCI(bt.name, name)) {
      if (found !is null) *found = true;
      return bt;
    }
    if (firstAbbr < 0 && name.length < bt.name.length && strEquCI(bt.name[0..name.length], name)) { firstAbbr = cast(int)idx; }
  }
  if (found !is null) *found = (firstAbbr >= 0);
  if (firstAbbr < 0) firstAbbr = 0;
  return baseTimings[firstAbbr];
}


// ////////////////////////////////////////////////////////////////////////// //
public struct ZXKeyInfo {
  string name;
  //ubyte port;
  //ubyte mask;
  ushort portmask;

pure nothrow @safe @nogc:
  this (string aname, ubyte aport, ubyte amask) { name = aname; portmask = cast(ushort)((aport<<8)|amask); }
  @property ubyte port () const { pragma(inline, true); return cast(ubyte)(portmask>>8); }
  @property ubyte mask () const { pragma(inline, true); return cast(ubyte)portmask; }
}


public static immutable ZXKeyInfo[49] zxKeyInfo = [
  // keyboard
  ZXKeyInfo("cap", 0, 0x01),
  ZXKeyInfo("cs", 0, 0x01),
  ZXKeyInfo("z", 0, 0x02),
  ZXKeyInfo("x", 0, 0x04),
  ZXKeyInfo("c", 0, 0x08),
  ZXKeyInfo("v", 0, 0x10),
  ZXKeyInfo("a", 1, 0x01),
  ZXKeyInfo("s", 1, 0x02),
  ZXKeyInfo("d", 1, 0x04),
  ZXKeyInfo("f", 1, 0x08),
  ZXKeyInfo("g", 1, 0x10),
  ZXKeyInfo("q", 2, 0x01),
  ZXKeyInfo("w", 2, 0x02),
  ZXKeyInfo("e", 2, 0x04),
  ZXKeyInfo("r", 2, 0x08),
  ZXKeyInfo("t", 2, 0x10),
  ZXKeyInfo("1", 3, 0x01),
  ZXKeyInfo("2", 3, 0x02),
  ZXKeyInfo("3", 3, 0x04),
  ZXKeyInfo("4", 3, 0x08),
  ZXKeyInfo("5", 3, 0x10),
  ZXKeyInfo("0", 4, 0x01),
  ZXKeyInfo("9", 4, 0x02),
  ZXKeyInfo("8", 4, 0x04),
  ZXKeyInfo("7", 4, 0x08),
  ZXKeyInfo("6", 4, 0x10),
  ZXKeyInfo("p", 5, 0x01),
  ZXKeyInfo("o", 5, 0x02),
  ZXKeyInfo("i", 5, 0x04),
  ZXKeyInfo("u", 5, 0x08),
  ZXKeyInfo("y", 5, 0x10),
  ZXKeyInfo("ent", 6, 0x01),
  ZXKeyInfo("enter", 6, 0x01),
  ZXKeyInfo("l", 6, 0x02),
  ZXKeyInfo("k", 6, 0x04),
  ZXKeyInfo("j", 6, 0x08),
  ZXKeyInfo("h", 6, 0x10),
  ZXKeyInfo("spc", 7, 0x01),
  ZXKeyInfo("space", 7, 0x01),
  ZXKeyInfo("sym", 7, 0x02),
  ZXKeyInfo("ss", 7, 0x02),
  ZXKeyInfo("m", 7, 0x04),
  ZXKeyInfo("n", 7, 0x08),
  ZXKeyInfo("b", 7, 0x10),
  // kempston
  ZXKeyInfo("kright", 8, 0x01),
  ZXKeyInfo("kleft", 8, 0x02),
  ZXKeyInfo("kdown", 8, 0x04),
  ZXKeyInfo("kup", 8, 0x08),
  ZXKeyInfo("kfire", 8, 0x10),
];
