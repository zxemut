/* ProTracker 1 module decoder
 * Copyright (C) 2007 by Deryabin Andrew ( andrewderyabin@gmail.com )
 * Copyright (C) 2017 Ketmar // Invisible Vector
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
module modplay.modsqt is aliced;
private:

import modplay.mod_low;

immutable ushort[$] SQT_Table =
[ 0xd5d, 0xc9c, 0xbe7, 0xb3c, 0xa9b, 0xa02, 0x973, 0x8eb, 0x86b, 0x7f2, 0x780, 0x714, 0x6ae, 0x64e, 0x5f4, 0x59e, 0x54f, 0x501, 0x4b9, 0x475, 0x435, 0x3f9, 0x3c0, 0x38a, 0x357, 0x327, 0x2fa, 0x2cf, 0x2a7, 0x281, 0x25d, 0x23b, 0x21b, 0x1fc, 0x1e0, 0x1c5, 0x1ac, 0x194, 0x17d, 0x168, 0x153, 0x140, 0x12e, 0x11d, 0x10d, 0xfe, 0xf0, 0xe2, 0xd6, 0xca, 0xbe, 0xb4, 0xaa, 0xa0, 0x97, 0x8f, 0x87, 0x7f, 0x78, 0x71, 0x6b, 0x65, 0x5f, 0x5a, 0x55, 0x50, 0x4c, 0x47, 0x43, 0x40, 0x3c, 0x39, 0x35, 0x32, 0x30, 0x2d, 0x2a, 0x28, 0x26, 0x24, 0x22, 0x20, 0x1e, 0x1c, 0x1b, 0x19, 0x18, 0x16, 0x15, 0x14, 0x13, 0x12, 0x11, 0x10, 0xf, 0xe ];

align(1) struct SQT_File
{
align(1):
    ubyte SQT_Size0, SQT_Size1;
    ubyte SQT_SamplesPointer0, SQT_SamplesPointer1;
    ubyte SQT_OrnamentsPointer0, SQT_OrnamentsPointer1;
    ubyte SQT_PatternsPointer0, SQT_PatternsPointer1;
    ubyte SQT_PositionsPointer0, SQT_PositionsPointer1;
    ubyte SQT_LoopPointer0, SQT_LoopPointer1;
}

const(SQT_File)* Header (ref SQT_SongInfo info) { return cast(const(SQT_File)*)info.mod; }

ushort SQT_Size0 (ref SQT_SongInfo info) { return cast(ushort)(info.Header.SQT_Size0 | (info.Header.SQT_Size0 << 8)); }
ushort SQT_SamplesPointer (ref SQT_SongInfo info) { return cast(ushort)(info.Header.SQT_SamplesPointer0 | (info.Header.SQT_SamplesPointer1 << 8)); }
ushort SQT_OrnamentsPointer (ref SQT_SongInfo info) { return cast(ushort)(info.Header.SQT_OrnamentsPointer0 | (info.Header.SQT_OrnamentsPointer1 << 8)); }
ushort SQT_PatternsPointer (ref SQT_SongInfo info) { return cast(ushort)(info.Header.SQT_PatternsPointer0 | (info.Header.SQT_PatternsPointer1 << 8)); }
ushort SQT_PositionsPointer (ref SQT_SongInfo info) { return cast(ushort)(info.Header.SQT_PositionsPointer0 | (info.Header.SQT_PositionsPointer1 << 8)); }
ushort SQT_LoopPointer (ref SQT_SongInfo info) { return cast(ushort)(info.Header.SQT_LoopPointer0 | (info.Header.SQT_LoopPointer1 << 8)); }

//#define MAKE_PWORD(x) (ushort *)(x)

align(1) struct SQT_Channel_Parameters
{
align(1):
    ushort Address_In_Pattern, SamplePointer, Point_In_Sample, OrnamentPointer, Point_In_Ornament, Ton, ix27;
    ubyte Volume, Amplitude, Note, ix21;
    short Ton_Slide_Step, Current_Ton_Sliding;
    byte Sample_Tik_Counter, Ornament_Tik_Counter, Transposit;
    bool Enabled, Envelope_Enabled, Ornament_Enabled, Gliss, MixNoise, MixTon, b4ix0, b6ix0, b7ix0;
}

align(1) struct SQT_Parameters
{
align(1):
    ubyte Delay, DelayCounter, Lines_Counter;
    ushort Positions_Pointer;
}

align(1) struct SQT_SongInfo
{
align(1):
    SQT_Parameters SQT;
    SQT_Channel_Parameters SQT_A, SQT_B, SQT_C;
    ubyte[] data;
    const(ubyte)* mod () { return data.ptr; }
}

/*
#define SQT_A ((SQT_SongInfo *)info.data).SQT_A
#define SQT_B ((SQT_SongInfo *)info.data).SQT_B
#define SQT_C ((SQT_SongInfo *)info.data).SQT_C
#define SQT ((SQT_SongInfo *)info.data).SQT
*/

bool SQT_PreInit(ref SQT_SongInfo info)
{
    const(ubyte)* mod = info.mod;
    auto header = cast(const(SQT_File)*)mod;
    int i, i1, i2;
    uint j2;
    ushort *pwrd;
    i = info.SQT_SamplesPointer - 10;
    if(i < 0)
        return false;
    i1 = 0;
    i2 = info.SQT_PositionsPointer - i;
    if(i2 < 0)
        return false;
    while(mod[i2] != 0)
    {
        if(i2 > 65536 - 8)
            return false;
        if(i1 < (mod[i2] & 0x7f))
            i1 = mod[i2] & 0x7f;
        i2 += 2;
        if(i1 < (mod[i2] & 0x7f))
            i1 = mod[i2] & 0x7f;
        i2 += 2;
        if(i1 < (mod[i2] & 0x7f))
            i1 = mod[i2] & 0x7f;
        i2 += 3;
    }
    j2 = cast(uint)(&mod[65535]);
    //pwrd = MAKE_PWORD(&header.SQT_SamplesPointer0);
    pwrd = cast(ushort*)(&header.SQT_SamplesPointer0);
    i1 = (info.SQT_PatternsPointer - i + i1 * 2) / 2;
    if(i1 < 1)
        return false;
    for(i2 = 1; i2 <= i1; i2++)
    {
        if(cast(uint)(pwrd) >= j2)
            return false;
        if(*pwrd < i)
            return false;
        *pwrd -= i;
        pwrd++;
    }
    return true;
}

bool SQT_Init(ref SQT_SongInfo info)
{
    const(ubyte)* mod = info.mod;
    auto header = cast(const(SQT_File)*)mod;

    if(!SQT_PreInit(info))
        return false;

    info.SQT_A.Ton = 0;
    info.SQT_A.Envelope_Enabled = false;
    info.SQT_A.Ornament_Enabled = false;
    info.SQT_A.Gliss = false;
    info.SQT_A.Enabled = false;

    info.SQT_B.Ton = 0;
    info.SQT_B.Envelope_Enabled = false;
    info.SQT_B.Ornament_Enabled = false;
    info.SQT_B.Gliss = false;
    info.SQT_B.Enabled = false;

    info.SQT_C.Ton = 0;
    info.SQT_C.Envelope_Enabled = false;
    info.SQT_C.Ornament_Enabled = false;
    info.SQT_C.Gliss = false;
    info.SQT_C.Enabled = false;

    info.SQT.DelayCounter = 1;
    info.SQT.Delay = 1;
    info.SQT.Lines_Counter = 1;
    info.SQT.Positions_Pointer = info.SQT_PositionsPointer;

    return true;
}

void SQT_Call_LC1D1(ref SQT_SongInfo info, ref SQT_Channel_Parameters chan, ref ushort Ptr, ubyte a)
{
    const(ubyte)* mod = info.mod;
    Ptr++;
    if(chan.b6ix0)
    {
        chan.Address_In_Pattern = cast(ushort)(Ptr + 1);
        chan.b6ix0 = false;
    }
    switch(a - 1)
    {
        case 0:
            if(chan.b4ix0)
                chan.Volume = mod[Ptr] & 15;
            break;
        case 1:
            if(chan.b4ix0)
                chan.Volume = (chan.Volume + mod[Ptr]) & 15;
            break;
        case 2:
            if(chan.b4ix0)
            {
                info.SQT_A.Volume = mod[Ptr];
                info.SQT_B.Volume = mod[Ptr];
                info.SQT_C.Volume = mod[Ptr];
            }
            break;
        case 3:
            if(chan.b4ix0)
            {
                info.SQT_A.Volume = (info.SQT_A.Volume + mod[Ptr]) & 15;
                info.SQT_B.Volume = (info.SQT_B.Volume + mod[Ptr]) & 15;
                info.SQT_C.Volume = (info.SQT_C.Volume + mod[Ptr]) & 15;
            }
            break;
        case 4:
            if(chan.b4ix0)
            {
                info.SQT.DelayCounter = mod[Ptr] & 31;
                if(info.SQT.DelayCounter == 0)
                    info.SQT.DelayCounter = 32;
                info.SQT.Delay = info.SQT.DelayCounter;
            }
            break;
        case 5:
            if(chan.b4ix0)
            {
                info.SQT.DelayCounter = (info.SQT.DelayCounter + mod[Ptr]) & 31;
                if(info.SQT.DelayCounter == 0)
                    info.SQT.DelayCounter = 32;
                info.SQT.Delay = info.SQT.DelayCounter;
            }
            break;
        case 6:
            chan.Current_Ton_Sliding = 0;
            chan.Gliss = true;
            chan.Ton_Slide_Step = -(mod[Ptr]);
            break;
        case 7:
            chan.Current_Ton_Sliding = 0;
            chan.Gliss = true;
            chan.Ton_Slide_Step = mod[Ptr];
            break;
        default:
            chan.Envelope_Enabled = true;
            ay_writeay(AY_ENV_SHAPE, (a - 1) & 15);
            ay_writeay(AY_ENV_FINE, mod[Ptr]);
            break;
    }
}

void SQT_Call_LC2A8(ref SQT_SongInfo info, ref SQT_Channel_Parameters chan, ubyte a)
{
    const(ubyte)* mod = info.mod;
    auto header = cast(const(SQT_File)*)mod;
    chan.Envelope_Enabled = false;
    chan.Ornament_Enabled = false;
    chan.Gliss = false;
    chan.Enabled = true;
    chan.SamplePointer = ay_sys_getword(&mod[a * 2 + info.SQT_SamplesPointer]);
    chan.Point_In_Sample = cast(ushort)(chan.SamplePointer + 2);
    chan.Sample_Tik_Counter = 32;
    chan.MixNoise = true;
    chan.MixTon = true;
}

void SQT_Call_LC2D9(ref SQT_SongInfo info, ref SQT_Channel_Parameters chan, ubyte a)
{
    const(ubyte)*mod = info.mod;
    auto header = cast(const(SQT_File)*)mod;
    chan.OrnamentPointer = ay_sys_getword(&mod[a * 2 + info.SQT_OrnamentsPointer]);
    chan.Point_In_Ornament = cast(ushort)(chan.OrnamentPointer + 2);
    chan.Ornament_Tik_Counter = 32;
    chan.Ornament_Enabled = true;
}

void SQT_Call_LC283(ref SQT_SongInfo info, ref SQT_Channel_Parameters chan, ref ushort Ptr)
{
    const(ubyte)* mod = info.mod;
    ubyte val = mod[Ptr];
    if(val <= 0x7f)
    {
        SQT_Call_LC1D1(info, chan, Ptr, val);
    }
    else if(val >= 0x80)
    {
        if(((val >> 1) & 31) != 0)
            SQT_Call_LC2A8(info, chan, (val >> 1) & 31);
        if((val & 64) != 0)
        {
            int Temp = mod[Ptr + 1] >> 4;
            if((val & 1) != 0)
                Temp = Temp | 16;
            if(Temp != 0)
                SQT_Call_LC2D9(info, chan, cast(ubyte)Temp);
            Ptr++;
            if((mod[Ptr] & 15) != 0)
                SQT_Call_LC1D1(info, chan, Ptr, mod[Ptr] & 15);
        }
    }
    Ptr++;
}

void SQT_Call_LC191(ref SQT_SongInfo info, ref SQT_Channel_Parameters chan, ref ushort Ptr)
{
    const(ubyte)* mod = info.mod;
    Ptr = chan.ix27;
    chan.b6ix0 = false;
    if(mod[Ptr] <= 0x7f)
    {
        Ptr++;
        SQT_Call_LC283(info, chan, Ptr);
    }
    else if(mod[Ptr] >= 0x80)
    {
        SQT_Call_LC2A8(info, chan, mod[Ptr] & 31);
    }

}

void SQT_PatternInterpreter(ref SQT_SongInfo info, ref SQT_Channel_Parameters chan)
{
    const(ubyte)* mod = info.mod;
    ushort Ptr = 0;
    if(chan.ix21 != 0)
    {
        chan.ix21--;
        if(chan.b7ix0)
            SQT_Call_LC191(info, chan, Ptr);
        return;
    }
    Ptr = chan.Address_In_Pattern;
    chan.b6ix0 = true;
    chan.b7ix0 = false;
    while(true)
    {
        ubyte val = mod[Ptr];
        if(val <= 0x5f)
        {
            chan.Note = mod[Ptr];
            chan.ix27 = Ptr;
            Ptr++;
            SQT_Call_LC283(info, chan, Ptr);
            if(chan.b6ix0)
                chan.Address_In_Pattern = Ptr;
            break;
        }
        else if(val >= 0x60 && val <= 0x6e)
        {
            SQT_Call_LC1D1(info, chan, Ptr, cast(ubyte)(mod[Ptr] - 0x60));
            break;
        }
        else if(val >= 0x6f && val <= 0x7f)
        {
            chan.MixNoise = false;
            chan.MixTon = false;
            chan.Enabled = false;
            if(val != 0x6f)
                SQT_Call_LC1D1(info, chan, Ptr, cast(ubyte)(mod[Ptr] - 0x6f));
            else
                chan.Address_In_Pattern = cast(ushort)(Ptr + 1);
            break;
        }
        else if(val >= 0x80 && val <= 0xbf)
        {
            chan.Address_In_Pattern = cast(ushort)(Ptr + 1);
            if(val <= 0x9f)
            {
                if((val & 16) == 0)
                    chan.Note += val & 15;
                else
                    chan.Note -= val & 15;
            }
            else
            {
                chan.ix21 = val & 15;
                if((val & 16) == 0)
                    break;
                if(chan.ix21 != 0)
                    chan.b7ix0 = true;
            }
            SQT_Call_LC191(info, chan, Ptr);
            break;
        }
        else if(val >= 0xc0)
        {
            chan.Address_In_Pattern = cast(ushort)(Ptr + 1);
            chan.ix27 = Ptr;
            SQT_Call_LC2A8(info, chan, val & 31);
            break;
        }
    }
}

void SQT_GetRegisters(ref SQT_SongInfo info, ref SQT_Channel_Parameters chan, ref ubyte TempMixer)
{
    const(ubyte)* mod = info.mod;
    ubyte j, b0, b1;
    TempMixer = cast(ubyte)(TempMixer << 1);
    if(chan.Enabled)
    {
        b0 = mod[chan.Point_In_Sample];
        chan.Amplitude = b0 & 15;
        if(chan.Amplitude != 0)
        {
            chan.Amplitude -= chan.Volume;
            if(cast(byte)(chan.Amplitude) < 0)
                chan.Amplitude = 0;
        }
        else if(chan.Envelope_Enabled)
            chan.Amplitude = 16;
        b1 = mod[chan.Point_In_Sample + 1];
        if((b1 & 32) != 0)
        {
            TempMixer |= 8;
            ubyte noise = (b0 & 0xf0) >> 3;
            if(cast(byte)(b1) < 0)
                noise++;
            ay_writeay(AY_NOISE_PERIOD, noise);
        }
        if((b1 & 64) != 0)
        {
            TempMixer |= 1;
        }
        j = chan.Note;
        if(chan.Ornament_Enabled)
        {
            j += mod[chan.Point_In_Ornament];
            chan.Ornament_Tik_Counter--;
            if(chan.Ornament_Tik_Counter == 0)
            {
                if(mod[chan.OrnamentPointer] != 32)
                {
                    chan.Ornament_Tik_Counter = mod[chan.OrnamentPointer + 1];
                    chan.Point_In_Ornament = cast(ushort)(chan.OrnamentPointer + 2 + mod[chan.OrnamentPointer]);
                }
                else
                {
                    chan.Ornament_Tik_Counter = mod[chan.SamplePointer + 1];
                    chan.Point_In_Ornament = cast(ushort)(chan.OrnamentPointer + 2 + mod[chan.SamplePointer]);
                }
            }
            else
                chan.Point_In_Ornament++;
        }
        j += chan.Transposit;
        if(j > 0x5f)
            j = 0x5f;
        if((b1 & 16) == 0)
            chan.Ton = cast(ushort)(SQT_Table[j] - ((cast(ushort)(b1 & 15) << 8) + mod[chan.Point_In_Sample + 2]));
        else
            chan.Ton = cast(ushort)(SQT_Table[j] + ((cast(ushort)(b1 & 15) << 8) + mod[chan.Point_In_Sample + 2]));
        chan.Sample_Tik_Counter--;
        if(chan.Sample_Tik_Counter == 0)
        {
            chan.Sample_Tik_Counter = mod[chan.SamplePointer + 1];
            if(mod[chan.SamplePointer] == 32)
            {
                chan.Enabled = false;
                chan.Ornament_Enabled = false;
            }
            chan.Point_In_Sample = cast(ushort)(chan.SamplePointer + 2 + mod[chan.SamplePointer] * 3);
        }
        else
            chan.Point_In_Sample += 3;
        if(chan.Gliss)
        {
            chan.Ton += chan.Current_Ton_Sliding;
            chan.Current_Ton_Sliding += chan.Ton_Slide_Step;
        }
        chan.Ton = chan.Ton & 0xfff;
    }
    else
        chan.Amplitude = 0;
}

void SQT_Play(ref SQT_SongInfo info)
{
    const(ubyte)* mod = info.mod;
    auto header = cast(const(SQT_File)*)mod;
    ubyte TempMixer;

    if(--info.SQT.DelayCounter == 0)
    {
        info.SQT.DelayCounter = info.SQT.Delay;
        if(--info.SQT.Lines_Counter == 0)
        {
            if(mod[info.SQT.Positions_Pointer] == 0)
                info.SQT.Positions_Pointer = info.SQT_LoopPointer;
            if(cast(byte)(mod[info.SQT.Positions_Pointer]) < 0)
                info.SQT_C.b4ix0 = true;
            else
                info.SQT_C.b4ix0 = false;
            info.SQT_C.Address_In_Pattern = ay_sys_getword(&mod[cast(ubyte)(mod[info.SQT.Positions_Pointer] * 2) + info.SQT_PatternsPointer]);
            info.SQT.Lines_Counter = mod[info.SQT_C.Address_In_Pattern];
            info.SQT_C.Address_In_Pattern++;
            info.SQT.Positions_Pointer++;
            info.SQT_C.Volume = mod[info.SQT.Positions_Pointer] & 15;
            if((mod[info.SQT.Positions_Pointer] >> 4) < 9)
                info.SQT_C.Transposit = mod[info.SQT.Positions_Pointer] >> 4;
            else
                info.SQT_C.Transposit = -((mod[info.SQT.Positions_Pointer] >> 4) - 9) - 1;
            info.SQT.Positions_Pointer++;
            info.SQT_C.ix21 = 0;

            if(mod[info.SQT.Positions_Pointer] == 0)
                info.SQT.Positions_Pointer = info.SQT_LoopPointer;
            if(cast(byte)(mod[info.SQT.Positions_Pointer]) < 0)
                info.SQT_B.b4ix0 = true;
            else
                info.SQT_B.b4ix0 = false;
            info.SQT_B.Address_In_Pattern = cast(ushort)(ay_sys_getword(&mod[cast(ubyte)(mod[info.SQT.Positions_Pointer] * 2) + info.SQT_PatternsPointer]) + 1);
            info.SQT.Positions_Pointer++;
            info.SQT_B.Volume = mod[info.SQT.Positions_Pointer] & 15;
            if((mod[info.SQT.Positions_Pointer] >> 4) < 9)
                info.SQT_B.Transposit = mod[info.SQT.Positions_Pointer] >> 4;
            else
                info.SQT_B.Transposit = -((mod[info.SQT.Positions_Pointer] >> 4) - 9) - 1;
            info.SQT.Positions_Pointer++;
            info.SQT_B.ix21 = 0;

            if(mod[info.SQT.Positions_Pointer] == 0)
                info.SQT.Positions_Pointer = info.SQT_LoopPointer;
            if(cast(byte)(mod[info.SQT.Positions_Pointer]) < 0)
                info.SQT_A.b4ix0 = true;
            else
                info.SQT_A.b4ix0 = false;
            info.SQT_A.Address_In_Pattern = cast(ushort)(ay_sys_getword(&mod[cast(ubyte)(mod[info.SQT.Positions_Pointer] * 2) + info.SQT_PatternsPointer]) + 1);
            info.SQT.Positions_Pointer++;
            info.SQT_A.Volume = mod[info.SQT.Positions_Pointer] & 15;
            if((mod[info.SQT.Positions_Pointer] >> 4) < 9)
                info.SQT_A.Transposit = mod[info.SQT.Positions_Pointer] >> 4;
            else
                info.SQT_A.Transposit = -((mod[info.SQT.Positions_Pointer] >> 4) - 9) - 1;
            info.SQT.Positions_Pointer++;
            info.SQT_A.ix21 = 0;

            info.SQT.Delay = mod[info.SQT.Positions_Pointer];
            info.SQT.DelayCounter = info.SQT.Delay;
            info.SQT.Positions_Pointer++;
        }
        SQT_PatternInterpreter(info, info.SQT_C);
        SQT_PatternInterpreter(info, info.SQT_B);
        SQT_PatternInterpreter(info, info.SQT_A);
    }

    TempMixer = 0;
    SQT_GetRegisters(info, info.SQT_C, TempMixer);
    SQT_GetRegisters(info, info.SQT_B, TempMixer);
    SQT_GetRegisters(info, info.SQT_A, TempMixer);
    TempMixer = (-(TempMixer + 1)) & 0x3f;

    if(!info.SQT_A.MixNoise)
        TempMixer |= 8;
    if(!info.SQT_A.MixTon)
        TempMixer |= 1;

    if(!info.SQT_B.MixNoise)
        TempMixer |= 16;
    if(!info.SQT_B.MixTon)
        TempMixer |= 2;

    if(!info.SQT_C.MixNoise)
        TempMixer |= 32;
    if(!info.SQT_C.MixTon)
        TempMixer |= 4;

    ay_writeay(AY_MIXER, TempMixer);
    ay_writeay(AY_CHNL_A_FINE, info.SQT_A.Ton & 0xff);
    ay_writeay(AY_CHNL_A_COARSE, (info.SQT_A.Ton >> 8) & 0xf);
    ay_writeay(AY_CHNL_B_FINE, info.SQT_B.Ton & 0xff);
    ay_writeay(AY_CHNL_B_COARSE, (info.SQT_B.Ton >> 8) & 0xf);
    ay_writeay(AY_CHNL_C_FINE, info.SQT_C.Ton & 0xff);
    ay_writeay(AY_CHNL_C_COARSE, (info.SQT_C.Ton >> 8) & 0xf);
    ay_writeay(AY_CHNL_A_VOL, info.SQT_A.Amplitude);
    ay_writeay(AY_CHNL_B_VOL, info.SQT_B.Amplitude);
    ay_writeay(AY_CHNL_C_VOL, info.SQT_C.Amplitude);
}

void SQT_GetChannelInfo(ref SQT_SongInfo info, ref ubyte b, ref uint tm, ref byte a1, ref ushort j1, ref ushort pptr, ref ushort cptr, ref bool f71, ref bool f61, ref bool f41, ref ushort j11, ubyte chnl_num)
{
    const(ubyte)* mod = info.mod;
    if(a1 != 0)
    {
        a1--;
        if(f71)
        {
            cptr = j11;
            f61 = false;
            if(mod[cptr] <= 0x7f)
            {
                cptr++;
                ubyte val = mod[cptr];
                if(val <= 0x7f)
                {
                    cptr++;
                    if(f61)
                        j1 = cast(ushort)(cptr + 1);
                    switch(mod[cptr - 1] - 1)
                    {
                        case 4:
                            if(f41)
                            {
                                b = mod[cptr] & 31;
                                if(b == 0)
                                    b = 32;
                            }
                            break;
                        case 5:
                            if(f41)
                            {
                                b = (b + mod[cptr]) & 31;
                                if(b == 0)
                                    b = 32;
                            }
                            break;
                        default:
                            break;
                    }
                }
                else if(val >= 0x80)
                {
                    if((val & 64) != 0)
                    {
                        cptr++;
                        if((mod[cptr] & 15) != 0)
                        {
                            cptr++;
                            if(f61)
                                j1 = cast(ushort)(cptr + 1);
                            switch((mod[cptr - 1] & 15) - 1)
                            {
                                case 4:
                                    if(f41)
                                    {
                                        b = mod[cptr] & 31;
                                        if(b == 0)
                                            b = 32;
                                    }
                                    break;
                                case 5:
                                    if(f41)
                                    {
                                        b = (b + mod[cptr]) & 31;
                                        if(b == 0)
                                            b = 32;
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
        }
    }
    else
    {
        cptr = j1;
        f61 = true;
        f71 = false;
        while(true)
        {
            ubyte val = mod[cptr];
            if(val <= 0x5f)
            {
                j11 = cptr;
                cptr++;
                val = mod[cptr];
                if(val <= 0x7f)
                {
                    cptr++;
                    if(f61)
                    {
                        j1 = cast(ushort)(cptr + 1);
                        f61 = false;
                    }
                    switch(mod[cptr - 1] - 1)
                    {
                        case 4:
                            if(f41)
                            {
                                b = mod[cptr] & 31;
                                if(b == 0)
                                    b = 32;
                            }
                            break;
                        case 5:
                            if(f41)
                            {
                                b = (b + mod[cptr]) & 31;
                                if(b == 0)
                                    b = 32;
                            }
                            break;
                        default:
                            break;
                    }
                }
                else if(val >= 0x80)
                {
                    if((mod[cptr] & 64) != 0)
                    {
                        cptr++;
                        if((mod[cptr] & 15) != 0)
                        {
                            cptr++;
                            if(f61)
                            {
                                j1 = cast(ushort)(cptr + 1);
                                f61 = false;
                            }
                            switch((mod[cptr - 1] & 15) - 1)
                            {
                                case 4:
                                    if(f41)
                                    {
                                        b = mod[cptr] & 31;
                                        if(b == 0)
                                            b = 32;
                                    }
                                    break;
                                case 5:
                                    if(f41)
                                    {
                                        b = (b + mod[cptr]) & 31;
                                        if(b == 0)
                                            b = 32;
                                    }
                                    break;
                                default:
                                    break;
                            }

                        }
                    }
                }
                cptr++;
                if(f61)
                    j1 = cptr;
                break;
            }
            else if(val >= 0x60 && val <= 0x6e)
            {
                cptr++;
                if(f61)
                    j1 = cast(ushort)(cptr + 1);
                switch(mod[cptr - 1] - 0x60 - 1)
                {
                    case 4:
                        if(f41)
                        {
                            b = mod[cptr] & 31;
                            if(b == 0)
                                b = 32;
                        }
                        break;
                    case 5:
                        if(f41)
                        {
                            b = (b + mod[cptr]) & 31;
                            if(b == 0)
                                b = 32;
                        }
                        break;
                    default:
                        break;
                }
                break;
            }
            else if(val >= 0x6f && val <= 0x7f)
            {
                if(mod[cptr] != 0x6f)
                {
                    cptr++;
                    if(f61)
                        j1 = cast(ushort)(cptr + 1);
                    switch(mod[cptr - 1] - 0x6f - 1)
                    {
                        case 4:
                            if(f41)
                            {
                                b = mod[cptr] & 31;
                                if(b == 0)
                                    b = 32;
                            }
                            break;
                        case 5:
                            if(f41)
                            {
                                b = (b + mod[cptr]) & 31;
                                if(b == 0)
                                    b = 32;
                            }
                            break;
                        default:
                            break;
                    }
                }
                else
                    j1 = cast(ushort)(cptr + 1);
                break;
            }
            else if(val >= 0x80 && val <= 0xbf)
            {
                j1 = cast(ushort)(cptr + 1);
                if(chnl_num == 1)
                {
                    if(val >= 0xa0)
                    {
                        a1 = mod[cptr] & 15;
                        if((mod[cptr] & 16) == 0)
                            break;
                        if(a1 != 0)
                            f71 = true;
                    }
                }
                else if(chnl_num == 2 || chnl_num == 3)
                {
                    if(val > 0x9f)
                    {
                        a1 = mod[cptr] & 15;
                        if((mod[cptr] & 16) == 0)
                            break;
                        if(a1 != 0)
                            f71 = true;
                    }
                }
                cptr = j11;
                f61 = false;
                if(mod[cptr] <= 0x7f)
                {
                    cptr++;
                    val = mod[cptr];
                    if(val <= 0x7f)
                    {
                        cptr++;
                        if(f61)
                            j1 = cast(ushort)(cptr + 1);
                        switch(mod[cptr - 1] - 1)
                        {
                            case 4:
                                if(f41)
                                {
                                    b = mod[cptr] & 31;
                                    if(b == 0)
                                        b = 32;
                                }
                                break;
                            case 5:
                                if(f41)
                                {
                                    b = (b + mod[cptr]) & 31;
                                    if(b == 0)
                                        b = 32;
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    else if(val >= 0x80)
                    {
                        if((mod[cptr] & 64) != 0)
                        {
                            cptr++;
                            if((mod[cptr] & 15) != 0)
                            {
                                cptr++;
                                if(f61)
                                    j1 = cast(ushort)(cptr + 1);
                                switch((mod[cptr - 1] & 15) - 1)
                                {
                                    case 4:
                                        if(f41)
                                        {
                                            b = mod[cptr] & 31;
                                            if(b == 0)
                                                b = 32;
                                        }
                                        break;
                                    case 5:
                                        if(f41)
                                        {
                                            b = (b + mod[cptr]) & 31;
                                            if(b == 0)
                                                b = 32;
                                        }
                                        break;
                                    default:
                                        break;
                                }

                            }
                        }
                    }

                }
                break;
            }
            else if(val >= 0xc0)
            {
                j1 = cast(ushort)(cptr + 1);
                j11 = cptr;
                break;
            }
        }
    }
}

uint SQT_GetTime(ref SQT_SongInfo info, out uint mLoop)
{
    mLoop = uint.max;
    //memcpy(info.mod, info.file_data, info.file_len);
    const(ubyte)* mod = info.mod;
    if (!SQT_PreInit(info))
    {
        //info.Length = 0;
        return 0;
    }
    auto header = cast(const(SQT_File)*)mod;
    ubyte b;
    uint tm = 0;
    int i;
    byte a1, a2, a3;
    ushort j1, j2, j3;
    ushort pptr, cptr;
    bool f71, f72, f73, f61, f62, f63, f41, f42, f43, flg;
    ushort j11, j22, j33;
    f71 = f72 = f73 = f61 = f62 = f63 = f41 = f42 = f43 = flg = false;
    j11 = j22 = j33 = 0;

    pptr = info.SQT_PositionsPointer;
    while(mod[pptr] != 0)
    {
        if(pptr == info.SQT_LoopPointer)
            mLoop = tm;
        f41 = (mod[pptr] & 128) ? true : false;
        j1 = ay_sys_getword(&mod[cast(ubyte)(mod[pptr] * 2) + info.SQT_PatternsPointer]);
        j1++;
        pptr += 2;
        f42 = (mod[pptr] & 128) ? true : false;
        j2 = ay_sys_getword(&mod[cast(ubyte)(mod[pptr] * 2) + info.SQT_PatternsPointer]);
        j2++;
        pptr += 2;
        f43 = (mod[pptr] & 128) ? true : false;
        j3 = ay_sys_getword(&mod[cast(ubyte)(mod[pptr] * 2) + info.SQT_PatternsPointer]);
        j3++;
        pptr += 2;
        b = mod[pptr];
        pptr++;
        a1 = a2 = a3 = 0;
        ubyte limit = mod[j1 - 1];
        for(i = 0; i < limit; i++)
        {
            SQT_GetChannelInfo(info, b, tm, a1, j1, pptr, cptr, f71, f61, f41, j11, 1);
            SQT_GetChannelInfo(info, b, tm, a2, j2, pptr, cptr, f72, f62, f42, j22, 2);
            SQT_GetChannelInfo(info, b, tm, a3, j3, pptr, cptr, f73, f63, f43, j33, 3);
            tm += b;
        }
    }
    //info.Length = tm;
    return tm;
}


bool SQT_Detect(ubyte[] moda)
{
  auto info = new SQT_SongInfo();
  scope(exit) delete info;
  info.data = moda;
  auto length = moda.length;
  ubyte* mod = moda.ptr;

    int j, j1, j2, j3;
    auto header = cast(SQT_File*)mod;
    ushort *pwrd;
    if(length < 17)
        return false;
    if((*info).SQT_SamplesPointer < 10)
        return false;
    if((*info).SQT_OrnamentsPointer <= (*info).SQT_SamplesPointer)
        return false;
    if((*info).SQT_PatternsPointer < (*info).SQT_OrnamentsPointer)
        return false;
    if((*info).SQT_PositionsPointer <= (*info).SQT_PatternsPointer)
        return false;
    if((*info).SQT_LoopPointer < (*info).SQT_PositionsPointer)
        return false;

    j = (*info).SQT_SamplesPointer - 10;
    if(cast(uint)((*info).SQT_LoopPointer - j) >= length)
        return false;

    j1 = (*info).SQT_PositionsPointer - j;
    if(mod[j1] == 0)
        return false;
    j2 = 0;
    while(mod[j1] != 0)
    {
        if(cast(uint)(j1 + 7) >= length)
            return false;
        if(j2 < (mod[j1] & 0x7f))
            j2 = mod[j1] & 0x7f;
        j1 += 2;
        if(j2 < (mod[j1] & 0x7f))
            j2 = mod[j1] & 0x7f;
        j1 += 2;
        if(j2 < (mod[j1] & 0x7f))
            j2 = mod[j1] & 0x7f;
        j1 += 3;
    }

    pwrd = cast(ushort*)&mod[(*info).SQT_SamplesPointer - j + 2];
    if(*pwrd - (*info).SQT_PatternsPointer - 2 != j2 * 2)
        return false;

    //int F_Length = j1 + 7;
    pwrd = cast(ushort*)&mod[12];
    j2 = *pwrd;
    for(j1 = 1; j1 <= ((*info).SQT_OrnamentsPointer- (*info).SQT_SamplesPointer) / 2; j1++)
    {
        pwrd++;
        j3 = *pwrd;
        if( j3 - j2 != 0x62 )return false;
        j2 = j3;
    }

    for(j1 = 1; j1 <= ((*info).SQT_PatternsPointer - (*info).SQT_OrnamentsPointer) / 2; j1++)
    {
        pwrd++;
        j3 = *pwrd;
        if( j3 - j2 != 0x22) return false;
        j2 = j3;
    }

    return true;
}


shared static this () {
  zxModuleRegister!".sqt"((ubyte[] data) {
    if (SQT_Detect(data)) return [cast(ZXModule)(new ZXModuleSQT(data))];
    return null;
  });
}


final class ZXModuleSQT : ZXModule {
private:
  SQT_SongInfo si;
  uint mIntrs, mLoop;

public:
  this (ubyte[] adata) {
    si.data = adata;
    SQT_Init(si);
    mIntrs = SQT_GetTime(si, mLoop);
  }

  override string typeStr () { return "SQT"; }
  override const(char)[] name () { return null; }
  override const(char)[] author () { return null; }
  override const(char)[] misc () { return null; }
  override uint intrCount () { return mIntrs; }
  override uint fadeCount () { return 0; }
  override uint loopPos () { return mLoop; }
  override bool doIntr () { SQT_Play(si); return true; }
  override void reset () { super.reset(); SQT_Init(si); }
}
