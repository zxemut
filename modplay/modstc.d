/* ProTracker 1 module decoder
 * Copyright (C) 2007 by Deryabin Andrew ( andrewderyabin@gmail.com )
 * Copyright (C) 2017 Ketmar // Invisible Vector
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
module modplay.modstc is aliced;
private:

import modplay.mod_low;

immutable ushort[96] ST_Table =
[ 0xef8, 0xe10, 0xd60, 0xc80, 0xbd8, 0xb28, 0xa88, 0x9f0, 0x960, 0x8e0, 0x858, 0x7e0, 0x77c, 0x708, 0x6b0, 0x640, 0x5ec, 0x594, 0x544, 0x4f8, 0x4b0, 0x470, 0x42c, 0x3f0, 0x3be, 0x384, 0x358, 0x320, 0x2f6, 0x2ca, 0x2a2, 0x27c, 0x258, 0x238, 0x216, 0x1f8, 0x1df, 0x1c2, 0x1ac, 0x190, 0x17b, 0x165, 0x151, 0x13e, 0x12c, 0x11c, 0x10b, 0xfc, 0xef, 0xe1, 0xd6, 0xc8, 0xbd, 0xb2, 0xa8, 0x9f, 0x96, 0x8e, 0x85, 0x7e, 0x77, 0x70, 0x6b, 0x64, 0x5e, 0x59, 0x54, 0x4f, 0x4b, 0x47, 0x42, 0x3f, 0x3b, 0x38, 0x35, 0x32, 0x2f, 0x2c, 0x2a, 0x27, 0x25, 0x23, 0x21, 0x1f, 0x1d, 0x1c, 0x1a, 0x19, 0x17, 0x16, 0x15, 0x13, 0x12, 0x11, 0x10, 0xf ];

align(1) struct STC_File
{
align(1):
    ubyte ST_Delay;
    ubyte ST_PositionsPointer0, ST_PositionsPointer1;
    ubyte ST_OrnamentsPointer0, ST_OrnamentsPointer1;
    ubyte ST_PatternsPointer0, ST_PatternsPointer1;
    byte[18] ST_Name;
    ubyte ST_Size0, ST_Size1;
};

align(1) struct STC_Channel_Parameters
{
align(1):
    ushort Address_In_Pattern, SamplePointer, OrnamentPointer, Ton;
    ubyte Amplitude, Note, Position_In_Sample, Number_Of_Notes_To_Skip;
    byte Sample_Tik_Counter, Note_Skip_Counter;
    bool Envelope_Enabled;
};

align(1) struct STC_Parameters
{
align(1):
    ubyte DelayCounter, Transposition, CurrentPosition;
};

align(1) struct STC_SongInfo
{
align(1):
    STC_Parameters STC;
    STC_Channel_Parameters STC_A, STC_B, STC_C;
    ubyte[] data;
    const(ubyte)* mod () { return data.ptr; }
};

const(STC_File)* Header (ref STC_SongInfo info) { return cast(const(STC_File)*)info.mod; }

/*
#define STC_A ((STC_SongInfo *)info.data).STC_A
#define STC_B ((STC_SongInfo *)info.data).STC_B
#define STC_C ((STC_SongInfo *)info.data).STC_C
#define STC ((STC_SongInfo *)info.data).STC
*/

ushort ST_PositionsPointer (ref STC_SongInfo info) { return cast(ushort)(info.Header.ST_PositionsPointer0 | (info.Header.ST_PositionsPointer1 << 8)); }
ushort ST_OrnamentsPointer (ref STC_SongInfo info) { return cast(ushort)(info.Header.ST_OrnamentsPointer0 | (info.Header.ST_OrnamentsPointer1 << 8)); }
ushort ST_PatternsPointer (ref STC_SongInfo info) { return cast(ushort)(info.Header.ST_PatternsPointer0 | (info.Header.ST_PatternsPointer1 << 8)); }
ushort ST_Size (ref STC_SongInfo info) { return cast(ushort)(info.Header.ST_Size0 | (info.Header.ST_Size1 << 8)); }
ubyte ST_Delay (ref STC_SongInfo info) { return info.Header.ST_Delay; }

void STC_Init(ref STC_SongInfo info)
{
    const(ubyte)* mod = info.mod;
    auto header = cast(const(STC_File)*)mod;
    //memset(&STC_A, 0, sizeof(STC_Channel_Parameters));
    //memset(&STC_B, 0, sizeof(STC_Channel_Parameters));
    //memset(&STC_C, 0, sizeof(STC_Channel_Parameters));
    info.STC_A = info.STC_A.init;
    info.STC_B = info.STC_B.init;
    info.STC_C = info.STC_C.init;

    info.STC.CurrentPosition = 0;
    info.STC.Transposition = mod[info.ST_PositionsPointer + 2];
    info.STC.DelayCounter = 1;

    uint i = 0;
    while(mod[info.ST_PatternsPointer + 7 * i] != mod[info.ST_PositionsPointer + 1])
        i++;
    info.STC_A.Address_In_Pattern = ay_sys_getword(&mod[info.ST_PatternsPointer + 7 * i + 1]);
    info.STC_B.Address_In_Pattern = ay_sys_getword(&mod[info.ST_PatternsPointer + 7 * i + 3]);
    info.STC_C.Address_In_Pattern = ay_sys_getword(&mod[info.ST_PatternsPointer + 7 * i + 5]);

    info.STC_A.Note_Skip_Counter = 0;
    info.STC_A.Envelope_Enabled = false;
    info.STC_A.Number_Of_Notes_To_Skip = 0;
    info.STC_A.Sample_Tik_Counter = -1;
    info.STC_A.Position_In_Sample = 0;
    info.STC_A.OrnamentPointer = cast(ushort)(info.ST_OrnamentsPointer + 1);
    info.STC_A.Ton = 0;

    info.STC_B.Note_Skip_Counter = 0;
    info.STC_B.Envelope_Enabled = false;
    info.STC_B.Number_Of_Notes_To_Skip = 0;
    info.STC_B.Sample_Tik_Counter = -1;
    info.STC_B.Position_In_Sample = 0;
    info.STC_B.OrnamentPointer = cast(ushort)(info.ST_OrnamentsPointer + 1);
    info.STC_B.Ton = 0;

    info.STC_C.Note_Skip_Counter = 0;
    info.STC_C.Envelope_Enabled = false;
    info.STC_C.Number_Of_Notes_To_Skip = 0;
    info.STC_C.Sample_Tik_Counter = -1;
    info.STC_C.Position_In_Sample = 0;
    info.STC_C.OrnamentPointer = cast(ushort)(info.ST_OrnamentsPointer + 1);
    info.STC_C.Ton = 0;
}

uint STC_GetTime (ref STC_SongInfo info, out uint mLoop) {
    mLoop = uint.max;
    const(ubyte)* mod = info.mod;
    auto header = cast(const(STC_File)*)mod;
    uint tm = 0;
    int j, j1, j2, i;
    ubyte stDelay = mod[0];
    ushort stPosPt = ay_sys_getword(&mod[1]);
    ushort stPatPt = ay_sys_getword(&mod[5]);
    ubyte a;

    j = -1;
    do
    {
        j++;
        j2 = stPosPt + j * 2 + 1;
        j2 = mod[j2];
        i = -1;
        do
        {
            i++;
            j1 = stPatPt + 7 * i;
        }
        while(mod[j1] != j2);
        j1 = ay_sys_getword(&mod[j1 + 1]);
        a = 1;
        while(mod[j1] != 255)
        {
            ubyte val = mod[j1];
            if((val <= 0x5f) || (val == 0x80) || (val == 0x81))
            {
                tm += a;
            }
            else if(val >= 0xa1 && val <= 0xe0)
            {
                a = cast(ubyte)(val - 0xa0);
            }
            else if(val >= 0x83 && val <= 0x8e)
            {
                j1++;
            }
            j1++;
        }
    } while(j != mod[stPosPt]);
    tm *= stDelay;
    //info.Length = tm;
    return tm;

    /*
    ubyte *st_name = (ubyte *)header.ST_Name;

    if(!memcmp(st_name, "SONG BY ST COMPILE", 18) || !memcmp(st_name, "SONG BY MB COMPILE", 18) || !memcmp(st_name, "SONG BY ST-COMPILE", 18) || !memcmp(st_name, "SOUND TRACKER v1.1", 18) || !memcmp(st_name, "S.T.FULL EDITION ", 17) || !memcmp(st_name, "SOUND TRACKER v1.3", 18))
        return;
    uint len = 18;
    if(st_name[18] >= 32 && st_name[18] <= 127)
    {
        len++;
        if(st_name[19] >= 32 && st_name[19] <= 127)
            len++;
    }
    info.Name = ay_sys_getstr(st_name, len);
    */
}

const(char)[] STC_GetName (ref STC_SongInfo info) {
  import core.stdc.string : memcmp;

  //const(char)* mod = cast(const(char)*)si.mod;
  const(ubyte)* mod = info.mod;
  auto header = cast(const(STC_File)*)mod;
  const(ubyte)* st_name = cast(const(ubyte)*)(header.ST_Name.ptr);

  if (!memcmp(st_name, "SONG BY ST COMPILE".ptr, 18) ||
      !memcmp(st_name, "SONG BY MB COMPILE".ptr, 18) ||
      !memcmp(st_name, "SONG BY ST-COMPILE".ptr, 18) ||
      !memcmp(st_name, "SOUND TRACKER v1.1".ptr, 18) ||
      !memcmp(st_name, "S.T.FULL EDITION ".ptr, 17) ||
      !memcmp(st_name, "SOUND TRACKER v1.3".ptr, 18))
    return null;

  uint len = 18;
  if (st_name[18] >= 32 && st_name[18] <= 127) {
    ++len;
    if (st_name[19] >= 32 && st_name[19] <= 127) ++len;
  }

  return ay_sys_getstr(st_name, len);
}

void STC_PatternInterpreter (ref STC_SongInfo info, ref STC_Channel_Parameters chan)
{
    ushort k;
    const(ubyte)* mod = info.mod;
    auto header = cast(const(STC_File)*)mod;
    while(true)
    {
        ubyte val = mod[chan.Address_In_Pattern];
        if(val <= 0x5f)
        {
            chan.Note = val;
            chan.Sample_Tik_Counter = 32;
            chan.Position_In_Sample = 0;
            chan.Address_In_Pattern++;
            break;
        }
        else if(val >= 0x60 && val <= 0x6f)
        {
            k = 0;
            while(mod[0x1b + 0x63 * k] != (val - 0x60))
                k++;
            chan.SamplePointer = cast(ushort)(0x1c + 0x63 * k);
        }
        else if(val >= 0x70 && val <= 0x7f)
        {
            k = 0;
            while(mod[info.ST_OrnamentsPointer + 0x21 * k] != (val - 0x70))
                k++;
            chan.OrnamentPointer = cast(ushort)(info.ST_OrnamentsPointer + 0x21 * k + 1);
            chan.Envelope_Enabled = false;
        }
        else if(val == 0x80)
        {
            chan.Sample_Tik_Counter = -1;
            chan.Address_In_Pattern++;
            break;
        }
        else if(val == 0x81)
        {
            chan.Address_In_Pattern++;
            break;
        }
        else if(val == 0x82)
        {
            k = 0;
            while(mod[info.ST_OrnamentsPointer + 0x21 * k] != 0)
                k++;
            chan.OrnamentPointer = cast(ushort)(info.ST_OrnamentsPointer + 0x21 * k + 1);
            chan.Envelope_Enabled = false;
        }
        else if(val >= 0x83 && val <= 0x8e)
        {
            ay_writeay(AY_ENV_SHAPE, cast(ubyte)(val - 0x80));
            chan.Address_In_Pattern++;
            ay_writeay(AY_ENV_FINE, mod[chan.Address_In_Pattern]);
            chan.Envelope_Enabled = true;
            k = 0;
            while(mod[info.ST_OrnamentsPointer + 0x21 * k] != 0)
                k++;
            chan.OrnamentPointer = cast(ushort)(info.ST_OrnamentsPointer + 0x21 * k + 1);
        }
        else
            chan.Number_Of_Notes_To_Skip = cast(ubyte)(val - 0xa1);
        chan.Address_In_Pattern++;
    }
    chan.Note_Skip_Counter = chan.Number_Of_Notes_To_Skip;
}

void STC_GetRegisters(ref STC_SongInfo info, ref STC_Channel_Parameters chan, ref ubyte TempMixer)
{
    ushort i;
    ubyte j;
    const(ubyte)* mod = info.mod;
    //STC_File *header = (STC_File *)mod;
    if(chan.Sample_Tik_Counter >= 0)
    {
        chan.Sample_Tik_Counter--;
        chan.Position_In_Sample = (chan.Position_In_Sample + 1) & 0x1f;
        if(chan.Sample_Tik_Counter == 0)
        {
            if(mod[chan.SamplePointer + 0x60] != 0)
            {
                chan.Position_In_Sample = mod[chan.SamplePointer + 0x60] & 0x1f;
                chan.Sample_Tik_Counter = cast(byte)(mod[chan.SamplePointer + 0x61] + 1);
            }
            else
                chan.Sample_Tik_Counter = -1;
        }
    }
    if(chan.Sample_Tik_Counter >= 0)
    {
        i = cast(ushort)(((chan.Position_In_Sample - 1) & 0x1f) * 3 + chan.SamplePointer);
        if((mod[i + 1] & 0x80) != 0)
            TempMixer = TempMixer | 64;
        else
            ay_writeay(AY_NOISE_PERIOD, mod[i + 1] & 0x1f);
        if((mod[i + 1] & 0x40) != 0)
            TempMixer = TempMixer | 8;
        chan.Amplitude = mod[i] & 15;
        j = cast(ubyte)(chan.Note + mod[chan.OrnamentPointer + ((chan.Position_In_Sample - 1) & 0x1f)] + info.STC.Transposition);
        if(j > 95)
            j = 95;
        if((mod[i + 1] & 0x20) != 0)
            chan.Ton = (ST_Table[j] + mod[i + 2] + ((cast(ushort)(mod[i] & 0xf0)) << 4)) & 0xFFF;
        else
            chan.Ton = (ST_Table[j] - mod[i + 2] - ((cast(ushort)(mod[i] & 0xf0)) << 4)) & 0xFFF;
        if(chan.Envelope_Enabled)
            chan.Amplitude = chan.Amplitude | 16;
    }
    else
        chan.Amplitude = 0;

    TempMixer = TempMixer >> 1;
}

void STC_Play(ref STC_SongInfo info)
{
    ubyte TempMixer;
    ushort i;
    const(ubyte)* mod = info.mod;
    auto header = cast(const(STC_File)*)mod;

    info.STC.DelayCounter--;
    if(info.STC.DelayCounter == 0)
    {
        info.STC.DelayCounter = info.ST_Delay;
        info.STC_A.Note_Skip_Counter--;
        if(info.STC_A.Note_Skip_Counter < 0)
        {
            if(mod[info.STC_A.Address_In_Pattern] == 255)
            {
                if(info.STC.CurrentPosition == mod[info.ST_PositionsPointer])
                    info.STC.CurrentPosition = 0;
                else
                    info.STC.CurrentPosition++;
                info.STC.Transposition = mod[info.ST_PositionsPointer + 2 + info.STC.CurrentPosition * 2];
                i = 0;
                while(mod[info.ST_PatternsPointer + 7 * i] != mod[info.ST_PositionsPointer + 1 + info.STC.CurrentPosition * 2])
                    i++;
                info.STC_A.Address_In_Pattern = ay_sys_getword(&mod[info.ST_PatternsPointer + 7 * i + 1]);
                info.STC_B.Address_In_Pattern = ay_sys_getword(&mod[info.ST_PatternsPointer + 7 * i + 3]);
                info.STC_C.Address_In_Pattern = ay_sys_getword(&mod[info.ST_PatternsPointer + 7 * i + 5]);
            }
            STC_PatternInterpreter(info, info.STC_A);
        }

        info.STC_B.Note_Skip_Counter--;
        if(info.STC_B.Note_Skip_Counter < 0)
            STC_PatternInterpreter(info, info.STC_B);
        info.STC_C.Note_Skip_Counter--;
        if(info.STC_C.Note_Skip_Counter < 0)
            STC_PatternInterpreter(info, info.STC_C);
    }

    TempMixer = 0;
    STC_GetRegisters(info, info.STC_A, TempMixer);
    STC_GetRegisters(info, info.STC_B, TempMixer);
    STC_GetRegisters(info, info.STC_C, TempMixer);

    ay_writeay(AY_MIXER, TempMixer);

    ay_writeay(AY_CHNL_A_FINE, info.STC_A.Ton & 0xff);
    ay_writeay(AY_CHNL_A_COARSE, (info.STC_A.Ton >> 8) & 0xf);
    ay_writeay(AY_CHNL_B_FINE, info.STC_B.Ton & 0xff);
    ay_writeay(AY_CHNL_B_COARSE, (info.STC_B.Ton >> 8) & 0xf);
    ay_writeay(AY_CHNL_C_FINE, info.STC_C.Ton & 0xff);
    ay_writeay(AY_CHNL_C_COARSE, (info.STC_C.Ton >> 8) & 0xf);
    ay_writeay(AY_CHNL_A_VOL, info.STC_A.Amplitude);
    ay_writeay(AY_CHNL_B_VOL, info.STC_B.Amplitude);
    ay_writeay(AY_CHNL_C_VOL, info.STC_C.Amplitude);
}


bool STC_Detect(ubyte[] moda)
{
  auto info = new STC_SongInfo();
  scope(exit) delete info;
  info.data = moda;
  auto length = moda.length;
  ubyte* mod = moda.ptr;

    auto header = cast(STC_File*)mod;
    uint j, j1, j2;
    if(length < 6)
        return false;
    if((*info).ST_PositionsPointer> length)
        return false;
    if(cast(int)((*info).ST_PatternsPointer- (*info).ST_OrnamentsPointer) <= 0)
    return false;
    if((((*info).ST_PatternsPointer - (*info).ST_OrnamentsPointer) % 0x21) != 0)
    return false;
    if(cast(int)((*info).ST_PositionsPointer - (*info).ST_OrnamentsPointer) >= 0)
    return false;
    if((mod[(*info).ST_PositionsPointer] * 2 + 3 + (*info).ST_PositionsPointer - (*info).ST_OrnamentsPointer) != 0)
    return false;

    j = (*info).ST_OrnamentsPointer + 0x21;
    if(j> 65535)
    return false;
    if(j> length)
    return false;
    do
    {
        j--;
        if(mod[j] != 0)
        return false;
    }
    while(j != (*info).ST_OrnamentsPointer);

    j = (*info).ST_PatternsPointer;
    if(j> length)
    return false;
    j1 = 0;
    j2 = 0;
    while((j + 6 <= length) && (j + 6 < 65536) && (mod[j] != 255))
    {
        j++;
        j2 = ay_sys_getword(&mod[j]);
        if(j1 < j2)
        j1 = j2;
        j += 2;
        j2 = ay_sys_getword(&mod[j]);
        if(j1 < j2)
        j1 = j2;
        j += 2;
        j2 = ay_sys_getword(&mod[j]);
        if(j1 < j2)
        j1 = j2;
        j += 2;
    }
    if(mod[j] != 255)
    return false;
    if(j1> length)
    return false;
    if(mod[j1 - 1] != 255)
    return false;
    do
    {
        if((mod[j1] >= 0x83) && (mod[j1] <= 0x8e))
        j1++;
        j1++;
    }
    while((j1 <= 65535) && (j1 < length) && (mod[j1] != 255));
    if(j1> 65535)
    return false;
    if(j1> length)
    return false;

    return true;
}


shared static this () {
  zxModuleRegister!".stc"((ubyte[] data) {
    if (STC_Detect(data)) return [cast(ZXModule)(new ZXModuleSTC(data))];
    return null;
  });
}


final class ZXModuleSTC : ZXModule {
private:
  STC_SongInfo si;
  uint mIntrs, mLoop;

public:
  this (ubyte[] adata) {
    si.data = adata;
    STC_Init(si);
    mIntrs = STC_GetTime(si, mLoop);
  }

  override string typeStr () { return "STC"; }
  override const(char)[] name () { return STC_GetName(si); }
  override const(char)[] author () { return null; }
  override const(char)[] misc () { return null; }
  override uint intrCount () { return mIntrs; }
  override uint fadeCount () { return 0; }
  override uint loopPos () { return mLoop; }
  override bool doIntr () { STC_Play(si); return true; }
  override void reset () { super.reset(); STC_Init(si); }
}
