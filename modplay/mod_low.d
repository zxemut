/* ProTracker 3 module decoder
 * Copyright (C) 2007 by Deryabin Andrew ( andrewderyabin@gmail.com )
 * Copyright (C) 2017 Ketmar // Invisible Vector
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
module modplay.mod_low is aliced;
private:

import iv.strex;

import zxinfo;
import emuconfig;
import sound;


package(modplay) enum {
  AY_CHNL_A_FINE = 0,
  AY_CHNL_A_COARSE,
  AY_CHNL_B_FINE,
  AY_CHNL_B_COARSE,
  AY_CHNL_C_FINE,
  AY_CHNL_C_COARSE,
  AY_NOISE_PERIOD,
  AY_MIXER,
  AY_CHNL_A_VOL,
  AY_CHNL_B_VOL,
  AY_CHNL_C_VOL,
  AY_ENV_FINE,
  AY_ENV_COARSE,
  AY_ENV_SHAPE,
  AY_GPIO_A,
  AY_GPIO_B,
}

__gshared ubyte[15] ay_regs;

package(modplay) void ay_reset () nothrow @trusted @nogc { ay_regs[] = 0; }

package(modplay) void ay_writeay (ubyte reg, ubyte val, uint tstates=1) nothrow @trusted @nogc {
  ay_regs[reg&0x0f] = val;
  soundWriteAY(reg, val, tstates);
}

package(modplay) ubyte ay_readay (ubyte reg) nothrow @trusted @nogc {
  return ay_regs[reg&0x0f];
}


// val is ULA out, unmodified
package(modplay) void ay_writebeeper (uint tstates, ubyte value) nothrow @trusted @nogc {
  soundWriteBeeper(tstates, (!!(value&0x10)<<1)+(!(value&0x8)));
}


public abstract class ZXModule {
public:
  //this (ubyte[] adata);
  string typeStr ();
  const(char)[] name ();
  const(char)[] author ();
  const(char)[] misc ();
  uint intrCount ();
  uint fadeCount ();
  uint loopPos (); // uint.max: no loop
  bool doIntr (); // return `false` to stop
  void reset () { ay_reset(); } // reset module (i.e. start from the very beginning)

  string modelName () { return "pentagon"; } // zx spectrum model name

  final @property bool hasLoop () { return (loopPos != uint.max); }
}


public alias ZXModuleDetector = ZXModule[] delegate (ubyte[] data);


__gshared ZXModuleDetector[] detectors;
__gshared string[] extensions;


public void zxModuleRegister(string ext) (ZXModuleDetector det) {
  static assert(ext.length > 1 && ext[0] == '.');
  if (det !is null) {
    detectors ~= det;
    extensions ~= ext;
  }
}


public bool zxModuleGoodExtension (const(char)[] fname) {
  if (fname.length < 2) return false;
  foreach (string ext; extensions) if (fname.endsWithCI(ext)) return true;
  return false;
}


public ZXModule[] zxModuleDetect(bool dothrow=false) (ubyte[] data) {
  foreach (auto det; detectors) {
    try {
      auto mod = det(data);
      if (mod.length) return mod;
    } catch (Exception e) {
      static if (dothrow) throw e;
    }
  }
  return null;
}


package(modplay) ushort ay_sys_getword (const(void) *p) {
  auto pp = cast(const(ubyte)*)p;
  return cast(ushort)(pp[0]|(pp[1]<<8));
}


package(modplay) void ay_sys_writeword (void *p, ushort val) {
  auto pp = cast(ubyte*)p;
  pp[0] = val&0xff;
  pp[1] = (val>>8)&0xff;
}


package(modplay) const(char)[] ay_sys_getstr (const(void)* p, uint len) {
  if (len == 0 || p is null) return null;
  auto pp = cast(const(char)*)p;
  uint end = 0;
  while (end < len && pp[end]) ++end;
  auto res = pp[0..end];
  while (res.length && res[0] <= ' ') res = res[1..$];
  while (res.length && res[$-1] <= ' ') res = res[0..$-1];
  return (res.length ? res : null);
}
