/* ProTracker 1 module decoder
 * Copyright (C) 2007 by Deryabin Andrew ( andrewderyabin@gmail.com )
 * Copyright (C) 2017 Ketmar // Invisible Vector
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
module modplay.modasc is aliced;
private:

import modplay.mod_low;

immutable ushort[$] ASM_Table =
[ 0xedc, 0xe07, 0xd3e, 0xc80, 0xbcc, 0xb22, 0xa82, 0x9ec, 0x95c, 0x8d6, 0x858, 0x7e0, 0x76e, 0x704, 0x69f, 0x640, 0x5e6, 0x591, 0x541, 0x4f6, 0x4ae, 0x46b, 0x42c, 0x3f0, 0x3b7, 0x382, 0x34f, 0x320, 0x2f3, 0x2c8, 0x2a1, 0x27b, 0x257, 0x236, 0x216, 0x1f8, 0x1dc, 0x1c1, 0x1a8, 0x190, 0x179, 0x164, 0x150, 0x13d, 0x12c, 0x11b, 0x10b, 0xfc, 0xee, 0xe0, 0xd4, 0xc8, 0xbd, 0xb2, 0xa8, 0x9f, 0x96, 0x8d, 0x85, 0x7e, 0x77, 0x70, 0x6a, 0x64, 0x5e, 0x59, 0x54, 0x50, 0x4b, 0x47, 0x43, 0x3f, 0x3c, 0x38, 0x35, 0x32, 0x2f, 0x2d, 0x2a, 0x28, 0x26, 0x24, 0x22, 0x20, 0x1e, 0x1c ];

align(1) struct ASC0_File
{
align(1):
    ubyte ASC0_Delay;
    ubyte ASC0_PatternsPointers0, ASC0_PatternsPointers1;
    ubyte ASC0_SamplesPointers0, ASC0_SamplesPointers1;
    ubyte ASC0_OrnamentsPointers0, ASC0_OrnamentsPointers1;
    ubyte ASC0_Number_Of_Positions;
    ubyte[65536 - 8] ASC0_Positions;
    ubyte[] data;
    const(ubyte)* mod () { return data.ptr; }
}

align(1) struct ASC1_File
{
align(1):
    ubyte ASC1_Delay, ASC1_LoopingPosition;
    ubyte ASC1_PatternsPointers0, ASC1_PatternsPointers1;
    ubyte ASC1_SamplesPointers0, ASC1_SamplesPointers1;
    ubyte ASC1_OrnamentsPointers0, ASC1_OrnamentsPointers1;
    ubyte ASC1_Number_Of_Positions;
    ubyte[65536 - 8] ASC1_Positions;
}

const(ASC0_File)* Header0 (ref ASC_SongInfo info) { return cast(const(ASC0_File)*)info.mod; }
const(ASC1_File)* Header1 (ref ASC_SongInfo info) { return cast(const(ASC1_File)*)info.mod; }

ushort ASC1_PatternsPointers (ref ASC_SongInfo info) { return cast(uint)(info.Header1.ASC1_PatternsPointers0 | (info.Header1.ASC1_PatternsPointers1 << 8)); }
ushort ASC1_SamplesPointers (ref ASC_SongInfo info) { return cast(uint)(info.Header1.ASC1_SamplesPointers0 | (info.Header1.ASC1_SamplesPointers1 << 8)); }
ushort ASC1_OrnamentsPointers (ref ASC_SongInfo info) { return cast(uint)(info.Header1.ASC1_OrnamentsPointers0 | (info.Header1.ASC1_OrnamentsPointers1 << 8)); }

ushort ASC0_PatternsPointers (ref ASC_SongInfo info) { return cast(uint)(info.Header0.ASC0_PatternsPointers0 | (info.Header0.ASC0_PatternsPointers1 << 8)); }
ushort ASC0_SamplesPointers (ref ASC_SongInfo info) { return cast(uint)(info.Header0.ASC0_SamplesPointers0 | (info.Header0.ASC0_SamplesPointers1 << 8)); }
ushort ASC0_OrnamentsPointers (ref ASC_SongInfo info) { return cast(uint)(info.Header0.ASC0_OrnamentsPointers0 | (info.Header0.ASC0_OrnamentsPointers1 << 8)); }

align(1) struct ASC_Channel_Parameters
{
align(1):
    ushort Initial_Point_In_Sample, Point_In_Sample, Loop_Point_In_Sample, Initial_Point_In_Ornament, Point_In_Ornament, Loop_Point_In_Ornament, Address_In_Pattern, Ton, Ton_Deviation;
    ubyte Note, Addition_To_Note, Number_Of_Notes_To_Skip, Initial_Noise, Current_Noise, Volume, Ton_Sliding_Counter, Amplitude, Amplitude_Delay, Amplitude_Delay_Counter;
    short Current_Ton_Sliding, Substruction_for_Ton_Sliding;
    byte Note_Skip_Counter, Addition_To_Amplitude;
    bool Envelope_Enabled, Sound_Enabled, Sample_Finished, Break_Sample_Loop, Break_Ornament_Loop;
}

align(1) struct ASC_Parameters
{
align(1):
    ubyte Delay, DelayCounter, CurrentPosition;
}

align(1) struct ASC_SongInfo
{
align(1):
    ASC_Parameters ASC;
    ASC_Channel_Parameters ASC_A, ASC_B, ASC_C;
    ubyte[] data;
    const(ubyte)* mod () { return data.ptr; }
}

/*
#define ASC_A ((ASC_SongInfo *)info.data).ASC_A
#define ASC_B ((ASC_SongInfo *)info.data).ASC_B
#define ASC_C ((ASC_SongInfo *)info.data).ASC_C
#define ASC ((ASC_SongInfo *)info.data).ASC
*/

//#define GET_ADDRINPATT(x,y) ((mod[ascPatPt + 6 * mod[x + 9] + y * 2] | (mod[ascPatPt + 6 * mod[x + 9] + 1 + y * 2] << 8))  + ascPatPt)

void ASC_Init(ref ASC_SongInfo info)
{
    const(ubyte)* mod = info.mod;
    auto header = cast(const(ASC1_File)*)mod;
    ushort ascPatPt = info.ASC1_PatternsPointers;
    //memset(&ASC_A, 0, sizeof(ASC_Channel_Parameters));
    //memset(&ASC_B, 0, sizeof(ASC_Channel_Parameters));
    //memset(&ASC_C, 0, sizeof(ASC_Channel_Parameters));
    info.ASC_A = info.ASC_A.init;
    info.ASC_B = info.ASC_B.init;
    info.ASC_C = info.ASC_C.init;
    info.ASC.CurrentPosition = 0;
    info.ASC.DelayCounter = 1;
    info.ASC.Delay = header.ASC1_Delay;
    info.ASC_A.Address_In_Pattern = cast(ushort)(ay_sys_getword(&mod[ascPatPt + 6 * mod[9]]) + ascPatPt);
    info.ASC_B.Address_In_Pattern = cast(ushort)(ay_sys_getword(&mod[ascPatPt + 6 * mod[9] + 2]) + ascPatPt);
    info.ASC_C.Address_In_Pattern = cast(ushort)(ay_sys_getword(&mod[ascPatPt + 6 * mod[9] + 4]) + ascPatPt);
}

void ASC_PatternInterpreter(ref ASC_SongInfo info, ref ASC_Channel_Parameters chan)
{
    const(ubyte)* mod = info.mod;
    auto header = cast(const(ASC1_File)*)mod;
    short delta_ton;
    bool Initialization_Of_Ornament_Disabled, Initialization_Of_Sample_Disabled;

    Initialization_Of_Ornament_Disabled = Initialization_Of_Sample_Disabled = false;

    chan.Ton_Sliding_Counter = 0;
    chan.Amplitude_Delay_Counter = 0;
    while(true)
    {
        ubyte val = mod[chan.Address_In_Pattern];
        if(val <= 0x55)
        {
            chan.Note = val;
            chan.Address_In_Pattern++;
            chan.Current_Noise = chan.Initial_Noise;
            if(cast(byte)(chan.Ton_Sliding_Counter) <= 0)
                chan.Current_Ton_Sliding = 0;
            if(!Initialization_Of_Sample_Disabled)
            {
                chan.Addition_To_Amplitude = 0;
                chan.Ton_Deviation = 0;
                chan.Point_In_Sample = chan.Initial_Point_In_Sample;
                chan.Sound_Enabled = true;
                chan.Sample_Finished = false;
                chan.Break_Sample_Loop = false;
            }
            if(!Initialization_Of_Ornament_Disabled)
            {
                chan.Point_In_Ornament = chan.Initial_Point_In_Ornament;
                chan.Addition_To_Note = 0;
            }
            if(chan.Envelope_Enabled)
            {
                ay_writeay(AY_ENV_FINE, mod[chan.Address_In_Pattern]);
                chan.Address_In_Pattern++;
            }
            break;
        }
        else if((val >= 0x56) && (val <= 0x5d))
        {
            chan.Address_In_Pattern++;
            break;
        }
        else if(val == 0x5e)
        {
            chan.Break_Sample_Loop = true;
            chan.Address_In_Pattern++;
            break;
        }
        else if(val == 0x5f)
        {
            chan.Sound_Enabled = false;
            chan.Address_In_Pattern++;
            break;
        }
        else if((val >= 0x60) && (val <= 0x9f))
        {
            chan.Number_Of_Notes_To_Skip = cast(ubyte)(val - 0x60);
        }
        else if((val >= 0xa0) && (val <= 0xbf))
        {
            //chan.Initial_Point_In_Sample = (*(ushort *)&module[(module[chan.Address_In_Pattern] - 0xa0) * 2 + ASC1_SamplesPointers]) + ASC1_SamplesPointers;
            chan.Initial_Point_In_Sample = cast(ushort)(ay_sys_getword(&mod[(mod[chan.Address_In_Pattern] - 0xa0) * 2 + info.ASC1_SamplesPointers]) + info.ASC1_SamplesPointers);
        }
        else if((val >= 0xc0) && (val <= 0xdf))
        {
            //chan.Initial_Point_In_Ornament = (*(ushort *)&module[(module[chan.Address_In_Pattern] - 0xc0) * 2 + ASC1_OrnamentsPointers]) + ASC1_OrnamentsPointers;
            chan.Initial_Point_In_Ornament = cast(ushort)(ay_sys_getword(&mod[(mod[chan.Address_In_Pattern] - 0xc0) * 2 + info.ASC1_OrnamentsPointers]) + info.ASC1_OrnamentsPointers);
        }
        else if(val == 0xe0)
        {
            chan.Volume = 15;
            chan.Envelope_Enabled = true;
        }
        else if((val >= 0xe1) && (val <= 0xef))
        {
            chan.Volume = cast(ubyte)(val - 0xe0);
            chan.Envelope_Enabled = false;
        }
        else if(val == 0xf0)
        {
            chan.Address_In_Pattern++;
            chan.Initial_Noise = mod[chan.Address_In_Pattern];
        }
        else if(val == 0xf1)
        {
            Initialization_Of_Sample_Disabled = true;
        }
        else if(val == 0xf2)
        {
            Initialization_Of_Ornament_Disabled = true;
        }
        else if(val == 0xf3)
        {
            Initialization_Of_Sample_Disabled = true;
            Initialization_Of_Ornament_Disabled = true;
        }
        else if(val == 0xf4)
        {
            chan.Address_In_Pattern++;
            info.ASC.Delay = mod[chan.Address_In_Pattern];
        }
        else if(val == 0xf5)
        {
            chan.Address_In_Pattern++;
            chan.Substruction_for_Ton_Sliding = -(cast(byte)(mod[chan.Address_In_Pattern])) * 16;
            chan.Ton_Sliding_Counter = 255;
        }
        else if(val == 0xf6)
        {
            chan.Address_In_Pattern++;
            chan.Substruction_for_Ton_Sliding = cast(byte)(mod[chan.Address_In_Pattern]) * 16;
            chan.Ton_Sliding_Counter = 255;
        }
        else if(val == 0xf7)
        {
            chan.Address_In_Pattern++;
            Initialization_Of_Sample_Disabled = true;
            if(mod[chan.Address_In_Pattern + 1] < 0x56)
                delta_ton = cast(short)(ASM_Table[chan.Note] + (chan.Current_Ton_Sliding / 16) - ASM_Table[mod[chan.Address_In_Pattern + 1]]);
            else
                delta_ton = chan.Current_Ton_Sliding / 16;
            delta_ton = cast(short)(delta_ton << 4);
            chan.Substruction_for_Ton_Sliding = cast(short)(-(delta_ton) / cast(byte)(mod[chan.Address_In_Pattern]));
            chan.Current_Ton_Sliding = cast(short)(delta_ton - delta_ton % cast(byte)(mod[chan.Address_In_Pattern]));
            chan.Ton_Sliding_Counter = cast(byte)(mod[chan.Address_In_Pattern]);
        }
        else if(val == 0xf8)
        {
            ay_writeay(AY_ENV_SHAPE, 8);
        }
        else if(val == 0xf9)
        {
            chan.Address_In_Pattern++;
            if(mod[chan.Address_In_Pattern + 1] < 0x56)
            {
                delta_ton = cast(short)(ASM_Table[chan.Note] - ASM_Table[mod[chan.Address_In_Pattern + 1]]);
            }
            else
                delta_ton = chan.Current_Ton_Sliding / 16;
            delta_ton = cast(short)(delta_ton << 4);
            chan.Substruction_for_Ton_Sliding = cast(short)(-(delta_ton) / cast(byte)(mod[chan.Address_In_Pattern]));
            chan.Current_Ton_Sliding = cast(short)(delta_ton - delta_ton % cast(byte)(mod[chan.Address_In_Pattern]));
            chan.Ton_Sliding_Counter = cast(byte)(mod[chan.Address_In_Pattern]);

        }
        else if(val == 0xfa)
        {
            ay_writeay(AY_ENV_SHAPE, 10);
        }
        else if(val == 0xfb)
        {
            chan.Address_In_Pattern++;
            if((mod[chan.Address_In_Pattern] & 32) == 0)
            {
                chan.Amplitude_Delay = cast(ubyte)(mod[chan.Address_In_Pattern] << 3);
                chan.Amplitude_Delay_Counter = chan.Amplitude_Delay;
            }
            else
            {
                chan.Amplitude_Delay = cast(ubyte)(((mod[chan.Address_In_Pattern] << 3) ^ 0xf8) + 9);
                chan.Amplitude_Delay_Counter = chan.Amplitude_Delay;
            }
        }
        else if(val == 0xfc)
        {
            ay_writeay(AY_ENV_SHAPE, 12);
        }
        else if(val == 0xfe)
        {
            ay_writeay(AY_ENV_SHAPE, 14);
        }
        chan.Address_In_Pattern++;
    }
    chan.Note_Skip_Counter = chan.Number_Of_Notes_To_Skip;
}

void ASC_GetRegisters(ref ASC_SongInfo info, ref ASC_Channel_Parameters chan, ref ubyte TempMixer)
{
    const(ubyte)* mod = info.mod;
    byte j;
    bool Sample_Says_OK_for_Envelope;
    if(chan.Sample_Finished || !chan.Sound_Enabled)
        chan.Amplitude = 0;
    else
    {
        if(chan.Amplitude_Delay_Counter != 0)
        {
            if(chan.Amplitude_Delay_Counter >= 16)
            {
                chan.Amplitude_Delay_Counter -= 8;
                if(chan.Addition_To_Amplitude < -15)
                    chan.Addition_To_Amplitude++;
                else if(chan.Addition_To_Amplitude > 15)
                    chan.Addition_To_Amplitude--;
            }
            else
            {
                if((chan.Amplitude_Delay_Counter & 1) != 0)
                {
                    if(chan.Addition_To_Amplitude > -15)
                        chan.Addition_To_Amplitude--;
                }
                else if(chan.Addition_To_Amplitude < 15)
                    chan.Addition_To_Amplitude++;
                chan.Amplitude_Delay_Counter = chan.Amplitude_Delay;
            }
        }
        if((mod[chan.Point_In_Sample] & 128) != 0)
            chan.Loop_Point_In_Sample = chan.Point_In_Sample;
        if((mod[chan.Point_In_Sample] & 96) == 32)
            chan.Sample_Finished = true;
        chan.Ton_Deviation += cast(byte)(mod[chan.Point_In_Sample + 1]);
        TempMixer |= (mod[chan.Point_In_Sample + 2] & 9) << 3;
        if((mod[chan.Point_In_Sample + 2] & 6) == 2)
            Sample_Says_OK_for_Envelope = true;
        else
            Sample_Says_OK_for_Envelope = false;
        if((mod[chan.Point_In_Sample + 2] & 6) == 4)
        {
            if(chan.Addition_To_Amplitude > -15)
                chan.Addition_To_Amplitude--;
        }
        if((mod[chan.Point_In_Sample + 2] & 6) == 6)
        {
            if(chan.Addition_To_Amplitude < 15)
                chan.Addition_To_Amplitude++;
        }
        chan.Amplitude = cast(ubyte)(chan.Addition_To_Amplitude + (mod[chan.Point_In_Sample + 2] >> 4));
        if(cast(byte)(chan.Amplitude) < 0)
            chan.Amplitude = 0;
        else if(chan.Amplitude > 15)
            chan.Amplitude = 15;
        chan.Amplitude = cast(ubyte)((chan.Amplitude * (chan.Volume + 1)) >> 4);
        if(Sample_Says_OK_for_Envelope && ((TempMixer & 64) != 0))
            ay_writeay(AY_ENV_FINE, cast(ubyte)(ay_readay(AY_ENV_FINE) + (cast(byte)(mod[chan.Point_In_Sample] << 3) / 8)));
        else
            chan.Current_Noise += cast(byte)(mod[chan.Point_In_Sample] << 3) / 8;
        chan.Point_In_Sample += 3;
        if((mod[chan.Point_In_Sample - 3] & 64) != 0)
        {
            if(!chan.Break_Sample_Loop)
                chan.Point_In_Sample = chan.Loop_Point_In_Sample;
            else if((mod[chan.Point_In_Sample - 3] & 32) != 0)
                chan.Sample_Finished = true;
        }
        if((mod[chan.Point_In_Ornament] & 128) != 0)
            chan.Loop_Point_In_Ornament = chan.Point_In_Ornament;
        chan.Addition_To_Note += mod[chan.Point_In_Ornament + 1];
        chan.Current_Noise += (-(cast(byte)(mod[chan.Point_In_Ornament] & 0x10))) | mod[chan.Point_In_Ornament];
        chan.Point_In_Ornament += 2;
        if((mod[chan.Point_In_Ornament - 2] & 64) != 0)
            chan.Point_In_Ornament = chan.Loop_Point_In_Ornament;
        if((TempMixer & 64) == 0)
            ay_writeay(AY_NOISE_PERIOD, (cast(ubyte)(chan.Current_Ton_Sliding >> 8) + chan.Current_Noise) & 0x1f);
        j = cast(byte)(chan.Note + chan.Addition_To_Note);
        if(j < 0)
            j = 0;
        else if(j > 0x55)
            j = 0x55;
        chan.Ton = (ASM_Table[j] + chan.Ton_Deviation + (chan.Current_Ton_Sliding / 16)) & 0xfff;
        if(chan.Ton_Sliding_Counter != 0)
        {
            if(cast(byte)(chan.Ton_Sliding_Counter) > 0)
                chan.Ton_Sliding_Counter--;
            chan.Current_Ton_Sliding += chan.Substruction_for_Ton_Sliding;
        }
        if(chan.Envelope_Enabled && Sample_Says_OK_for_Envelope)
            chan.Amplitude |= 0x10;
    }
    TempMixer = TempMixer >> 1;
}

void ASC_Play(ref ASC_SongInfo info)
{
    const(ubyte)* mod = info.mod;
    auto header = cast(const(ASC1_File)*)mod;
    ubyte TempMixer;

    if(--info.ASC.DelayCounter <= 0)
    {
        if(--info.ASC_A.Note_Skip_Counter < 0)
        {
            if(mod[info.ASC_A.Address_In_Pattern] == 255)
            {
                if(++info.ASC.CurrentPosition >= header.ASC1_Number_Of_Positions)
                    info.ASC.CurrentPosition = header.ASC1_LoopingPosition;
                ushort ascPatPt = info.ASC1_PatternsPointers;
                //ASC_A.Address_In_Pattern = (*(ushort *)&module[ascPatPt + 6 * module[ASC.CurrentPosition + 9]]) + ascPatPt;
                //ASC_B.Address_In_Pattern = (*(ushort *)&module[ascPatPt + 6 * module[ASC.CurrentPosition + 9] + 2]) + ascPatPt;
                //ASC_C.Address_In_Pattern = (*(ushort *)&module[ascPatPt + 6 * module[ASC.CurrentPosition + 9] + 4]) + ascPatPt;
                info.ASC_A.Address_In_Pattern = cast(ushort)(ay_sys_getword(&mod[ascPatPt + 6 * mod[info.ASC.CurrentPosition + 9]]) + ascPatPt);
                info.ASC_B.Address_In_Pattern = cast(ushort)(ay_sys_getword(&mod[ascPatPt + 6 * mod[info.ASC.CurrentPosition + 9] + 2]) + ascPatPt);
                info.ASC_C.Address_In_Pattern = cast(ushort)(ay_sys_getword(&mod[ascPatPt + 6 * mod[info.ASC.CurrentPosition + 9] + 4]) + ascPatPt);
                info.ASC_A.Initial_Noise = 0;
                info.ASC_B.Initial_Noise = 0;
                info.ASC_C.Initial_Noise = 0;
            }
            ASC_PatternInterpreter(info, info.ASC_A);
        }
        if(--info.ASC_B.Note_Skip_Counter < 0)
        {
            ASC_PatternInterpreter(info, info.ASC_B);
        }
        if(--info.ASC_C.Note_Skip_Counter < 0)
        {
            ASC_PatternInterpreter(info, info.ASC_C);
        }
        info.ASC.DelayCounter = info.ASC.Delay;
    }

    TempMixer = 0;
    ASC_GetRegisters(info, info.ASC_A, TempMixer);
    ASC_GetRegisters(info, info.ASC_B, TempMixer);
    ASC_GetRegisters(info, info.ASC_C, TempMixer);

    ay_writeay(AY_MIXER, TempMixer);
    ay_writeay(AY_CHNL_A_FINE, info.ASC_A.Ton & 0xff);
    ay_writeay(AY_CHNL_A_COARSE, (info.ASC_A.Ton >> 8) & 0xf);
    ay_writeay(AY_CHNL_B_FINE, info.ASC_B.Ton & 0xff);
    ay_writeay(AY_CHNL_B_COARSE, (info.ASC_B.Ton >> 8) & 0xf);
    ay_writeay(AY_CHNL_C_FINE, info.ASC_C.Ton & 0xff);
    ay_writeay(AY_CHNL_C_COARSE, (info.ASC_C.Ton >> 8) & 0xf);
    ay_writeay(AY_CHNL_A_VOL, info.ASC_A.Amplitude);
    ay_writeay(AY_CHNL_B_VOL, info.ASC_B.Amplitude);
    ay_writeay(AY_CHNL_C_VOL, info.ASC_C.Amplitude);
}

uint ASC_GetTime(ref ASC_SongInfo info, out uint mLoop)
{
    mLoop = uint.max;
    const(ubyte)* mod = info.mod;
    auto header = cast(const(ASC1_File)*)mod;
    short a1, a2, a3, a11, a22, a33;
    uint j1, j2, j3;
    bool env1, env2, env3;
    int i, tm = 0;
    ubyte b;

    ubyte ascDelay = header.ASC1_Delay;
    ushort ascLoopPos = header.ASC1_LoopingPosition;
    ushort ascPatPt = info.ASC1_PatternsPointers;
    ubyte ascNumPos = header.ASC1_Number_Of_Positions;

    b = ascDelay;
    a1 = a2 = a3 = a11 = a22 = a33 = 0;
    env1 = env2 = env3 = false;
    for(i = 0; i < ascNumPos; i++)
    {
        if(ascLoopPos == i)
            mLoop = tm;
        j1 = ay_sys_getword(&mod[ascPatPt + 6 * mod[i + 9]]) + ascPatPt;
        j2 = ay_sys_getword(&mod[ascPatPt + 6 * mod[i + 9] + 2]) + ascPatPt;
        j3 = ay_sys_getword(&mod[ascPatPt + 6 * mod[i + 9] + 4]) + ascPatPt;

        while(true)
        {
            a1--;
            if(a1 < 0)
            {
                if(mod[j1] == 255)
                    break;
                while(true)
                {
                    ubyte val = mod[j1];
                    if(val <= 0x55)
                    {
                        a1 = a11;
                        j1++;
                        if(env1)
                            j1++;
                        break;
                    }
                    else if((val >= 0x56) && (val <= 0x5f))
                    {
                        a1 = a11;
                        j1++;
                        break;
                    }
                    else if((val >= 0x60) && (val <= 0x9f))
                    {
                        a11 = val - 0x60;
                    }
                    else if(val == 0xe0)
                    {
                        env1 = true;
                    }
                    else if((val >= 0xe1) && (val <= 0xef))
                    {
                        env1 = false;
                    }
                    else if((val == 0xf0) || ((val >= 0xf5) && (val <= 0xf7)) || (val == 0xf9) || (val == 0xfb))
                    {
                        j1++;
                    }
                    else if(val == 0xf4)
                    {
                        j1++;
                        b = mod[j1];
                    }
                    j1++;
                }
            }

            a2--;
            if(a2 < 0)
            {
                while(true)
                {
                    ubyte val = mod[j2];
                    if(val <= 0x55)
                    {
                        a2 = a22;
                        j2++;
                        if(env2)
                            j2++;
                        break;
                    }
                    else if((val >= 0x56) && (val <= 0x5f))
                    {
                        a2 = a22;
                        j2++;
                        break;
                    }
                    else if((val >= 0x60) && (val <= 0x9f))
                    {
                        a22 = val - 0x60;
                    }
                    else if(val == 0xe0)
                    {
                        env2 = true;
                    }
                    else if((val >= 0xe1) && (val <= 0xef))
                    {
                        env2 = false;
                    }
                    else if((val == 0xf0) || ((val >= 0xf5) && (val <= 0xf7)) || (val == 0xf9) || (val == 0xfb))
                    {
                        j2++;
                    }
                    else if(val == 0xf4)
                    {
                        j2++;
                        b = mod[j2];
                    }
                    j2++;
                }
            }

            a3--;
            if(a3 < 0)
            {
                while(true)
                {
                    ubyte val = cast(ubyte)mod[j3];
                    if(val <= 0x55)
                    {
                        a3 = a33;
                        j3++;
                        if(env3)
                            j3++;
                        break;
                    }
                    else if((val >= 0x56) && (val <= 0x5f))
                    {
                        a3 = a33;
                        j3++;
                        break;
                    }
                    else if((val >= 0x60) && (val <= 0x9f))
                    {
                        a33 = val - 0x60;
                    }
                    else if(val == 0xe0)
                    {
                        env3 = true;
                    }
                    else if((val >= 0xe1) && (val <= 0xef))
                    {
                        env3 = false;
                    }
                    else if((val == 0xf0) || ((val >= 0xf5) && (val <= 0xf7)) || (val == 0xf9) || (val == 0xfb))
                    {
                        j3++;
                    }
                    else if(val == 0xf4)
                    {
                        j3++;
                        b = mod[j3];
                    }
                    j3++;
                }
            }
            tm += b;
        }
    }
    return tm;
    /*
    info.Length = tm;
    ushort pp = ay_sys_getword(&mod [2]);
    ubyte np = mod [8];
    if(pp - np == 72)
    {
        info.Name = ay_sys_getstr(&mod [pp - 44], 20);
        info.Author = ay_sys_getstr(&mod [pp - 20], 20);
    }
    */
}

const(char)[] ASC_GetName (ref ASC_SongInfo si) {
  const(char)* mod = cast(const(char)*)si.mod;
  ushort pp = ay_sys_getword(&mod[2]);
  ubyte np = mod[8];
  if (pp-np == 72) return ay_sys_getstr(&mod[pp-44], 20);
  return null;
}

const(char)[] ASC_GetAuthor (ref ASC_SongInfo si) {
  const(char)* mod = cast(const(char)*)si.mod;
  ushort pp = ay_sys_getword(&mod[2]);
  ubyte np = mod[8];
  if (pp-np == 72) return ay_sys_getstr(&mod[pp-20], 20);
  return null;
}

bool ASC0_Detect (ubyte[] moda)
{
  auto info = new ASC_SongInfo();
  scope(exit) delete info;
  info.data = moda;
  auto length = moda.length;
  ubyte* mod = moda.ptr;

    int j, j1;
    ubyte j3;
    auto header = cast(ASC0_File *)mod;
    if(length < 9)
        return false;
    if((*info).ASC0_PatternsPointers - 8 - header.ASC0_Number_Of_Positions != 0)
        return false;
    if((*info).ASC0_PatternsPointers> length)
        return false;
    if((*info).ASC0_SamplesPointers> length)
        return false;
    if((*info).ASC0_OrnamentsPointers> length)
        return false;

    j = ay_sys_getword(&mod[(*info).ASC0_SamplesPointers]);
    if(j != 0x40)
        return false;
    j = ay_sys_getword(&mod[(*info).ASC0_OrnamentsPointers]);
    if(j != 0x40)
        return false;

    j3 = 0;
    for(j1 = 0; j1 < header.ASC0_Number_Of_Positions; j1++)
        if(j3 < header.ASC0_Positions[j1])
            j3 = header.ASC0_Positions[j1];

    j = ay_sys_getword(&mod[(*info).ASC0_PatternsPointers]);
    if(j != (j3 + 1) * 6)
        return false;
    j = ay_sys_getword(&mod[(*info).ASC0_OrnamentsPointers + 0x40 - 2]);
    j += (*info).ASC0_OrnamentsPointers;
    while((cast(uint)j < length) && (j < 65535) && ((mod[j] & 0x40) == 0))
        j += 2;

    if(j > 65534)
        return false;
    if(cast(uint)j >= length)
        return false;
    return true;
}

bool ASC1_Detect(ubyte[] moda)
{
  auto info = new ASC_SongInfo();
  scope(exit) delete info;
  info.data = moda;
  auto length = moda.length;
  ubyte* mod = moda.ptr;

    int j, j1;
    ubyte j3;
    auto header = cast(ASC1_File*)mod;
    if(length < 9)
        return false;
    int delta = (*info).ASC1_PatternsPointers - header.ASC1_Number_Of_Positions;
    if(delta < 9 || delta > 72)
        return false;
    if((*info).ASC1_PatternsPointers> length)
        return false;
    if((*info).ASC1_SamplesPointers> length)
        return false;
    if((*info).ASC1_OrnamentsPointers> length)
        return false;

    j = ay_sys_getword(&mod[(*info).ASC1_SamplesPointers]);
    if(j != 0x40)
        return false;
    j = ay_sys_getword(&mod[(*info).ASC1_OrnamentsPointers]);
    if(j != 0x40)
        return false;

    j3 = 0;
    for(j1 = 0; j1 < header.ASC1_Number_Of_Positions; j1++)
        if(j3 < header.ASC1_Positions[j1])
            j3 = header.ASC1_Positions[j1];

    j = ay_sys_getword(&mod[(*info).ASC1_PatternsPointers]);
    if(j != (j3 + 1) * 6)
        return false;
    j = ay_sys_getword(&mod[(*info).ASC1_OrnamentsPointers + 0x40 - 2]);
    j += (*info).ASC1_OrnamentsPointers;
    while((cast(uint)j < length) && (j < 65535) && ((mod[j] & 0x40) == 0))
        j += 2;

    if(j > 65534)
        return false;
    if(cast(uint)j >= length)
        return false;
    return true;
}

bool ASC_Detect (ubyte[] mod) {
  return (ASC1_Detect(mod) || ASC0_Detect(mod));
}


shared static this () {
  zxModuleRegister!".asc"((ubyte[] data) {
    if (ASC_Detect(data)) return [cast(ZXModule)(new ZXModuleASC(data))];
    return null;
  });
}


final class ZXModuleASC : ZXModule {
private:
  ASC_SongInfo si;
  uint mIntrs, mLoop;

public:
  this (ubyte[] adata) {
    si.data = adata;
    ASC_Init(si);
    mIntrs = ASC_GetTime(si, mLoop);
  }

  override string typeStr () { return "ASC"; }
  override const(char)[] name () { return ASC_GetName(si); }
  override const(char)[] author () { return ASC_GetAuthor(si); }
  override const(char)[] misc () { return null; }
  override uint intrCount () { return mIntrs; }
  override uint fadeCount () { return 0; }
  override uint loopPos () { return mLoop; }
  override bool doIntr () { ASC_Play(si); return true; }
  override void reset () { super.reset(); ASC_Init(si); }
}
