/* ProTracker 1 module decoder
 * Copyright (C) 2007 by Deryabin Andrew ( andrewderyabin@gmail.com )
 * Copyright (C) 2017 Ketmar // Invisible Vector
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
module modplay.modpsc is aliced;
private:

import modplay.mod_low;

immutable ushort[$] ASM_Table =
[ 0xedc, 0xe07, 0xd3e, 0xc80, 0xbcc, 0xb22, 0xa82, 0x9ec, 0x95c, 0x8d6, 0x858, 0x7e0, 0x76e, 0x704, 0x69f, 0x640, 0x5e6, 0x591, 0x541, 0x4f6, 0x4ae, 0x46b, 0x42c, 0x3f0, 0x3b7, 0x382, 0x34f, 0x320, 0x2f3, 0x2c8, 0x2a1, 0x27b, 0x257, 0x236, 0x216, 0x1f8, 0x1dc, 0x1c1, 0x1a8, 0x190, 0x179, 0x164, 0x150, 0x13d, 0x12c, 0x11b, 0x10b, 0xfc, 0xee, 0xe0, 0xd4, 0xc8, 0xbd, 0xb2, 0xa8, 0x9f, 0x96, 0x8d, 0x85, 0x7e, 0x77, 0x70, 0x6a, 0x64, 0x5e, 0x59, 0x54, 0x50, 0x4b, 0x47, 0x43, 0x3f, 0x3c, 0x38, 0x35, 0x32, 0x2f, 0x2d, 0x2a, 0x28, 0x26, 0x24, 0x22, 0x20, 0x1e, 0x1c ];

align(1) struct PSC_File
{
align(1):
    byte[69] PSC_MusicName;
    ubyte PSC_UnknownPointer0, PSC_UnknownPointer1;
    ubyte PSC_PatternsPointer0, PSC_PatternsPointer1;
    ubyte PSC_Delay;
    ubyte PSC_OrnamentsPointer0, PSC_OrnamentsPointer1;
    ubyte[64] PSC_SamplesPointers0;
}

const(PSC_File)* Header (ref PSC_SongInfo info) { return cast(const(PSC_File)*)info.mod; }

ushort PSC_UnknownPointer (ref PSC_SongInfo info) { return cast(ushort)(info.Header.PSC_UnknownPointer0 | (info.Header.PSC_UnknownPointer1 << 8)); }
ushort PSC_PatternsPointer (ref PSC_SongInfo info) { return cast(ushort)(info.Header.PSC_PatternsPointer0 | (info.Header.PSC_PatternsPointer1 << 8)); }
ushort PSC_OrnamentsPointer (ref PSC_SongInfo info) { return cast(ushort)(info.Header.PSC_OrnamentsPointer0 | (info.Header.PSC_OrnamentsPointer1 << 8)); }
ushort PSC_SamplesPointers (ref PSC_SongInfo info, uint x) { return cast(ushort)(info.Header.PSC_SamplesPointers0 [(x) * 2] | ((info.Header.PSC_SamplesPointers0 [((x) * 2) + 1]) << 8)); }

align(1) struct PSC_Channel_Parameters
{
align(1):
    ubyte num;
    ushort Address_In_Pattern, OrnamentPointer, SamplePointer, Ton;
    short Current_Ton_Sliding, Ton_Accumulator, Addition_To_Ton;
    byte Initial_Volume, Note_Skip_Counter;
    ubyte Note, Volume, Amplitude, Volume_Counter, Volume_Counter1, Volume_Counter_Init, Noise_Accumulator, Position_In_Sample, Loop_Sample_Position, Position_In_Ornament, Loop_Ornament_Position;
    bool Enabled, Ornament_Enabled, Envelope_Enabled, Gliss, Ton_Slide_Enabled, Break_Sample_Loop, Break_Ornament_Loop, Volume_Inc;
}

align(1) struct PSC_Parameters
{
align(1):
    ubyte Delay, DelayCounter, Lines_Counter, Noise_Base;
    ushort Positions_Pointer;
}

align(1) struct PSC_SongInfo
{
align(1):
    PSC_Parameters PSC;
    PSC_Channel_Parameters PSC_A, PSC_B, PSC_C;
    ubyte[] data;
    const(ubyte)* mod () { return data.ptr; }
}

/*
#define PSC_A ((PSC_SongInfo *)info.data).PSC_A
#define PSC_B ((PSC_SongInfo *)info.data).PSC_B
#define PSC_C ((PSC_SongInfo *)info.data).PSC_C
#define PSC ((PSC_SongInfo *)info.data).PSC
*/

void PSC_Init(ref PSC_SongInfo info)
{
    const(ubyte)* mod = info.mod;
    auto header = cast(const(PSC_File)*)mod;

    info.PSC_A = info.PSC_A.init;
    info.PSC_B = info.PSC_B.init;
    info.PSC_C = info.PSC_C.init;

    info.PSC.DelayCounter = 1;
    info.PSC.Delay = header.PSC_Delay;
    info.PSC.Positions_Pointer = info.PSC_PatternsPointer;
    info.PSC.Lines_Counter = 1;
    info.PSC.Noise_Base = 0;

    info.PSC_A.num = 0;
    info.PSC_B.num = 1;
    info.PSC_C.num = 2;

    info.PSC_A.SamplePointer = cast(ushort)(info.PSC_SamplesPointers(0) + 0x4c);
    info.PSC_B.SamplePointer = info.PSC_A.SamplePointer;
    info.PSC_C.SamplePointer = info.PSC_A.SamplePointer;
    info.PSC_A.OrnamentPointer = cast(ushort)(ay_sys_getword(&mod[info.PSC_OrnamentsPointer]) + info.PSC_OrnamentsPointer);
    info.PSC_B.OrnamentPointer = info.PSC_A.OrnamentPointer;
    info.PSC_C.OrnamentPointer = info.PSC_A.OrnamentPointer;

    info.PSC_A.Break_Ornament_Loop = false;
    info.PSC_A.Ornament_Enabled = false;
    info.PSC_A.Enabled = false;
    info.PSC_A.Break_Sample_Loop = false;
    info.PSC_A.Ton_Slide_Enabled = false;
    info.PSC_A.Note_Skip_Counter = 1;
    info.PSC_A.Ton = 0;

    info.PSC_B.Break_Ornament_Loop = false;
    info.PSC_B.Ornament_Enabled = false;
    info.PSC_B.Enabled = false;
    info.PSC_B.Break_Sample_Loop = false;
    info.PSC_B.Ton_Slide_Enabled = false;
    info.PSC_B.Note_Skip_Counter = 1;
    info.PSC_B.Ton = 0;

    info.PSC_C.Break_Ornament_Loop = false;
    info.PSC_C.Ornament_Enabled = false;
    info.PSC_C.Enabled = false;
    info.PSC_C.Break_Sample_Loop = false;
    info.PSC_C.Ton_Slide_Enabled = false;
    info.PSC_C.Note_Skip_Counter = 1;
    info.PSC_C.Ton = 0;
}

void PSC_PatternInterpreter(ref PSC_SongInfo info, ref PSC_Channel_Parameters chan)
{
    const(ubyte)* mod = info.mod;
    auto header = cast(const(PSC_File)*)mod;
    bool quit;
    bool b1b, b2b, b3b, b4b, b5b, b6b, b7b;
    quit = b1b = b2b = b3b = b4b = b5b = b6b = b7b = false;
    do
    {
        ubyte val = mod[chan.Address_In_Pattern];
        if(val >= 0xc0)
        {
            chan.Note_Skip_Counter = cast(byte)(val - 0xbf);
            quit = true;
        }
        else if(val >= 0xa0 && val <= 0xbf)
        {
            //chan.OrnamentPointer = (*(ushort *) &mod[PSC_OrnamentsPointer + (val - 0xa0) * 2]) +PSC_OrnamentsPointer;
            chan.OrnamentPointer = cast(ushort)(ay_sys_getword(&mod[info.PSC_OrnamentsPointer + (val - 0xa0) * 2]) + info.PSC_OrnamentsPointer);
        }
        else if(val >= 0x7e && val <= 0x9f)
        {
            if(val >= 0x80)
                chan.SamplePointer = cast(ushort)(info.PSC_SamplesPointers(val - 0x80) + 0x4c);
        }
        else if(val == 0x6b)
        {
            chan.Address_In_Pattern++;
            chan.Addition_To_Ton = mod[chan.Address_In_Pattern];
            b5b = true;
        }
        else if(val == 0x6c)
        {
            chan.Address_In_Pattern++;
            chan.Addition_To_Ton = -(cast(byte)(mod[chan.Address_In_Pattern]));
            b5b = true;
        }
        else if(val == 0x6d)
        {
            b4b = true;
            chan.Address_In_Pattern++;
            chan.Addition_To_Ton = mod[chan.Address_In_Pattern];
        }
        else if(val == 0x6e)
        {
            chan.Address_In_Pattern++;
            info.PSC.Delay = mod[chan.Address_In_Pattern];
        }
        else if(val == 0x6f)
        {
            b1b = true;
            chan.Address_In_Pattern++;
        }
        else if(val == 0x70)
        {
            b3b = true;
            chan.Address_In_Pattern++;
            chan.Volume_Counter1 = mod[chan.Address_In_Pattern];
        }
        else if(val == 0x71)
        {
            chan.Break_Ornament_Loop = true;
            chan.Address_In_Pattern++;
        }
        else if(val == 0x7a)
        {
            chan.Address_In_Pattern++;
            if(chan.num == 1)
            {
                ay_writeay(AY_ENV_SHAPE, mod[chan.Address_In_Pattern] & 15);
                ay_writeay(AY_ENV_FINE, mod[chan.Address_In_Pattern + 1]);
                ay_writeay(AY_ENV_COARSE, mod[chan.Address_In_Pattern + 2]);
                chan.Address_In_Pattern += 2;
            }
        }
        else if(val == 0x7b)
        {
            chan.Address_In_Pattern++;
            if(chan.num == 1)
                info.PSC.Noise_Base = mod[chan.Address_In_Pattern];
        }
        else if(val == 0x7c)
        {
            b1b = b3b = b4b = b5b = b6b = b7b = false;
            b2b = true;
        }
        else if(val == 0x7d)
        {
            chan.Break_Sample_Loop = true;
        }
        else if(val >= 0x58 && val <= 0x66)
        {
            chan.Initial_Volume = cast(byte)(val - 0x57);
            chan.Envelope_Enabled = false;
            b6b = true;
        }
        else if(val == 0x57)
        {
            chan.Initial_Volume = 0xf;
            chan.Envelope_Enabled = true;
            b6b = true;
        }
        else if(val <= 0x56)
        {
            chan.Note = val;
            b6b = true;
            b7b = true;
        }
        else
            chan.Address_In_Pattern++;
        chan.Address_In_Pattern++;
    }
    while(!quit);

    if(b7b)
    {
        chan.Break_Ornament_Loop = false;
        chan.Ornament_Enabled = true;
        chan.Enabled = true;
        chan.Break_Sample_Loop = false;
        chan.Ton_Slide_Enabled = false;
        chan.Ton_Accumulator = 0;
        chan.Current_Ton_Sliding = 0;
        chan.Noise_Accumulator = 0;
        chan.Volume_Counter = 0;
        chan.Position_In_Sample = 0;
        chan.Position_In_Ornament = 0;
    }
    if(b6b)
        chan.Volume = chan.Initial_Volume;
    if(b5b)
    {
        chan.Gliss = false;
        chan.Ton_Slide_Enabled = true;
    }
    if(b4b)
    {
        chan.Current_Ton_Sliding = cast(short)(chan.Ton - ASM_Table[chan.Note]);
        chan.Gliss = true;
        if(chan.Current_Ton_Sliding >= 0)
            chan.Addition_To_Ton = -(chan.Addition_To_Ton);
        chan.Ton_Slide_Enabled = true;
    }
    if(b3b)
    {
        chan.Volume_Counter = chan.Volume_Counter1;
        chan.Volume_Inc = true;
        if((chan.Volume_Counter & 0x40) != 0)
        {
            chan.Volume_Counter = -(cast(byte)(chan.Volume_Counter | 128));
            chan.Volume_Inc = false;
        }
        chan.Volume_Counter_Init = chan.Volume_Counter;
    }
    if(b2b)
    {
        chan.Break_Ornament_Loop = false;
        chan.Ornament_Enabled = false;
        chan.Enabled = false;
        chan.Break_Sample_Loop = false;
        chan.Ton_Slide_Enabled = false;
    }
    if(b1b)
        chan.Ornament_Enabled = false;
}

void PSC_GetRegisters(ref PSC_SongInfo info, ref PSC_Channel_Parameters chan, ref ubyte TempMixer)
{
    const(ubyte)* mod = info.mod;
    ubyte j, b;
    if(chan.Enabled)
    {
        j = chan.Note;
        if(chan.Ornament_Enabled)
        {
            b = mod[chan.OrnamentPointer + chan.Position_In_Ornament * 2];
            chan.Noise_Accumulator += b;
            j += mod[chan.OrnamentPointer + chan.Position_In_Ornament * 2 + 1];
            if(cast(byte)j < 0)
                j += 0x56;
            if(j > 0x55)
                j -= 0x56;
            if(j > 0x55)
                j = 0x55;
            if((b & 128) == 0)
                chan.Loop_Ornament_Position = chan.Position_In_Ornament;
            if((b & 64) == 0)
            {
                if(!chan.Break_Ornament_Loop)
                    chan.Position_In_Ornament = chan.Loop_Ornament_Position;
                else
                {
                    chan.Break_Ornament_Loop = false;
                    if((b & 32) == 0)
                        chan.Ornament_Enabled = false;
                    chan.Position_In_Ornament++;
                }
            }
            else
            {
                if((b & 32) == 0)
                    chan.Ornament_Enabled = false;
                chan.Position_In_Ornament++;
            }
        }
        chan.Note = j;
        //chan.Ton = *(ushort *) &mod[chan.SamplePointer + chan.Position_In_Sample * 6];
        chan.Ton = ay_sys_getword(&mod[chan.SamplePointer + chan.Position_In_Sample * 6]);
        chan.Ton_Accumulator += chan.Ton;
        chan.Ton = cast(ushort)(ASM_Table[j] + chan.Ton_Accumulator);
        if(chan.Ton_Slide_Enabled)
        {
            chan.Current_Ton_Sliding += chan.Addition_To_Ton;
            if(chan.Gliss && (((chan.Current_Ton_Sliding < 0) && (chan.Addition_To_Ton <= 0)) || ((chan.Current_Ton_Sliding >= 0) && (chan.Addition_To_Ton >= 0))))
                chan.Ton_Slide_Enabled = false;
            chan.Ton += chan.Current_Ton_Sliding;
        }
        chan.Ton = chan.Ton & 0xfff;
        b = mod[chan.SamplePointer + chan.Position_In_Sample * 6 + 4];
        TempMixer = TempMixer | ((b & 9) << 3);
        j = 0;
        if((b & 2) != 0)
            j++;
        if((b & 4) != 0)
            j--;
        if(chan.Volume_Counter > 0)
        {
            chan.Volume_Counter--;
            if(chan.Volume_Counter == 0)
            {
                if(chan.Volume_Inc)
                    j++;
                else
                    j--;
                chan.Volume_Counter = chan.Volume_Counter_Init;
            }
        }
        chan.Volume += j;
        if(cast(byte)chan.Volume < 0)
            chan.Volume = 0;
        else if(chan.Volume > 15)
            chan.Volume = 15;
        chan.Amplitude = cast(ubyte)(((chan.Volume + 1) * (mod[chan.SamplePointer + chan.Position_In_Sample * 6 + 3] & 15)) >> 4);
        if(chan.Envelope_Enabled && ((b & 16) == 0))
            chan.Amplitude = chan.Amplitude | 16;
        if(((chan.Amplitude & 16) != 0) & ((b & 8) != 0))
        {
            ushort env = ay_readay(AY_ENV_FINE) | (ay_readay(AY_ENV_COARSE) << 8);
            env += cast(byte)(mod[chan.SamplePointer + chan.Position_In_Sample * 6 + 2]);
            ay_writeay(AY_ENV_FINE, env & 0xff);
            ay_writeay(AY_ENV_COARSE, (env >> 8) & 0xff);
        }
        else
        {
            chan.Noise_Accumulator += mod[chan.SamplePointer + chan.Position_In_Sample * 6 + 2];
            if((b & 8) == 0)
                ay_writeay(AY_NOISE_PERIOD, chan.Noise_Accumulator & 31);
        }
        if((b & 128) == 0)
            chan.Loop_Sample_Position = chan.Position_In_Sample;
        if((b & 64) == 0)
        {
            if(!chan.Break_Sample_Loop)
                chan.Position_In_Sample = chan.Loop_Sample_Position;
            else
            {
                chan.Break_Sample_Loop = false;
                if((b & 32) == 0)
                    chan.Enabled = false;
                chan.Position_In_Sample++;
            }
        }
        else
        {
            if((b & 32) == 0)
                chan.Enabled = false;
            chan.Position_In_Sample++;
        }
    }
    else
        chan.Amplitude = 0;
    TempMixer = TempMixer >> 1;
}

void PSC_Play(ref PSC_SongInfo info)
{
    const(ubyte)* mod = info.mod;
    ubyte TempMixer;

    if(--info.PSC.DelayCounter <= 0)
    {
        if(--info.PSC.Lines_Counter <= 0)
        {
            if(mod[info.PSC.Positions_Pointer + 1] == 255)
            {
                //PSC.Positions_Pointer = *(ushort *) &mod[PSC.Positions_Pointer + 2];
                info.PSC.Positions_Pointer = ay_sys_getword(&mod[info.PSC.Positions_Pointer + 2]);
            }
            info.PSC.Lines_Counter = mod[info.PSC.Positions_Pointer + 1];
            //PSC_A.Address_In_Pattern = *(ushort *) &mod[PSC.Positions_Pointer + 2];
            //PSC_B.Address_In_Pattern = *(ushort *) &mod[PSC.Positions_Pointer + 4];
            //PSC_C.Address_In_Pattern = *(ushort *) &mod[PSC.Positions_Pointer + 6];
            info.PSC_A.Address_In_Pattern = ay_sys_getword(&mod[info.PSC.Positions_Pointer + 2]);
            info.PSC_B.Address_In_Pattern = ay_sys_getword(&mod[info.PSC.Positions_Pointer + 4]);
            info.PSC_C.Address_In_Pattern = ay_sys_getword(&mod[info.PSC.Positions_Pointer + 6]);
            info.PSC.Positions_Pointer += 8;
            info.PSC_A.Note_Skip_Counter = 1;
            info.PSC_B.Note_Skip_Counter = 1;
            info.PSC_C.Note_Skip_Counter = 1;
        }
        if(--info.PSC_A.Note_Skip_Counter == 0)
            PSC_PatternInterpreter(info, info.PSC_A);
        if(--info.PSC_B.Note_Skip_Counter == 0)
            PSC_PatternInterpreter(info, info.PSC_B);
        if(--info.PSC_C.Note_Skip_Counter == 0)
            PSC_PatternInterpreter(info, info.PSC_C);
        info.PSC_A.Noise_Accumulator += info.PSC.Noise_Base;
        info.PSC_B.Noise_Accumulator += info.PSC.Noise_Base;
        info.PSC_C.Noise_Accumulator += info.PSC.Noise_Base;
        info.PSC.DelayCounter = info.PSC.Delay;
    }
    TempMixer = 0;
    PSC_GetRegisters(info, info.PSC_A, TempMixer);
    PSC_GetRegisters(info, info.PSC_B, TempMixer);
    PSC_GetRegisters(info, info.PSC_C, TempMixer);

    ay_writeay(AY_MIXER, TempMixer);
    ay_writeay(AY_CHNL_A_FINE, info.PSC_A.Ton & 0xff);
    ay_writeay(AY_CHNL_A_COARSE, (info.PSC_A.Ton >> 8) & 0xf);
    ay_writeay(AY_CHNL_B_FINE, info.PSC_B.Ton & 0xff);
    ay_writeay(AY_CHNL_B_COARSE, (info.PSC_B.Ton >> 8) & 0xf);
    ay_writeay(AY_CHNL_C_FINE, info.PSC_C.Ton & 0xff);
    ay_writeay(AY_CHNL_C_COARSE, (info.PSC_C.Ton >> 8) & 0xf);
    ay_writeay(AY_CHNL_A_VOL, info.PSC_A.Amplitude);
    ay_writeay(AY_CHNL_B_VOL, info.PSC_B.Amplitude);
    ay_writeay(AY_CHNL_C_VOL, info.PSC_C.Amplitude);
}

uint PSC_GetTime (ref PSC_SongInfo info, out uint mLoop)
{
    mLoop = uint.max;
    const(ubyte)* mod = info.mod;
    auto header = cast(const(PSC_File)*)mod;
    ubyte b;
    uint tm = 0;
    int i;
    byte a1, a2, a3;
    uint j1, j2, j3;
    uint pptr, cptr;

    b = header.PSC_Delay;
    pptr = info.PSC_PatternsPointer;
    pptr++;
    while(mod[pptr] != 255)
    {
        pptr += 8;
        if(pptr >= 65536)
        {
            return 0;
        }
    }
    if(pptr >= 65546 - 2)
    {
        //info.Length = 0;
        return 0;
    }
    //cptr = *(ushort *) &mod[pptr + 1];
    cptr = ay_sys_getword(&mod[pptr + 1]);
    cptr++;
    pptr = info.PSC_PatternsPointer;
    pptr++;
    while(mod[pptr] != 255)
    {
        if(pptr == cptr)
            mLoop = tm;
        if(pptr >= 65536 - 6)
        {
            //info.Length = 0;
            return 0;
        }
        //j1 = *(ushort *) &mod[pptr + 1];
        //j2 = *(ushort *) &mod[pptr + 3];
        //j3 = *(ushort *) &mod[pptr + 5];
        j1 = ay_sys_getword(&mod[pptr + 1]);
        j2 = ay_sys_getword(&mod[pptr + 3]);
        j3 = ay_sys_getword(&mod[pptr + 5]);
        pptr += 8;
        if(pptr >= 65536)
        {
            //info.Length = 0;
            return 0;
        }
        a1 = a2 = a3 = 1;
        for(i = 0; i < mod[pptr - 8]; i++)
        {
            a1--;
            if(a1 == 0)
            {
                while(true)
                {
                    ubyte val = mod[j1];
                    if(val >= 0xc0)
                    {
                        a1 = cast(byte)(val - 0xbf);
                        j1++;
                        break;
                    }
                    else if((val >= 0x67 && val <= 0x6d) || (val >= 0x6f && val <= 0x7b))
                    {
                        j1++;
                    }
                    else if(val == 0x6e)
                    {
                        j1++;
                        b = mod[j1];
                    }
                    j1++;
                }

            }

            a2--;
            if(a2 == 0)
            {
                while(true)
                {
                    ubyte val = mod[j2];
                    if(val >= 0xc0)
                    {
                        a2 = cast(byte)(val - 0xbf);
                        j2++;
                        break;
                    }
                    else if((val >= 0x67 && val <= 0x6d) || (val >= 0x6f && val <= 0x7b))
                    {
                        j2++;
                    }
                    else if(val == 0x6e)
                    {
                        j2++;
                        b = mod[j2];
                    }
                    j2++;
                }
            }

            a3--;
            if(a3 == 0)
            {
                while(true)
                {
                    ubyte val = mod[j3];
                    if(val >= 0xc0)
                    {
                        a3 = cast(byte)(val - 0xbf);
                        j3++;
                        break;
                    }
                    else if((val >= 0x67 && val <= 0x6d) || (val >= 0x6f && val <= 0x7b))
                    {
                        j3++;
                    }
                    else if(val == 0x6e)
                    {
                        j3++;
                        b = mod[j3];
                    }
                    j3++;
                }
            }
            tm += b;
        }
    }
    return tm;
}

const(char)[] PSC_GetName (ref PSC_SongInfo info) {
  const(ubyte)* mod = info.mod;
  return ay_sys_getstr(mod+0x19, 20);
}

const(char)[] PSC_GetAuthor (ref PSC_SongInfo info) {
  const(ubyte)* mod = info.mod;
  return ay_sys_getstr(mod+0x31, 20);
}

bool PSC_Detect(ubyte[] moda) {
  auto info = new PSC_SongInfo();
  scope(exit) delete info;
  info.data = moda;
  auto length = moda.length;
  ubyte* mod = moda.ptr;

    int j, j1;
    auto header = cast(PSC_File*)mod;
    if(length < 0x4c + 2)
        return false;
    if(cast(uint)(*info).PSC_OrnamentsPointer >= length)
        return false;
    if((*info).PSC_OrnamentsPointer < 0x4c + 2)
        return false;
    if((*info).PSC_OrnamentsPointer > 64 + 0x4c)
        return false;
    if(((*info).PSC_OrnamentsPointer % 2) != 0)
        return false;
    if((*info).PSC_SamplesPointers(0) + 0x4c + 5 > 65534)
        return false;
    if(cast(uint)((*info).PSC_SamplesPointers(0) + 0x4c + 5) > length)
        return false;

    j = ay_sys_getword(&mod[(*info).PSC_OrnamentsPointer]);
    j += (*info).PSC_OrnamentsPointer;
    if(j > 65535)
        return false;
    if(cast(uint)j >= length)
        return false;

    j1 = 0;
    j1 = ay_sys_getword(&mod[(*info).PSC_OrnamentsPointer-2]);
    j1 += 0x4c;
    if(j1 > 65535)
        return false;
    if(cast(uint)j1 >= length)
        return false;
    if(j - j1 < 8)
        return false;
    if(((j - j1) % 6) != 2)
        return false;

    j1 = (*info).PSC_SamplesPointers(0) + 0x4c + 4;
    while((j1 < 65536) && (cast(uint)j1 <= length) && ((mod[j1] & 32) != 0))
        j1 += 6;
    if(j1 > 65534)
        return false;
    if(cast(uint)j1 > length)
        return false;

    if((*info).PSC_OrnamentsPointer - 0x4c - 2 > 0)
    {
        if(j1 + 3 != (*info).PSC_SamplesPointers(1) + 0x4c)
            return false;
    }
    else if(j1 + 4 != j)
        return false;

    if((*info).PSC_PatternsPointer + 11 > 65535)
        return false;
    if(cast(uint)((*info).PSC_PatternsPointer + 11) >= length)
        return false;

    j = (*info).PSC_PatternsPointer + 1;
    if(mod[j] == 255)
        return false;

    while(1)
    {
        j += 8;
        if((j > 65532 && cast(uint)(j + 2) > length) || mod[j] == 255)
            break;
    }

    if(j > 65532)
        return false;
    if(cast(uint)(j + 2) > length)
        return false;

    return true;

}


shared static this () {
  zxModuleRegister!".psc"((ubyte[] data) {
    if (PSC_Detect(data)) return [cast(ZXModule)(new ZXModulePSC(data))];
    return null;
  });
}


final class ZXModulePSC : ZXModule {
private:
  PSC_SongInfo si;
  uint mIntrs, mLoop;

public:
  this (ubyte[] adata) {
    si.data = adata;
    PSC_Init(si);
    mIntrs = PSC_GetTime(si, mLoop);
  }

  override string typeStr () { return "PSC"; }
  override const(char)[] name () { return PSC_GetName(si); }
  override const(char)[] author () { return PSC_GetAuthor(si); }
  override const(char)[] misc () { return null; }
  override uint intrCount () { return mIntrs; }
  override uint fadeCount () { return 0; }
  override uint loopPos () { return mLoop; }
  override bool doIntr () { PSC_Play(si); return true; }
  override void reset () { super.reset(); PSC_Init(si); }
}
