/* ProTracker 1 module decoder
 * Copyright (C) 2007 by Deryabin Andrew ( andrewderyabin@gmail.com )
 * Copyright (C) 2017 Ketmar // Invisible Vector
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
module modplay.modstp is aliced;
private:

import modplay.mod_low;

immutable ushort[96] ST_Table =
[ 0xef8, 0xe10, 0xd60, 0xc80, 0xbd8, 0xb28, 0xa88, 0x9f0, 0x960, 0x8e0, 0x858, 0x7e0, 0x77c, 0x708, 0x6b0, 0x640, 0x5ec, 0x594, 0x544, 0x4f8, 0x4b0, 0x470, 0x42c, 0x3f0, 0x3be, 0x384, 0x358, 0x320, 0x2f6, 0x2ca, 0x2a2, 0x27c, 0x258, 0x238, 0x216, 0x1f8, 0x1df, 0x1c2, 0x1ac, 0x190, 0x17b, 0x165, 0x151, 0x13e, 0x12c, 0x11c, 0x10b, 0xfc, 0xef, 0xe1, 0xd6, 0xc8, 0xbd, 0xb2, 0xa8, 0x9f, 0x96, 0x8e, 0x85, 0x7e, 0x77, 0x70, 0x6b, 0x64, 0x5e, 0x59, 0x54, 0x4f, 0x4b, 0x47, 0x42, 0x3f, 0x3b, 0x38, 0x35, 0x32, 0x2f, 0x2c, 0x2a, 0x27, 0x25, 0x23, 0x21, 0x1f, 0x1d, 0x1c, 0x1a, 0x19, 0x17, 0x16, 0x15, 0x13, 0x12, 0x11, 0x10, 0xf ];

align(1) struct STP_File
{
align(1):
    ubyte STP_Delay;
    ubyte STP_PositionsPointer0, STP_PositionsPointer1;
    ubyte STP_PatternsPointer0, STP_PatternsPointer1;
    ubyte STP_OrnamentsPointer0, STP_OrnamentsPointer1;
    ubyte STP_SamplesPointer0, STP_SamplesPointer1;
    ubyte STP_Init_Id;
}

align(1) struct STP_Channel_Parameters
{
align(1):
    ushort OrnamentPointer, SamplePointer, Address_In_Pattern, Ton;
    ubyte Position_In_Ornament, Loop_Ornament_Position, Ornament_Length, Position_In_Sample, Loop_Sample_Position, Sample_Length, Volume, Number_Of_Notes_To_Skip, Note, Amplitude;
    short Current_Ton_Sliding;
    bool Envelope_Enabled, Enabled;
    byte Glissade, Note_Skip_Counter;
}

align(1) struct STP_Parameters
{
align(1):
    ubyte DelayCounter, CurrentPosition, Transposition;
}

align(1) struct STP_SongInfo
{
align(1):
    STP_Parameters STP;
    STP_Channel_Parameters STP_A, STP_B, STP_C;
    ubyte[] data;
    const(ubyte)* mod () { return data.ptr; }
}

/*
#define STP_A (((STP_SongInfo *)info.data).STP_A)
#define STP_B (((STP_SongInfo *)info.data).STP_B)
#define STP_C (((STP_SongInfo *)info.data).STP_C)
#define STP (((STP_SongInfo *)info.data).STP)

*/
enum KsaId = "KSA SOFTWARE COMPILATION OF ";

const(STP_File)* Header (ref STP_SongInfo info) { return cast(const(STP_File)*)info.mod; }

ushort STP_PositionsPointer (ref STP_SongInfo info) { return cast(ushort)(info.Header.STP_PositionsPointer0 | (info.Header.STP_PositionsPointer1 << 8)); }
ushort STP_OrnamentsPointer (ref STP_SongInfo info) { return cast(ushort)(info.Header.STP_OrnamentsPointer0 | (info.Header.STP_OrnamentsPointer1 << 8)); }
ushort STP_PatternsPointer (ref STP_SongInfo info) { return cast(ushort)(info.Header.STP_PatternsPointer0 | (info.Header.STP_PatternsPointer1 << 8)); }
ushort STP_SamplesPointer (ref STP_SongInfo info) { return cast(ushort)(info.Header.STP_SamplesPointer0 | (info.Header.STP_SamplesPointer1 << 8)); }
ubyte STP_Delay (ref STP_SongInfo info) { return info.Header.STP_Delay; }

void STP_Init(ref STP_SongInfo info)
{
    const(ubyte)* mod = info.mod;
    auto header = cast(const(STP_File)*)mod;
    //memset(&STP_A, 0, sizeof(STP_Channel_Parameters));
    //memset(&STP_B, 0, sizeof(STP_Channel_Parameters));
    //memset(&STP_C, 0, sizeof(STP_Channel_Parameters));
    info.STP_A = info.STP_A.init;
    info.STP_B = info.STP_B.init;
    info.STP_C = info.STP_C.init;

    info.STP.DelayCounter = 1;
    info.STP.Transposition = mod[info.STP_PositionsPointer + 3];
    info.STP.CurrentPosition = 0;
    info.STP_A.Address_In_Pattern = ay_sys_getword(&mod[info.STP_PatternsPointer + mod[info.STP_PositionsPointer + 2]]);
    info.STP_B.Address_In_Pattern = ay_sys_getword(&mod[info.STP_PatternsPointer + mod[info.STP_PositionsPointer + 2] + 2]);
    info.STP_C.Address_In_Pattern = ay_sys_getword(&mod[info.STP_PatternsPointer + mod[info.STP_PositionsPointer + 2] + 4]);

    info.STP_A.SamplePointer = ay_sys_getword(&mod[info.STP_SamplesPointer]);
    info.STP_A.Loop_Sample_Position = mod[info.STP_A.SamplePointer];
    info.STP_A.SamplePointer++;
    info.STP_A.Sample_Length = mod[info.STP_A.SamplePointer];
    info.STP_A.SamplePointer++;
    info.STP_B.SamplePointer = info.STP_A.SamplePointer;
    info.STP_B.Loop_Sample_Position = info.STP_A.Loop_Sample_Position;
    info.STP_B.Sample_Length = info.STP_A.Sample_Length;
    info.STP_C.SamplePointer = info.STP_A.SamplePointer;
    info.STP_C.Loop_Sample_Position = info.STP_A.Loop_Sample_Position;
    info.STP_C.Sample_Length = info.STP_A.Sample_Length;

    info.STP_A.OrnamentPointer = ay_sys_getword(&mod[info.STP_OrnamentsPointer]);
    info.STP_A.Loop_Ornament_Position = mod[info.STP_A.OrnamentPointer];
    info.STP_A.OrnamentPointer++;
    info.STP_A.Ornament_Length = mod[info.STP_A.OrnamentPointer];
    info.STP_A.OrnamentPointer++;
    info.STP_B.OrnamentPointer = info.STP_A.OrnamentPointer;
    info.STP_B.Loop_Ornament_Position = info.STP_A.Loop_Ornament_Position;
    info.STP_B.Ornament_Length = info.STP_A.Ornament_Length;
    info.STP_C.OrnamentPointer = info.STP_A.OrnamentPointer;
    info.STP_C.Loop_Ornament_Position = info.STP_A.Loop_Ornament_Position;
    info.STP_C.Ornament_Length = info.STP_A.Ornament_Length;

    info.STP_A.Envelope_Enabled = false;
    info.STP_A.Glissade = 0;
    info.STP_A.Current_Ton_Sliding = 0;
    info.STP_A.Enabled = false;
    info.STP_A.Number_Of_Notes_To_Skip = 0;
    info.STP_A.Note_Skip_Counter = 0;
    info.STP_A.Volume = 0;
    info.STP_A.Ton = 0;

    info.STP_B.Envelope_Enabled = false;
    info.STP_B.Glissade = 0;
    info.STP_B.Current_Ton_Sliding = 0;
    info.STP_B.Enabled = false;
    info.STP_B.Number_Of_Notes_To_Skip = 0;
    info.STP_B.Note_Skip_Counter = 0;
    info.STP_B.Volume = 0;
    info.STP_B.Ton = 0;

    info.STP_C.Envelope_Enabled = false;
    info.STP_C.Glissade = 0;
    info.STP_C.Current_Ton_Sliding = 0;
    info.STP_C.Enabled = false;
    info.STP_C.Number_Of_Notes_To_Skip = 0;
    info.STP_C.Note_Skip_Counter = 0;
    info.STP_C.Volume = 0;
    info.STP_C.Ton = 0;
}

uint STP_GetTime (ref STP_SongInfo info, out uint mLoop)
{
    mLoop = uint.max;
    const(ubyte)* mod = info.mod;
    uint tm = 0;
    ubyte a = 1;
    uint i, j1;
    ubyte stDelay = mod[0];
    ushort stPosPt = ay_sys_getword(&mod[1]);
    uint stPatPt = ay_sys_getword(&mod[3]);

    for(i = 0; i < mod[stPosPt]; i++)
    {
        if(i == mod[stPosPt + 1])
            mLoop = tm * stDelay;
        j1 = ay_sys_getword(&mod[stPatPt + mod[stPosPt + 2 + i * 2]]);
        while(mod[j1] != 0)
        {
            ubyte val = mod[j1];
            if((val >= 1 && val <= 0x60) || (val >= 0xd0 && val <= 0xef))
            {
                tm += a;
            }
            else if(val >= 0x80 && val <= 0xbf)
            {
                a = cast(ubyte)(val - 0x7f);
            }
            else if((val >= 0xc0 && val <= 0xcf) || val == 0xf0)
            {
                j1++;
            }
            j1++;
        }
    }
    tm *= stDelay;
    return tm;
}

const(char)[] STP_GetName (ref STP_SongInfo info) {
  import core.stdc.string : memcmp;
  const(ubyte)* mod = info.mod;
  if (memcmp(&mod[10], KsaId.ptr, 28) == 0) return ay_sys_getstr(&mod[38], 25);
  return null;
}

void STP_PatternInterpreter(ref STP_SongInfo info, ref STP_Channel_Parameters chan)
{
    const(ubyte)* mod = info.mod;
    auto header = cast(const(STP_File)*)mod;
    bool quit = false;

    do
    {
        ubyte val = mod[chan.Address_In_Pattern];
        if(val >= 1 && val <= 0x60)
        {
            chan.Note = cast(ubyte)(val - 1);
            chan.Position_In_Sample = 0;
            chan.Position_In_Ornament = 0;
            chan.Current_Ton_Sliding = 0;
            chan.Enabled = true;
            quit = true;
        }
        else if(val >= 0x61 && val <= 0x6f)
        {
            chan.SamplePointer = ay_sys_getword(&mod[info.STP_SamplesPointer + (val - 0x61) * 2]);
            chan.Loop_Sample_Position = mod[chan.SamplePointer];
            chan.SamplePointer++;
            chan.Sample_Length = mod[chan.SamplePointer];
            chan.SamplePointer++;
        }
        else if(val >= 0x70 && val <= 0x7f)
        {
            chan.OrnamentPointer = ay_sys_getword(&mod[info.STP_OrnamentsPointer + (val - 0x70) * 2]);
            chan.Loop_Ornament_Position = mod[chan.OrnamentPointer];
            chan.OrnamentPointer++;
            chan.Ornament_Length = mod[chan.OrnamentPointer];
            chan.OrnamentPointer++;
            chan.Envelope_Enabled = false;
            chan.Glissade = 0;
        }
        else if(val >= 0x80 && val <= 0xbf)
        {
            chan.Number_Of_Notes_To_Skip = cast(ubyte)(val - 0x80);
        }
        else if(val >= 0xc0 && val <= 0xcf)
        {
            if(val != 0xc0)
            {
                ay_writeay(AY_ENV_SHAPE, cast(ubyte)(val - 0xc0));
                chan.Address_In_Pattern++;
                ay_writeay(AY_ENV_FINE, mod[chan.Address_In_Pattern]);
            }
            chan.Envelope_Enabled = true;
            chan.Loop_Ornament_Position = 0;
            chan.Glissade = 0;
            chan.Ornament_Length = 1;
        }
        else if(val >= 0xd0 && val <= 0xdf)
        {
            chan.Enabled = false;
            quit = true;
        }
        else if(val >= 0xe0 && val <= 0xef)
        {
            quit = true;
        }
        else if(val == 0xf0)
        {
            chan.Address_In_Pattern++;
            chan.Glissade = mod[chan.Address_In_Pattern];
        }
        else if(val >= 0xf1 && val <= 0xff)
        {
            chan.Volume = cast(ubyte)(val - 0xf1);
        }
        chan.Address_In_Pattern++;
    }
    while(!quit);
    chan.Note_Skip_Counter = chan.Number_Of_Notes_To_Skip;
}

void STP_GetRegisters(ref STP_SongInfo info, ref STP_Channel_Parameters chan, ref ubyte TempMixer)
{
    const(ubyte)* mod = info.mod;
    //STP_File *header = (STP_File *)mod;
    ubyte j, b0, b1;
    if(chan.Enabled)
    {
        chan.Current_Ton_Sliding += chan.Glissade;
        if(chan.Envelope_Enabled)
            j = cast(ubyte)(chan.Note + info.STP.Transposition);
        else
            j = cast(ubyte)(chan.Note + info.STP.Transposition + mod[chan.OrnamentPointer + chan.Position_In_Ornament]);
        if(j > 95)
            j = 95;
        b0 = mod[chan.SamplePointer + chan.Position_In_Sample * 4];
        b1 = mod[chan.SamplePointer + chan.Position_In_Sample * 4 + 1];
        chan.Ton = (ST_Table[j] + chan.Current_Ton_Sliding + ay_sys_getword(&mod[chan.SamplePointer + chan.Position_In_Sample * 4 + 2])) & 0xfff;
        chan.Amplitude = cast(ubyte)((b0 & 15) - chan.Volume);
        if(cast(byte)(chan.Amplitude) < 0)
            chan.Amplitude = 0;
        if(((b1 & 1) != 0) && chan.Envelope_Enabled)
            chan.Amplitude = chan.Amplitude | 16;
        TempMixer = ((b0 >> 1) & 0x48) | TempMixer;
        if(cast(byte)(b0) >= 0)
            ay_writeay(AY_NOISE_PERIOD, (b1 >> 1) & 31);
        chan.Position_In_Ornament++;
        if(chan.Position_In_Ornament >= chan.Ornament_Length)
            chan.Position_In_Ornament = chan.Loop_Ornament_Position;
        chan.Position_In_Sample++;
        if(chan.Position_In_Sample >= chan.Sample_Length)
        {
            chan.Position_In_Sample = chan.Loop_Sample_Position;
            if(cast(byte)(chan.Loop_Sample_Position) < 0)
                chan.Enabled = false;
        }
    }
    else
    {
        TempMixer = TempMixer | 0x48;
        chan.Amplitude = 0;
    }
    TempMixer = TempMixer >> 1;
}

void STP_Play(ref STP_SongInfo info)
{
    const(ubyte)* mod = info.mod;
    auto header = cast(const(STP_File)*)mod;
    ubyte TempMixer;
    info.STP.DelayCounter--;
    if(info.STP.DelayCounter == 0)
    {
        info.STP.DelayCounter = info.STP_Delay;
        info.STP_A.Note_Skip_Counter--;
        if(info.STP_A.Note_Skip_Counter < 0)
        {
            if(mod[info.STP_A.Address_In_Pattern] == 0)
            {
                info.STP.CurrentPosition++;
                if(info.STP.CurrentPosition == mod[info.STP_PositionsPointer])
                    info.STP.CurrentPosition = mod[info.STP_PositionsPointer + 1];
                info.STP_A.Address_In_Pattern = ay_sys_getword(&mod[info.STP_PatternsPointer + mod[info.STP_PositionsPointer + 2 + info.STP.CurrentPosition * 2]]);
                info.STP_B.Address_In_Pattern = ay_sys_getword(&mod[info.STP_PatternsPointer + mod[info.STP_PositionsPointer + 2 + info.STP.CurrentPosition * 2] + 2]);
                info.STP_C.Address_In_Pattern = ay_sys_getword(&mod[info.STP_PatternsPointer + mod[info.STP_PositionsPointer + 2 + info.STP.CurrentPosition * 2] + 4]);
                info.STP.Transposition = mod[info.STP_PositionsPointer + 3 + info.STP.CurrentPosition * 2];
            }
            STP_PatternInterpreter(info, info.STP_A);
        }
        info.STP_B.Note_Skip_Counter--;
        if(info.STP_B.Note_Skip_Counter < 0)
            STP_PatternInterpreter(info, info.STP_B);
        info.STP_C.Note_Skip_Counter--;
        if(info.STP_C.Note_Skip_Counter < 0)
            STP_PatternInterpreter(info, info.STP_C);
    }

    TempMixer = 0;
    STP_GetRegisters(info, info.STP_A, TempMixer);
    STP_GetRegisters(info, info.STP_B, TempMixer);
    STP_GetRegisters(info, info.STP_C, TempMixer);

    ay_writeay(AY_MIXER, TempMixer);

    ay_writeay(AY_CHNL_A_FINE, info.STP_A.Ton & 0xff);
    ay_writeay(AY_CHNL_A_COARSE, (info.STP_A.Ton >> 8) & 0xf);
    ay_writeay(AY_CHNL_B_FINE, info.STP_B.Ton & 0xff);
    ay_writeay(AY_CHNL_B_COARSE, (info.STP_B.Ton >> 8) & 0xf);
    ay_writeay(AY_CHNL_C_FINE, info.STP_C.Ton & 0xff);
    ay_writeay(AY_CHNL_C_COARSE, (info.STP_C.Ton >> 8) & 0xf);
    ay_writeay(AY_CHNL_A_VOL, info.STP_A.Amplitude);
    ay_writeay(AY_CHNL_B_VOL, info.STP_B.Amplitude);
    ay_writeay(AY_CHNL_C_VOL, info.STP_C.Amplitude);
}

bool STP_Detect(ubyte[] moda) {
  auto info = new STP_SongInfo();
  scope(exit) delete info;
  info.data = moda;
  auto length = moda.length;
  ubyte* mod = moda.ptr;

    auto header = cast(STP_File*)mod;
    int j, j1, j2, j3;
    if(length < 10)
        return false;
    if((*info).STP_PositionsPointer> length)
        return false;
    if((*info).STP_PatternsPointer> length)
        return false;
    if((*info).STP_OrnamentsPointer> length)
        return false;
    if((*info).STP_SamplesPointer> length)
        return false;
    if(((*info).STP_SamplesPointer- (*info).STP_OrnamentsPointer) != 0x20)
    return false;
    if(cast(int)((*info).STP_OrnamentsPointer - (*info).STP_PatternsPointer) <= 0)
    return false;
    if((((*info).STP_OrnamentsPointer - (*info).STP_PatternsPointer) % 6) != 0)
    return false;
    if((mod[(*info).STP_PositionsPointer] * 2 + 2 + (*info).STP_PositionsPointer - (*info).STP_PatternsPointer) != 0)
    return false;
    uint F_Length = (*info).STP_SamplesPointer + 30;
    if(F_Length> 65535)
    return false;
    if(F_Length> length + 1)
    return false;

    j2 = 0;
    j3 = header.STP_Init_Id;
    if(j3 == 0)
    {
        j2 = ay_sys_getword(&mod[(*info).STP_PatternsPointer]);
        {
          /*
          if(!strncmp((char *)&mod [10], KsaId, strlen(KsaId)))
          j2 -= 0xa + 53;
          else
          j2 -= 0xa;
          */
          bool found = true;
          foreach (immutable idx, char ch; KsaId) {
            if (mod[10+idx] != cast(ubyte)ch) { found = false; break; }
          }
          if (found) j2 -= 0xa + 53; else j2 -= 0xa;
        }

        if (j2 < 0) return false;
        //int F_Address = j2;
        j3 = (F_Length - (*info).STP_PatternsPointer) / 2;
        for(j1 = 0; j1 < j3; j1++)
        {
            j = ay_sys_getword(&mod[(*info).STP_PatternsPointer + j1 * 2]);
            j -= j2;
            ay_sys_writeword(&mod[(*info).STP_PatternsPointer + j1 * 2], cast(ushort)j);
        }
    }

    j = ay_sys_getword(&mod[(*info).STP_OrnamentsPointer]);
    j--;
    if(cast(uint)(j) <= cast(uint)(length - 1))
    {
        j = ay_sys_getword(&mod [j]);
        if(j == 0)
        {
            header.STP_Init_Id = cast(ubyte)j3;
            return true;
        }
    }

    /*for(j1 = 0; j1 < j3; j1++)
    {
        j = ay_sys_getword(&mod[STP_PatternsPointer + j1 * 2]);
        j += j2;
        ay_sys_writeword(&mod[STP_PatternsPointer + j1 * 2], j);
    }*/
    return false;
}


shared static this () {
  zxModuleRegister!".stp"((ubyte[] data) {
    if (STP_Detect(data)) return [cast(ZXModule)(new ZXModuleSTP(data))];
    return null;
  });
}


final class ZXModuleSTP : ZXModule {
private:
  STP_SongInfo si;
  uint mIntrs, mLoop;

public:
  this (ubyte[] adata) {
    si.data = adata;
    STP_Init(si);
    mIntrs = STP_GetTime(si, mLoop);
  }

  override string typeStr () { return "STP"; }
  override const(char)[] name () { return STP_GetName(si); }
  override const(char)[] author () { return null; }
  override const(char)[] misc () { return null; }
  override uint intrCount () { return mIntrs; }
  override uint fadeCount () { return 0; }
  override uint loopPos () { return mLoop; }
  override bool doIntr () { STP_Play(si); return true; }
  override void reset () { super.reset(); STP_Init(si); }
}
