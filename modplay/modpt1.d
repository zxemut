/* ProTracker 1 module decoder
 * Copyright (C) 2007 by Deryabin Andrew ( andrewderyabin@gmail.com )
 * Copyright (C) 2017 Ketmar // Invisible Vector
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
module modplay.modpt1 is aliced;
private:

import modplay.mod_low;

immutable ushort[96] PT2_Table =
[ 0xef8, 0xe10, 0xd60, 0xc80, 0xbd8, 0xb28, 0xa88, 0x9f0, 0x960, 0x8e0, 0x858, 0x7e0, 0x77c, 0x708, 0x6b0, 0x640, 0x5ec, 0x594, 0x544, 0x4f8, 0x4b0, 0x470, 0x42c, 0x3fd, 0x3be, 0x384, 0x358, 0x320, 0x2f6, 0x2ca, 0x2a2, 0x27c, 0x258, 0x238, 0x216, 0x1f8, 0x1df, 0x1c2, 0x1ac, 0x190, 0x17b, 0x165, 0x151, 0x13e, 0x12c, 0x11c, 0x10a, 0xfc, 0xef, 0xe1, 0xd6, 0xc8, 0xbd, 0xb2, 0xa8, 0x9f, 0x96, 0x8e, 0x85, 0x7e, 0x77, 0x70, 0x6b, 0x64, 0x5e, 0x59, 0x54, 0x4f, 0x4b, 0x47, 0x42, 0x3f, 0x3b, 0x38, 0x35, 0x32, 0x2f, 0x2c, 0x2a, 0x27, 0x25, 0x23, 0x21, 0x1f, 0x1d, 0x1c, 0x1a, 0x19, 0x17, 0x16, 0x15, 0x13, 0x12, 0x11, 0x10, 0xf ];

align(1) struct PT1_File {
align(1):
    ubyte PT1_Delay;
    ubyte PT1_NumberOfPositions;
    ubyte PT1_LoopPosition;
    ubyte[32] PT1_SamplesPointers0; //word
    ubyte[32] PT1_OrnamentsPointers0; //word
    ubyte PT1_PatternsPointer0, PT1_PatternsPointer1;
    byte[30] PT1_MusicName;
    ubyte[65536 - 99] PT1_PositionList;
}

const(PT1_File)* PT1_Header (ref PT1_SongInfo info) { return cast(const(PT1_File)*)info.mod; }

ushort PT1_SamplesPointers (ref PT1_SongInfo info, uint x) { return cast(ushort)(info.PT1_Header.PT1_SamplesPointers0 [(x) * 2] | (info.PT1_Header.PT1_SamplesPointers0 [(x) * 2 + 1] << 8)); }
ushort PT1_OrnamentsPointers (ref PT1_SongInfo info, uint x) { return cast(ushort)(info.PT1_Header.PT1_OrnamentsPointers0 [(x) * 2] | (info.PT1_Header.PT1_OrnamentsPointers0 [(x) * 2 + 1] << 8)); }
ushort PT1_PatternsPointer(ref PT1_SongInfo info) { return cast(ushort)(info.PT1_Header.PT1_PatternsPointer0 | (info.PT1_Header.PT1_PatternsPointer1 << 8)); }

align(1) struct PT1_Channel_Parameters {
align(1):
    ushort Address_In_Pattern, OrnamentPointer, SamplePointer, Ton;
    ubyte Number_Of_Notes_To_Skip, Volume, Loop_Sample_Position, Position_In_Sample, Sample_Length, Amplitude, Note;
    byte Note_Skip_Counter;
    bool Envelope_Enabled, Enabled;
}

align(1) struct PT1_Parameters {
align(1):
    ubyte Delay, DelayCounter, CurrentPosition;
}

align(1) struct PT1_SongInfo {
align(1):
    PT1_Parameters PT1;
    PT1_Channel_Parameters PT1_A, PT1_B, PT1_C;
    ubyte[] data;
    const(ubyte)* mod () { return data.ptr; }
};

//#define PT1_A ((PT1_SongInfo *)info.data).PT1_A
//#define PT1_B ((PT1_SongInfo *)info.data).PT1_B
//#define PT1_C ((PT1_SongInfo *)info.data).PT1_C
//#define PT1 ((PT1_SongInfo *)info.data).PT1

void PT1_Init(ref PT1_SongInfo info)
{
    const(ubyte)* mod = info.mod;
    auto header = cast(const(PT1_File)*)mod;

    ref PT1_Channel_Parameters PT1_A () { return info.PT1_A; }
    ref PT1_Channel_Parameters PT1_B () { return info.PT1_B; }
    ref PT1_Channel_Parameters PT1_C () { return info.PT1_C; }
    ref PT1_Parameters PT1 () { return info.PT1; }

    //memset(&PT1_A, 0, sizeof(PT1_Channel_Parameters));
    //memset(&PT1_B, 0, sizeof(PT1_Channel_Parameters));
    //memset(&PT1_C, 0, sizeof(PT1_Channel_Parameters));
    PT1_A = PT1_A.init;
    PT1_B = PT1_B.init;
    PT1_C = PT1_C.init;

    PT1.DelayCounter = 1;
    PT1.Delay = header.PT1_Delay;
    PT1.CurrentPosition = 0;
    PT1_A.Address_In_Pattern = ay_sys_getword(&mod[info.PT1_PatternsPointer + header.PT1_PositionList[0] * 6]);
    PT1_B.Address_In_Pattern = ay_sys_getword(&mod[info.PT1_PatternsPointer + header.PT1_PositionList[0] * 6 + 2]);
    PT1_C.Address_In_Pattern = ay_sys_getword(&mod[info.PT1_PatternsPointer + header.PT1_PositionList[0] * 6 + 4]);

    PT1_A.OrnamentPointer = info.PT1_OrnamentsPointers(0);
    PT1_A.Envelope_Enabled = false;
    PT1_A.Position_In_Sample = 0;
    PT1_A.Enabled = false;
    PT1_A.Number_Of_Notes_To_Skip = 0;
    PT1_A.Note_Skip_Counter = 0;
    PT1_A.Volume = 15;
    PT1_A.Ton = 0;

    PT1_B.OrnamentPointer = PT1_A.OrnamentPointer;
    PT1_B.Envelope_Enabled = false;
    PT1_B.Position_In_Sample = 0;
    PT1_B.Enabled = false;
    PT1_B.Number_Of_Notes_To_Skip = 0;
    PT1_B.Note_Skip_Counter = 0;
    PT1_B.Volume = 15;
    PT1_B.Ton = 0;

    PT1_C.OrnamentPointer = PT1_A.OrnamentPointer;
    PT1_C.Envelope_Enabled = false;
    PT1_C.Position_In_Sample = 0;
    PT1_C.Enabled = false;
    PT1_C.Number_Of_Notes_To_Skip = 0;
    PT1_C.Note_Skip_Counter = 0;
    PT1_C.Volume = 15;
    PT1_C.Ton = 0;
}

void PT1_PatternInterpreter(ref PT1_SongInfo info, ref PT1_Channel_Parameters chan)
{
    const(ubyte)* mod = info.mod;
    auto header = cast(const(PT1_File)*)mod;
    bool quit = false;
    do
    {
        ubyte val = mod[chan.Address_In_Pattern];
        if(val <= 0x5f)
        {
            chan.Note = val;
            chan.Enabled = true;
            chan.Position_In_Sample = 0;
            quit = true;
        }
        else if(val >= 0x60 && val <= 0x6f)
        {
            chan.SamplePointer = info.PT1_SamplesPointers(val - 0x60);
            chan.Sample_Length = mod[chan.SamplePointer];
            chan.SamplePointer++;
            chan.Loop_Sample_Position = mod[chan.SamplePointer];
            chan.SamplePointer++;
        }
        else if(val >= 0x70 && val <= 0x7f)
            chan.OrnamentPointer = info.PT1_OrnamentsPointers(val - 0x70);
        else if(val == 0x80)
        {
            chan.Enabled = false;
            quit = true;
        }
        else if(val == 0x81)
            chan.Envelope_Enabled = false;
        else if(val >= 0x82 && val <= 0x8f)
        {
            chan.Envelope_Enabled = true;
            ay_writeay(AY_ENV_SHAPE, cast(ubyte)(val - 0x81));
            chan.Address_In_Pattern++;
            ay_writeay(AY_ENV_FINE, mod[chan.Address_In_Pattern]);
            chan.Address_In_Pattern++;
            ay_writeay(AY_ENV_COARSE, mod[chan.Address_In_Pattern]);
        }
        else if(val == 0x90)
            quit = true;
        else if(val >= 0x91 && val <= 0xa0)
            info.PT1.Delay = cast(ubyte)(val - 0x91);
        else if(val >= 0xa1 && val <= 0xb0)
            chan.Volume = cast(ubyte)(val - 0xa1);
        else
            chan.Number_Of_Notes_To_Skip = cast(ubyte)(val - 0xb1);
        chan.Address_In_Pattern++;
    }
    while(!quit);
    chan.Note_Skip_Counter = chan.Number_Of_Notes_To_Skip;
}

void PT1_GetRegisters(ref PT1_SongInfo info, ref PT1_Channel_Parameters chan, ref ubyte TempMixer)
{
    const(ubyte)* mod = info.mod;
    ubyte j, b;
    if(chan.Enabled)
    {
        j = cast(ubyte)(chan.Note + mod[chan.OrnamentPointer + chan.Position_In_Sample]);
        if(j > 95)
            j = 95;
        b = mod[chan.SamplePointer + chan.Position_In_Sample * 3];
        chan.Ton = (((cast(ushort)(b)) << 4) & 0xf00) + mod[chan.SamplePointer + chan.Position_In_Sample * 3 + 2];
        chan.Amplitude = cast(ubyte)((chan.Volume * 17 + (chan.Volume > 7 ? 1 : 0)) * (b & 15) / 256);
        b = mod[chan.SamplePointer + chan.Position_In_Sample * 3 + 1];
        if((b & 32) == 0)
            chan.Ton = -(chan.Ton);
        chan.Ton = (chan.Ton + PT2_Table[j] + (j == 46 ? 1 : 0)) & 0xfff;
        if(chan.Envelope_Enabled)
            chan.Amplitude |= 16;
        if(cast(byte)b < 0)
            TempMixer = TempMixer | 64;
        else
            ay_writeay(AY_NOISE_PERIOD, b & 31);
        if((b & 64) != 0)
            TempMixer = TempMixer | 8;
        chan.Position_In_Sample++;
        if(chan.Position_In_Sample == chan.Sample_Length)
            chan.Position_In_Sample = chan.Loop_Sample_Position;
    }
    else
        chan.Amplitude = 0;
    TempMixer = TempMixer >> 1;

}

void PT1_Play(ref PT1_SongInfo info)
{
    const(ubyte)* mod = info.mod;
    auto header = cast(const(PT1_File)*)mod;
    ubyte TempMixer;
    info.PT1.DelayCounter--;
    if(info.PT1.DelayCounter == 0)
    {
        info.PT1_A.Note_Skip_Counter--;
        if(info.PT1_A.Note_Skip_Counter < 0)
        {
            if(mod[info.PT1_A.Address_In_Pattern] == 255)
            {
                info.PT1.CurrentPosition++;
                if(info.PT1.CurrentPosition == header.PT1_NumberOfPositions)
                    info.PT1.CurrentPosition = header.PT1_LoopPosition;
                info.PT1_A.Address_In_Pattern = ay_sys_getword(&mod[info.PT1_PatternsPointer + header.PT1_PositionList[info.PT1.CurrentPosition] * 6]);
                info.PT1_B.Address_In_Pattern = ay_sys_getword(&mod[info.PT1_PatternsPointer + header.PT1_PositionList[info.PT1.CurrentPosition] * 6 + 2]);
                info.PT1_C.Address_In_Pattern = ay_sys_getword(&mod[info.PT1_PatternsPointer + header.PT1_PositionList[info.PT1.CurrentPosition] * 6 + 4]);
            }
            PT1_PatternInterpreter(info, info.PT1_A);
        }
        info.PT1_B.Note_Skip_Counter--;
        if(info.PT1_B.Note_Skip_Counter < 0)
            PT1_PatternInterpreter(info, info.PT1_B);
        info.PT1_C.Note_Skip_Counter--;
        if(info.PT1_C.Note_Skip_Counter < 0)
            PT1_PatternInterpreter(info, info.PT1_C);
        info.PT1.DelayCounter = info.PT1.Delay;
    }
    TempMixer = 0;
    PT1_GetRegisters(info, info.PT1_A, TempMixer);
    PT1_GetRegisters(info, info.PT1_B, TempMixer);
    PT1_GetRegisters(info, info.PT1_C, TempMixer);

    ay_writeay(AY_MIXER, TempMixer);

    ay_writeay(AY_CHNL_A_FINE, info.PT1_A.Ton & 0xff);
    ay_writeay(AY_CHNL_A_COARSE, (info.PT1_A.Ton >> 8) & 0xf);
    ay_writeay(AY_CHNL_B_FINE, info.PT1_B.Ton & 0xff);
    ay_writeay(AY_CHNL_B_COARSE, (info.PT1_B.Ton >> 8) & 0xf);
    ay_writeay(AY_CHNL_C_FINE, info.PT1_C.Ton & 0xff);
    ay_writeay(AY_CHNL_C_COARSE, (info.PT1_C.Ton >> 8) & 0xf);
    ay_writeay(AY_CHNL_A_VOL, info.PT1_A.Amplitude);
    ay_writeay(AY_CHNL_B_VOL, info.PT1_B.Amplitude);
    ay_writeay(AY_CHNL_C_VOL, info.PT1_C.Amplitude);
}

uint PT1_GetTime (ref PT1_SongInfo info, out uint mLoop)
{
    mLoop = uint.max;
    const(ubyte)* mod = info.mod;
    auto header = cast(const(PT1_File)*)mod;
    short a1, a2, a3, a11, a22, a33;
    uint j1, j2, j3;
    int i, tm = 0;
    ubyte b;
    b = header.PT1_Delay;
    a1 = a2 = a3 = a11 = a22 = a33 = 0;
    for(i = 0; i < header.PT1_NumberOfPositions; i++)
    {
        j1 = ay_sys_getword(&mod[info.PT1_PatternsPointer + header.PT1_PositionList[i] * 6]);
        j2 = ay_sys_getword(&mod[info.PT1_PatternsPointer + header.PT1_PositionList[i] * 6 + 2]);
        j3 = ay_sys_getword(&mod[info.PT1_PatternsPointer + header.PT1_PositionList[i] * 6 + 4]);
        while(true)
        {
            a1--;
            if(a1 < 0)
            {

                if(mod[j1] == 255)
                    break;
                while(true)
                {
                    ubyte val = mod[j1];
                    if(val == 0x80 || val == 0x90 || val <= 0x5f)
                    {
                        a1 = a11;
                        j1++;
                        break;
                    }
                    if(val >= 0x82 && val <= 0x8f)
                    {
                        j1 += 2;
                    }
                    if(val >= 0xb1 && val <= 0xfe)
                    {
                        a11 = val - 0xb1;
                    }
                    if(val >= 0x91 && val <= 0xa0)
                    {
                        b = cast(ubyte)(val - 0x91);
                    }
                    j1++;
                }
            }
            a2--;
            if(a2 < 0)
            {

                while(true)
                {
                    ubyte val = mod[j2];
                    if(val == 0x80 || val == 0x90 || val <= 0x5f)
                    {
                        a2 = a22;
                        j2++;
                        break;
                    }
                    if(val >= 0x82 && val <= 0x8f)
                    {
                        j2 += 2;
                    }
                    if(val >= 0xb1 && val <= 0xfe)
                    {
                        a22 = val - 0xb1;
                    }
                    if(val >= 0x91 && val <= 0xa0)
                    {
                        b = cast(ubyte)(val - 0x91);
                    }
                    j2++;
                }
            }
            a3--;
            if(a3 < 0)
            {

                while(true)
                {
                    ubyte val = mod[j3];
                    if(val == 0x80 || val == 0x90 || val <= 0x5f)
                    {
                        a3 = a33;
                        j3++;
                        break;
                    }
                    if(val >= 0x82 && val <= 0x8f)
                    {
                        j3 += 2;
                    }
                    if(val >= 0xb1 && val <= 0xfe)
                    {
                        a33 = val - 0xb1;
                    }
                    if(val >= 0x91 && val <= 0xa0)
                    {
                        b = cast(ubyte)(val - 0x91);
                    }
                    j3++;
                }
            }
            tm += b;
        }
    }
    return tm;
    //info.Length = tm;
    //ubyte *ptr = mod + 69;
    //info.Name = ay_sys_getstr(ptr, 30);
}

const(char)[] PT1_GetName (ref PT1_SongInfo si) {
  const(char)* mod = cast(const(char)*)si.mod;
  return ay_sys_getstr(mod+69, 30);
}


bool PT1_Detect (ubyte[] moda) {
  auto info = new PT1_SongInfo();
  scope(exit) delete info;
  info.data = moda;
  auto length = moda.length;

    uint j, j1, j2;
    ubyte* mod = moda.ptr;
    PT1_File *header = cast(PT1_File*)mod;
    if(length < 0x66)
        return false;
    if((*info).PT1_PatternsPointer >= length)
        return false;

    j = 0;
    j1 = 65535;
    for(j2 = 0; j2 <= 15; j2++)
    {
        if(j < (*info).PT1_SamplesPointers(j2))
            j = (*info).PT1_SamplesPointers(j2);
        if(((*info).PT1_OrnamentsPointers(j2) != 0) && (j1 > (*info).PT1_OrnamentsPointers(j2)))
            j1 = (*info).PT1_OrnamentsPointers(j2);
    }

    if(j1 < 0x67)
        return false;
    if(j < 0x67)
        return false;
    if(j > 65534)
        return false;
    if(j > length)
        return false;
    if((j + mod[j] * 3 + 2) != j1)
        return false;

    j = 0;
    for(j2 = 0; j2 <= 15; j2++)
        if(j < (*info).PT1_OrnamentsPointers(j2))
            j = (*info).PT1_OrnamentsPointers(j2);
    if(j < 0x67)
        return false;
    int F_Length = j + 64;
    if(F_Length > 65536)
        return false;
    if(cast(uint)F_Length > length + 1)
        return false;

    j = 0x63;
    while((j <= (*info).PT1_PatternsPointer) && (mod[j] != 255))
        j++;
    if(j + 1 != (*info).PT1_PatternsPointer)
        return false;

    header.PT1_NumberOfPositions = cast(ubyte)(j - 0x63);
    return true;
}


shared static this () {
  zxModuleRegister!".pt1"((ubyte[] data) {
    if (PT1_Detect(data)) return [cast(ZXModule)(new ZXModulePT1(data))];
    return null;
  });
}


final class ZXModulePT1 : ZXModule {
private:
  PT1_SongInfo si;
  uint mIntrs, mLoop;

public:
  this (ubyte[] adata) {
    si.data = adata;
    PT1_Init(si);
    mIntrs = PT1_GetTime(si, mLoop);
  }

  override string typeStr () { return "PT1"; }
  override const(char)[] name () { return PT1_GetName(si); }
  override const(char)[] author () { return null; }
  override const(char)[] misc () { return null; }
  override uint intrCount () { return mIntrs; }
  override uint fadeCount () { return 0; }
  override uint loopPos () { return mLoop; }
  override bool doIntr () { PT1_Play(si); return true; }
  override void reset () { super.reset(); PT1_Init(si); }
}
