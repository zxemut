/* ProTracker 3 module decoder
 * Copyright (C) 2007 by Deryabin Andrew ( andrewderyabin@gmail.com )
 * Copyright (C) 2017 Ketmar // Invisible Vector
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
module modplay.modpt3 is aliced;
private:

import modplay.mod_low;


//Pro tracker 3.x player was written by S.V. Bulba.

//Table #0 of Pro Tracker 3.3x - 3.4r
immutable ushort[$] PT3NoteTable_PT_33_34r = [
  0x0C21, 0x0B73, 0x0ACE, 0x0A33, 0x09A0, 0x0916, 0x0893, 0x0818, 0x07A4, 0x0736, 0x06CE,
  0x066D, 0x0610, 0x05B9, 0x0567, 0x0519, 0x04D0, 0x048B, 0x0449, 0x040C, 0x03D2, 0x039B,
  0x0367, 0x0336, 0x0308, 0x02DC, 0x02B3, 0x028C, 0x0268, 0x0245, 0x0224, 0x0206, 0x01E9,
  0x01CD, 0x01B3, 0x019B, 0x0184, 0x016E, 0x0159, 0x0146, 0x0134, 0x0122, 0x0112, 0x0103,
  0x00F4, 0x00E6, 0x00D9, 0x00CD, 0x00C2, 0x00B7, 0x00AC, 0x00A3, 0x009A, 0x0091, 0x0089,
  0x0081, 0x007A, 0x0073, 0x006C, 0x0066, 0x0061, 0x005B, 0x0056, 0x0051, 0x004D, 0x0048,
  0x0044, 0x0040, 0x003D, 0x0039, 0x0036, 0x0033, 0x0030, 0x002D, 0x002B, 0x0028, 0x0026,
  0x0024, 0x0022, 0x0020, 0x001E, 0x001C, 0x001B, 0x0019, 0x0018, 0x0016, 0x0015, 0x0014,
  0x0013, 0x0012, 0x0011, 0x0010, 0x000F, 0x000E, 0x000D, 0x000C ];

//{Table #0 of Pro Tracker 3.4x - 3.5x}
immutable ushort[$] PT3NoteTable_PT_34_35 =
[ 0x0C22, 0x0B73, 0x0ACF, 0x0A33, 0x09A1, 0x0917, 0x0894, 0x0819, 0x07A4, 0x0737, 0x06CF, 0x066D, 0x0611, 0x05BA, 0x0567, 0x051A, 0x04D0, 0x048B, 0x044A, 0x040C, 0x03D2, 0x039B, 0x0367, 0x0337, 0x0308, 0x02DD, 0x02B4, 0x028D, 0x0268, 0x0246, 0x0225, 0x0206, 0x01E9, 0x01CE, 0x01B4, 0x019B, 0x0184, 0x016E, 0x015A, 0x0146, 0x0134, 0x0123, 0x0112, 0x0103, 0x00F5, 0x00E7, 0x00DA, 0x00CE, 0x00C2, 0x00B7, 0x00AD, 0x00A3, 0x009A, 0x0091, 0x0089, 0x0082, 0x007A, 0x0073, 0x006D, 0x0067, 0x0061, 0x005C, 0x0056, 0x0052, 0x004D, 0x0049, 0x0045, 0x0041, 0x003D, 0x003A, 0x0036, 0x0033, 0x0031, 0x002E, 0x002B, 0x0029, 0x0027, 0x0024, 0x0022, 0x0020, 0x001F, 0x001D, 0x001B, 0x001A, 0x0018, 0x0017, 0x0016, 0x0014, 0x0013, 0x0012, 0x0011, 0x0010, 0x000F, 0x000E, 0x000D, 0x000C ];

//{Table #1 of Pro Tracker 3.3x - 3.5x)}
immutable ushort[$] PT3NoteTable_ST =
[ 0x0EF8, 0x0E10, 0x0D60, 0x0C80, 0x0BD8, 0x0B28, 0x0A88, 0x09F0, 0x0960, 0x08E0, 0x0858, 0x07E0, 0x077C, 0x0708, 0x06B0, 0x0640, 0x05EC, 0x0594, 0x0544, 0x04F8, 0x04B0, 0x0470, 0x042C, 0x03FD, 0x03BE, 0x0384, 0x0358, 0x0320, 0x02F6, 0x02CA, 0x02A2, 0x027C, 0x0258, 0x0238, 0x0216, 0x01F8, 0x01DF, 0x01C2, 0x01AC, 0x0190, 0x017B, 0x0165, 0x0151, 0x013E, 0x012C, 0x011C, 0x010A, 0x00FC, 0x00EF, 0x00E1, 0x00D6, 0x00C8, 0x00BD, 0x00B2, 0x00A8, 0x009F, 0x0096, 0x008E, 0x0085, 0x007E, 0x0077, 0x0070, 0x006B, 0x0064, 0x005E, 0x0059, 0x0054, 0x004F, 0x004B, 0x0047, 0x0042, 0x003F, 0x003B, 0x0038, 0x0035, 0x0032, 0x002F, 0x002C, 0x002A, 0x0027, 0x0025, 0x0023, 0x0021, 0x001F, 0x001D, 0x001C, 0x001A, 0x0019, 0x0017, 0x0016, 0x0015, 0x0013, 0x0012, 0x0011, 0x0010, 0x000F ];

//{Table #2 of Pro Tracker 3.4r}
immutable ushort[$] PT3NoteTable_ASM_34r =
[ 0x0D3E, 0x0C80, 0x0BCC, 0x0B22, 0x0A82, 0x09EC, 0x095C, 0x08D6, 0x0858, 0x07E0, 0x076E, 0x0704, 0x069F, 0x0640, 0x05E6, 0x0591, 0x0541, 0x04F6, 0x04AE, 0x046B, 0x042C, 0x03F0, 0x03B7, 0x0382, 0x034F, 0x0320, 0x02F3, 0x02C8, 0x02A1, 0x027B, 0x0257, 0x0236, 0x0216, 0x01F8, 0x01DC, 0x01C1, 0x01A8, 0x0190, 0x0179, 0x0164, 0x0150, 0x013D, 0x012C, 0x011B, 0x010B, 0x00FC, 0x00EE, 0x00E0, 0x00D4, 0x00C8, 0x00BD, 0x00B2, 0x00A8, 0x009F, 0x0096, 0x008D, 0x0085, 0x007E, 0x0077, 0x0070, 0x006A, 0x0064, 0x005E, 0x0059, 0x0054, 0x0050, 0x004B, 0x0047, 0x0043, 0x003F, 0x003C, 0x0038, 0x0035, 0x0032, 0x002F, 0x002D, 0x002A, 0x0028, 0x0026, 0x0024, 0x0022, 0x0020, 0x001E, 0x001D, 0x001B, 0x001A, 0x0019, 0x0018, 0x0015, 0x0014, 0x0013, 0x0012, 0x0011, 0x0010, 0x000F, 0x000E ];

//{Table #2 of Pro Tracker 3.4x - 3.5x}
immutable ushort[$] PT3NoteTable_ASM_34_35 =
[ 0x0D10, 0x0C55, 0x0BA4, 0x0AFC, 0x0A5F, 0x09CA, 0x093D, 0x08B8, 0x083B, 0x07C5, 0x0755, 0x06EC, 0x0688, 0x062A, 0x05D2, 0x057E, 0x052F, 0x04E5, 0x049E, 0x045C, 0x041D, 0x03E2, 0x03AB, 0x0376, 0x0344, 0x0315, 0x02E9, 0x02BF, 0x0298, 0x0272, 0x024F, 0x022E, 0x020F, 0x01F1, 0x01D5, 0x01BB, 0x01A2, 0x018B, 0x0174, 0x0160, 0x014C, 0x0139, 0x0128, 0x0117, 0x0107, 0x00F9, 0x00EB, 0x00DD, 0x00D1, 0x00C5, 0x00BA, 0x00B0, 0x00A6, 0x009D, 0x0094, 0x008C, 0x0084, 0x007C, 0x0075, 0x006F, 0x0069, 0x0063, 0x005D, 0x0058, 0x0053, 0x004E, 0x004A, 0x0046, 0x0042, 0x003E, 0x003B, 0x0037, 0x0034, 0x0031, 0x002F, 0x002C, 0x0029, 0x0027, 0x0025, 0x0023, 0x0021, 0x001F, 0x001D, 0x001C, 0x001A, 0x0019, 0x0017, 0x0016, 0x0015, 0x0014, 0x0012, 0x0011, 0x0010, 0x000F, 0x000E, 0x000D ];

//{Table #3 of Pro Tracker 3.4r}
immutable ushort[$] PT3NoteTable_REAL_34r =
[ 0x0CDA, 0x0C22, 0x0B73, 0x0ACF, 0x0A33, 0x09A1, 0x0917, 0x0894, 0x0819, 0x07A4, 0x0737, 0x06CF, 0x066D, 0x0611, 0x05BA, 0x0567, 0x051A, 0x04D0, 0x048B, 0x044A, 0x040C, 0x03D2, 0x039B, 0x0367, 0x0337, 0x0308, 0x02DD, 0x02B4, 0x028D, 0x0268, 0x0246, 0x0225, 0x0206, 0x01E9, 0x01CE, 0x01B4, 0x019B, 0x0184, 0x016E, 0x015A, 0x0146, 0x0134, 0x0123, 0x0113, 0x0103, 0x00F5, 0x00E7, 0x00DA, 0x00CE, 0x00C2, 0x00B7, 0x00AD, 0x00A3, 0x009A, 0x0091, 0x0089, 0x0082, 0x007A, 0x0073, 0x006D, 0x0067, 0x0061, 0x005C, 0x0056, 0x0052, 0x004D, 0x0049, 0x0045, 0x0041, 0x003D, 0x003A, 0x0036, 0x0033, 0x0031, 0x002E, 0x002B, 0x0029, 0x0027, 0x0024, 0x0022, 0x0020, 0x001F, 0x001D, 0x001B, 0x001A, 0x0018, 0x0017, 0x0016, 0x0014, 0x0013, 0x0012, 0x0011, 0x0010, 0x000F, 0x000E, 0x000D ];

//{Table #3 of Pro Tracker 3.4x - 3.5x}
immutable ushort[$] PT3NoteTable_REAL_34_35 =
[ 0x0CDA, 0x0C22, 0x0B73, 0x0ACF, 0x0A33, 0x09A1, 0x0917, 0x0894, 0x0819, 0x07A4, 0x0737, 0x06CF, 0x066D, 0x0611, 0x05BA, 0x0567, 0x051A, 0x04D0, 0x048B, 0x044A, 0x040C, 0x03D2, 0x039B, 0x0367, 0x0337, 0x0308, 0x02DD, 0x02B4, 0x028D, 0x0268, 0x0246, 0x0225, 0x0206, 0x01E9, 0x01CE, 0x01B4, 0x019B, 0x0184, 0x016E, 0x015A, 0x0146, 0x0134, 0x0123, 0x0112, 0x0103, 0x00F5, 0x00E7, 0x00DA, 0x00CE, 0x00C2, 0x00B7, 0x00AD, 0x00A3, 0x009A, 0x0091, 0x0089, 0x0082, 0x007A, 0x0073, 0x006D, 0x0067, 0x0061, 0x005C, 0x0056, 0x0052, 0x004D, 0x0049, 0x0045, 0x0041, 0x003D, 0x003A, 0x0036, 0x0033, 0x0031, 0x002E, 0x002B, 0x0029, 0x0027, 0x0024, 0x0022, 0x0020, 0x001F, 0x001D, 0x001B, 0x001A, 0x0018, 0x0017, 0x0016, 0x0014, 0x0013, 0x0012, 0x0011, 0x0010, 0x000F, 0x000E, 0x000D ];

//{Volume table of Pro Tracker 3.3x - 3.4x}
immutable ubyte[16][16] PT3VolumeTable_33_34 =
[
[ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 ],
[ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01 ],
[ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x01, 0x01, 0x02, 0x02, 0x02, 0x02, 0x02 ],
[ 0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x01, 0x02, 0x02, 0x02, 0x02, 0x03, 0x03, 0x03, 0x03 ],
[ 0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x02, 0x02, 0x02, 0x03, 0x03, 0x03, 0x04, 0x04, 0x04 ],
[ 0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x02, 0x02, 0x03, 0x03, 0x03, 0x04, 0x04, 0x04, 0x05, 0x05 ],
[ 0x00, 0x00, 0x00, 0x01, 0x01, 0x02, 0x02, 0x03, 0x03, 0x03, 0x04, 0x04, 0x05, 0x05, 0x06, 0x06 ],
[ 0x00, 0x00, 0x01, 0x01, 0x02, 0x02, 0x03, 0x03, 0x04, 0x04, 0x05, 0x05, 0x06, 0x06, 0x07, 0x07 ],
[ 0x00, 0x00, 0x01, 0x01, 0x02, 0x02, 0x03, 0x03, 0x04, 0x05, 0x05, 0x06, 0x06, 0x07, 0x07, 0x08 ],
[ 0x00, 0x00, 0x01, 0x01, 0x02, 0x03, 0x03, 0x04, 0x05, 0x05, 0x06, 0x06, 0x07, 0x08, 0x08, 0x09 ],
[ 0x00, 0x00, 0x01, 0x02, 0x02, 0x03, 0x04, 0x04, 0x05, 0x06, 0x06, 0x07, 0x08, 0x08, 0x09, 0x0A ],
[ 0x00, 0x00, 0x01, 0x02, 0x03, 0x03, 0x04, 0x05, 0x06, 0x06, 0x07, 0x08, 0x09, 0x09, 0x0A, 0x0B ],
[ 0x00, 0x00, 0x01, 0x02, 0x03, 0x04, 0x04, 0x05, 0x06, 0x07, 0x08, 0x08, 0x09, 0x0A, 0x0B, 0x0C ],
[ 0x00, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D ],
[ 0x00, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E ],
[ 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F ] ];

//{Volume table of Pro Tracker 3.5x}
immutable ubyte[16][16] PT3VolumeTable_35 =
[
[ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 ],
[ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01 ],
[ 0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x02, 0x02, 0x02, 0x02 ],
[ 0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x01, 0x01, 0x02, 0x02, 0x02, 0x02, 0x02, 0x03, 0x03, 0x03 ],
[ 0x00, 0x00, 0x01, 0x01, 0x01, 0x01, 0x02, 0x02, 0x02, 0x02, 0x03, 0x03, 0x03, 0x03, 0x04, 0x04 ],
[ 0x00, 0x00, 0x01, 0x01, 0x01, 0x02, 0x02, 0x02, 0x03, 0x03, 0x03, 0x04, 0x04, 0x04, 0x05, 0x05 ],
[ 0x00, 0x00, 0x01, 0x01, 0x02, 0x02, 0x02, 0x03, 0x03, 0x04, 0x04, 0x04, 0x05, 0x05, 0x06, 0x06 ],
[ 0x00, 0x00, 0x01, 0x01, 0x02, 0x02, 0x03, 0x03, 0x04, 0x04, 0x05, 0x05, 0x06, 0x06, 0x07, 0x07 ],
[ 0x00, 0x01, 0x01, 0x02, 0x02, 0x03, 0x03, 0x04, 0x04, 0x05, 0x05, 0x06, 0x06, 0x07, 0x07, 0x08 ],
[ 0x00, 0x01, 0x01, 0x02, 0x02, 0x03, 0x04, 0x04, 0x05, 0x05, 0x06, 0x07, 0x07, 0x08, 0x08, 0x09 ],
[ 0x00, 0x01, 0x01, 0x02, 0x03, 0x03, 0x04, 0x05, 0x05, 0x06, 0x07, 0x07, 0x08, 0x09, 0x09, 0x0A ],
[ 0x00, 0x01, 0x01, 0x02, 0x03, 0x04, 0x04, 0x05, 0x06, 0x07, 0x07, 0x08, 0x09, 0x0A, 0x0A, 0x0B ],
[ 0x00, 0x01, 0x02, 0x02, 0x03, 0x04, 0x05, 0x06, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0A, 0x0B, 0x0C ],
[ 0x00, 0x01, 0x02, 0x03, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0A, 0x0B, 0x0C, 0x0D ],
[ 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E ],
[ 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F ] ];

align(1) struct PT3_File {
align(1):
  char[0x63] PT3_MusicName;
  ubyte PT3_TonTableId;
  ubyte PT3_Delay;
  ubyte PT3_NumberOfPositions;
  ubyte PT3_LoopPosition;
  ubyte PT3_PatternsPointer0, PT3_PatternsPointer1;
  ubyte[64] PT3_SamplesPointers0;
  ubyte[32] PT3_OrnamentsPointers0;
  ubyte[65335] PT3_PositionList;
}

//#define PT3_PatternsPointer ((unsigned long)(header.PT3_PatternsPointer0 | (header.PT3_PatternsPointer1 << 8)))
//#define PT3_SamplesPointers(x) ((unsigned long)(header.PT3_SamplesPointers0 [(x) * 2] | (header.PT3_SamplesPointers0 [(x) * 2 + 1] << 8)))
//#define PT3_OrnamentsPointers(x) ((unsigned long)(header.PT3_OrnamentsPointers0 [(x) * 2] | (header.PT3_OrnamentsPointers0 [(x) * 2 + 1] << 8)))

align(1) struct PT3_Channel_Parameters {
align(1):
  ushort Address_In_Pattern, OrnamentPointer, SamplePointer, Ton;
  ubyte Loop_Ornament_Position, Ornament_Length, Position_In_Ornament, Loop_Sample_Position, Sample_Length, Position_In_Sample, Volume, Number_Of_Notes_To_Skip, Note, Slide_To_Note, Amplitude;
  bool Envelope_Enabled, Enabled, SimpleGliss;
  short Current_Amplitude_Sliding, Current_Noise_Sliding, Current_Envelope_Sliding, Ton_Slide_Count, Current_OnOff, OnOff_Delay, OffOn_Delay, Ton_Slide_Delay, Current_Ton_Sliding, Ton_Accumulator, Ton_Slide_Step, Ton_Delta;
  byte Note_Skip_Counter;
}

align(1) struct PT3_Parameters {
align(1):
  ubyte Env_Base_lo;
  ubyte Env_Base_hi;
  short Cur_Env_Slide, Env_Slide_Add;
  byte Cur_Env_Delay, Env_Delay;
  ubyte Noise_Base, Delay, AddToNoise, DelayCounter, CurrentPosition;
  int Version;
}

align(1) struct PT3_SongInfo {
align(1):
private:
  PT3_Parameters PT3;
  PT3_Channel_Parameters PT3_A, PT3_B, PT3_C;
public:
  const(ubyte)[] data;
}

//#define PT3_A ((PT3_SongInfo *)data).PT3_A
//#define PT3_B ((PT3_SongInfo *)data).PT3_B
//#define PT3_C ((PT3_SongInfo *)data).PT3_C
//#define PT3 ((PT3_SongInfo *)data).PT3

/*
#define GET_COMMON_VARS(x) \
    __attribute__((unused)) ubyte *module;\
    __attribute__((unused)) PT3_File *header;\
    void *data;\
    if(!info.is_ts || x == 0)\
    {\
        module = info.module;\
        header = (PT3_File *)info.module;\
        data = info.data;\
    }\
    else\
    {\
        module = info.module1;\
        header = (PT3_File *)info.module1;\
        data = info.data1;\
    }
*/

//bool PT3_Detect(ubyte *module, unsigned long length);

inout(ubyte)* PT3_FindSig (inout(ubyte)* buffer, int length) nothrow @trusted @nogc {
  static immutable ubyte[$] sig = [ 0x50, 0x72, 0x6f, 0x54, 0x72, 0x61, 0x63, 0x6b, 0x65, 0x72, 0x20, 0x33, 0x2e ];
  static immutable ubyte[$] sig1 = [ 0x56, 0x6f, 0x72, 0x74, 0x65, 0x78, 0x20, 0x54, 0x72, 0x61, 0x63, 0x6b, 0x65, 0x72, 0x20, 0x49, 0x49 ];
  if (length < sig.length || length < sig1.length) return null;
  const(ubyte)* ptr = buffer;
  int remaining = length-cast(int)sig.length;
  while (remaining > sig.length) {
    if (ptr[0..sig.length] == sig[] || ptr[0..sig1.length] == sig1[]) return cast(typeof(return))ptr;
    ++ptr;
    --remaining;
  }
  return null;
}


bool PT3_Init (ref PT3_SongInfo si) {
  if (si.data.length < 0x80 || si.data.length > int.max/8) return false;

  const(ubyte)* mod = si.data.ptr;
  PT3_File* header = cast(PT3_File*)mod;

  int ver = 6;

  if (header.PT3_MusicName[13] >= '0' && header.PT3_MusicName[13] <= '9') ver = header.PT3_MusicName[13]-0x30;

  /+
  const(ubyte)* ptr = PT3_FindSig(mod+0x63, cast(int)si.data.length-0x63);
  if (ptr !is null) {
    assert(0, "fuck off, turbosound!");
    /*
    info.is_ts = true;
    info.module1 = ptr;
    info.data1 = (void *)new PT3_SongInfo;
    if (!info.data1) {
        delete (PT3_SongInfo *)info.data;
        info.data = 0;
        return;
    }
    memset(info.data1, 0, sizeof(PT3_SongInfo));
    */
  }
  +/

  const(ubyte)* data = si.data.ptr;
  mod = si.data.ptr;

  uint PT3_PatternsPointer () { return cast(uint)(header.PT3_PatternsPointer0|(header.PT3_PatternsPointer1<<8)); }
  ushort PT3_SamplesPointers (ubyte x) { return cast(ushort)(header.PT3_SamplesPointers0[x*2]|(header.PT3_SamplesPointers0[x*2+1]<<8)); }
  ushort PT3_OrnamentsPointers (ubyte x) { return cast(ushort)(header.PT3_OrnamentsPointers0[x*2]|(header.PT3_OrnamentsPointers0[x*2+1]<<8)); }

  foreach (immutable y; 0..2) {
    uint i = header.PT3_PositionList[0];
    ubyte b = header.PT3_MusicName[0x62];
    if (b != 0x20) i = b*3-3-i;

    si.PT3_A = si.PT3_A.init;
    si.PT3_B = si.PT3_B.init;
    si.PT3_C = si.PT3_C.init;

    si.PT3.Version = ver;
    si.PT3.DelayCounter = 1;
    si.PT3.Delay = header.PT3_Delay;
    si.PT3_A.Address_In_Pattern = ay_sys_getword(&mod[PT3_PatternsPointer+i*2]);
    si.PT3_B.Address_In_Pattern = ay_sys_getword(&mod[PT3_PatternsPointer+i*2+2]);
    si.PT3_C.Address_In_Pattern = ay_sys_getword(&mod[PT3_PatternsPointer+i*2+4]);

    si.PT3_A.OrnamentPointer = PT3_OrnamentsPointers(0);
    si.PT3_A.Loop_Ornament_Position = mod[si.PT3_A.OrnamentPointer];
    ++si.PT3_A.OrnamentPointer;
    si.PT3_A.Ornament_Length = mod[si.PT3_A.OrnamentPointer];
    ++si.PT3_A.OrnamentPointer;
    si.PT3_A.SamplePointer = PT3_SamplesPointers(1);
    si.PT3_A.Loop_Sample_Position = mod[si.PT3_A.SamplePointer];
    ++si.PT3_A.SamplePointer;
    si.PT3_A.Sample_Length = mod[si.PT3_A.SamplePointer];
    ++si.PT3_A.SamplePointer;
    si.PT3_A.Volume = 15;
    si.PT3_A.Note_Skip_Counter = 1;

    si.PT3_B.OrnamentPointer = si.PT3_A.OrnamentPointer;
    si.PT3_B.Loop_Ornament_Position = si.PT3_A.Loop_Ornament_Position;
    si.PT3_B.Ornament_Length = si.PT3_A.Ornament_Length;
    si.PT3_B.SamplePointer = si.PT3_A.SamplePointer;
    si.PT3_B.Loop_Sample_Position = si.PT3_A.Loop_Sample_Position;
    si.PT3_B.Sample_Length = si.PT3_A.Sample_Length;
    si.PT3_B.Volume = 15;
    si.PT3_B.Note_Skip_Counter = 1;

    si.PT3_C.OrnamentPointer = si.PT3_A.OrnamentPointer;
    si.PT3_C.Loop_Ornament_Position = si.PT3_A.Loop_Ornament_Position;
    si.PT3_C.Ornament_Length = si.PT3_A.Ornament_Length;
    si.PT3_C.SamplePointer = si.PT3_A.SamplePointer;
    si.PT3_C.Loop_Sample_Position = si.PT3_A.Loop_Sample_Position;
    si.PT3_C.Sample_Length = si.PT3_A.Sample_Length;
    si.PT3_C.Volume = 15;
    si.PT3_C.Note_Skip_Counter = 1;

    /*
    if (!info.is_ts) break;
    data = info.data1;
    mod = info.module1;
    header = (PT3_File *)mod;
    */
    break;
  }

  //ay_resetay(&info, 0);
  //ay_resetay(&info, 1);

  return true;
}


void PT3_Reset (ref PT3_SongInfo si) {
  if (!PT3_Init(si)) assert(0, "PT3 internal error");
}


int PT3_GetNoteFreq (ref PT3_SongInfo si, ubyte j) {
  //GET_COMMON_VARS(chip_num);
  PT3_File* header = cast(PT3_File*)si.data.ptr;
  switch (header.PT3_TonTableId) {
    case 0: return (si.PT3.Version <= 3 ? PT3NoteTable_PT_33_34r[j] : PT3NoteTable_PT_34_35[j]);
    case 1: return PT3NoteTable_ST[j];
    case 2: return (si.PT3.Version <= 3 ? PT3NoteTable_ASM_34r[j] : PT3NoteTable_ASM_34_35[j]);
    default: return (si.PT3.Version <= 3 ? PT3NoteTable_REAL_34r[j] : PT3NoteTable_REAL_34_35[j]);
  }
}


void PT3_PatternIntterpreter (ref PT3_SongInfo si, ref PT3_Channel_Parameters chan) {
  //GET_COMMON_VARS(chip_num);
  const(ubyte)* mod = si.data.ptr;
  auto header = cast(const(PT3_File)*)mod;
  bool quit;
  ubyte flag9, flag8, flag5, flag4, flag3, flag2, flag1;
  ubyte counter;
  int prnote, prsliding;
  prnote = chan.Note;
  prsliding = chan.Current_Ton_Sliding;
  quit = false;
  counter = 0;
  flag9 = flag8 = flag5 = flag4 = flag3 = flag2 = flag1 = 0;

  uint PT3_PatternsPointer () { return cast(uint)(header.PT3_PatternsPointer0|(header.PT3_PatternsPointer1<<8)); }
  ushort PT3_SamplesPointers (ubyte x) { return cast(ushort)(header.PT3_SamplesPointers0[x*2]|(header.PT3_SamplesPointers0[x*2+1]<<8)); }
  ushort PT3_OrnamentsPointers (ubyte x) { return cast(ushort)(header.PT3_OrnamentsPointers0[x*2]|(header.PT3_OrnamentsPointers0[x*2+1]<<8)); }

  do {
    ubyte val = mod[chan.Address_In_Pattern];
    if (val >= 0xf0) {
      chan.OrnamentPointer = PT3_OrnamentsPointers(cast(ubyte)(val-0xf0));
      chan.Loop_Ornament_Position = mod[chan.OrnamentPointer];
      ++chan.OrnamentPointer;
      chan.Ornament_Length = mod[chan.OrnamentPointer];
      ++chan.OrnamentPointer;
      ++chan.Address_In_Pattern;
      chan.SamplePointer = PT3_SamplesPointers(mod[chan.Address_In_Pattern]/2);
      chan.Loop_Sample_Position = mod[chan.SamplePointer];
      ++chan.SamplePointer;
      chan.Sample_Length = mod[chan.SamplePointer];
      ++chan.SamplePointer;
      chan.Envelope_Enabled = false;
      chan.Position_In_Ornament = 0;
    } else if (val >= 0xd1 && val <= 0xef) {
      chan.SamplePointer = PT3_SamplesPointers(cast(ubyte)(val-0xd0));
      chan.Loop_Sample_Position = mod[chan.SamplePointer];
      ++chan.SamplePointer;
      chan.Sample_Length = mod[chan.SamplePointer];
      ++chan.SamplePointer;
    } else if (val == 0xd0) {
      quit = true;
    } else if (val >= 0xc1 && val <= 0xcf) {
      chan.Volume = cast(ubyte)(val-0xc0);
    } else if (val == 0xc0) {
      chan.Position_In_Sample = 0;
      chan.Current_Amplitude_Sliding = 0;
      chan.Current_Noise_Sliding = 0;
      chan.Current_Envelope_Sliding = 0;
      chan.Position_In_Ornament = 0;
      chan.Ton_Slide_Count = 0;
      chan.Current_Ton_Sliding = 0;
      chan.Ton_Accumulator = 0;
      chan.Current_OnOff = 0;
      chan.Enabled = false;
      quit = true;
    } else if(val >= 0xb2 && val <= 0xbf) {
      chan.Envelope_Enabled = true;
      ay_writeay(AY_ENV_SHAPE, cast(ubyte)(val-0xb1));
      ++chan.Address_In_Pattern;
      si.PT3.Env_Base_hi = mod[chan.Address_In_Pattern];
      ++chan.Address_In_Pattern;
      si.PT3.Env_Base_lo = mod[chan.Address_In_Pattern];
      chan.Position_In_Ornament = 0;
      si.PT3.Cur_Env_Slide = 0;
      si.PT3.Cur_Env_Delay = 0;
    } else if (val == 0xb1) {
      ++chan.Address_In_Pattern;
      chan.Number_Of_Notes_To_Skip = mod[chan.Address_In_Pattern];
    } else if (val == 0xb0) {
      chan.Envelope_Enabled = false;
      chan.Position_In_Ornament = 0;
    } else if (val >= 0x50 && val <= 0xaf) {
      chan.Note = cast(ubyte)(val-0x50);
      chan.Position_In_Sample = 0;
      chan.Current_Amplitude_Sliding = 0;
      chan.Current_Noise_Sliding = 0;
      chan.Current_Envelope_Sliding = 0;
      chan.Position_In_Ornament = 0;
      chan.Ton_Slide_Count = 0;
      chan.Current_Ton_Sliding = 0;
      chan.Ton_Accumulator = 0;
      chan.Current_OnOff = 0;
      chan.Enabled = true;
      quit = true;
    } else if (val >= 0x40 && val <= 0x4f) {
      chan.OrnamentPointer = PT3_OrnamentsPointers(cast(ubyte)(val-0x40));
      chan.Loop_Ornament_Position = mod[chan.OrnamentPointer];
      ++chan.OrnamentPointer;
      chan.Ornament_Length = mod[chan.OrnamentPointer];
      ++chan.OrnamentPointer;
      chan.Position_In_Ornament = 0;
    } else if (val >= 0x20 && val <= 0x3f) {
      si.PT3.Noise_Base = cast(ubyte)(val-0x20);
    } else if (val >= 0x10 && val <= 0x1f) {
      if (val == 0x10) {
        chan.Envelope_Enabled = false;
      } else {
        ay_writeay(AY_ENV_SHAPE, cast(ubyte)(val-0x10));
        ++chan.Address_In_Pattern;
        si.PT3.Env_Base_hi = mod[chan.Address_In_Pattern];
        ++chan.Address_In_Pattern;
        si.PT3.Env_Base_lo = mod[chan.Address_In_Pattern];
        chan.Envelope_Enabled = true;
        si.PT3.Cur_Env_Slide = 0;
        si.PT3.Cur_Env_Delay = 0;
      }
      ++chan.Address_In_Pattern;
      chan.SamplePointer = PT3_SamplesPointers(mod[chan.Address_In_Pattern]/2);
      chan.Loop_Sample_Position = mod[chan.SamplePointer];
      ++chan.SamplePointer;
      chan.Sample_Length = mod[chan.SamplePointer];
      ++chan.SamplePointer;
      chan.Position_In_Ornament = 0;
    } else if (val == 0x9) {
      ++counter;
      flag9 = counter;
    } else if (val == 0x8) {
      ++counter;
      flag8 = counter;
    } else if (val == 0x5) {
      ++counter;
      flag5 = counter;
    } else if (val == 0x4) {
      ++counter;
      flag4 = counter;
    } else if (val == 0x3) {
      ++counter;
      flag3 = counter;
    } else if (val == 0x2) {
      ++counter;
      flag2 = counter;
    } else if (val == 0x1) {
      ++counter;
      flag1 = counter;
    }
    ++chan.Address_In_Pattern;
  } while (!quit);

  while (counter > 0) {
    if (counter == flag1) {
      chan.Ton_Slide_Delay = mod[chan.Address_In_Pattern];
      chan.Ton_Slide_Count = chan.Ton_Slide_Delay;
      ++chan.Address_In_Pattern;
      chan.Ton_Slide_Step = ay_sys_getword(&mod[chan.Address_In_Pattern]);
      chan.Address_In_Pattern += 2;
      chan.SimpleGliss = true;
      chan.Current_OnOff = 0;
      if (chan.Ton_Slide_Count == 0 && si.PT3.Version >= 7) ++chan.Ton_Slide_Count;
    } else if (counter == flag2) {
      import std.math : abs;
      chan.SimpleGliss = false;
      chan.Current_OnOff = 0;
      chan.Ton_Slide_Delay = mod[chan.Address_In_Pattern];
      chan.Ton_Slide_Count = chan.Ton_Slide_Delay;
      chan.Address_In_Pattern += 3;
      chan.Ton_Slide_Step = abs(cast(short)ay_sys_getword(&mod[chan.Address_In_Pattern]));
      chan.Address_In_Pattern += 2;
      chan.Ton_Delta = cast(short)(PT3_GetNoteFreq(si, cast(ubyte)chan.Note)-PT3_GetNoteFreq(si, cast(ubyte)prnote));
      chan.Slide_To_Note = chan.Note;
      chan.Note = cast(ubyte)prnote;
      if (si.PT3.Version >= 6) chan.Current_Ton_Sliding = cast(short)prsliding;
      if (chan.Ton_Delta-chan.Current_Ton_Sliding < 0) chan.Ton_Slide_Step = -(chan.Ton_Slide_Step);
    } else if (counter == flag3) {
      chan.Position_In_Sample = mod[chan.Address_In_Pattern];
      ++chan.Address_In_Pattern;
    } else if (counter == flag4) {
      chan.Position_In_Ornament = mod[chan.Address_In_Pattern];
      ++chan.Address_In_Pattern;
    } else if (counter == flag5) {
      chan.OnOff_Delay = mod[chan.Address_In_Pattern];
      ++chan.Address_In_Pattern;
      chan.OffOn_Delay = mod[chan.Address_In_Pattern];
      chan.Current_OnOff = chan.OnOff_Delay;
      ++chan.Address_In_Pattern;
      chan.Ton_Slide_Count = 0;
      chan.Current_Ton_Sliding = 0;
    } else if (counter == flag8) {
      si.PT3.Env_Delay = mod[chan.Address_In_Pattern];
      si.PT3.Cur_Env_Delay = si.PT3.Env_Delay;
      ++chan.Address_In_Pattern;
      si.PT3.Env_Slide_Add = ay_sys_getword(&mod[chan.Address_In_Pattern]);
      chan.Address_In_Pattern += 2;
    } else if (counter == flag9) {
      si.PT3.Delay = mod[chan.Address_In_Pattern];
      ++chan.Address_In_Pattern;
    }
    --counter;
  }
  chan.Note_Skip_Counter = chan.Number_Of_Notes_To_Skip;
}


void PT3_ChangeRegisters (ref PT3_SongInfo si, ref PT3_Channel_Parameters chan, ref byte AddToEnv, ref ubyte TempMixer) {
  //GET_COMMON_VARS(chip_num);
  const(ubyte)* mod = si.data.ptr;
  auto header = cast(const(PT3_File)*)mod;
  ubyte j, b1, b0;
  ushort w;
  if (chan.Enabled) {
    chan.Ton = ay_sys_getword(&mod[chan.SamplePointer+chan.Position_In_Sample*4+2]);
    chan.Ton += chan.Ton_Accumulator;
    b0 = mod[chan.SamplePointer+chan.Position_In_Sample*4];
    b1 = mod[chan.SamplePointer+chan.Position_In_Sample*4+1];
    if ((b1&0x40) != 0) chan.Ton_Accumulator = chan.Ton;
    j = cast(ubyte)(chan.Note+mod[chan.OrnamentPointer+chan.Position_In_Ornament]);
    if (cast(byte)j < 0) j = 0; else if (j > 95) j = 95;
    w = cast(ushort)PT3_GetNoteFreq(si, j);
    chan.Ton = (chan.Ton+chan.Current_Ton_Sliding+w)&0xfff;
    if (chan.Ton_Slide_Count > 0) {
      --chan.Ton_Slide_Count;
      if (chan.Ton_Slide_Count == 0) {
        chan.Current_Ton_Sliding += chan.Ton_Slide_Step;
        chan.Ton_Slide_Count = chan.Ton_Slide_Delay;
        if (!chan.SimpleGliss) {
          if ((chan.Ton_Slide_Step < 0 && chan.Current_Ton_Sliding <= chan.Ton_Delta) || (chan.Ton_Slide_Step >= 0 && chan.Current_Ton_Sliding >= chan.Ton_Delta)) {
            chan.Note = chan.Slide_To_Note;
            chan.Ton_Slide_Count = 0;
            chan.Current_Ton_Sliding = 0;
          }
        }
      }
    }
    chan.Amplitude = b1 & 0xf;
    if ((b0&0x80) != 0) {
      if ((b0&0x40) != 0) {
        if (chan.Current_Amplitude_Sliding < 15) ++chan.Current_Amplitude_Sliding;
      } else if (chan.Current_Amplitude_Sliding > -15) --chan.Current_Amplitude_Sliding;
    }
    chan.Amplitude += chan.Current_Amplitude_Sliding;
    if (cast(byte)(chan.Amplitude) < 0) chan.Amplitude = 0; else if (chan.Amplitude > 15) chan.Amplitude = 15;
    if (si.PT3.Version <= 4) {
      chan.Amplitude = PT3VolumeTable_33_34[chan.Volume][chan.Amplitude];
    } else {
      chan.Amplitude = PT3VolumeTable_35[chan.Volume][chan.Amplitude];
    }
    if (((b0 & 1) == 0) && chan.Envelope_Enabled) chan.Amplitude = chan.Amplitude|16;
    if ((b1&0x80) != 0) {
      if ((b0 & 0x20) != 0) {
        j = cast(ubyte)(((b0>>1)|0xf0)+chan.Current_Envelope_Sliding);
      } else {
        j = cast(ubyte)(((b0>>1)&0xf)+chan.Current_Envelope_Sliding);
      }
      if ((b1&0x20) != 0) chan.Current_Envelope_Sliding = j;
      AddToEnv += j;
    } else {
      si.PT3.AddToNoise = cast(ubyte)((b0>>1)+chan.Current_Noise_Sliding);
      if ((b1&0x20) != 0) chan.Current_Noise_Sliding = si.PT3.AddToNoise;
    }
    TempMixer = ((b1>>1)&0x48)|TempMixer;
    ++chan.Position_In_Sample;
    if (chan.Position_In_Sample >= chan.Sample_Length) chan.Position_In_Sample = chan.Loop_Sample_Position;
    ++chan.Position_In_Ornament;
    if (chan.Position_In_Ornament >= chan.Ornament_Length) chan.Position_In_Ornament = chan.Loop_Ornament_Position;
  } else {
    chan.Amplitude = 0;
  }
  TempMixer >>= 1;
  if (chan.Current_OnOff > 0) {
    --chan.Current_OnOff;
    if (chan.Current_OnOff == 0) {
      chan.Enabled = !chan.Enabled;
      if (chan.Enabled) {
        chan.Current_OnOff = chan.OnOff_Delay;
      } else {
        chan.Current_OnOff = chan.OffOn_Delay;
      }
    }
  }
}


void PT3_Play_Chip (ref PT3_SongInfo si) {
  //GET_COMMON_VARS(chip_num);
  const(ubyte)* mod = si.data.ptr;
  auto header = cast(const(PT3_File)*)mod;
  ubyte TempMixer;
  byte AddToEnv;

  uint PT3_PatternsPointer () { return cast(uint)(header.PT3_PatternsPointer0|(header.PT3_PatternsPointer1<<8)); }
  ushort PT3_SamplesPointers (ubyte x) { return cast(ushort)(header.PT3_SamplesPointers0[x*2]|(header.PT3_SamplesPointers0[x*2+1]<<8)); }
  ushort PT3_OrnamentsPointers (ubyte x) { return cast(ushort)(header.PT3_OrnamentsPointers0[x*2]|(header.PT3_OrnamentsPointers0[x*2+1]<<8)); }

  si.PT3.DelayCounter--;
  if (si.PT3.DelayCounter == 0) {
    --si.PT3_A.Note_Skip_Counter;
    if (si.PT3_A.Note_Skip_Counter == 0) {
      if (mod[si.PT3_A.Address_In_Pattern] == 0) {
        ++si.PT3.CurrentPosition;
        if (si.PT3.CurrentPosition == header.PT3_NumberOfPositions) si.PT3.CurrentPosition = header.PT3_LoopPosition;
        si.PT3_A.Address_In_Pattern = ay_sys_getword(&mod[PT3_PatternsPointer+header.PT3_PositionList[si.PT3.CurrentPosition]*2]);
        si.PT3_B.Address_In_Pattern = ay_sys_getword(&mod[PT3_PatternsPointer+header.PT3_PositionList[si.PT3.CurrentPosition]*2+2]);
        si.PT3_C.Address_In_Pattern = ay_sys_getword(&mod[PT3_PatternsPointer+header.PT3_PositionList[si.PT3.CurrentPosition]*2+4]);
        si.PT3.Noise_Base = 0;
      }
      PT3_PatternIntterpreter(si, si.PT3_A);
    }
    --si.PT3_B.Note_Skip_Counter;
    if (si.PT3_B.Note_Skip_Counter == 0) PT3_PatternIntterpreter(si, si.PT3_B);
    --si.PT3_C.Note_Skip_Counter;
    if (si.PT3_C.Note_Skip_Counter == 0) PT3_PatternIntterpreter(si, si.PT3_C);
    si.PT3.DelayCounter = si.PT3.Delay;
  }

  AddToEnv = 0;
  TempMixer = 0;
  PT3_ChangeRegisters(si, si.PT3_A, AddToEnv, TempMixer);
  PT3_ChangeRegisters(si, si.PT3_B, AddToEnv, TempMixer);
  PT3_ChangeRegisters(si, si.PT3_C, AddToEnv, TempMixer);

  ay_writeay(AY_MIXER, TempMixer);

  ay_writeay(AY_CHNL_A_FINE, si.PT3_A.Ton & 0xff);
  ay_writeay(AY_CHNL_A_COARSE, (si.PT3_A.Ton >> 8) & 0xf);
  ay_writeay(AY_CHNL_B_FINE, si.PT3_B.Ton & 0xff);
  ay_writeay(AY_CHNL_B_COARSE, (si.PT3_B.Ton >> 8) & 0xf);
  ay_writeay(AY_CHNL_C_FINE, si.PT3_C.Ton & 0xff);
  ay_writeay(AY_CHNL_C_COARSE, (si.PT3_C.Ton >> 8) & 0xf);
  ay_writeay(AY_CHNL_A_VOL, si.PT3_A.Amplitude);
  ay_writeay(AY_CHNL_B_VOL, si.PT3_B.Amplitude);
  ay_writeay(AY_CHNL_C_VOL, si.PT3_C.Amplitude);

  ay_writeay(AY_NOISE_PERIOD, (si.PT3.Noise_Base + si.PT3.AddToNoise) & 31);
  ushort cur_env = cast(ushort)(ay_sys_getword(&si.PT3.Env_Base_lo)+AddToEnv+si.PT3.Cur_Env_Slide);
  ay_writeay(AY_ENV_FINE, cur_env & 0xff);
  ay_writeay(AY_ENV_COARSE, (cur_env >> 8) & 0xff);

  if (si.PT3.Cur_Env_Delay > 0) {
    si.PT3.Cur_Env_Delay--;
    if (si.PT3.Cur_Env_Delay == 0) {
      si.PT3.Cur_Env_Delay = si.PT3.Env_Delay;
      si.PT3.Cur_Env_Slide += si.PT3.Env_Slide_Add;
    }
  }
}


uint PT3_GetTime (const(ubyte)[] mod, out uint Loop) {
  byte a1, a2, a3, a11, a22, a33;
  uint j1, j2, j3;
  int c1, c2, c3, c4, c5, c8;
  int i, j, tm = 0;
  ubyte b;
  PT3_File *header = cast(PT3_File*)mod.ptr;
  Loop = uint.max;

  uint PT3_PatternsPointer () { return cast(uint)(header.PT3_PatternsPointer0|(header.PT3_PatternsPointer1<<8)); }

  ubyte ptDelay = header.PT3_Delay;
  ubyte ptNumPos = header.PT3_NumberOfPositions;
  ushort ptLoopPos = header.PT3_LoopPosition;
  ushort ptPatPt = cast(ushort)PT3_PatternsPointer;
  const(ubyte)* ptPosList = cast(const(ubyte)*)&header.PT3_PositionList[0];

  b = ptDelay;
  a11 = a22 = a33 = 1;
  for (i = 0; i < ptNumPos; ++i) {
    if (i == ptLoopPos) Loop = tm;
    j1 = ay_sys_getword(&mod[ptPatPt+ptPosList[i]*2]);
    j2 = ay_sys_getword(&mod[ptPatPt+ptPosList[i]*2+2]);
    j3 = ay_sys_getword(&mod[ptPatPt+ptPosList[i]*2+4]);
    a1 = a2 = a3 = 1;
    do {
      --a1;
      if (a1 == 0) {
        if (mod[j1] == 0) break;
        j = c1 = c2 = c3 = c4 = c5 = c8 = 0;
        for (;;) {
          ubyte val = mod[j1];
          if (val == 0xd0 || val == 0xc0 || (val >= 0x50 && val <= 0xaf)) {
            a1 = a11;
            ++j1;
            break;
          } else if (val == 0x10 || val >= 0xf0) {
            ++j1;
          } else if (val >= 0xb2 && val <= 0xbf) {
            j1 += 2;
          } else if (val == 0xb1) {
            ++j1;
            a11 = mod[j1];
          } else if (val >= 0x11 && val <= 0x1f) {
            j1 += 3;
          } else {
            switch (val) {
              case 1: ++j; c1 = j; break;
              case 2: ++j; c2 = j; break;
              case 3: ++j; c3 = j; break;
              case 4: ++j; c4 = j; break;
              case 5: ++j; c5 = j; break;
              case 8: ++j; c8 = j; break;
              case 9: ++j; break;
              default: break;
            }
          }
          ++j1;
        }

        while (j > 0) {
               if (j == c1 || j == c8) j1 += 3;
          else if (j == c2) j1 += 5;
          else if (j == c3 || j == c4) ++j1;
          else if (j == c5) j1 += 2;
          else { b = mod[j1]; ++j1; }
          --j;
        }
      }
      --a2;
      if (a2 == 0) {
        j = c1 = c2 = c3 = c4 = c5 = c8 = 0;
        do
        {
            ubyte val = mod[j2];
            if(val == 0xd0 || val == 0xc0 || (val >= 0x50 && val <= 0xaf))
            {
                a2 = a22;
                j2++;
                break;
            }
            else if(val == 0x10 || val >= 0xf0)
            {
                j2++;
            }
            else if(val >= 0xb2 && val <= 0xbf)
            {
                j2 += 2;
            }
            else if(val == 0xb1)
            {
                j2++;
                a22 = mod[j2];
            }
            else if(val >= 0x11 && val <= 0x1f)
            {
                j2 += 3;
            }
            else
            {
                switch(val)
                {
                    case 1:
                        j++;
                        c1 = j;
                        break;
                    case 2:
                        j++;
                        c2 = j;
                        break;
                    case 3:
                        j++;
                        c3 = j;
                        break;
                    case 4:
                        j++;
                        c4 = j;
                        break;
                    case 5:
                        j++;
                        c5 = j;
                        break;
                    case 8:
                        j++;
                        c8 = j;
                        break;
                    case 9:
                        j++;
                        break;
                    default:
                        break;
                }
            }
            j2++;
        }
        while(true);
        while(j > 0)
        {
            if(j == c1 || j == c8)
            {
                j2 += 3;
            }
            else if(j == c2)
            {
                j2 += 5;
            }
            else if(j == c3 || j == c4)
            {
                j2++;
            }
            else if(j == c5)
            {
                j2 += 2;
            }
            else
            {
                b = mod[j2];
                j2++;
            }
            j--;
        }
      }
      a3--;
      if(a3 == 0)
      {
          j = c1 = c2 = c3 = c4 = c5 = c8 = 0;
          do
          {
              ubyte val = mod[j3];
              if(val == 0xd0 || val == 0xc0 || (val >= 0x50 && val <= 0xaf))
              {
                  a3 = a33;
                  j3++;
                  break;
              }
              else if(val == 0x10 || val >= 0xf0)
              {
                  j3++;
              }
              else if(val >= 0xb2 && val <= 0xbf)
              {
                  j3 += 2;
              }
              else if(val == 0xb1)
              {
                  j3++;
                  a33 = mod[j3];
              }
              else if(val >= 0x11 && val <= 0x1f)
              {
                  j3 += 3;
              }
              else
              {
                  switch(val)
                  {
                      case 1:
                          j++;
                          c1 = j;
                          break;
                      case 2:
                          j++;
                          c2 = j;
                          break;
                      case 3:
                          j++;
                          c3 = j;
                          break;
                      case 4:
                          j++;
                          c4 = j;
                          break;
                      case 5:
                          j++;
                          c5 = j;
                          break;
                      case 8:
                          j++;
                          c8 = j;
                          break;
                      case 9:
                          j++;
                          break;
                      default:
                          break;
                  }
              }
              j3++;
          }
          while(true);
          while(j > 0)
          {
              if(j == c1 || j == c8)
              {
                  j3 += 3;
              }
              else if(j == c2)
              {
                  j3 += 5;
              }
              else if(j == c3 || j == c4)
              {
                  j3++;
              }
              else if(j == c5)
              {
                  j3 += 2;
              }
              else
              {
                  b = mod[j3];
                  j3++;
              }
              j--;
          }
      }
      tm += b;
    }
    while(true);
  }
  return tm;
}


const(char)[] PT3_GetName (ref PT3_SongInfo si) {
  const(char)* mod = cast(const(char)*)si.data.ptr;
  return ay_sys_getstr(mod+0x1e, 32);
}


const(char)[] PT3_GetAuthor (ref PT3_SongInfo si) {
  const(char)* mod = cast(const(char)*)si.data.ptr;
  return ay_sys_getstr(mod+0x42, 32);
}


bool PT3_Detect (const(ubyte)[] mod) {
  uint j, j1, j2;
  PT3_File* header = cast(PT3_File*)mod.ptr;

  uint PT3_PatternsPointer () { return cast(uint)(header.PT3_PatternsPointer0|(header.PT3_PatternsPointer1<<8)); }
  ushort PT3_OrnamentsPointers (ubyte x) { return cast(ushort)(header.PT3_OrnamentsPointers0[x*2]|(header.PT3_OrnamentsPointers0[x*2+1]<<8)); }

  if (mod.length < 202) return false;
  if (PT3_PatternsPointer > mod.length || PT3_PatternsPointer == 0) return false;
  if (mod[PT3_PatternsPointer-1] != 255) return false;
  if (PT3_OrnamentsPointers(0)+2 > mod.length) return false;

  j = 0;
  //memcpy(&j, &mod[PT3_OrnamentsPointers(0)], 3);
  j = mod[PT3_OrnamentsPointers(0)]|(mod[PT3_OrnamentsPointers(0)+1]<<8)|(mod[PT3_OrnamentsPointers(0)+2]<<16);
  if (j != 256) return false;

  j = ay_sys_getword(&mod[PT3_PatternsPointer]);
  if(j > mod.length)
      return false;
  if (j-cast(int)(PT3_PatternsPointer) <= 0) return false;
  //if(((j - (int)(PT3_PatternsPointer)) % 6) != 0) return false;

  j1 = 0;
  j2 = 0;
  while (j2 < 256 && j2 <= mod.length-201 && header.PT3_PositionList[j2] != 255) {
    if (cast(uint)j1 < header.PT3_PositionList[j2]) j1 = header.PT3_PositionList[j2];
    if ((j1%3) != 0) return false;
    ++j2;
  }
  if ((j-cast(int)PT3_PatternsPointer)/6 != (j1/3)+1) return false;

  j = 15;
  while (j > 0 && PT3_OrnamentsPointers(cast(ubyte)j) == 0) --j;

  int F_Length = PT3_OrnamentsPointers(cast(ubyte)j)+mod[PT3_OrnamentsPointers(cast(ubyte)j)+1]+2;
  if (cast(uint)F_Length > mod.length+1) return false;

  //header->PT3_NumberOfPositions = j2;
  return true;
}


shared static this () {
  zxModuleRegister!".pt3"((ubyte[] data) {
    if (PT3_Detect(data)) return [cast(ZXModule)(new ZXModulePT3(data))];
    return null;
  });
}


final class ZXModulePT3 : ZXModule {
private:
  PT3_SongInfo si;
  uint mIntrs, mLoop;

public:
  this (const(ubyte)[] adata) {
    si.data = adata;
    if (!PT3_Init(si)) throw new Exception("not a PT3 module");
    mIntrs = PT3_GetTime(adata, mLoop);
  }

  override string typeStr () { return "PT3"; }
  override const(char)[] name () { return PT3_GetName(si); }
  override const(char)[] author () { return PT3_GetAuthor(si); }
  override const(char)[] misc () { return null; }
  override uint intrCount () { return mIntrs; }
  override uint fadeCount () { return 0; }
  override uint loopPos () { return mLoop; }
  override bool doIntr () { PT3_Play_Chip(si); return true; }
  override void reset () { super.reset(); PT3_Reset(si); }
}
