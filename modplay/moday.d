/* daylet -- .AY music file player
 * Copyright (C) 2001 Russell Marks and Ian Collier
 * Copyright (C) 2012-2017 Ketmar // Invisible Vector
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
module modplay.moday is aliced;

import iv.cmdcon;
import iv.zymosis;

import zxinfo;
import emuconfig;

import modplay.mod_low;


// ////////////////////////////////////////////////////////////////////////// //
final class AYFile {
private:
  ubyte[] data; // file data

  const(char)[] authorstr;
  const(char)[] miscstr;
  uint tracknum;
  uint firstsng;
  ubyte relver;
  ubyte playerver;

public:
  this () {}
  this (ubyte[] adata) { load(adata); }

  @property bool valid () const pure nothrow @safe @nogc { return (data.length > 0); }

  @property ubyte releaseVer () const pure nothrow @safe @nogc { return relver; }
  @property ushort trackCount () const pure nothrow @safe @nogc { return cast(ushort)tracknum; }
  @property ushort firstTrack () const pure nothrow @safe @nogc { return cast(ushort)firstsng; }
  @property const(char)[] author () const pure nothrow @safe @nogc { return authorstr; }
  @property const(char)[] misc () const pure nothrow @safe @nogc { return miscstr; }

  const(char)[] trackTitle (int idx) const {
    if (idx < 0 || idx >= tracknum) return null;
    uint sdofs = getOfs(18); // song structure offset
    sdofs += 4*idx;
    uint xofs = getOfs(sdofs);
    uint end = xofs;
    while (end < data.length && data[end] != 0) ++end;
    return cast(const(char)[])(data[xofs..end]);
  }

  uint trackDurationFrm (int idx) const {
    if (idx < 0 || idx >= tracknum) return 0;
    uint sdofs = getOfs(18); // song structure offset
    sdofs += 4*idx;
    sdofs = getOfs(sdofs+2); // song data offset
    return getWord(sdofs+4);
  }

  uint trackFadeFrm (int idx) const {
    if (idx < 0 || idx >= tracknum) return 0;
    uint sdofs = getOfs(18); // song structure offset
    sdofs += 4*idx;
    sdofs = getOfs(sdofs+2); // song data offset
    return getWord(sdofs+6);
  }

  void trackPrepare (int idx, ZymCPU z80cpu, ubyte[] memory) const {
    if (idx < 0 || idx >= tracknum) throw new Exception("invalid track number");
    uint sdofs = getOfs(18); // song structure offset
    sdofs += 4*idx;
    sdofs = getOfs(sdofs+2); // song data offset
    uint ptofs = getOfs(sdofs+10);
    uint adofs = getOfs(sdofs+12);
    if (z80cpu !is null) {
      ushort w = getWord(sdofs+8);
      z80cpu.reset();
      z80cpu.R = 0;
      z80cpu.I = 3; // player version
      z80cpu.IFF1 = z80cpu.IFF2 = false;
      z80cpu.IM = 0;
      z80cpu.SP = getWord(ptofs);
      z80cpu.AF.w = z80cpu.BC.w = z80cpu.DE.w = z80cpu.HL.w = z80cpu.IX.w = z80cpu.IY.w = w;
      z80cpu.AFx.w = z80cpu.BCx.w = z80cpu.DEx.w = z80cpu.HLx.w = w;
    }

    memory[] = 0;
    // let caller care if memory array is big enough
    if (memory.length) {
      static immutable ubyte[10] intz = [
        0xf3,       // di
        0xcd, 0, 0, // call init
        0xed, 0x5e, // loop: im 2
        0xfb,       // ei
        0x76,       // halt
        0x18, 0xfa  // jr loop
      ];
      static immutable ubyte[13] intnz = [
        0xf3,       // di
        0xcd, 0, 0, // call init
        0xed, 0x56, // loop: im 1
        0xfb,       // ei
        0x76,       // halt
        0xcd, 0, 0, // call interrupt
        0x18, 0xf7  // jr loop
      ];
      memory[0x0000..0x0100] = 0xC9;
      memory[0x0100..0x4000] = 0xFF;
      memory[0x38] = 0xFB; // ei
      ushort initv = getWord(ptofs+2);
      ushort intrv = getWord(ptofs+4);
      // put player
      if (intrv) {
        memory[0..13] = intnz[];
        memory[9] = intrv&0xff;
        memory[10] = (intrv>>8)&0xff;
      } else {
        memory[0..10] = intz[];
      }
      // put memory blocks
      while (adofs+2*3 < data.length) {
        ushort z80addr = getWord(adofs);
        if (z80addr == 0) break;
        if (initv == 0) initv = z80addr;
        adofs += 2;
        uint len = getWord(adofs);
        adofs += 2;
        uint bytesofs = getOfs(adofs);
        adofs += 2;
        while (len > 0 && bytesofs < data.length) {
          memory[z80addr++] = getByte(bytesofs++);
          --len;
        }
      }
      // call init or first AY block
      memory[2] = initv&0xff;
      memory[3] = (initv>>8)&0xff;
    }
  }

  void clear () {
    data = null;
    authorstr = null;
    miscstr = null;
    tracknum = 0;
    firstsng = 0;
    relver = 0;
    playerver = 0;
  }

  void load (ubyte[] adata) {
    clear();
    scope(failure) clear();
    if (adata.length < 10) throw new Exception("invalid AY file");
    if (adata.length > 1024*1024*4) throw new Exception("AY file too big");
    data = adata;
    if (cast(const(char)[])(data[0..8]) != "ZXAYEMUL") throw new Exception("invalid AY file");
    relver = getByte(8);
    playerver = getByte(9);
    if (playerver > 3) throw new Exception("invalid AY file");
    // author
    auto xofs = getOfs(12);
    uint end = xofs;
    while (end < data.length && data[end] != 0) ++end;
    authorstr = cast(const(char)[])(data[xofs..end]);
    // misc
    xofs = getOfs(14);
    end = xofs;
    while (end < data.length && data[end] != 0) ++end;
    miscstr = cast(const(char)[])(data[xofs..end]);
    tracknum = getByte(16)+1;
    firstsng = getByte(17)+1;
    if (firstsng >= tracknum) firstsng = 0; // just in case
  }

private:
  ubyte getByte (int ofs) const {
    if (ofs < 0 || ofs >= data.length) throw new Exception("invalid AY file");
    return data.ptr[ofs];
  }

  ushort getWord (int ofs) const {
    if (ofs < 0 || ofs >= data.length || data.length-ofs < 2) throw new Exception("invalid AY file");
    return cast(ushort)(data.ptr[ofs+1]|(data.ptr[ofs]<<8));
  }

  uint getOfs (int ofs) const {
    if (ofs < 0 || ofs >= data.length || data.length-ofs < 2) throw new Exception("invalid AY file");
    int shift = cast(short)(data.ptr[ofs+1]|(data.ptr[ofs]<<8));
    if (shift < -(cast(int)data.length) || shift > data.length) throw new Exception("invalid AY file");
    ofs += shift;
    if (ofs < 0 || ofs > data.length) throw new Exception("invalid AY file");
    return cast(uint)ofs;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
class Z80CPU : ZymCPU {
  ubyte[] zxmemory;

  ubyte cpcF4 = 0;

  /* -1 to run as speccy, allowing only speccy ports;
   *  0 to run as speccy, and allow speccy and CPC ports (initial value);
   *  1 to run as CPC, allowing only CPC ports.
   */
  int cpcMode = 0;

  ubyte ayCurReg;

  this (ubyte[] mem) {
    zxmemory = mem;
    super();
    setupMemory();
  }

  final @property uint tsmax () const nothrow @trusted @nogc {
    pragma(inline, true);
    return (cpcMode <= 0 ? Config.machine.timings.tstatesPerFrame : 4000000/50);
  }

  override void reset (bool poweron=false) {
    super.reset(poweron);
    ayCurReg = 0;
    cpcF4 = 0;
    cpcMode = 0;
    tstates = 0;
  }

  override void setupMemory () {
    foreach (immutable idx; 0..65536/MemPage.Size) {
      mem[idx] = MemPage.init;
      mem[idx].mem = zxmemory.ptr+idx*MemPage.Size;
    }
  }

  override ubyte portRead (ushort port) nothrow @trusted @nogc {
    return 0xff;
  }

  override void portWrite (ushort port, ubyte value) nothrow @trusted @nogc {
    ubyte h = (port>>8)&0xff;
    ubyte l = port&0xff;
    // unlike a real speccy, it seems we should only emulate exact port number matches, rather than using bitmasks
    if (cpcMode < 1) {
      switch (l) {
        case 0xfd:
          switch (h) {
            case 0xff:
              cpcMode = -1;
              ayCurReg = (value&15);
              break;
            case 0xbf:
              cpcMode = -1;
              ay_writeay(ayCurReg, value, tstates);
              break;
            default:
              /* ok, since we do at least have low byte=FDh,
               * do bitmask for top byte to allow for
               * crappy .ay conversions. But don't disable
               * CPC autodetect, just in case.
               */
              if ((h&0xc0) == 0xc0) { ayCurReg = (value&15); break; }
              if ((h&0xc0) == 0x80) { ay_writeay(ayCurReg, value, tstates); break; }
              break;
          }
          break;
        case 0xfe:
          cpcMode = -1;
          //sound_beeper(value&0x10);
          /// on is: bit0 is tape microphone bit, bit1 is speaker bit, i.e.: (!!(b&0x10)<<1)+((!(b&0x8))|tape_microphone)
          ay_writebeeper(tstates, value);
          break;
        default: break;
      }
    }
    if (cpcMode > -1) {
      switch (h) {
        case 0xf6:
          switch (value&0xc0) {
            case 0x80: // write
              ay_writeay(ayCurReg, cpcF4, tstates);
              break;
            case 0xc0: // select
              ayCurReg = (cpcF4&15);
              break;
            default: break;
          }
          break;
        case 0xf4:
          cpcF4 = value;
          if (cpcMode == 0) {
            // restart as a more CPC-ish emulation
            assert(0, "sorry, no CPC yet");
            /+
            do_cpc = 1;
            Config.machine = zxFindMachine("cpc");
            tstates %= tsmax;
            conwriteln(" -- switched to CPC mode");
            sound_reinit();
            +/
          }
          break;
        default: break;
      }
    }
  }
}


shared static this () {
  zxModuleRegister!".ay"((ubyte[] data) {
    if (data.length >= 10) {
      if (cast(const(char)[])(data[0..8]) == "ZXAYEMUL") return loadAY(data);
    }
    return null;
  });
}


ZXModule[] loadAY (ubyte[] data) {
  auto ayfile = new AYFile(data);
  if (ayfile.trackCount == 0) throw new Exception("ay file has no tracks");
  auto zxmemory = new ubyte[](64*1024);
  Z80CPU z80cpu = new Z80CPU(zxmemory);
  ZXModule[] res;
  res.reserve(ayfile.trackCount);
  foreach (immutable ushort tno; 0..ayfile.trackCount) res ~= new ZXModuleAY(ayfile, z80cpu, tno);
  return res;
}


final class ZXModuleAY : ZXModule {
private:
  AYFile ayfile;
  ushort trackIdx;
  Z80CPU z80cpu;

public:
  this (AYFile ayf, Z80CPU acpu, ushort trackno) {
    ayfile = ayf;
    trackIdx = trackno;
    z80cpu = acpu;
  }

  override string typeStr () { return "AY"; }
  override const(char)[] name () { return ayfile.trackTitle(trackIdx); }
  override const(char)[] author () { return ayfile.author; }
  override const(char)[] misc () { return ayfile.misc; }
  override uint intrCount () { return ayfile.trackDurationFrm(trackIdx); }
  override uint fadeCount () { return ayfile.trackFadeFrm(trackIdx); }
  override uint loopPos () { return uint.max; }
  override bool doIntr () {
    if (z80cpu.tstates >= z80cpu.tsmax) {
      z80cpu.tstates -= z80cpu.tsmax;
      while (z80cpu.tstates < Config.machine.timings.interruptLength) {
        z80cpu.intr();
        z80cpu.execStep();
      }
    }
    z80cpu.nextEventTS = z80cpu.tsmax;
    z80cpu.exec();
    return true;
  }
  override void reset () {
    z80cpu.zxmemory[] = 0;
    z80cpu.reset();
    ayfile.trackPrepare(trackIdx, z80cpu, z80cpu.zxmemory[]);
  }

  override string modelName () { return "128"; }
}
