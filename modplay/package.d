/* ProTracker 3 module decoder
 * Copyright (C) 2007 by Deryabin Andrew ( andrewderyabin@gmail.com )
 * Copyright (C) 2017 Ketmar // Invisible Vector
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
module modplay is aliced;
private:

public import modplay.mod_low;
import modplay.moday;
import modplay.modpt3;
import modplay.modpt2;
import modplay.modpt1;
import modplay.modstc;
import modplay.modasc;
import modplay.modstp;
import modplay.modpsc;
import modplay.modsqt;
