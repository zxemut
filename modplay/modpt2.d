/* ProTracker 2 module decoder
 * Copyright (C) 2007 by Deryabin Andrew ( andrewderyabin@gmail.com )
 * Copyright (C) 2017 Ketmar // Invisible Vector
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
module modplay.modpt2 is aliced;
private:

import modplay.mod_low;

immutable ushort[96] PT2_Table =
[ 0xef8, 0xe10, 0xd60, 0xc80, 0xbd8, 0xb28, 0xa88, 0x9f0, 0x960, 0x8e0, 0x858, 0x7e0, 0x77c, 0x708, 0x6b0, 0x640, 0x5ec, 0x594, 0x544, 0x4f8, 0x4b0, 0x470, 0x42c, 0x3fd, 0x3be, 0x384, 0x358, 0x320, 0x2f6, 0x2ca, 0x2a2, 0x27c, 0x258, 0x238, 0x216, 0x1f8, 0x1df, 0x1c2, 0x1ac, 0x190, 0x17b, 0x165, 0x151, 0x13e, 0x12c, 0x11c, 0x10a, 0xfc, 0xef, 0xe1, 0xd6, 0xc8, 0xbd, 0xb2, 0xa8, 0x9f, 0x96, 0x8e, 0x85, 0x7e, 0x77, 0x70, 0x6b, 0x64, 0x5e, 0x59, 0x54, 0x4f, 0x4b, 0x47, 0x42, 0x3f, 0x3b, 0x38, 0x35, 0x32, 0x2f, 0x2c, 0x2a, 0x27, 0x25, 0x23, 0x21, 0x1f, 0x1d, 0x1c, 0x1a, 0x19, 0x17, 0x16, 0x15, 0x13, 0x12, 0x11, 0x10, 0xf ];


align(1) struct PT2_File {
align(1):
  ubyte PT2_Delay;
  ubyte PT2_NumberOfPositions;
  ubyte PT2_LoopPosition;
  ubyte[64] PT2_SamplesPointers0;
  ubyte[32] PT2_OrnamentsPointers0;
  ubyte PT2_PatternsPointer0, PT2_PatternsPointer1;
  char[30] PT2_MusicName;
  ubyte[65535-131] PT2_PositionList;
}


align(1) struct PT2_Channel_Parameters {
align(1):
  ushort Address_In_Pattern, OrnamentPointer, SamplePointer, Ton;
  ubyte Loop_Ornament_Position, Ornament_Length, Position_In_Ornament, Loop_Sample_Position, Sample_Length, Position_In_Sample, Volume, Number_Of_Notes_To_Skip, Note, Slide_To_Note, Amplitude;
  byte Current_Ton_Sliding, Ton_Delta;
  int GlissType;
  bool Envelope_Enabled, Enabled;
  byte Glissade, Addition_To_Noise, Note_Skip_Counter;
}


align(1) struct PT2_Parameters {
align(1):
  ubyte DelayCounter, Delay, CurrentPosition;
}


align(1) struct PT2_SongInfo {
  PT2_Parameters PT2;
  PT2_Channel_Parameters PT2_A, PT2_B, PT2_C;
  const(ubyte)[] mod;
}


/*
#define PT2_A ((PT2_SongInfo *)info.data)->PT2_A
#define PT2_B ((PT2_SongInfo *)info.data)->PT2_B
#define PT2_C ((PT2_SongInfo *)info.data)->PT2_C
#define PT2 ((PT2_SongInfo *)info.data)->PT2

#define PT2_SamplesPointers(x) ((unsigned long)(header->PT2_SamplesPointers0 [(x) * 2] | (header->PT2_SamplesPointers0 [(x) * 2 + 1] << 8)))
#define PT2_OrnamentsPointers(x) ((unsigned long)(header->PT2_OrnamentsPointers0 [(x) * 2] | (header->PT2_OrnamentsPointers0 [(x) * 2 + 1] << 8)))
#define PT2_PositionList(x) (header->PT2_PositionList [(x)])
#define PT2_PatternsPointer ((unsigned long)(header->PT2_PatternsPointer0 | (header->PT2_PatternsPointer1 << 8)))
#define PT2_Delay (header->PT2_Delay)
#define PT2_NumberOfPositions (header->PT2_NumberOfPositions)
#define PT2_LoopPosition (header->PT2_LoopPosition)
*/

const(PT2_File)* PT2_Header (ref PT2_SongInfo info) { return cast(const(PT2_File)*)info.mod.ptr; }

ushort PT2_SamplesPointers (ref PT2_SongInfo info, uint x) { return cast(ushort)(info.PT2_Header.PT2_SamplesPointers0[x*2]|(info.PT2_Header.PT2_SamplesPointers0[x*2+1]<<8)); }
ushort PT2_OrnamentsPointers (ref PT2_SongInfo info, uint x) { return cast(ushort)(info.PT2_Header.PT2_OrnamentsPointers0[x*2]|(info.PT2_Header.PT2_OrnamentsPointers0[x*2+1]<<8)); }
ubyte PT2_PositionList (ref PT2_SongInfo info, uint x) { return info.PT2_Header.PT2_PositionList[x]; }
ushort PT2_PatternsPointer (ref PT2_SongInfo info) { return cast(ushort)(info.PT2_Header.PT2_PatternsPointer0|(info.PT2_Header.PT2_PatternsPointer1<<8)); }
ubyte PT2_Delay (ref PT2_SongInfo info) { return info.PT2_Header.PT2_Delay; }
ubyte PT2_NumberOfPositions (ref PT2_SongInfo info) { return info.PT2_Header.PT2_NumberOfPositions; }
ubyte PT2_LoopPosition (ref PT2_SongInfo info) { return info.PT2_Header.PT2_LoopPosition; }


void PT2_Init (ref PT2_SongInfo info) {
  const(ubyte)* mod = info.mod.ptr;
  //auto header = cast(const(PT2_File)*)mod;
  //memset(&PT2_A, 0, sizeof(PT2_Channel_Parameters));
  //memset(&PT2_B, 0, sizeof(PT2_Channel_Parameters));
  //memset(&PT2_C, 0, sizeof(PT2_Channel_Parameters));

  info.PT2_A = info.PT2_A.init;
  info.PT2_B = info.PT2_B.init;
  info.PT2_C = info.PT2_C.init;

  info.PT2.DelayCounter = 1;
  info.PT2.Delay = info.PT2_Delay;
  info.PT2.CurrentPosition = 0;

  info.PT2_A.Address_In_Pattern = ay_sys_getword(&mod[info.PT2_PatternsPointer+info.PT2_PositionList(0) * 6]);
  info.PT2_B.Address_In_Pattern = ay_sys_getword(&mod[info.PT2_PatternsPointer+info.PT2_PositionList(0) * 6 + 2]);
  info.PT2_C.Address_In_Pattern = ay_sys_getword(&mod[info.PT2_PatternsPointer+info.PT2_PositionList(0) * 6 + 4]);

  info.PT2_A.OrnamentPointer = info.PT2_OrnamentsPointers(0);
  info.PT2_A.Ornament_Length = mod[info.PT2_A.OrnamentPointer];
  info.PT2_A.OrnamentPointer++;
  info.PT2_A.Loop_Ornament_Position = mod[info.PT2_A.OrnamentPointer];
  info.PT2_A.OrnamentPointer++;
  info.PT2_A.Envelope_Enabled = false;
  info.PT2_A.Position_In_Sample = 0;
  info.PT2_A.Position_In_Ornament = 0;
  info.PT2_A.Addition_To_Noise = 0;
  info.PT2_A.Glissade = 0;
  info.PT2_A.Current_Ton_Sliding=0;
  info.PT2_A.GlissType = 0;
  info.PT2_A.Enabled = false;
  info.PT2_A.Number_Of_Notes_To_Skip = 0;
  info.PT2_A.Note_Skip_Counter = 0;
  info.PT2_A.Volume = 15;
  info.PT2_A.Ton = 0;

  info.PT2_B.OrnamentPointer = info.PT2_A.OrnamentPointer;
  info.PT2_B.Loop_Ornament_Position = info.PT2_A.Loop_Ornament_Position;
  info.PT2_B.Ornament_Length = info.PT2_A.Ornament_Length;
  info.PT2_B.Envelope_Enabled = false;
  info.PT2_B.Position_In_Sample = 0;
  info.PT2_B.Position_In_Ornament = 0;
  info.PT2_B.Addition_To_Noise = 0;
  info.PT2_B.Glissade = 0;
  info.PT2_B.Current_Ton_Sliding = 0;
  info.PT2_B.GlissType = 0;
  info.PT2_B.Enabled = false;
  info.PT2_B.Number_Of_Notes_To_Skip = 0;
  info.PT2_B.Note_Skip_Counter = 0;
  info.PT2_B.Volume = 15;
  info.PT2_B.Ton = 0;

  info.PT2_C.OrnamentPointer = info.PT2_A.OrnamentPointer;
  info.PT2_C.Loop_Ornament_Position = info.PT2_A.Loop_Ornament_Position;
  info.PT2_C.Ornament_Length = info.PT2_A.Ornament_Length;
  info.PT2_C.Envelope_Enabled = false;
  info.PT2_C.Position_In_Sample = 0;
  info.PT2_C.Position_In_Ornament = 0;
  info.PT2_C.Addition_To_Noise = 0;
  info.PT2_C.Glissade = 0;
  info.PT2_C.Current_Ton_Sliding = 0;
  info.PT2_C.GlissType = 0;
  info.PT2_C.Enabled = false;
  info.PT2_C.Number_Of_Notes_To_Skip = 0;
  info.PT2_C.Note_Skip_Counter = 0;
  info.PT2_C.Volume = 15;
  info.PT2_C.Ton = 0;
}


void PT2_Reset (ref PT2_SongInfo info) {
  PT2_Init(info);
}


uint PT2_GetTime (ref PT2_SongInfo info, out uint oloop) {
  const(ubyte)* mod = info.mod.ptr;
  short a1, a2, a3, a11, a22, a33;
  uint j1, j2, j3;
  int i, tm = 0;
  ubyte b;
  ubyte ptDelay = mod[0];
  ubyte ptNumPos = mod[1];
  ushort ptLoopPos = mod[2];
  ushort ptPatPt = ay_sys_getword(&mod[99]);
  const(ubyte)* ptPosList = cast(const(ubyte)*)&mod[131];
  oloop = uint.max;

  b = ptDelay;
  a1 = a2 = a3 = a11 = a22 = a33 = 0;
  for(i = 0; i < ptNumPos; i++)
  {
      if(i == ptLoopPos)
      {
          oloop = tm;
      }
      j1 = ay_sys_getword(&mod[ptPatPt + ptPosList[i] * 6]);
      j2 = ay_sys_getword(&mod[ptPatPt + ptPosList[i] * 6 + 2]);
      j3 = ay_sys_getword(&mod[ptPatPt + ptPosList[i] * 6 + 4]);
      do
      {
          a1--;
          if(a1 < 0)
          {
              if(mod[j1] == 0)
                  break;
              do
              {
                  ubyte val = cast(ubyte)mod[j1];
                  if(val == 0x70 || (val >= 0x80 && val <= 0xe0))
                  {
                      a1 = a11;
                      j1++;
                      break;
                  }
                  else if(val >= 0x71 && val <= 0x7e)
                  {
                      j1 += 2;
                  }
                  else if(val >= 0x20 && val <= 0x5f)
                  {
                      a11 = mod[j1] - 0x20;
                  }
                  else if(val == 0xf)
                  {
                      j1++;
                      b = mod[j1];
                  }
                  else if((val >= 1 && val <= 0xb) || val == 0xe)
                  {
                      j1++;
                  }
                  else if(val == 0xd)
                  {
                      j1 += 3;
                  }
                  j1++;
              }
              while(true);
          }

          a2--;
          if(a2 < 0)
          {
              do
              {
                  ubyte val = cast(ubyte)mod[j2];
                  if(val == 0x70 || (val >= 0x80 && val <= 0xe0))
                  {
                      a2 = a22;
                      j2++;
                      break;
                  }
                  else if(val >= 0x71 && val <= 0x7e)
                  {
                      j2 += 2;
                  }
                  else if(val >= 0x20 && val <= 0x5f)
                  {
                      a22 = mod[j2] - 0x20;
                  }
                  else if(val == 0xf)
                  {
                      j2++;
                      b = mod[j2];
                  }
                  else if((val >= 1 && val <= 0xb) || val == 0xe)
                  {
                      j2++;
                  }
                  else if(val == 0xd)
                  {
                      j2 += 3;
                  }
                  j2++;
              }
              while(true);
          }
          a3--;
          if(a3 < 0)
          {
              do
              {
                  ubyte val = cast(ubyte)mod[j3];
                  if(val == 0x70 || (val >= 0x80 && val <= 0xe0))
                  {
                      a3 = a33;
                      j3++;
                      break;
                  }
                  else if(val >= 0x71 && val <= 0x7e)
                  {
                      j3 += 2;
                  }
                  else if(val >= 0x20 && val <= 0x5f)
                  {
                      a33 = mod[j3] - 0x20;
                  }
                  else if(val == 0xf)
                  {
                      j3++;
                      b = mod[j3];
                  }
                  else if((val >= 1 && val <= 0xb) || val == 0xe)
                  {
                      j3++;
                  }
                  else if(val == 0xd)
                  {
                      j3 += 3;
                  }
                  j3++;
              }
              while(true);
          }
          tm += b;
      }
      while(true);
  }
  //info.Length = tm;
  //const(ubyte)* ptr = mod + 101;
  //info.Name = ay_sys_getstr(ptr, 30);
  return tm;
}


const(char)[] PT2_GetName (ref PT2_SongInfo si) {
  const(char)* mod = cast(const(char)*)si.mod.ptr;
  return ay_sys_getstr(mod+101, 30);
}


void PT2_PatternInterpreter (ref PT2_SongInfo info, ref PT2_Channel_Parameters chan) {
  const(ubyte)* mod = info.mod.ptr;
  //PT2_File *header = (PT2_File *)mod;
  bool quit, gliss;
  quit = false;
  gliss = false;
  do
  {
      ubyte val = mod[chan.Address_In_Pattern];
      if(val >= 0xe1)
      {
          chan.SamplePointer = info.PT2_SamplesPointers(val - 0xe0);
          chan.Sample_Length = mod[chan.SamplePointer];
          chan.SamplePointer++;
          chan.Loop_Sample_Position = mod[chan.SamplePointer];
          chan.SamplePointer++;
      }
      else if(val == 0xe0)
      {
          chan.Position_In_Sample = 0;
          chan.Position_In_Ornament = 0;
          chan.Current_Ton_Sliding = 0;
          chan.GlissType = 0;
          chan.Enabled = false;
          quit = true;
      }
      else if(val >= 0x80 && val <= 0xdf)
      {
          chan.Position_In_Sample = 0;
          chan.Position_In_Ornament = 0;
          chan.Current_Ton_Sliding = 0;
          if(gliss)
          {
              chan.Slide_To_Note = cast(ubyte)(val - 0x80);
              if(chan.GlissType == 1)
                  chan.Note = chan.Slide_To_Note;
          }
          else
          {
              chan.Note = cast(ubyte)(val - 0x80);
              chan.GlissType = 0;
          }
          chan.Enabled = true;
          quit = true;
      }
      else if(val == 0x7f)
          chan.Envelope_Enabled = false;
      else if(val >= 0x71 && val <= 0x7e)
      {
          chan.Envelope_Enabled = true;
          ay_writeay(AY_ENV_SHAPE, cast(ubyte)(val - 0x70));
          chan.Address_In_Pattern++;
          ay_writeay(AY_ENV_FINE, mod[chan.Address_In_Pattern]);
          chan.Address_In_Pattern++;
          ay_writeay(AY_ENV_COARSE, mod[chan.Address_In_Pattern]);
      }
      else if(val == 0x70)
          quit = true;
      else if(val >= 0x60 && val <= 0x6f)
      {
          chan.OrnamentPointer = info.PT2_OrnamentsPointers(val - 0x60);
          chan.Ornament_Length = mod[chan.OrnamentPointer];
          chan.OrnamentPointer++;
          chan.Loop_Ornament_Position = mod[chan.OrnamentPointer];
          chan.OrnamentPointer++;
          chan.Position_In_Ornament = 0;
      }
      else if(val >= 0x20 && val <= 0x5f)
          chan.Number_Of_Notes_To_Skip = cast(ubyte)(val - 0x20);
      else if(val >= 0x10 && val <= 0x1f)
          chan.Volume = cast(ubyte)(val - 0x10);
      else if(val == 0xf)
      {
          chan.Address_In_Pattern++;
          info.PT2.Delay = mod[chan.Address_In_Pattern];
      }
      else if(val == 0xe)
      {
          chan.Address_In_Pattern++;
          chan.Glissade = mod[chan.Address_In_Pattern];
          chan.GlissType = 1;
          gliss = true;
      }
      else if(val == 0xd)
      {
          import std.math : abs;
          chan.Address_In_Pattern++;
          chan.Glissade = abs(cast(byte)(mod[chan.Address_In_Pattern]));
          chan.Address_In_Pattern += 2; //Not use precalculated Ton_Delta
          //to avoide error with first note of pattern
          chan.GlissType = 2;
          gliss = true;
      }
      else if(val == 0xc)
          chan.GlissType = 0;
      else
      {
          chan.Address_In_Pattern++;
          chan.Addition_To_Noise = mod[chan.Address_In_Pattern];
      }
      chan.Address_In_Pattern++;
  }
  while(!quit);
  if(gliss && (chan.GlissType == 2))
  {
      import std.math : abs;
      chan.Ton_Delta = cast(ubyte)abs(PT2_Table[chan.Slide_To_Note] - PT2_Table[chan.Note]);
      if(chan.Slide_To_Note > chan.Note)
          chan.Glissade = -(chan.Glissade);
  }
  chan.Note_Skip_Counter = chan.Number_Of_Notes_To_Skip;
}


void PT2_GetRegisters (ref PT2_SongInfo info, ref PT2_Channel_Parameters chan, ref ubyte TempMixer) {
  ubyte j, b0, b1;
  const(ubyte)* mod = info.mod.ptr;
  //PT2_File *header = (PT2_File *)module;
  if(chan.Enabled)
  {
      b0 = mod[chan.SamplePointer + chan.Position_In_Sample * 3];
      b1 = mod[chan.SamplePointer + chan.Position_In_Sample * 3 + 1];
      chan.Ton = mod[chan.SamplePointer + chan.Position_In_Sample * 3 + 2] + cast(ushort)((b1 & 15) << 8);
      if((b0 & 4) == 0)
          chan.Ton = -(chan.Ton);
      j = cast(ubyte)(chan.Note + mod[chan.OrnamentPointer + chan.Position_In_Ornament]);
      if(j > 95)
          j = 95;
      chan.Ton = (chan.Ton + chan.Current_Ton_Sliding + PT2_Table[j]) & 0xfff;
      if(chan.GlissType == 2)
      {
          import std.math : abs;
          chan.Ton_Delta = cast(byte)(chan.Ton_Delta - abs(chan.Glissade));
          if(chan.Ton_Delta < 0)
          {
              chan.Note = chan.Slide_To_Note;
              chan.GlissType = 0;
              chan.Current_Ton_Sliding = 0;
          }
      }
      if(chan.GlissType != 0)
          chan.Current_Ton_Sliding += chan.Glissade;
      chan.Amplitude = cast(ubyte)((chan.Volume * 17 + cast(ubyte)(chan.Volume > 7)) * (b1 >> 4) / 256);
      if(chan.Envelope_Enabled)
          chan.Amplitude = chan.Amplitude | 16;
      if((mod[chan.SamplePointer + chan.Position_In_Sample * 3] & 1) != 0)
          TempMixer = TempMixer | 64;
      else
          ay_writeay(AY_NOISE_PERIOD, ((b0 >> 3) + chan.Addition_To_Noise) & 31);
      if((b0 & 2) != 0)
          TempMixer = TempMixer | 8;
      chan.Position_In_Sample++;
      if(chan.Position_In_Sample == chan.Sample_Length)
          chan.Position_In_Sample = chan.Loop_Sample_Position;
      chan.Position_In_Ornament++;
      if(chan.Position_In_Ornament == chan.Ornament_Length)
          chan.Position_In_Ornament = chan.Loop_Ornament_Position;
  }
  else
      chan.Amplitude = 0;
  TempMixer = TempMixer >> 1;
}


void PT2_Play (ref PT2_SongInfo info) {
  ubyte TempMixer;
  const(ubyte)* mod = info.mod.ptr;
  //PT2_File *header = (PT2_File *)mod;
  info.PT2.DelayCounter--;
  if(info.PT2.DelayCounter == 0)
  {
      info.PT2_A.Note_Skip_Counter--;
      if(info.PT2_A.Note_Skip_Counter < 0)
      {
          if(mod[info.PT2_A.Address_In_Pattern] == 0)
          {
              info.PT2.CurrentPosition++;
              if(info.PT2.CurrentPosition == info.PT2_NumberOfPositions)
                  info.PT2.CurrentPosition = info.PT2_LoopPosition;
              info.PT2_A.Address_In_Pattern = ay_sys_getword(&mod[info.PT2_PatternsPointer+ info.PT2_PositionList(info.PT2.CurrentPosition) * 6]);
              info.PT2_B.Address_In_Pattern = ay_sys_getword(&mod[info.PT2_PatternsPointer + info.PT2_PositionList(info.PT2.CurrentPosition) * 6 + 2]);
              info.PT2_C.Address_In_Pattern = ay_sys_getword(&mod[info.PT2_PatternsPointer + info.PT2_PositionList(info.PT2.CurrentPosition) * 6 + 4]);
          }
          PT2_PatternInterpreter(info, info.PT2_A);
      }
      info.PT2_B.Note_Skip_Counter--;
      if(info.PT2_B.Note_Skip_Counter < 0)
      PT2_PatternInterpreter(info, info.PT2_B);
      info.PT2_C.Note_Skip_Counter--;
      if(info.PT2_C.Note_Skip_Counter < 0)
      PT2_PatternInterpreter(info, info.PT2_C);
      info.PT2.DelayCounter = info.PT2.Delay;
  }

  TempMixer = 0;
  PT2_GetRegisters(info, info.PT2_A, TempMixer);
  PT2_GetRegisters(info, info.PT2_B, TempMixer);
  PT2_GetRegisters(info, info.PT2_C, TempMixer);

  ay_writeay(AY_MIXER, TempMixer);

  ay_writeay(AY_CHNL_A_FINE, info.PT2_A.Ton & 0xff);
  ay_writeay(AY_CHNL_A_COARSE, (info.PT2_A.Ton >> 8) & 0xf);
  ay_writeay(AY_CHNL_B_FINE, info.PT2_B.Ton & 0xff);
  ay_writeay(AY_CHNL_B_COARSE, (info.PT2_B.Ton >> 8) & 0xf);
  ay_writeay(AY_CHNL_C_FINE, info.PT2_C.Ton & 0xff);
  ay_writeay(AY_CHNL_C_COARSE, (info.PT2_C.Ton >> 8) & 0xf);
  ay_writeay(AY_CHNL_A_VOL, info.PT2_A.Amplitude);
  ay_writeay(AY_CHNL_B_VOL, info.PT2_B.Amplitude);
  ay_writeay(AY_CHNL_C_VOL, info.PT2_C.Amplitude);
}


bool PT2_Detect0 (ubyte[] mod) {
  immutable length = mod.length;
  auto info = new PT2_SongInfo();
  scope(exit) delete info;
  info.mod = mod;
  auto header = cast(PT2_File*)mod.ptr;
  int j, j1, j2;
  if(length < 132)
      return false;
  if((*info).PT2_PatternsPointer > length || (*info).PT2_PatternsPointer == 0)
      return false;
  if(mod[(*info).PT2_PatternsPointer-1] != 255)
      return false;
  if((*info).PT2_SamplesPointers(0) != 0)
      return false;
  //j = 0;
  //memcpy(&j, &mod[info.PT2_OrnamentsPointers(0)], 3);
  if ((*info).PT2_OrnamentsPointers(0)+2 >= length) return false;
  j = mod[(*info).PT2_OrnamentsPointers(0)]|(mod[(*info).PT2_OrnamentsPointers(0)+1]<<8)|(mod[(*info).PT2_OrnamentsPointers(0)+2]<<16);
  if(j != 1)
      return false;

  j = ay_sys_getword(&mod[(*info).PT2_PatternsPointer]);
  if(cast(uint)j > length)
      return false;
  if(j - cast(int)((*info).PT2_PatternsPointer) <= 0)
      return false;
  if(((j - cast(int)((*info).PT2_PatternsPointer)) % 6) != 2)
      return false;

  j1 = 0;
  j2 = 0;
  while(((j2 < 256) && (cast(uint)j2 <= length - 131)) && (header.PT2_PositionList[j2] < 128))
  {
      if(cast(uint)(j1) < header.PT2_PositionList[j2])
          j1 = header.PT2_PositionList[j2];
      j2++;
  }
  if(((j - cast(int)((*info).PT2_PatternsPointer)) / 6) != j1 + 1)
      return false;

  j = 15;
  while((j > 0) && ((*info).PT2_OrnamentsPointers(j) == 0))
      j--;
  if ((*info).PT2_OrnamentsPointers(j) >= length) return false;
  int F_Length = (*info).PT2_OrnamentsPointers(j) + mod[(*info).PT2_OrnamentsPointers(j)] + 2;
  if(cast(uint)F_Length > length + 1)
      return false;

  header.PT2_NumberOfPositions = cast(ubyte)j2;
  return true;
}

bool PT2_Detect1(ubyte[] mod)
{
  immutable length = mod.length;
  auto info = new PT2_SongInfo();
  scope(exit) delete info;
  info.mod = mod;
  auto header = cast(PT2_File*)mod.ptr;
  int j, j1, j2, j3;
  if(length < 132)
      return false;
  if((*info).PT2_PatternsPointer > length || (*info).PT2_PatternsPointer == 0)
      return false;
  if(mod[(*info).PT2_PatternsPointer-1] != 255)
      return false;
  if((*info).PT2_SamplesPointers(0) != 0)
      return false;

  j3 = (*info).PT2_SamplesPointers(0);
  if((*info).PT2_OrnamentsPointers(0) - j3 - 2 > length)
      return false;
  if((*info).PT2_OrnamentsPointers(0)- j3 < 0)
      return false;

  //j = 0;
  //memcpy(&j, &mod[info.PT2_OrnamentsPointers(0)], 3);
  j = mod[(*info).PT2_OrnamentsPointers(0)]|(mod[(*info).PT2_OrnamentsPointers(0)+1]<<8)|(mod[(*info).PT2_OrnamentsPointers(0)+2]<<16);
  if(j != 1)
      return false;

  j = ay_sys_getword(&mod[(*info).PT2_PatternsPointer]);
  j -= j3;
  if(cast(uint)j > length)
      return false;
  if(j - cast(int)((*info).PT2_PatternsPointer) <= 0)
      return false;
  if(((j - cast(int)((*info).PT2_PatternsPointer)) % 6) != 2)
      return false;

  j1 = 0;
  j2 = 0;
  while(((j2 < 256) && (cast(uint)j2 <= length - 131)) && (header.PT2_PositionList[j2] < 128))
  {
      if(cast(uint)(j1) < header.PT2_PositionList[j2])
          j1 = header.PT2_PositionList[j2];
      j2++;
  }
  if(((j - cast(int)((*info).PT2_PatternsPointer)) / 6) != j1 + 1)
      return false;

  j = 15;
  while((j > 0) && ((*info).PT2_OrnamentsPointers(j) == cast(uint)j3))
      j--;
  if ((*info).PT2_OrnamentsPointers(j) < j3 || (*info).PT2_OrnamentsPointers(j)-j3 >= length) return false;
  int F_Length = (*info).PT2_OrnamentsPointers(j) - j3 + mod[(*info).PT2_OrnamentsPointers(j) - j3] + 2;
  if(cast(uint)F_Length > length + 1)
      return false;

  header.PT2_NumberOfPositions = cast(ubyte)j2;
  for(ushort jj = 0; jj < 32; jj++)
  {
      ay_sys_writeword(&header.PT2_SamplesPointers0[jj * 2], cast(ushort)((*info).PT2_SamplesPointers(jj) - j3));
  }
  for(ushort jj = 0; jj < 16; jj++)
  {
      ay_sys_writeword(&header.PT2_OrnamentsPointers0[jj * 2], cast(ushort)((*info).PT2_OrnamentsPointers(jj) - j3));
  }
  for(j2 = 0; j2 < j1 * 3 + 1; j2++)
  {
      j = ay_sys_getword(&mod[(*info).PT2_PatternsPointer+j2 * 2]);
      j -= j3;
      ay_sys_writeword(&mod[(*info).PT2_PatternsPointer+j2 * 2], cast(ushort)j);
  }
  return true;
}


bool PT2_Detect (ubyte[] mod) {
  return (PT2_Detect0(mod) || PT2_Detect1(mod));
}


shared static this () {
  zxModuleRegister!".pt2"((ubyte[] data) {
    if (PT2_Detect(data)) return [cast(ZXModule)(new ZXModulePT2(data))];
    return null;
  });
}


final class ZXModulePT2 : ZXModule {
private:
  PT2_SongInfo si;
  uint mIntrs, mLoop;

public:
  this (const(ubyte)[] adata) {
    si.mod = adata;
    PT2_Init(si);
    mIntrs = PT2_GetTime(si, mLoop);
  }

  override string typeStr () { return "PT2"; }
  override const(char)[] name () { return PT2_GetName(si); }
  override const(char)[] author () { return null; }
  override const(char)[] misc () { return null; }
  override uint intrCount () { return mIntrs; }
  override uint fadeCount () { return 0; }
  override uint loopPos () { return mLoop; }
  override bool doIntr () { PT2_Play(si); return true; }
  override void reset () { super.reset(); PT2_Reset(si); }
}
