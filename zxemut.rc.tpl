v_scale 3
wd_nodelay tan
trd_traps tan
detect_noflic tan
mouse_grab tan

trd_model128 128
trd_model128 pentagon512

trd_bootfile $data/bootz/sboot32.$c

pentagon_floating_bus ona

v_borderpix 32

zxbind_clear

zxbind a "a"
zxbind b "b"
zxbind c "c"
zxbind d "d"
zxbind e "e"
zxbind f "f"
zxbind g "g"
zxbind h "h"
zxbind i "i"
zxbind j "j"
zxbind k "k"
zxbind l "l"
zxbind m "m"
zxbind n "n"
zxbind o "o"
zxbind p "p"
zxbind q "q"
zxbind r "r"
zxbind s "s"
zxbind t "t"
zxbind u "u"
zxbind v "v"
zxbind w "w"
zxbind x "x"
zxbind y "y"
zxbind z "z"
zxbind 0 "0"
zxbind 1 "1"
zxbind 2 "2"
zxbind 3 "3"
zxbind 4 "4"
zxbind 5 "5"
zxbind 6 "6"
zxbind 7 "7"
zxbind 8 "8"
zxbind 9 "9"
zxbind space "spc"
zxbind return "ent"
zxbind padenter "ent"
zxbind shift "cap"
zxbind shift_r "sym"
zxbind alt "sym"
zxbind alt_r "sym"

zxbind backspace   "cap+0"
zxbind escape      "cap+1"
zxbind comma       "sym+n"
zxbind period      "sym+m"
zxbind slash       "sym+v"
zxbind dash        "sym+j"
zxbind equals      "sym+l"
zxbind minus       "sym+j"
zxbind apostrophe  "sym+p"
zxbind semicolon   "sym+o"
zxbind tab         "sym+cap"
zxbind minus       "sym+j"
zxbind plus        "sym+k"
zxbind multiply    "sym+b"
zxbind divide      "sym+v"
zxbind paddot      "sym+m"

zxbind left  "cap+5"
zxbind right "cap+8"
zxbind up    "cap+7"
zxbind down  "cap+6"

zxbind pad4 "kleft"
zxbind pad6 "kright"
zxbind pad8 "kup"
zxbind pad2 "kdown"
zxbind pad0 "kfire"
zxbind ctrl "kfire"

#zxbind "_" "sym+0"
