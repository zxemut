/* ZX Spectrum Emulator
 * Copyright (C) 2017 Ketmar // Invisible Vector
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
module lstape;
version(Posix) {
pragma(lib, "spectrum");

enum LibSpectrumHere = true;

extern(C) nothrow @nogc:


const int LIBSPECTRUM_TAPE_FLAGS_BLOCK;  /* End of block */
const int LIBSPECTRUM_TAPE_FLAGS_STOP;    /* Stop tape */
const int LIBSPECTRUM_TAPE_FLAGS_STOP48; /* Stop tape if in 48K mode */
const int LIBSPECTRUM_TAPE_FLAGS_NO_EDGE; /* Edge isn't really an edge */
const int LIBSPECTRUM_TAPE_FLAGS_LEVEL_LOW; /* Set level low */
const int LIBSPECTRUM_TAPE_FLAGS_LEVEL_HIGH; /* Set level high */
const int LIBSPECTRUM_TAPE_FLAGS_LENGTH_SHORT;/* Short edge; used for loader acceleration */
const int LIBSPECTRUM_TAPE_FLAGS_LENGTH_LONG; /* Long edge; used for loader acceleration */
const int LIBSPECTRUM_TAPE_FLAGS_TAPE; /* Tape has finished */


alias libspectrum_byte = ubyte;
alias libspectrum_dword = uint;

/* The states which a block can be in */
alias libspectrum_tape_state_type = int;
enum /*libspectrum_tape_state_type*/ : int {
  LIBSPECTRUM_TAPE_STATE_INVALID = 0,

  LIBSPECTRUM_TAPE_STATE_PILOT, /* Pilot pulses */
  LIBSPECTRUM_TAPE_STATE_SYNC1, /* First sync pulse */
  LIBSPECTRUM_TAPE_STATE_SYNC2, /* Second sync pulse */
  LIBSPECTRUM_TAPE_STATE_DATA1, /* First edge of a data bit */
  LIBSPECTRUM_TAPE_STATE_DATA2, /* Second edge of a data bit */
  LIBSPECTRUM_TAPE_STATE_PAUSE, /* The pause at the end of a block */
  LIBSPECTRUM_TAPE_STATE_TAIL,  /* The pulse at the end of a block */
}


/* The various errors which can occur */
alias libspectrum_error = int;
enum /*libspectrum_error*/ : int {
  LIBSPECTRUM_ERROR_NONE = 0,

  LIBSPECTRUM_ERROR_WARNING,

  LIBSPECTRUM_ERROR_MEMORY,
  LIBSPECTRUM_ERROR_UNKNOWN,
  LIBSPECTRUM_ERROR_CORRUPT,
  LIBSPECTRUM_ERROR_SIGNATURE,
  LIBSPECTRUM_ERROR_SLT,      /* .slt data found at end of a .z80 file */
  LIBSPECTRUM_ERROR_INVALID,  /* Invalid parameter supplied */

  LIBSPECTRUM_ERROR_LOGIC = -1,
}


/* Various types of file we might manage to identify */
alias libspectrum_id_t = int;
enum /*libspectrum_id_t*/ : int {
  /* These types present in all versions of libspectrum */

  LIBSPECTRUM_ID_UNKNOWN = 0,   /* Unidentified file */
  LIBSPECTRUM_ID_RECORDING_RZX, /* RZX input recording */
  LIBSPECTRUM_ID_SNAPSHOT_SNA,  /* .sna snapshot */
  LIBSPECTRUM_ID_SNAPSHOT_Z80,  /* .z80 snapshot */
  LIBSPECTRUM_ID_TAPE_TAP,      /* Z80-style .tap tape image */
  LIBSPECTRUM_ID_TAPE_TZX,      /* TZX tape image */

  /* Below here, present only in 0.1.1 and later */

  /* The next entry is deprecated in favour of the more specific
     LIBSPECTRUM_ID_DISK_CPC and LIBSPECTRUM_ID_DISK_ECPC */
  LIBSPECTRUM_ID_DISK_DSK,      /* .dsk +3 disk image */

  LIBSPECTRUM_ID_DISK_SCL,      /* .scl TR-DOS disk image */
  LIBSPECTRUM_ID_DISK_TRD,      /* .trd TR-DOS disk image */
  LIBSPECTRUM_ID_CARTRIDGE_DCK, /* .dck Timex cartridge image */

  /* Below here, present only in 0.2.0 and later */

  LIBSPECTRUM_ID_TAPE_WARAJEVO,   /* Warajevo-style .tap tape image */

  LIBSPECTRUM_ID_SNAPSHOT_PLUSD,  /* DISCiPLE/+D snapshot */
  LIBSPECTRUM_ID_SNAPSHOT_SP,     /* .sp snapshot */
  LIBSPECTRUM_ID_SNAPSHOT_SNP,    /* .snp snapshot */
  LIBSPECTRUM_ID_SNAPSHOT_ZXS,    /* .zxs snapshot (zx32) */
  LIBSPECTRUM_ID_SNAPSHOT_SZX,    /* .szx snapshot (Spectaculator) */

  /* Below here, present only in 0.2.1 and later */

  LIBSPECTRUM_ID_COMPRESSED_BZ2,  /* bzip2 compressed file */
  LIBSPECTRUM_ID_COMPRESSED_GZ,   /* gzip compressed file */

  /* Below here, present only in 0.2.2 and later */

  LIBSPECTRUM_ID_HARDDISK_HDF,    /* .hdf hard disk image */
  LIBSPECTRUM_ID_CARTRIDGE_IF2,   /* .rom Interface 2 cartridge image */

  /* Below here, present only in 0.3.0 and later */

  LIBSPECTRUM_ID_MICRODRIVE_MDR, /* .mdr microdrive cartridge */
  LIBSPECTRUM_ID_TAPE_CSW,       /* .csw tape image */
  LIBSPECTRUM_ID_TAPE_Z80EM,     /* Z80Em tape image */

  /* Below here, present only in 0.4.0 and later */

  LIBSPECTRUM_ID_TAPE_WAV,       /* .wav tape image */
  LIBSPECTRUM_ID_TAPE_SPC,       /* SP-style .spc tape image */
  LIBSPECTRUM_ID_TAPE_STA,       /* Speculator-style .sta tape image */
  LIBSPECTRUM_ID_TAPE_LTP,       /* Nuclear ZX-style .ltp tape image */
  LIBSPECTRUM_ID_COMPRESSED_XFD, /* xfdmaster (Amiga) compressed file */
  LIBSPECTRUM_ID_DISK_IMG,       /* .img DISCiPLE/+D disk image */
  LIBSPECTRUM_ID_DISK_MGT,       /* .mgt DISCiPLE/+D disk image */

  /* Below here, present only in 0.5.0 and later */

  LIBSPECTRUM_ID_DISK_UDI,  /* .udi generic disk image */
  LIBSPECTRUM_ID_DISK_FDI,  /* .fdi generic disk image */
  LIBSPECTRUM_ID_DISK_CPC,  /* .dsk plain CPC +3 disk image */
  LIBSPECTRUM_ID_DISK_ECPC, /* .dsk extended CPC +3 disk image */
  LIBSPECTRUM_ID_DISK_SAD,  /* .sad generic disk image */
  LIBSPECTRUM_ID_DISK_TD0,  /* .td0 generic disk image */

  /* Below here, present only in 1.0.0 and later */

  LIBSPECTRUM_ID_DISK_OPD, /* .opu/.opd Opus Discovery disk image */

  /* Below here, present only in 1.1.0 and later */

  LIBSPECTRUM_ID_TAPE_PZX, /* PZX tape image */

  LIBSPECTRUM_ID_AUX_POK, /* POKE file */

  /* Below here, present only in 1.2.0 and later */

  LIBSPECTRUM_ID_DISK_D80, /* .d80/.d40 Didaktik disk image */

  /* Below here, present only in 1.2.2 and later */

  LIBSPECTRUM_ID_COMPRESSED_ZIP, /* zip compressed file */
}


/* A linked list of tape blocks */
struct libspectrum_tape {}
struct libspectrum_tape_block {}
//alias libspectrum_tape_iterator = libspectrum_tape_iterator_s;
//struct libspectrum_tape_iterator_s {}

libspectrum_dword libspectrum_tape_block_length (libspectrum_tape_block* block);

libspectrum_tape *libspectrum_tape_alloc ();

/* Free the memory used by the blocks in a libspectrum_tape object, but
not the object itself; useful if you're about to read a new tape into
a current object. */
libspectrum_error libspectrum_tape_clear (libspectrum_tape* tape);

libspectrum_error libspectrum_tape_free (libspectrum_tape* tape);

/* Take the tape image of type `type' of `length' bytes starting at
`buffer' and convert it to a `libspectrum_tape' structure. If `type'
is `LIBSPECTRUM_ID_UNKNOWN', guess the file format via
`libspectrum_identify_file'; `filename' is generally used only to help
with the identification process and can be set to NULL (or anything
else) if `type' is not `LIBSPECTRUM_ID_UNKNOWN' unless the tape is a
WAV file where the underlying audiofile library will reread the
file and will not use the buffer. Tape images compressed with
bzip2 or gzip will be automatically and transparently decompressed.
*/
libspectrum_error libspectrum_tape_read (libspectrum_tape* tape, const(libspectrum_byte)* buffer, size_t length, libspectrum_id_t type, const(char)* filename=null);

/* Take the snapshot in `tape and serialise it as a `type' format file
into `*buffer'. On entry, '*buffer' is assumed to be allocated
'*length' bytes, and will grow if necessary; if '*length' is zero,
'*buffer' can be uninitialised on entry.
*/
libspectrum_error libspectrum_tape_write (libspectrum_byte** buffer, size_t* length, libspectrum_tape* tape, libspectrum_id_t type);

/* Does this tape structure actually contain a tape? */
int libspectrum_tape_present (const(libspectrum_tape)* tape);

/* This is the main workhorse function of the tape routines and will
return in `tstates' the number of tstates until the next edge should
occur from `tape'. `flags' will be set to the bitwise or of the
following:

LIBSPECTRUM_TAPE_FLAGS_BLOCK       The current block ends with this edge
LIBSPECTRUM_TAPE_FLAGS_STOP        User code should stop playing the tape after this edge
LIBSPECTRUM_TAPE_FLAGS_STOP48      User code should stop playing the tape after this edge if it was emulating a 48K machine.
                                   The desired behaviour for things like the TC2048 is undefined in the .tzx format :-(
LIBSPECTRUM_TAPE_FLAGS_LEVEL_LOW   The input signal from the tape should be forced low at this edge
LIBSPECTRUM_TAPE_FLAGS_LEVEL_HIGH  The input signal from the tape should be forced high at this edge
LIBSPECTRUM_TAPE_FLAGS_NO_EDGE     This "edge" isn't really an edge and doesn't change the input signal from the tape.
*/
libspectrum_error libspectrum_tape_get_next_edge (libspectrum_dword* tstates, int* flags, libspectrum_tape* tape);

/* Get the current block from the tape */
libspectrum_tape_block* libspectrum_tape_current_block (libspectrum_tape* tape);

/* Get the state of the active block on the tape */
libspectrum_tape_state_type libspectrum_tape_state (libspectrum_tape* tape);

/* Set the state of the active block on the tape */
libspectrum_error libspectrum_tape_set_state (libspectrum_tape* tape, libspectrum_tape_state_type state);

/* Peek at the next block on the tape */
libspectrum_tape_block* libspectrum_tape_peek_next_block (libspectrum_tape* tape);

/* Peek at the last block on the tape */
libspectrum_tape_block* libspectrum_tape_peek_last_block (libspectrum_tape* tape);

/* Cause the next block on the tape to be active, initialise it and return it */
libspectrum_tape_block* libspectrum_tape_select_next_block (libspectrum_tape* tape);

/* Return in `n' the position of the current block on the tape. The first
block is block 0, the second block 1, etc.
*/
libspectrum_error libspectrum_tape_position (int* n, libspectrum_tape* tape);

/* Select the nth block on the tape */
libspectrum_error libspectrum_tape_nth_block (libspectrum_tape* tape, int n);

/* Append a block to the current tape */
//void libspectrum_tape_append_block (libspectrum_tape* tape, libspectrum_tape_block* block);

//void libspectrum_tape_remove_block (libspectrum_tape* tape, libspectrum_tape_iterator it);

//libspectrum_error libspectrum_tape_insert_block (libspectrum_tape* tape, libspectrum_tape_block* block, size_t position);

/*** Routines for iterating through a tape ***/
/*
libspectrum_tape_block* libspectrum_tape_iterator_init (libspectrum_tape_iterator* iterator, libspectrum_tape* tape);
libspectrum_tape_block* libspectrum_tape_iterator_current (libspectrum_tape_iterator iterator);
libspectrum_tape_block* libspectrum_tape_iterator_next (libspectrum_tape_iterator* iterator);
*/

} else {
  enum LibSpectrumHere = false;
}
