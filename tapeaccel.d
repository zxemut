/* loader.c: loader detection
 * Copyright (c) 2006 Philip Kendall
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Author contact information:
 *
 * E-mail: philip-fuse@shadowmagic.org.uk
 *
 * Adapted for DZXEmuT by Ketmar // Invisible Vector
 */
module tapeaccel;
private:

import iv.alice;
import iv.cmdcon;

import emuconfig;
import lstape;


// ////////////////////////////////////////////////////////////////////////// //
private @property uint tsmax () nothrow @trusted @nogc {
  pragma(inline, true);
  return Config.machine.timings.tstatesPerFrame;
}


// ////////////////////////////////////////////////////////////////////////// //
public bool isTapePlaying () nothrow @trusted @nogc { pragma(inline, true); return tapePlaying; }

public __gshared bool optFlashLoad = true;

__gshared bool optDebugFlashLoad = false;
__gshared bool optTapeFlashLoad = true;
__gshared bool optTapeDetector = true;
__gshared bool optAutoMaxSpeed = true;
__gshared bool optTapeSlowScan = true;
__gshared bool optTapeAutoRewind = false;
public __gshared bool optTapeShowProgress = true;

__gshared bool tapeComplete = false;
__gshared bool tapePlaying = false;
__gshared bool tapeLastEdge = false;
__gshared long tapeNextInFrame = -1;
public __gshared bool tapeNeedRewind = false;
public __gshared bool tapeAutostarted = false; // true: drop maxspeed on tape stop
__gshared int tapStopAfterNextEdge = 0; // <0: stop on 48k; >0: stop anyway
//__gshared bool tapNextEdge = false;
__gshared int tapeBlockCount = 0;
__gshared int tapeBlockLastSeen = 0;
// in t-states
__gshared int tapeCurrTState = 0;
__gshared int tapeTotalTStates = 0;

public __gshared bool tapeCurrentIs48K = true; // used for `LIBSPECTRUM_TAPE_FLAGS_STOP48`


public struct TapeState {
  int currBlock, blockCount;
  // t-states
  int curr, total;
  bool playing;
  bool complete;
}


static if (LibSpectrumHere) {
public __gshared libspectrum_tape *curtape = null;


public TapeState tapeGetState () nothrow @nogc {
  TapeState ts;
  ts.currBlock = tapeBlockLastSeen;
  ts.blockCount = tapeBlockCount;
  ts.curr = tapeCurrTState;
  ts.total = tapeTotalTStates;
  ts.playing = tapePlaying;
  ts.complete = tapeComplete;
  return ts;
}


public void tapeNextFrame () nothrow @nogc {
  if (tapePlaying) tapeNextInFrame -= tsmax;
  if (tapDetectLastInTS > -100666) tapDetectLastInTS -= tsmax;
}


void tapeShowProgress () nothrow {
  if (!curtape || tapeComplete || !libspectrum_tape_present(curtape)) return;
  int cblk;
  auto tres = libspectrum_tape_position(&cblk, curtape);
  if (tres != LIBSPECTRUM_ERROR_NONE && tres != LIBSPECTRUM_ERROR_WARNING) return;
  if (cblk != tapeBlockLastSeen) {
    tapeBlockLastSeen = cblk;
    if (!optTapeAutoRewind && cblk == 0) {
      // autorewind; stop the tape, set the flags
      emuStopTape();
      tapeComplete = true;
      tapeCurrTState = 0;
      conwriteln("tape: complete");
    } else {
      conwriteln("tape: block ", cblk, " of ", tapeBlockCount);
    }
  }
}


public bool tapeCurEdge () nothrow {
  if (!tapePlaying || !curtape || !libspectrum_tape_present(curtape)) return false;
  if (tapeNextInFrame > emuz80.tstates || tapeComplete) return tapeLastEdge;
  tapeShowProgress();
  while (tapeNextInFrame <= emuz80.tstates && tapePlaying) {
    int flags = 0;
    uint tstates = 0;
    auto tres = libspectrum_tape_get_next_edge(&tstates, &flags, curtape);
    if (tres != LIBSPECTRUM_ERROR_NONE && tres != LIBSPECTRUM_ERROR_WARNING) {
      conwriteln("tape reading error");
      tapePlaying = false;
      tapeComplete = true;
      return tapeLastEdge;
    }
    tapeCurrTState += tstates;
         if (flags&LIBSPECTRUM_TAPE_FLAGS_LEVEL_LOW) tapeLastEdge = false;
    else if (flags&LIBSPECTRUM_TAPE_FLAGS_LEVEL_HIGH) tapeLastEdge = true;
    else if ((flags&LIBSPECTRUM_TAPE_FLAGS_NO_EDGE) == 0) tapeLastEdge = !tapeLastEdge;
    if (flags&(LIBSPECTRUM_TAPE_FLAGS_TAPE|LIBSPECTRUM_TAPE_FLAGS_STOP)) { conwriteln("tape: stop00"); tapePlaying = false; }
    if (flags&LIBSPECTRUM_TAPE_FLAGS_STOP48 && tapeCurrentIs48K) { conwriteln("tape: stop01"); tapePlaying = false; }
    if (flags&LIBSPECTRUM_TAPE_FLAGS_TAPE) { conwriteln("tape complete"); tapeComplete = true; }
    tapeNextInFrame += tstates;
    //conwriteln("tapeNextInFrame=", tapeNextInFrame, "; emuz80.tstates=", emuz80.tstates, "; tapeLastEdge=", tapeLastEdge);
  }
  return tapeLastEdge;
}


int emuTapeLdrGetNextPulse () nothrow {
  if (!tapePlaying || !curtape || !libspectrum_tape_present(curtape) || tapeComplete) return -1;
  /*
  int nts = 0;
  bool curedge = tapeLastEdge;
  for (;;) {
    if (!tapePlaying) return -1;
    ++nts;
    emuz80.tstates += 1;
    while (emuz80.tstates >= tsmax) { tapeNextFrame(); emuz80.tstates -= tsmax; }
    if (curedge != tapeCurEdge()) return nts;
  }
  */

  if (tapeNextInFrame > 0) tapeNextInFrame = 0; // just in case
  //emuz80.tstates = 0;

  bool curedge = tapeLastEdge;
  int ts = 0;
  tapeShowProgress();
  while (tapePlaying) {
    int flags = 0;
    uint tstates = 0;
    auto tres = libspectrum_tape_get_next_edge(&tstates, &flags, curtape);
    if (tres != LIBSPECTRUM_ERROR_NONE && tres != LIBSPECTRUM_ERROR_WARNING) {
      conwriteln("tape reading error");
      tapePlaying = false;
      break;
    }
    tapeCurrTState += tstates;
         if (flags&LIBSPECTRUM_TAPE_FLAGS_LEVEL_LOW) tapeLastEdge = false;
    else if (flags&LIBSPECTRUM_TAPE_FLAGS_LEVEL_HIGH) tapeLastEdge = true;
    else if ((flags&LIBSPECTRUM_TAPE_FLAGS_NO_EDGE) == 0) tapeLastEdge = !tapeLastEdge;
    if (flags&(LIBSPECTRUM_TAPE_FLAGS_TAPE|LIBSPECTRUM_TAPE_FLAGS_STOP)) { conwriteln("tape: stop10"); tapePlaying = false; }
    if (flags&LIBSPECTRUM_TAPE_FLAGS_STOP48 && tapeCurrentIs48K) { conwriteln("tape: stop11"); tapePlaying = false; }
    if (flags&LIBSPECTRUM_TAPE_FLAGS_TAPE) { conwriteln("tape complete"); tapeComplete = true; break; }
    ts += tstates;
    if (tapeLastEdge != curedge) return ts;
  }
  return -1; // oops
}


// ////////////////////////////////////////////////////////////////////////// //
shared static this () {
  // tape
  conRegVar!optDebugFlashLoad("tape_debug_flashload", "show FlashLoader(utm) debug messages?");

  conRegVar!optTapeFlashLoad("tape_flashload", "is FlashLoader(utm) enabled?");
  conRegVar!optTapeDetector("tape_detect_loaders", "detect loaders and automatically start/stop tape?");
  conRegVar!optAutoMaxSpeed("tape_auto_max_speed", "switch emu to max speed when loader detected?");

  conRegVar!tapePlaying("tape_is_playing", "is tape playing?");

  conRegVar!optTapeShowProgress("tape_progress", "show tape progress?");
  conRegVar!optTapeSlowScan("tape_slow_scan", "use slow, but more precise tape length scanner?");
  conRegVar!optTapeAutoRewind("tape_autorewind", "autorewind and continue playing on tape end?");

  static if (LibSpectrumHere) {
    conRegFunc!emuStartTape("tape_play", "play tape");
    conRegFunc!emuStopTape("tape_stop", "stop tape");
    conRegFunc!emuRewindTape("tape_rewind", "rewind tape");
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public void emuEjectTape () nothrow {
  emuStopTape();
  if (curtape !is null) { libspectrum_tape_free(curtape); curtape = null; }
  tapeBlockCount = tapeBlockLastSeen = 0;
  tapeCurrTState = tapeTotalTStates = 0;
  tapeAutostarted = false;
  tapeComplete = false;
}


void emuStopTape () nothrow {
  if (tapePlaying) {
    conwriteln("tape: stopped");
    emuTapeAcceleratorStop();
    tapePlaying = false;
    tapeLastEdge = false;
    tapeNextInFrame = -1;
    if (tapeAutostarted && optAutoMaxSpeed) Config.maxSpeed = false;
  }
  tapeAutostarted = false;
}


void emuStartTape () nothrow @nogc {
  if (curtape && libspectrum_tape_present(curtape) && !tapePlaying && !tapeComplete) {
    tapePlaying = true;
    emuTapeAcceleratorStart();
  }
  tapeAutostarted = false;
}


void emuStartTapeAuto () nothrow {
  bool oldpl = false;
  emuStartTape();
  if (tapePlaying) {
    tapeAutostarted = true;
    if (!oldpl) {
      conwriteln("tape autostarted");
      if (optAutoMaxSpeed) Config.maxSpeed = true;
    }
  }
}


public void emuRewindTape () nothrow {
  tapeAutostarted = false;
  tapeComplete = false;
  tapeBlockCount = tapeBlockLastSeen = 0;
  tapeCurrTState = tapeTotalTStates = 0;
  if (curtape && libspectrum_tape_present(curtape)) {
    if (auto last = libspectrum_tape_peek_last_block(curtape)) {
      libspectrum_tape_nth_block(curtape, 0);
      if (!optTapeSlowScan) tapeTotalTStates += libspectrum_tape_block_length(libspectrum_tape_current_block(curtape));
      ++tapeBlockCount;
      while (libspectrum_tape_current_block(curtape) !is last) {
        if (libspectrum_tape_select_next_block(curtape) is null) break;
        if (!optTapeSlowScan) tapeTotalTStates += libspectrum_tape_block_length(libspectrum_tape_current_block(curtape));
        ++tapeBlockCount;
      }
      // slow scanner
      if (optTapeSlowScan) {
        libspectrum_tape_nth_block(curtape, 0);
        int lastBlock = 0;
        tapeTotalTStates = 0;
        for (;;) {
          int flags = 0;
          uint tstates = 0;
          auto tres = libspectrum_tape_get_next_edge(&tstates, &flags, curtape);
          if (tres != LIBSPECTRUM_ERROR_NONE && tres != LIBSPECTRUM_ERROR_WARNING) break;
          int cblk;
          tres = libspectrum_tape_position(&cblk, curtape);
          if (tres != LIBSPECTRUM_ERROR_NONE && tres != LIBSPECTRUM_ERROR_WARNING) break;
          if (cblk != lastBlock) {
            lastBlock = cblk;
            if (cblk == 0) break;
          }
          tapeTotalTStates += tstates;
        }
      }
    }
    libspectrum_tape_nth_block(curtape, 0);
    conwriteln("tape rewound (", tapeBlockCount, " blocks; ", tapeTotalTStates, " tstates)");
    if (tapePlaying) emuTapeAcceleratorStart();
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public void emuTapeLoaderDetector () nothrow {
  int tdelta = emuz80.tstates-tapDetectLastInTS;
  ubyte bdelta = (cast(int)emuz80.BC.b-tapDetectLastInB)&0xff;

  tapDetectLastInTS = emuz80.tstates;
  tapDetectLastInB = emuz80.BC.b;

  if (optTapeDetector) {
    // detector is on
    if (tapePlaying) {
      // tape is playing
      if (tdelta > 1000 || (bdelta != 1 && bdelta != 0 && bdelta != 0xff)) {
        if (++tapDetectInCount >= 2) {
          // seems that this is not a loader anymore
          if (tapeAutostarted) {
            if (tapePlaying) conwriteln("tape autostopped");
            emuStopTape();
          }
        }
      } else {
        tapDetectInCount = 0;
      }
    } else {
      // tape is not playing
      if (tdelta <= 500 && (bdelta == 1 || bdelta == 0xff)) {
        if (curtape !is null && !tapeNeedRewind) {
          if (++tapDetectInCount >= 10) {
            // seems that this is LD-EDGE
            emuStartTapeAuto();
          }
        } else {
          tapDetectInCount = 0;
        }
      } else {
        tapDetectInCount = 0;
      }
    }
  } else {
    // detector is off
    tapDetectInCount = 0;
  }
  //
  if (tapePlaying) emuTapeCheckForAcceleration();
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared int lengthKnown1 = 0, lengthKnown2 = 0;
__gshared int lengthLong1 = 0, lengthLong2 = 0;

__gshared int tapNextEdgeTS; // can exceed tsperframe or be negative
__gshared int tapDetectInCount = 0;
__gshared int tapDetectLastInTS = -666;
__gshared int tapDetectLastInB = 0x00;


enum TapeAccelMode {
  None = 0,
  Increasing,
  Decreasing,
}


__gshared TapeAccelMode accelerationMode;
__gshared ushort accelerationPC;


public void emuTapeAcceleratorStart () nothrow @nogc {
  tapDetectInCount = 0;
  accelerationMode = TapeAccelMode.None;
}


public void emuTapeAcceleratorStop () nothrow @nogc {
  tapDetectInCount = 0;
  accelerationMode = TapeAccelMode.None;
}


public void emuTapeAcceleratorEdgeAdvance (int flags) nothrow @nogc {
  if (flags&LIBSPECTRUM_TAPE_FLAGS_LENGTH_SHORT) {
    lengthKnown2 = 1;
    lengthLong2 = 0;
  } else if (flags&LIBSPECTRUM_TAPE_FLAGS_LENGTH_LONG) {
    lengthKnown2 = 1;
    lengthLong2 = 1;
  } else {
    lengthKnown2 = 0;
  }
}


void emuTapeDoAcceleration () nothrow {
  if (lengthKnown1) {
    emuz80.BC.b = ((lengthLong1^(accelerationMode == TapeAccelMode.Decreasing)) ? 0xfe : 0x00);
    emuz80.AF.f |= 0x01;
    emuz80.PC = emuz80.pop();
    //tape_next_edge(z80.tstates, 0, NULL);
    tapNextEdgeTS = emuz80.tstates;
    tapDetectInCount = 0;
    zxBorderColor = 0;
  }
  lengthKnown1 = lengthKnown2;
  lengthLong1 = lengthLong2;
}


TapeAccelMode emuTapeAccelerationDetector (ushort pc) nothrow {
  int state = 0, count = 0;
  for (;;) {
    ubyte b = emuz80.memPeekB(pc);
    pc = (pc+1)&0xffff;
    ++count;
    switch (state) {
      case 0:
        switch (b) {
          case 0x04: state = 1; break; // INC B - Many loaders
          default: state = 13; break;  // Possible Digital Integration
        }
        break;
      case 1:
        switch (b) {
          case 0xc8: state = 2; break; // RET Z
          default: return TapeAccelMode.None;
        }
        break;
      case 2:
        switch (b) {
          case 0x3e: state = 3; break; // LD A,nn
          default: return TapeAccelMode.None;
        }
        break;
      case 3:
        switch (b) {
          case 0x00:          // Search Loader
          case 0x7f:          // ROM loader and variants
            state = 4; break; // Data byte
          default: return TapeAccelMode.None;
        }
        break;
      case 4:
        switch (b) {
          case 0xdb: state = 5; break; // IN A,(nn)
          default: return TapeAccelMode.None;
        }
        break;
      case 5:
        switch (b) {
          case 0xfe: state = 6; break; // Data byte
          default: return TapeAccelMode.None;
        }
        break;
      case 6:
        switch (b) {
          case 0x1f: state = 7; break;  // RRA
          case 0xa9: state = 24; break; // XOR C - Search Loader
          default: return TapeAccelMode.None;
        }
        break;
      case 7:
        switch (b) {
          case 0x00: // NOP - Bleepload
          case 0xa7: // AND A - Microsphere
          case 0xc8: // RET Z - Paul Owens
          case 0xd0: // RET NC - ROM loader
            state = 8; break;
          case 0xa9: state = 9; break; // XOR C - Speedlock
          default: return TapeAccelMode.None;
        }
        break;
      case 8:
        switch (b) {
          case 0xa9: state = 9; break; // XOR C
          default: return TapeAccelMode.None;
        }
        break;
      case 9:
        switch (b) {
          case 0xe6: state = 10; break; // AND nn
          default: return TapeAccelMode.None;
        }
        break;
      case 10:
        switch (b) {
          case 0x20: state = 11; break; // Data byte
          default: return TapeAccelMode.None;
        }
        break;
      case 11:
        switch (b) {
          case 0x28: state = 12; break; // JR nn
          default: return TapeAccelMode.None;
        }
        break;
      case 12:
        return (b == 0x100-count ? TapeAccelMode.Increasing : TapeAccelMode.None);
      /* Digital Integration loader */
      case 13:
        state = 14; break; // Possible Digital Integration
      case 14:
        switch (b) {
          case 0x05: state = 15; break; // DEC B - Digital Integration
          default: return TapeAccelMode.None;
        }
        break;
      case 15:
        switch (b) {
          case 0xc8: state = 16; break; // RET Z
          default: return TapeAccelMode.None;
        }
        break;
      case 16:
        switch (b) {
          case 0xdb: state = 17; break; // IN A,(nn)
          default: return TapeAccelMode.None;
        }
        break;
      case 17:
        switch (b) {
          case 0xfe: state = 18; break; // Data byte
          default: return TapeAccelMode.None;
        }
        break;
      case 18:
        switch (b) {
          case 0xa9: state = 19; break; // XOR C
          default: return TapeAccelMode.None;
        }
        break;
      case 19:
        switch (b) {
          case 0xe6: state = 20; break; // AND nn
          default: return TapeAccelMode.None;
        }
        break;
      case 20:
        switch (b) {
          case 0x40: state = 21; break; // Data byte
          default: return TapeAccelMode.None;
        }
        break;
      case 21:
        switch (b) {
          case 0xca: state = 22; break; // JP Z,nnnn
          default: return TapeAccelMode.None;
        }
        break;
      case 22: // LSB of jump target
        if (b == ((cast(int)emuz80.PC-4)&0xff)) {
          state = 23;
        } else {
          return TapeAccelMode.None;
        }
        break;
      case 23: // MSB of jump target
        return (b == (((cast(int)emuz80.PC-4)&0xff00)>>8) ? TapeAccelMode.Decreasing : TapeAccelMode.None);
      /* Search loader */
      case 24:
        switch (b) {
          case 0xe6: state = 25; break; // AND nn
          default: return TapeAccelMode.None;
        }
        break;
      case 25:
        switch (b) {
          case 0x40: state = 26; break; // Data byte
          default: return TapeAccelMode.None;
        }
        break;
      case 26:
        switch (b) {
          case 0xd8: state = 27; break; // RET C
          default: return TapeAccelMode.None;
        }
        break;
      case 27:
        switch (b) {
          case 0x00: state = 11; break; // NOP
          default: return TapeAccelMode.None;
        }
        break;
      default: assert(0); // can't happen
    }
  }
}


void emuTapeCheckForAcceleration () nothrow {
  // if the IN occured at a different location to the one we're accelerating, stop acceleration
  if (accelerationMode && emuz80.PC != accelerationPC) accelerationMode = TapeAccelMode.None;
  // if we're not accelerating, check if this is a loader
  if (!accelerationMode) {
    accelerationMode = emuTapeAccelerationDetector((cast(int)emuz80.PC-6)&0xffff);
    accelerationPC = emuz80.PC;
  }
  if (accelerationMode != TapeAccelMode.None) {
    //fprintf(stderr, "ACCELMODE: %d\n", (int)acceleration_mode);
    emuTapeDoAcceleration();
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// FlashLoader(utm): not taken from FUSE, entirely mine shitcode!

// custom flashloaders
enum {
  CFL_NO_TYPE       = 0x0001, // no type byte, assumes CFL_FLAG_ANY_TYPE
  CFL_NO_PARITY     = 0x0002, // no parity byte
  CFL_ANY_TYPE      = 0x0004, // don't check block type in A
  CFL_REVERSE       = 0x0008, // loading in reverse direction, first decrement IX
  CFL_NO_LEN_GLITCH = 0x0010, // no 'loader len' glitch
  CFL_PARITY_FIRST  = 0x0020, // parity byte comes before block
}


struct LoaderTimings {
  int pilot_minlen; // 807
  int pilot; // 2168
  int sync1; // 667
  int sync2; // 735
  int bit0; // 855
  int bit1; // 1710
  int init_parity; // >0: init parity with this
  uint flags; // CFL_xxx
}


// ////////////////////////////////////////////////////////////////////////// //
bool checkByte (int addr, ubyte v) nothrow @trusted {
  pragma(inline, true);
  return (emuz80.memPeekB(addr&0xffff) == v);
}


bool checkWord (int addr, ushort v) nothrow @trusted {
  pragma(inline, true);
  return
    emuz80.memPeekB(addr&0xffff) == (v&0xff) &&
    emuz80.memPeekB((addr+1)&0xffff) == ((v>>8)&0xff);
}


ushort getWord (int addr) nothrow @trusted {
  pragma(inline, true);
  return cast(ushort)(emuz80.memPeekB(addr&0xffff)|(emuz80.memPeekB((addr+1)&0xffff)<<8));
}


////////////////////////////////////////////////////////////////////////////////
/* legend:
 *   <0: new negative address offset
 *   0: next is 0 too? exit, else new positive address offset
 *   >0xff && <0x8000: check swapped word
 *   -32764 -- positive offset from the current address
 *   -32767 -- NOP
 *   -32666 -- skip this byte
 */
int isPatternOk(bool showData=false) (int addr, const(short)[] pattern) nothrow @trusted {
  usize ppos = 0;
  int a = addr;
  while (ppos < pattern.length) {
    short p = pattern.ptr[ppos++];
    if (p == -32764) {
      // positive offset from the current address
      if (ppos >= pattern.length) return true; // end of data, ok
      p = pattern.ptr[ppos++];
      a = (a+p)&0xffff;
      if (optDebugFlashLoad) conwritefln!" pattern #%04X: #%04X (%d)"(addr, a, p);
    } else if (p <= 0 && p > -32000) {
      // new offset or end
      if (p == 0) {
        // positive offset or end
        if (ppos >= pattern.length) return true; // end of data, ok
        p = pattern.ptr[ppos++];
        if (p == 0) return true; // end of data, ok
      }
      a = (addr+p)&0xffff;
      if (optDebugFlashLoad) conwritefln!" pattern #%04X: #%04X (%d)"(addr, a, p);
    } else if (p != -32666) {
      if (p == -32767) p = 0;
      static if (showData) conwritefln!"p=0x%04x"(p);
      if (p > 0xff) {
        if (emuz80.memPeekB(a&0xffff) != ((p>>8)&0xff)) return 0;
        a = (a+1)&0xffff;
        if (emuz80.memPeekB(a&0xffff) != (p&0xff)) return 0;
      } else {
        if (emuz80.memPeekB(a&0xffff) != p) return 0;
      }
      a = (a+1)&0xffff;
    } else {
      a = (a+1)&0xffff;
    }
  }
  return true; // end of data, ok
}


////////////////////////////////////////////////////////////////////////////////
struct LoaderInfo {
  string name;
  immutable short[] pattern;
  bool function () nothrow detectFn;
  bool function () nothrow accelFn;
  int exitOfs; // 0: detect
}


////////////////////////////////////////////////////////////////////////////////
static immutable LoaderInfo[$] knownLoaders = [
  LoaderInfo("ROM Loader", null, null, &emuTapeDoROMLoad, 123), // 0x564
  LoaderInfo("Edge Loader", pattern_edge, null, &do_edge, 105),
  LoaderInfo("Flash Load", pattern_flashload, null, &doFlashLoad, 114),
  LoaderInfo("FTL Loader", pattern_ftl, null, &doFTL, 131),
  LoaderInfo("Hewson Slowload", pattern_hewson, null/*&isHewson*/, &emuTapeDoROMLoad, 128),
  LoaderInfo("Elite UniLoader", pattern_uniloader, &isUniLoader, &emuTapeDoROMLoad, 116),
  LoaderInfo("Players2 Loader", pattern_pl2, null, &do_pl2, 121),
  //LoaderInfo("SetoLOAD Loader", pattern_seto, null, &do_seto, 123),
];


////////////////////////////////////////////////////////////////////////////////
/*
 *   <0: new negative address offset
 *   0: next is 0 too? exit, else new positive address offset
 *   >0xff && <0x8000: check swapped word
 *   -32767 -- NOP
 *   -32666 -- skip this byte
 */
static immutable short[$] pattern_seto = [
  -15,
  0x37, // SCF
  0x14, // INC D
  0x08, // EX AF,AF'
  0x15, // DEC D
  0xF3, // DI
  0x3E, -32666, // LD A,xx
  0xD3, 0xFE, // OUT (#FE),A
  0x21, -32666, -32666, // LD HL,xxx
  0xE5, // PUSH HL
  0xDB, 0xFE, // IN A,(#FE)
  0x1F, // RRA
  -32764, 7, 0xCD, // CALL LD-EDGE2
  -32764, 14, 0xCD, // CALL LD-EDGE2
  // check constants
  -32764, 4, 0x069C, // LD B,#9C
  -32764, 5, 0x3EC6, // LD A,#C6
  -32764, 6, 0x06C9, // LD B,#C9
  -32764, 6, 0xFE, 0xD4, // CP #D4
  -32764, 12, 0x0658, // LD B,#58
  //k8: enough, i am sooo lazy
  // end
  0, 0
];


bool do_seto () nothrow {
  static immutable LoaderTimings timing = {
    pilot_minlen: 807, // stdlen: 3223
    pilot: 2168,
    sync1: 714,
    sync2: 714,
    bit0: 454,
    bit1: 907,
  };
  return emuTapeDoFlashLoadEx(timing);
}


////////////////////////////////////////////////////////////////////////////////
static immutable short[$] pattern_edge = [
  -10,
  0xC5, // PUSH BC
  0xDD, 0xE1, // POP IX
  0xF3, // DI
  0x3E, -32666, // LD A,xx
  0xD3, 0xFE, // OUT (#FE),A
  0, 10, 0xCD, 0xE7, 0x05, // CALL LD-EDGE1
  0, 22, 0xCD, 0xE3, 0x05, // CALL LD-EDGE2
  // check constants
  0, 31, 0x069C, // LD B,#9C
  0, 38, 0x3EC6, // LD A,#C6
  0, 46, 0x06C9, // LD B,#C9
  0, 54, 0xFE, 0xD4, // CP #D4
  0, 79, 0x06B2, // LD B,#B2
  0, 88, 0x3EBD, // LD A,#BD
  0, 115, 0xED, 0x56, // IM 1
  // exit
  0, 105,
  0x7C, // LD A,H
  0xB7, // OR A
  0x20, -32666, // JR NZ, ...
  0xE1, // POP HL
  0xC1, // POP BC
  0x05, // DEC B
  // end
  0, 0
];


bool do_edge () nothrow {
  static immutable LoaderTimings timing = {
    pilot_minlen: 100,
    pilot: 2168,
    sync1: 667,
    sync2: 735,
    bit0: 550,
    bit1: 1110,
    flags: CFL_NO_TYPE
  };
  return emuTapeDoFlashLoadEx(timing);
}


////////////////////////////////////////////////////////////////////////////////
static immutable short[$] pattern_flashload = [
  -4, 0xD3, 0xFE, // OUT (#FE),A
  -14,
  0x14, // INC D
  0x08, // EX AF,AF'
  0x15, // DEC D
  0xDD, 0x19, // ADD IX,DE
  0xDD, 0x2B, // DEC IX
  0xF3, // DI
  0, 6, 0xCD, // CALL LD-EDGE2
  0, 21, 0xCD, // CALL LD-EDGE
  0, 114,
  0x7C, // LD A,H
  0xFE, 0x01, // CP #01
  0xC9, // RET
  // check constants
  0, 26, 0x069C, // LD B,#9C
  0, 33, 0x3EC6, // LD A,#C6
  0, 41, 0x06C9, // LD B,#C9
  0, 49, 0xFE, 0xD4, // CP #D4
  0, 63, 0x06D0, // LD B,#D0
  0, 89, 0x06D2, // LD B,#D2
  0, 97, 0x3ED7, // LD A,#D7
  0, 102, 0x06D0, // LD B,#D0
  // end
  0, 0
];


bool doFlashLoad () nothrow {
  static immutable LoaderTimings timing = {
    pilot_minlen: 807,
    pilot: 2168,
    sync1: 667,
    sync2: 735,
    bit0: 543,
    bit1: 839,
    flags: CFL_REVERSE,
  };
  return emuTapeDoFlashLoadEx(timing);
}


////////////////////////////////////////////////////////////////////////////////
static immutable short[$] pattern_ftl = [
  -4, 0xD3, 0xFE, // OUT (#FE),A
  -12,
  0x263D, // LD H,#3D
  0x01, 0x0140, // LD BC,#4001
  0xD9, // EXX
  0, 8, 0xCD, // CALL LD-EDGE2
  0, 23, 0xCD, // CALL LD-EDGE
  0, 131,
  0x7C, // LD A,H
  0xFE, 0x01, // CP #01
  0x21, // LD HL,...
  0, 137,
  0xE5, // PUSH HL
  0xC3, // JP ...
  // check constants
  0, 28, 0x069C, // LD B,#9C
  0, 35, 0x3EC6, // LD A,#C6
  0, 43, 0x06C9, // LD B,#C9
  0, 51, 0xFE, 0xD4, // CP #D4
  0, 72, 0x06B0, // LD B,#B0
  0, 106, 0x06B0, // LD B,#B0
  0, 114, 0x3ED4, // LD A,#D4
  0, 119, 0x06B0, // LD B,#B0
  // end
  0, 0
];


bool doFTL () nothrow {
  static immutable LoaderTimings timing = {
    pilot_minlen: 807,
    pilot: 2168,
    sync1: 667,
    sync2: 735,
    bit0: 829,
    bit1: 1658,
  };
  return emuTapeDoFlashLoadEx(timing);
}


////////////////////////////////////////////////////////////////////////////////
// Hewson Slowload
static immutable short[$] pattern_hewson = [
  -4, 0xD3, 0xFE, // OUT (#FE),A
  -14,
  0x14, // INC D
  0x08, // EX AF,AF'
  0x15, // DEC D
  0xF3, // DI
  0xD9, // EXX
  0, 8, 0xCD, // CALL LD-EDGE2
  0, 23, 0xCD, // CALL LD-EDGE
  0, 128,
  0x7C, // LD A,H
  0xFE, 0x01, // CP #01
  0xC9, // RET
  // check constants
  0, 28, 0x069C, // LD B,#9C
  0, 35, 0x3EC6, // LD A,#C6
  0, 43, 0x06C9, // LD B,#C9
  0, 51, 0xFE, 0xD4, // CP #D4
  0, 70, 0x06B0, // LD B,#B0
  0, 103, 0x06B2, // LD B,#B2
  0, 111, 0x3ECB, // LD A,#CB
  0, 116, 0x06B0, // LD B,#B0
  // end
  0, 0
];


/*
static int isHewson (void) {
  int addr = z80.pc;
  //if (z80.ix.w <= addr && z80.ix.w+z80.de.w >= addr) return 0; // don't accelerate self-modifying loaders
  return 1;
}
*/


////////////////////////////////////////////////////////////////////////////////
static immutable short[$] pattern_pl2 = [
  -15,
  0xAF, // XOR A
  0x37, // SCF
  0x14, // INC D
  0x08, // EX AF,AF'
  0x15, // DEC D
  0x3E, -32666, // LD A,xx
  0xD3, 0xFE, // OUT (#FE),A
  0x21, -32666, -32666, // LD HL,xxx
  0xE5, // PUSH HL
  0, 6, 0xCD, // CALL LD-EDGE2
  0, 21, 0xCD, // CALL LD-EDGE
  // check constants
  0, 26, 0x069C, // LD B,#9C
  0, 33, 0x3EC6, // LD A,#C6
  0, 41, 0x06C9, // LD B,#C9
  0, 49, 0xFE, 0xD4, // CP #D4
  0, 63, 0x067B, // LD B,#7B
  0, 96, 0x067D, // LD B,#7D
  0, 104, 0x3E8E, // LD A,#8E
  0, 109, 0x067B, // LD B,#7B
  // exit
  0, 121,
  0x7C, // LD A,H
  0xFE, 0x01, // CP #01
  0xC9, // RET
  // end
  0, 0
];


bool do_pl2 () nothrow {
  static immutable LoaderTimings timing = {
    pilot_minlen: 807,
    pilot: 2168,
    sync1: 667,
    sync2: 735,
    bit0: 560,
    bit1: 1120,
  };
  return emuTapeDoFlashLoadEx(timing);
}


////////////////////////////////////////////////////////////////////////////////
static immutable short[$] pattern_uniloader = [
  -4, 0xD3, 0xFE, // OUT (#FE),A
  -10,
  0x14, // INC D
  0x08, // EX AF,AF'
  0x15, // DEC D
  0xF3, // DI
  0, 6, 0xCD, // CALL LD-EDGE2
  0, 21, 0xCD, // CALL LD-EDGE
  0, 116,
  0x7C, // LD A,H
  0xFE, 0x01, // CP #01
  0xD8, // RET C
  // check constants
  0, 26, 0x069C, // LD B,#9C
  0, 33, 0x3EC6, // LD A,#C6
  0, 41, 0x06C9, // LD B,#C9
  0, 49, 0xFE, 0xD4, // CP #D4
  0, 64, 0x06B0, // LD B,#B0
  0, 91, 0x06B2, // LD B,#B2
  0, 100, 0x3ECB, // LD A,#CB
  0, 105, 0x06B0, // LD B,#B0
  // end
  0, 0
];


// Elite UniLoader
bool isUniLoader () nothrow {
  ushort lde, lde2;
  int addr = emuz80.PC;
  //if (z80.ix.w <= addr && z80.ix.w+z80.de.w >= addr) return 0; // don't accelerate self-modifying loaders
  // address of LD-EDGE & LD-EDGE2
  lde2 = getWord(addr+7); // LD-EDGE2 address
  lde = getWord(addr+22); // LD-EDGE address
  if (lde2-lde != 4) return false;
  // check LD-EDGE
  if (!checkByte(lde, 0xCD)) return false; // CALL
  if (!checkWord(lde+1, lde2)) return false;
  if (!checkWord(lde2, 0x163E)) return false; // LD A,0x16
  return true;
}


////////////////////////////////////////////////////////////////////////////////
bool executeLoader() (ushort addr, in auto ref LoaderInfo li) nothrow {
  static int loadErrorCount = 0;
  if (optDebugFlashLoad) conwritefln!"+++ LOADER: %s"(li.name);
  conwriteln("detected loader: ", li.name);
  if (!tapePlaying) emuStartTapeAuto();
  if (li.accelFn()) {
    if (emuz80.HL.h) {
      // prevent endless loading error loops
      conwriteln("*** tape loading error");
      if (++loadErrorCount >= 16) {
        loadErrorCount = 0;
        optTapeFlashLoad = false;
      }
    } else {
      loadErrorCount = 0;
    }
    emuz80.PC = cast(ushort)(addr+li.exitOfs);
    emuStopTape();
    //if (optDebugFlashLoad) dbgSetActive(1);
    return true;
  }
  conwriteln("block not found!");
  emuStopTape();
  //if (optDebugFlashLoad) dbgSetActive(1);
  return false;
}


public bool emuTapeFlashLoad () nothrow {
  if (!optTapeFlashLoad) return false;
  if (!curtape || !libspectrum_tape_present(curtape) || tapeComplete) return false;
  int addr = emuz80.PC;
  if (addr == 0x0564) return executeLoader(addr&0xffff, LoaderInfo("ROM Loader", null, null, &emuTapeDoROMLoad, 123));
  if (addr < 0x4000) return false; // ROM
  //if ((z80.afx.f&ZYM_FLAG_C) == 0) return -1; // don't accelerate VERIFY mode
  if (!checkByte(addr, 0x1F)) return false; //RRA?
  // check for SetoLOAD
  // not yet
  /*
  if (checkWord(addr-2, 0xFEDB) && // IN (#FE),A?
      checkByte(addr-3, 0xE5) && // PUSH HL
      checkWord(addr-5, 0x053F) && checkByte(addr-6, 0x21)) // LD HL,#053F
  {
    conwriteln("SETOLOAD!");
    //return false;
    if (isPatternOk(addr, pattern_seto)) {
      static immutable LoaderInfo setoli = LoaderInfo("SetoLOAD loader", pattern_seto, null, &do_pl2, 123);
      return executeLoader(addr&0xffff, setoli);
    }
  }
  */
  if (!checkWord(addr-4, 0xFED3) && // OUT (#FE),A?
      !checkByte(addr-3, 0xE5)) return false; // PUSH HL (elite loader)
  //if (optDebugFlashLoad) { dbgSetActive(1); return -1; }
  //fprintf(stderr, "trying to detect known loader at 0x%04x\n", addr);
  foreach (immutable ref li; knownLoaders) {
    //const LoaderInfo *li = detectors[f];
    //if (optDebugFlashLoad) fprintf(stderr, "*** trying loader #%u: %s (#%04X)\n", f, li.name, addr);
    if (li.pattern.length == 0 && li.detectFn is null) continue;
    if (li.pattern.length && !isPatternOk(addr, li.pattern)) continue;
    //if (optDebugFlashLoad) fprintf(stderr, " pattern ok\n");
    if (li.detectFn !is null && !li.detectFn()) continue;
    return executeLoader(addr&0xffff, li);
  }
  return false;
}


////////////////////////////////////////////////////////////////////////////////
enum {
  TAPE_ROM_BIT0_TS = 855,
  TAPE_ROM_BIT1_TS = 1710,
  TAPE_ROM_SYNC1_TS = 667,
  TAPE_ROM_SYNC2_TS = 735,
  TAPE_ROM_TAIL_TS = 945,
  TAPE_ROM_PILOT_TS = 2168
};


////////////////////////////////////////////////////////////////////////////////
bool goodTiming (const int ts, const int timing, int tolerance) nothrow @nogc {
  import std.math : abs;
  pragma(inline, true);
  return (abs(ts-timing) <= tolerance);
}


// -1: tape not running
// -2: tape loading error (tape MAY BE stopped)
int emuTapeLoadByte (int bit0ts, int bit1ts) nothrow {
  import std.math : abs;
  if (curtape !is null && tapePlaying) {
    /*
    static int strobe_debug = -1;
    if (strobe_debug < 0) {
      const char *var = getenv("ZXEMUT_DEBUG_STROBE");
      strobe_debug = (var != NULL && var[0]);
    }
    */
    int b = 0x01; // 'stop' flag
    int tolerance = abs(bit0ts-bit1ts)/4;
    if (tolerance < 18) tolerance = 18;
    while (b < 0x100) {
      int ts = emuTapeLdrGetNextPulse();
      //conwriteln("00: loading pulse(", bit0ts, ",", bit1ts, "): ts=", ts);
      if (ts < 0) return -2; // epic fail
      if (goodTiming(ts, bit0ts, tolerance) || goodTiming(ts, bit1ts, tolerance)) {
        // half-bit found, check the second half
        int ts1 = emuTapeLdrGetNextPulse();
        //conwriteln("01: loading pulse(", bit0ts, ",", bit1ts, "): ts=", ts1);
        if (ts1 < 0 || abs(ts1-ts) > tolerance) {
          //if (strobe_debug) fprintf(stderr, " bad pulse 1 timing: %d (0:%d, 1:%d)\n", ts, bit0ts, bit1ts);
          return -2; // epic fail
        }
        //if (strobe_debug) fprintf(stderr, " strobe timing: %d, %d (0:%d, 1:%d) value=%d\n", ts, ts1, bit0ts, bit1ts, (ts >= bit0ts+tolerance ? 1 : 0));
      } else {
        //if (strobe_debug) fprintf(stderr, " bad pulse 0 timing: %d (0:%d, 1:%d)\n", ts, bit0ts, bit1ts);
        return -2; // epic fail
      }
      b = (b<<1)|(ts >= bit0ts+tolerance ? 1 : 0);
    }
    b &= 0xff;
    //if (optDebugFlashLoad) conwritefln!"byte loaded: 0x%02x (%u)"(b, b);
    return b;
  }
  //if (optDebugFlashLoad) conwriteln("byte loading failed");
  return -1;
}


// -1: tape not running
// -2: tape loading error (tape MAY BE stopped)
int emuTapeROMLoadByte (void) nothrow {
  return emuTapeLoadByte(TAPE_ROM_BIT0_TS, TAPE_ROM_BIT1_TS);
}


// return:
//  false: pilot or block id searching fails (so just continue executing)
//   true: something was loaded (with or without error); execute loader epilogue
bool emuTapeDoFlashLoadEx() (in auto ref LoaderTimings ti) nothrow {
  import std.math : abs;
  int bt, ts, parity = (ti.init_parity < 0 ? 0 : ti.init_parity&0xff);
  ubyte flag = emuz80.AFx.a;
  int len = emuz80.DE.w, addr = emuz80.IX.w;
  if (len == 0) len = 65536;
  if (optDebugFlashLoad) conwritefln!"flashload: flags=0x%04x; type=#%02X; addr=#%04X, len=%d"(ti.flags, flag, emuz80.IX.w, len);
  emuz80.tstates = 0;
  tapeNextInFrame = 0;
  int badPilotsCount = 0;
skip_block:
  if (optDebugFlashLoad) conwriteln("searching for pilot...");
  // search for pilot
  if (ti.pilot_minlen > 0) {
    int pilotPulses = 0;
    int ptolerance = ti.pilot/4;
    int stolerance = abs(ti.sync1-ti.sync2)/1-8; // IDIOTIC MetalArmy.tzx!
    if (ptolerance < 18) ptolerance = 18;
    if (stolerance < 18) stolerance = 18;
    if (optDebugFlashLoad) conwritefln!"pilot tolerance: ts=%d; tolerance=%d"(ti.pilot, ptolerance);
    if (optDebugFlashLoad) conwritefln!"sync tolerance: ts=%d,%d; tolerance=%d"(ti.sync1, ti.sync2, stolerance);
    for (;;) {
      if ((ts = emuTapeLdrGetNextPulse()) < 0) return false; // end of tape, nothing was found
      if (goodTiming(ts, ti.pilot, ptolerance)) { ++pilotPulses; continue; }
      //fprintf(stderr, "pilot ts: %d\n", ts);
      // was pilot of enough length found?
      if (pilotPulses >= ti.pilot_minlen) {
        if (optDebugFlashLoad) conwritefln!"pilot; length=%d (%s)"(pilotPulses, (pilotPulses >= ti.pilot_minlen ? "good" : "BAD"));
        // sheck sync1
        if (goodTiming(ts, ti.sync1, stolerance)) {
          // sheck sync2
          if (optDebugFlashLoad) conwritefln!"GOOD sync1: %d"(ts);
          if ((ts = emuTapeLdrGetNextPulse()) < 0) return false; // end of tape, nothing was found
          // checking for SYNC1 too -- idiotic MetalArmy.tzx sucks cocks!
          if (goodTiming(ts, ti.sync2, stolerance) || goodTiming(ts, ti.sync1, stolerance)) {
            if (optDebugFlashLoad) conwritefln!"GOOD sync2: %d"(ts);
            break;
          }
          if (optDebugFlashLoad) conwritefln!"bad sync2: %d"(ts);
        } else {
          if (optDebugFlashLoad) conwritefln!"bad sync1: %d"(ts);
        }
        if (optDebugFlashLoad) conwriteln("bad sync");
      }
      if (optDebugFlashLoad && pilotPulses > 0) conwritefln!"bad pilot (pilotPulses=%d)"(pilotPulses);
      // pilot too short
      pilotPulses = 0;
      if (++badPilotsCount > 1_000_000) {
        // alas, something went wrong
        conwriteln("flashload: something is wrong with the tape (loop?)");
        return false; // error
      }
    }
  }
  if (optDebugFlashLoad) conwriteln("sync ok");
  // load first byte (block type)
  if ((ti.flags&CFL_NO_TYPE) == 0) {
    if ((bt = emuTapeLoadByte(ti.bit0, ti.bit1)) < 0) return false; // end of tape or tape error
    parity ^= bt;
    if (optDebugFlashLoad) conwritefln!"block type: #%02X"(bt);
    // check block type
    if ((ti.flags&CFL_ANY_TYPE) == 0 && ((ti.flags&CFL_NO_LEN_GLITCH) != 0 || emuz80.DE.d != 0xff)) {
      if (bt != flag) goto skip_block; // bad block type, skip it
    }
  }
  // load parity byte
  if ((ti.flags&(CFL_NO_PARITY|CFL_PARITY_FIRST)) == CFL_PARITY_FIRST) {
    if ((bt = emuTapeLoadByte(ti.bit0, ti.bit1)) < 0) return false; // end of tape or tape error
    parity ^= bt;
  }
  if (optDebugFlashLoad) conwritefln!"loading %d bytes to #%04X"(len, addr);
  // load block bytes
  while (len > 0) {
    if ((bt = emuTapeLoadByte(ti.bit0, ti.bit1)) < 0) break; // end of tape or tape error
    parity ^= bt;
    emuz80.memPokeB(addr&0xffff, bt&0xff);
    addr += (ti.flags&CFL_REVERSE ? -1 : 1);
    addr &= 0xffff;
    --len;
  }
  if (optDebugFlashLoad) conwritefln!" done bytes, addr=#%04X, len=%d"(addr, len);
  emuz80.DE.w = len&0xffff;
  emuz80.IX.w = addr&0xffff;
  // load parity byte
  if ((ti.flags&(CFL_NO_PARITY|CFL_PARITY_FIRST)) == 0) {
    if ((bt = emuTapeLoadByte(ti.bit0, ti.bit1)) >= 0) parity ^= bt;
  }
  if (optDebugFlashLoad) conwritefln!"done, parity: #%02X"(parity);
  emuz80.HL.h = parity&0xff;
  return true;
}


// return:
//   -1: pilot or block id searching fails (so just continue executing)
//    0: something was loaded (with or without error); execute loader epilogue
bool emuTapeDoROMLoad () nothrow {
  static immutable LoaderTimings timing = {
    pilot_minlen: 807,
    pilot: TAPE_ROM_PILOT_TS,
    sync1: TAPE_ROM_SYNC1_TS,
    sync2: TAPE_ROM_SYNC2_TS,
    bit0: TAPE_ROM_BIT0_TS,
    bit1: TAPE_ROM_BIT1_TS,
  };
  return emuTapeDoFlashLoadEx(timing);
}


// ////////////////////////////////////////////////////////////////////////// //
} else {
  // no libspectrum
  public void tapeNextFrame () nothrow {}
  public bool tapeCurEdge () nothrow { return false; }

  public TapeState tapeGetState () nothrow @nogc {
    TapeState ts;
    ts.currBlock = ts.blockCount = 0;
    ts.curr = ts.total = 0;
    ts.playing = false;
    ts.complete = true;
    return ts;
  }
}
